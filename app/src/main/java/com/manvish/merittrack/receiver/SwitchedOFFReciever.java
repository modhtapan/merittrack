package com.manvish.merittrack.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.manvish.merittrack.constants.ManvishPrefConstants;


public class SwitchedOFFReciever extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        // Once mobile is switched off make the login flag false .So that Again
        // after switch on,It will ask to relogIn.

        ManvishPrefConstants.LOG_IN_FLAG.write(false);
    }

}
