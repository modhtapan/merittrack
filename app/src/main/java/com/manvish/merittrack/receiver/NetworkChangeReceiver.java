package com.manvish.merittrack.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.widget.Toast;

import com.activeandroid.util.Log;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.utils.UploadFile;

import java.io.File;

/**
 * Created by tapan on 27/5/15.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    private static final String LOG_TAG = "NetworkChangeReceiver";
    private boolean isConnected = false;

    @Override
    public void onReceive(final Context context, final Intent intent) {

            if(isNetworkAvailable(context)){

                String path = StringConstants.OFFLINE_UPLOAD_PATH;
                Log.d("Files", "Path: " + path);
                File f = new File(path);
                File file[] = f.listFiles();
                if(file!=null && file.length!=0) {
                    Log.d("Files", "Size: " + file.length);
                    for (int i = 0; i < file.length; i++) {

                        Log.d("Files", "FileName:" + file[i].getName());
                        UploadFile.uploadMultipart(context, file[i].getName(), file[i].getAbsolutePath());
                    }
                }
            }
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        if (!isConnected) {
                            Log.v(LOG_TAG, "Now you are connected to Internet!");
                            Toast.makeText(context, "Internet availablle via Broadcast receiver", Toast.LENGTH_SHORT).show();
                            isConnected = true;
                            // do your processing here ---
                            // if you need to post any data to the server or get
                            // status
                            // update from the server
                        }
                        return true;
                    }
                }
            }
        }
        Log.v(LOG_TAG, "You are not connected to Internet!");
        Toast.makeText(context, "Internet NOT availablle via Broadcast receiver", Toast.LENGTH_SHORT).show();
        isConnected = false;
        return false;
    }
}
