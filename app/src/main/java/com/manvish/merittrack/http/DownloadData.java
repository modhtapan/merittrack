package com.manvish.merittrack.http;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.activeandroid.util.Log;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.receiver.DownloadResultReceiver;
import com.manvish.merittrack.service.DownloadIntentService;
import com.manvish.merittrack.utils.ManvishCommonUtil;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class DownloadData implements DownloadResultReceiver.Receiver {

    private DownloadResultReceiver mReceiver;
    private String mUrl;
    //private Context mContext;
    private byte[] mByteData;
    private Activity avt;

    public DownloadData(String mUrl, Activity avt, byte[] data) {
        this.mUrl = mUrl;
        //this.mContext = mContext;
        this.avt = avt;
        mByteData = data;
    }

    public void startDownLoadService() {
        mReceiver = new DownloadResultReceiver(new Handler());
        mReceiver.setReceiver(this);
        Intent intent = new Intent(Intent.ACTION_SYNC, null, avt,
                DownloadIntentService.class);

        intent.putExtra("url", mUrl);

        if (mUrl.contains(StringConstants.AUPD_URL)) {
            intent.putExtra(StringConstants.URL_TAG_KEY,
                    StringConstants.URL_TAG_AUPD);
        }
        if (mUrl.contains(StringConstants.AUPD_URL_TIME)) {
            intent.putExtra(StringConstants.URL_TAG_KEY,
                    StringConstants.URL_TAG_TIME);
        }

        if (mUrl.contains(StringConstants.AUPD_URL_ACKNOWLEDGEMENT)) {
            intent.putExtra(StringConstants.URL_TAG_KEY,
                    StringConstants.URL_TAG_AKN);
        }

        if (mUrl.contains(StringConstants.AUPD_URL_ENROLLMENT)) {
            intent.putExtra(StringConstants.URL_TAG_KEY,
                    StringConstants.URL_TAG_ENRL);
        }


        intent.putExtra("receiver", mReceiver);
        intent.putExtra("requestId", 101);
        intent.putExtra("DATA", mByteData);

        avt.startService(intent);

    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        String lURL_TAG = resultData.getString(StringConstants.URL_TAG_KEY);

        switch (resultCode) {
            case DownloadIntentService.STATUS_RUNNING:
                // mContext.getApplicationContext().setProgressBarIndeterminateVisibility(true);
                break;
            case DownloadIntentService.STATUS_FINISHED:
            /* Hide progress & extract result from bundle */
                // mContext.setProgressBarIndeterminateVisibility(false);
                byte[] results = resultData.getByteArray("result");

                if (lURL_TAG.equalsIgnoreCase(StringConstants.URL_TAG_AUPD)) {

                    //  Toast.makeText(avt, "I am in AUPD success", Toast.LENGTH_SHORT).show();
                    // Remove 48 character as response header ..
                    // Get first 48 byte[] of the AUPD Result =

                    byte[] aupdHeader = Arrays.copyOfRange(results, 0, 48);
                    System.out.println(StringConstants.TAG + "aupdHeader"
                            + new String(aupdHeader));

                    String aupdHeaderString = new String(aupdHeader);
                    System.out.println();

                    byte[] aupdHeadercmd = Arrays.copyOfRange(aupdHeader, 0, 6);
                    String aupdHeadercmdString = new String(aupdHeadercmd);

                    byte[] aupdHeaderstart = Arrays.copyOfRange(aupdHeader, 6,
                            6 + 5);
                    String aupdHeaderstartString = new String(aupdHeaderstart);

                    byte[] aupdHeaderunitID = Arrays.copyOfRange(aupdHeader, 11,
                            11 + 11);
                    String aupdHeaderunitIDString = new String(aupdHeaderunitID);
                    ManvishPrefConstants.UNIT_ID.write(aupdHeaderunitIDString);

                    byte[] aupdHeaderSrverDate = Arrays.copyOfRange(aupdHeader,
                            11 + 11, 11 + 11 + 11);
                    String aupdHeaderSrverDateString = new String(
                            aupdHeaderSrverDate);

                    byte[] aupdHeaderSrverTime = Arrays.copyOfRange(aupdHeader,
                            11 + 11 + 11, 11 + 11 + 11 + 11);
                    String aupdHeaderSrverTimeString = new String(
                            aupdHeaderSrverTime);


                    String toParse = aupdHeaderSrverDateString.trim() + " "
                            + aupdHeaderSrverTimeString.trim(); // Results

                    SimpleDateFormat sdf = new SimpleDateFormat(
                            "dd-M-yyyy HH:mm:ss");
                    String dateInString = toParse;
                    Date date = null;
                    try {
                        date = sdf.parse(dateInString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if(date!=null) {
                        System.out.println(dateInString);
                        System.out.println("Date - Time in milliseconds : "
                                + date.getTime());

                        if(TextUtils.isEmpty(date.getTime()+"".trim())){
                            ManvishCommonUtil.setTime(date.getTime());
                        }
                    }



                    byte[] aupdHeaderZipFileSize = Arrays.copyOfRange(aupdHeader,
                            11 + 11 + 11 + 11, 11 + 11 + 11 + 11 + 4);

                    long zipFileSize = byteToInt(aupdHeaderZipFileSize, 4);

                    // Toast.makeText(avt, "ZipFileSize==" + zipFileSize, Toast.LENGTH_SHORT).show();
                    if (zipFileSize > 0) {

                        // byte[] b = Arrays.copyOfRange(results, 48, results.length);
                        /*try {

                            File folder = new File(Environment.getExternalStorageDirectory()+"/MeritTrack/Download");
                            if(!folder.exists()){
                                folder.mkdir() ;
                                System.out.println("new folder created");

                            }

                            File file=new File(Environment.getExternalStorageDirectory()+"/MeritTrack/Download/"+ "AUPD.zip");

                            FileUtils.writeByteArrayToFile(file, b);*/


                            Intent i = new Intent(
                                    StringConstants.ACTION_AUPD_DOWNLOAD);

                            i.putExtra(StringConstants.KEY_INTENT_SUCCESS,
                                    StringConstants.KEY_INTENT_SUCCESS);
                            i.putExtra(StringConstants.URL_TAG_KEY,
                                    StringConstants.URL_TAG_AUPD);

                            LocalBroadcastManager.getInstance(avt)
                                    .sendBroadcast(i);


//                            Toast.makeText(
//                                    avt,
//                                    "SuccessFully wrote zip File and sent broadcast",
//                                    Toast.LENGTH_SHORT).show();
/*
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("print the exception==" + e.getMessage());

                            Intent i = new Intent(
                                    StringConstants.ACTION_AUPD_DOWNLOAD);

                            i.putExtra(StringConstants.KEY_INTENT_SUCCESS,
                                    StringConstants.VALUE_FAIL);
                            i.putExtra(StringConstants.URL_TAG_KEY,
                                    StringConstants.URL_TAG_AUPD);
                            LocalBroadcastManager.getInstance(avt)
                                    .sendBroadcast(i);
                        }*/

                    } else {
                        // Toast.makeText(mContext, "Zero file size",
                        // Toast.LENGTH_SHORT).show();

                        ManvishCommonUtil.showCustomToast(avt,"No data available");
                        Intent i = new Intent(StringConstants.ACTION_AUPD_DOWNLOAD);

                        i.putExtra(StringConstants.KEY_INTENT_SUCCESS,
                                StringConstants.VALUE_FAIL);
                        i.putExtra(StringConstants.URL_TAG_KEY,
                                StringConstants.URL_TAG_AUPD);
                        LocalBroadcastManager.getInstance(avt)
                                .sendBroadcast(i);
                    }
                }
                if (lURL_TAG.equalsIgnoreCase(StringConstants.URL_TAG_TIME)) {

                    try {

                        byte[] aupdHeader = Arrays.copyOfRange(results, 0, 48);
                        System.out.println(StringConstants.TAG + "aupdHeader"
                                + new String(aupdHeader));

                        String aupdHeaderString = new String(aupdHeader);
                        System.out.println();

                        byte[] aupdHeadercmd = Arrays.copyOfRange(aupdHeader, 0, 6);
                        String aupdHeadercmdString = new String(aupdHeadercmd);

                        byte[] aupdHeaderstart = Arrays.copyOfRange(aupdHeader, 6,
                                6 + 5);
                        String aupdHeaderstartString = new String(aupdHeaderstart);

                        byte[] aupdHeaderunitID = Arrays.copyOfRange(aupdHeader,
                                11, 11 + 11);
                        String aupdHeaderunitIDString = new String(aupdHeaderunitID);
                        ManvishPrefConstants.UNIT_ID.write(aupdHeaderunitIDString);

                        byte[] aupdHeaderSrverDate = Arrays.copyOfRange(aupdHeader,
                                11 + 11, 11 + 11 + 11);
                        String dateString = new String(aupdHeaderSrverDate);

                        byte[] aupdHeaderSrverTime = Arrays.copyOfRange(aupdHeader,
                                11 + 11 + 11, 11 + 11 + 11 + 11);
                        String aupdHeaderSrverTimeString = new String(
                                aupdHeaderSrverTime);
                        String toParse = dateString.trim() + " "
                                + aupdHeaderSrverTimeString.trim(); // Results

                        SimpleDateFormat sdf = new SimpleDateFormat(
                                "dd-M-yyyy HH:mm:ss");
                        String dateInString = toParse;
                        Date date = sdf.parse(dateInString);

                        System.out.println(dateInString);
                        System.out.println("Date - Time in milliseconds : "
                                + date.getTime());
                        ManvishCommonUtil.setTime(date.getTime());

                        Intent i = new Intent(
                                StringConstants.ACTION_AUPD_DOWNLOAD);

                        //This code needs to be refactured for further development.

                        i.putExtra(StringConstants.URL_TAG_KEY,
                                StringConstants.URL_TAG_TIME);
                        i.putExtra(StringConstants.KEY_INTENT_SUCCESS,
                                StringConstants.KEY_INTENT_SUCCESS);
                        LocalBroadcastManager.getInstance(avt)
                                .sendBroadcast(i);
                        // Toast.makeText(avt, "Send time broad cast", Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();

                        Intent i = new Intent(StringConstants.ACTION_AUPD_DOWNLOAD);
                        i.putExtra(StringConstants.URL_TAG_KEY,
                                StringConstants.URL_TAG_TIME);
                        i.putExtra(StringConstants.KEY_INTENT_SUCCESS,
                                StringConstants.VALUE_FAIL);
                        LocalBroadcastManager.getInstance(avt)
                                .sendBroadcast(i);
                        Log.d("print the exception==" + e.getMessage());
                    }
                }

                if (lURL_TAG.equalsIgnoreCase(StringConstants.URL_TAG_AKN)) {
                    // Do something for acknowledgement success and failure
                    // Toast.makeText(avt, "I am in ack succ", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(StringConstants.ACTION_AUPD_DOWNLOAD);

                    i.putExtra(StringConstants.URL_TAG_KEY,
                            StringConstants.URL_TAG_AKN);
                    i.putExtra(StringConstants.KEY_INTENT_SUCCESS,
                            StringConstants.KEY_INTENT_SUCCESS);
                    LocalBroadcastManager.getInstance(avt).sendBroadcast(i);

                    Log.d(StringConstants.TAG, resultCode + "");
                }

                if (lURL_TAG.equalsIgnoreCase(StringConstants.URL_TAG_ENRL)) {
                    // Do something for enrollMent success and failure
                }

                break;
            case DownloadIntentService.STATUS_ERROR:
			/* Handle the error */
                String error = resultData.getString(Intent.EXTRA_TEXT);

                if (lURL_TAG.equalsIgnoreCase(StringConstants.URL_TAG_AUPD)) {

                    Intent i = new Intent(StringConstants.ACTION_AUPD_DOWNLOAD);

                    i.putExtra(StringConstants.KEY_INTENT_SUCCESS,
                            StringConstants.VALUE_FAIL);
                    i.putExtra(StringConstants.URL_TAG_KEY,
                            StringConstants.URL_TAG_AUPD);
                    LocalBroadcastManager.getInstance(avt).sendBroadcast(i);
                }
                if (lURL_TAG.equalsIgnoreCase(StringConstants.URL_TAG_TIME)) {

                    Intent i = new Intent(StringConstants.ACTION_AUPD_DOWNLOAD);

                    i.putExtra(StringConstants.KEY_INTENT_SUCCESS,
                            StringConstants.VALUE_FAIL);
                    i.putExtra(StringConstants.URL_TAG_KEY,
                            StringConstants.URL_TAG_TIME);
                    LocalBroadcastManager.getInstance(avt).sendBroadcast(i);
                }
                if (lURL_TAG.equalsIgnoreCase(StringConstants.URL_TAG_AKN)) {

                    Intent i = new Intent(StringConstants.ACTION_AUPD_DOWNLOAD);

                    i.putExtra(StringConstants.KEY_INTENT_SUCCESS,
                            StringConstants.VALUE_FAIL);
                    i.putExtra(StringConstants.URL_TAG_KEY,
                            StringConstants.URL_TAG_AKN);
                    LocalBroadcastManager.getInstance(avt).sendBroadcast(i);
                }

                // Toast.makeText(mContext, error, Toast.LENGTH_LONG).show();
                break;

        }
    }

    public long byteToInt(byte[] bytes, int length) {
        int val = 0;
        if (length > 4)
            throw new RuntimeException("Too big to fit in int");
        for (int i = 0; i < length; i++) {
            val = val << 8;
            val = val | (bytes[i] & 0xFF);
        }
        return val;
    }


}
