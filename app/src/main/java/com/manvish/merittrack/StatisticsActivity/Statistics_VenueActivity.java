package com.manvish.merittrack.StatisticsActivity;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.util.Log;
import com.manvish.merittrack.R;
import com.manvish.merittrack.activities.BaseActivity;
import com.manvish.merittrack.adapters.BatchListAdapter;
import com.manvish.merittrack.adapters.DateListAdapter;
import com.manvish.merittrack.adapters.StatisticAdapter.Statistics_VenueAdapter;
import com.manvish.merittrack.adapters.VenueListAdapter;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.utils.ManvishCommonUtil;

import java.util.ArrayList;






// MenuActivity for showing all menu lists
public class Statistics_VenueActivity extends BaseActivity {

    ListView menuListView;
    Button buttonhome;
    DBAdapter dbAdapter;
    Statistics_VenueAdapter venueListAdapter;
    ArrayList<String> venueList;
    TextView tveventname,tvselecteddate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue);

        tveventname=(TextView)findViewById(R.id.tv_event_name);
        buttonhome=(Button)findViewById(R.id.btnHome);
        tvselecteddate=(TextView)findViewById(R.id.tv_selected_date);

        tveventname.setText(ManvishPrefConstants.SELECTED_EVENT_CODE.read());
        tvselecteddate.setText(ManvishPrefConstants.SELECTED_DATE.read());

        dbAdapter = DBAdapter.getInstance(this, MeritTrackApplication.getInstance().getDbName(), null, 1);
        menuListView = (ListView) findViewById(android.R.id.list);

        if(ManvishPrefConstants.IS_TEST.read()){
            venueListAdapter = new Statistics_VenueAdapter(null);
            menuListView.setAdapter(venueListAdapter);
        }else {
            setDateList();
        }

        // finish menu activity
        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                //ManvishCommonUtil.goToMainactivity(VenueActivity.this);
            }
        });
        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManvishCommonUtil.goToMainactivity(Statistics_VenueActivity.this);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        setHeading();
    }

    private void setDateList() {


        new AsyncTask<Void, Void, Boolean>() {
            protected void onPreExecute() {
                showProgressBar("Fetching Exam Dates");

            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {

                    venueList=dbAdapter.getVenueList();

                    if(venueList.size()>0) {
                        return true;
                    }else{
                        return false;
                    }

                } catch (Exception e) {
                    Log.e("MANVISH",
                            "Exception in parsing file"
                                    + e.getMessage());

                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean success) {

                if(success){
                    venueListAdapter = new Statistics_VenueAdapter(venueList);
                    menuListView.setAdapter(venueListAdapter);
                }

                dismissProgressBar();
            }

        }.execute();
    }

    ProgressDialog mProgressDialog;
    private void showProgressBar(String msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
        }


        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    private void dismissProgressBar() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }
}
