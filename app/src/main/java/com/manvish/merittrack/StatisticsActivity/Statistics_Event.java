package com.manvish.merittrack.StatisticsActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.activeandroid.util.Log;
import com.manvish.merittrack.R;
import com.manvish.merittrack.activities.BaseActivity;
import com.manvish.merittrack.adapters.EventCodeListAdapter;
import com.manvish.merittrack.adapters.EventListAdapter;
import com.manvish.merittrack.adapters.StatisticAdapter.Statistics_EventAdapter;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.http.DownloadData;
import com.manvish.merittrack.utils.ManvishCommonUtil;
import com.manvish.merittrack.utils.ParseAupdZipFile;
import com.manvish.merittrack.view.ManvishAlertDialog;

import java.io.File;
import java.util.LinkedHashMap;

// MenuActivity for showing all menu lists







public class Statistics_Event extends BaseActivity {

    DBAdapter dbAdapter;
    LinkedHashMap<String,String> eventMap;
    ListView menuListView;
    Statistics_EventAdapter eventcodeListAdapter;
    String stage; //pretest means stage 1 and precouncil means stage 2
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_codes);
      Button  buttonhome=(Button)findViewById(R.id.btnHomeevent);


        dbAdapter=DBAdapter.getInstance(this, MeritTrackApplication.getInstance().getDbName(), null, 1);
        // Close previus DB if exist , to connect to new DB
        if(dbAdapter!=null) {
            dbAdapter.closeDatabase();
        }

        dbAdapter = DBAdapter.getInstance(this, "common.db", null, 1); // Common is the common databse name
        menuListView = (ListView) findViewById(android.R.id.list);
        stage=getIntent().getStringExtra(StringConstants.KEY_STAGE);
        ManvishPrefConstants.STAGE.write(stage);

        if(ManvishPrefConstants.IS_TEST.read()){
            eventcodeListAdapter = new Statistics_EventAdapter(Statistics_Event.this,null);
            menuListView.setAdapter(eventcodeListAdapter);
        }else {
            setEventCodeList();
        }
        // finish menu activity
        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                // ManvishCommonUtil.goToMainactivity(EventCodesActivity.this);
            }
        });
        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManvishCommonUtil.goToMainactivity(Statistics_Event.this);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        setHeading();
    }

    private void setEventCodeList() {


        new AsyncTask<Void, Void, Boolean>() {
            protected void onPreExecute() {


            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {

                    eventMap=dbAdapter.getEventsFromCommonDatabaseCommonTable(stage);

                    if(eventMap.size()>0) {
                        return true;
                    }else{
                        return false;
                    }

                } catch (Exception e) {
                    Log.e("MANVISH",
                            "Exception in parsing file"
                                    + e.getMessage());

                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean success) {

                if(success){
                    eventcodeListAdapter = new Statistics_EventAdapter(Statistics_Event.this,eventMap);
                    menuListView.setAdapter(eventcodeListAdapter);
                    dbAdapter.closeDatabase();
                }else{

                    new ManvishAlertDialog(Statistics_Event.this,"No Data","No Project Found ,Please contact Admin").showAlertDialog();
                }

            }

        }.execute();
    }

    @Override
    protected void onStop() {

        if(dbAdapter!=null) {
            dbAdapter.closeDatabase();
        }
        super.onStop();
    }
}

