package com.manvish.merittrack.StatisticsActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.manvish.merittrack.R;
import com.manvish.merittrack.activities.BaseActivity;
import com.manvish.merittrack.adapters.StatisticAdapter.Reg_Ver_Stats_Adapter;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.utils.ManvishCommonUtil;

/**
 * Created by Hitesh on 4/13/2016.
 */
public class Statistics_RegVer extends BaseActivity {


    Button buttonhome;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistic_reg_ver_activity);

        TextView header = (TextView) findViewById(R.id.tvHeader);
        String statsName = ManvishPrefConstants.SELECTED_STATS.read();
        header.setText(statsName);
        ListView menuListView = (ListView) findViewById(android.R.id.list);
        buttonhome=(Button)findViewById(R.id.btnHome);
        Reg_Ver_Stats_Adapter regveradapter = new Reg_Ver_Stats_Adapter();
        menuListView.setAdapter(regveradapter);
        // finish menu activity
        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManvishCommonUtil.goToMainactivity(Statistics_RegVer.this);
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        setHeading();

    }
}
