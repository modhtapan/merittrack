package com.manvish.merittrack.StatisticsActivity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.activeandroid.util.Log;
import com.manvish.merittrack.Model.Student;
import com.manvish.merittrack.R;
import com.manvish.merittrack.activities.BaseActivity;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.utils.ManvishCommonUtil;
import com.manvish.merittrack.view.ManvishAlertDialog;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Hitesh on 4/13/2016.
 */






public class Statistics_Uploade_Activity extends BaseActivity {

    Button buttonhome;
    //ArrayList<String> mStudentIDList;
    ArrayList<Student> studentList ; // specific to a date,batch,venue and classRoom
    DBAdapter dbAdapter;
    LinkedHashMap<String,Student> studentLinkedHashMapReg;
   // LinkedHashMap<String,Student> studentLinkedHashMapVer;
    int mTotalStudent,mNoOfVerFailed,mNoOfVerSucc;
    int mNoOfRegFailed,mNoOfRegSucc;
    int uploadedRegStudentCount;
    int uploadedVerStudentCount;

    TextView atn, rf, verified, vf,uR,uV,tvToatalRegistered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics_upload_stats);

        TextView stageType = (TextView) findViewById(R.id.stageType);
        buttonhome=(Button)findViewById(R.id.btnHome);
        TextView atnTitle = (TextView) findViewById(R.id.atnTitle);
        atn = (TextView) findViewById(R.id.atn);
        rf = (TextView) findViewById(R.id.rf);
        verified = (TextView) findViewById(R.id.verified);
        vf = (TextView) findViewById(R.id.vf);
        uR=(TextView)findViewById(R.id.uR);
        uV=(TextView)findViewById(R.id.uV);
        tvToatalRegistered=(TextView)findViewById(R.id.tv_totalRegistered) ;
        LinearLayout rfLayout = (LinearLayout) findViewById(R.id.rfLayout);

        String stage =  ManvishPrefConstants.SELECTED_EVENT_STAGE.read();
        stageType.setText(stage);

        String selectedDate = ManvishPrefConstants.SELECTED_DATE.read();
        String eventCode = ManvishPrefConstants.SELECTED_EVENT_CODE.read();

        if(stage.equalsIgnoreCase(StringConstants.PRETEST)){

            rfLayout.setVisibility(View.VISIBLE);
        } else {
            rfLayout.setVisibility(View.GONE);
            atnTitle.setText("TOTAL IDs");
        }

        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManvishCommonUtil.goToMainactivity(Statistics_Uploade_Activity.this);
            }
        });
        //mStudentIDList=new ArrayList<>();
        studentList=new ArrayList<>();
        dbAdapter = DBAdapter.getInstance(this,MeritTrackApplication.getInstance().getDbName(), null, 1);
        setStudentList();

    }

    private void setStudentList() {


        new AsyncTask<Void, Void, Boolean>() {
            protected void onPreExecute() {
                showProgressBar("Fetching Student Data");

                if(studentLinkedHashMapReg!=null) {
                    studentLinkedHashMapReg.clear();
                }
                /*if(studentLinkedHashMapVer!=null) {
                    studentLinkedHashMapVer.clear();
                }*/
            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {

                   {
                        studentLinkedHashMapReg = dbAdapter.getStudentMapListFromDateVenueBatchClassForDtatistics
                                (ManvishPrefConstants.SELECTED_DATE.read(),
                                        ManvishPrefConstants.SELECTED_VENUE.read(),
                                        ManvishPrefConstants.SELECTED_BATCH.read(),
                                        ManvishPrefConstants.SELECTED_CLASS.read(),
                                       StringConstants.REGISTRATION);
                        mTotalStudent=studentLinkedHashMapReg.size();

                       /* studentLinkedHashMapVer = dbAdapter.getStudentMapListFromDateVenueBatchClassForDtatistics
                               (ManvishPrefConstants.SELECTED_DATE.read(),
                                       ManvishPrefConstants.SELECTED_VENUE.read(),
                                       ManvishPrefConstants.SELECTED_BATCH.read(),
                                       ManvishPrefConstants.SELECTED_CLASS.read(),
                                       StringConstants.VERIFICATION);
*/
                        uploadedRegStudentCount=dbAdapter.getUploadedStudentCount (ManvishPrefConstants.SELECTED_DATE.read(),
                                ManvishPrefConstants.SELECTED_VENUE.read(),
                                ManvishPrefConstants.SELECTED_BATCH.read(),
                                ManvishPrefConstants.SELECTED_CLASS.read(),
                                StringConstants.REGISTRATION);

                         if(ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.PRETEST)){

                             uploadedVerStudentCount = dbAdapter.getUploadedStudentCountForAllSession(ManvishPrefConstants.SELECTED_DATE.read(),
                                     ManvishPrefConstants.SELECTED_VENUE.read(),
                                     ManvishPrefConstants.SELECTED_BATCH.read(),
                                     ManvishPrefConstants.SELECTED_CLASS.read(),
                                     StringConstants.VERIFICATION);
                         }else {
                             uploadedVerStudentCount = dbAdapter.getUploadedStudentCount(ManvishPrefConstants.SELECTED_DATE.read(),
                                     ManvishPrefConstants.SELECTED_VENUE.read(),
                                     ManvishPrefConstants.SELECTED_BATCH.read(),
                                     ManvishPrefConstants.SELECTED_CLASS.read(),
                                     StringConstants.VERIFICATION);
                         }
                        // get all
                        ManvishPrefConstants.TOTAL_STUDENT.write(mTotalStudent+"");
                       // set it to a global variaable
                       mNoOfVerSucc=dbAdapter.getVerSuccCountForAllSession(ManvishPrefConstants.SELECTED_DATE.read(),
                               ManvishPrefConstants.SELECTED_VENUE.read(),
                               ManvishPrefConstants.SELECTED_BATCH.read(),
                               ManvishPrefConstants.SELECTED_CLASS.read());

                       mNoOfVerFailed=dbAdapter.getVerFailCountForAllSession(ManvishPrefConstants.SELECTED_DATE.read(),
                               ManvishPrefConstants.SELECTED_VENUE.read(),
                               ManvishPrefConstants.SELECTED_BATCH.read(),
                               ManvishPrefConstants.SELECTED_CLASS.read());
                    }

                    if( studentLinkedHashMapReg.size()>0 ) {
                        return true;
                    }else{
                        return false;
                    }

                } catch (Exception e) {
                    Log.e("MANVISH",
                            "Exception in parsing file"
                                    + e.getMessage());

                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean success) {

               // mStudentIDList.clear();
                if(studentLinkedHashMapReg!=null ){

                    for (Map.Entry<String,Student> ee : studentLinkedHashMapReg.entrySet()) {
                        String key = ee.getKey();
                        Student std = ee.getValue();
                        if (std.getRegStatus().equalsIgnoreCase("" + StringConstants.STUDENT_REGISTERED)) {
                            mNoOfRegSucc++;
                        }

                        if (std.getRegStatus().equalsIgnoreCase("" + StringConstants.STUDENT_REGISTRATION_FAILED)) {
                            mNoOfRegFailed++;
                        }
                    }

                        //atn.setText(mNoOfRegSucc+"/"+mTotalStudent);
                        atn.setText(""+mTotalStudent);

                        verified.setText(""+mNoOfVerSucc);
                        vf.setText(""+mNoOfVerFailed);

                        rf.setText(""+mNoOfRegFailed);
                        uR.setText("Reg uploaded :"+uploadedRegStudentCount);
                        uV.setText("Ver uploaded :"+uploadedVerStudentCount);
                        tvToatalRegistered.setText("Registered :"+mNoOfRegSucc);



                }else{
                    new ManvishAlertDialog(Statistics_Uploade_Activity.this,
                            "CONTACT ADMIN","NO DATA AVAILABLE .PLEASE CONTACT ADMIN").showAlertDialog();
                }

                dismissProgressBar();
            }

        }.execute();
    }


    ProgressDialog mProgressDialog;
    private void showProgressBar(String msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
        }


        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    private void dismissProgressBar() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setHeading();
       // setStudentList();
    }
}
