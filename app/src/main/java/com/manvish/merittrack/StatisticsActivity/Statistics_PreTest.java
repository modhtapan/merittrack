package com.manvish.merittrack.StatisticsActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.manvish.merittrack.R;
import com.manvish.merittrack.activities.BaseActivity;
import com.manvish.merittrack.activities.BatchActivity;
import com.manvish.merittrack.adapters.StatisticAdapter.PreTest_Adapter;
import com.manvish.merittrack.utils.ManvishCommonUtil;

/**
 * Created by Hitesh on 4/13/2016.
 */
public class Statistics_PreTest extends BaseActivity {
    Button buttonhome;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics_pre_test_activity);

        ListView list = (ListView) findViewById(android.R.id.list);
        buttonhome=(Button)findViewById(R.id.btnHome);
        PreTest_Adapter preTestAdapter = new PreTest_Adapter();
        list.setAdapter(preTestAdapter);

        // finish menu activity
        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManvishCommonUtil.goToMainactivity(Statistics_PreTest.this);
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        setHeading();

    }
}
