package com.manvish.merittrack.StatisticsActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.manvish.merittrack.R;
import com.manvish.merittrack.activities.BaseActivity;
import com.manvish.merittrack.adapters.EventListAdapter;
import com.manvish.merittrack.adapters.StatisticAdapter.StatisticsAdapter;
import com.manvish.merittrack.utils.ManvishCommonUtil;

/**
 * Created by Hitesh on 4/12/2016.
 */






public class StatisticsActivity extends BaseActivity{
    Button buttonhome;
    //StatisticsAdapter statisticsAdapter;
        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        ListView menuListView = (ListView) findViewById(android.R.id.list);
            buttonhome=(Button)findViewById(R.id.btnHome);
       StatisticsAdapter statisticsAdapter = new StatisticsAdapter();
        menuListView.setAdapter(statisticsAdapter);

        // finish menu activity
        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });


            buttonhome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ManvishCommonUtil.goToMainactivity(StatisticsActivity.this);
                }
            });

    }
    @Override
    protected void onResume() {
        super.onResume();
        setHeading();

    }
}
