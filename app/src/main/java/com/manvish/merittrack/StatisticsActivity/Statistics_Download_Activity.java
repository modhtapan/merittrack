package com.manvish.merittrack.StatisticsActivity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.activeandroid.util.Log;
import com.manvish.merittrack.Model.Student;
import com.manvish.merittrack.R;
import com.manvish.merittrack.activities.BaseActivity;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.utils.ManvishCommonUtil;
import com.manvish.merittrack.view.ManvishAlertDialog;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Hitesh on 4/13/2016.
 */
public class Statistics_Download_Activity extends BaseActivity {

    Button buttonhome;

    ArrayList<String> mStudentIDList;
    ArrayList<Student> studentList ; // specific to a date,batch,venue and classRoom
    DBAdapter dbAdapter;
    LinkedHashMap<String,Student> studentLinkedHashMapReg;
    LinkedHashMap<String,Student> studentLinkedHashMapVer;

    TextView newEmpidsDownload,newPhotosDownload,approvEmpidsDownload,approvPhotosDownload,approvFPImagesDownload;

    int mTotalStudent,mNoOfRegFailed,mNoOfRegSucc,mNoOfVerSucc,mNoOfVerFailed;
    int mTotalPhotos;int mTotalFP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics_download);

        LinearLayout newEmpidLayout = (LinearLayout) findViewById(R.id.newEmpidLayout);
        LinearLayout newPhotosLayout = (LinearLayout) findViewById(R.id.newPhotosLayout);
        LinearLayout approvEmpidLayout = (LinearLayout) findViewById(R.id.approvEmpidLayout);
        LinearLayout approvPhotosLayout = (LinearLayout) findViewById(R.id.approvPhotosLayout);
        LinearLayout approvFPImageLayout = (LinearLayout) findViewById(R.id.approvFPImagesLayout);

        newEmpidsDownload = (TextView) findViewById(R.id.newEmpidsDownload);
        newPhotosDownload = (TextView) findViewById(R.id.newPhotosDownload);
        approvEmpidsDownload = (TextView) findViewById(R.id.approvEmpidsDownload);
        approvPhotosDownload = (TextView) findViewById(R.id.approvPhotosDownload);
        approvFPImagesDownload = (TextView) findViewById(R.id.approvFPImagesDownload);

        buttonhome=(Button)findViewById(R.id.btnHome);

        if(ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.PRETEST)){

            approvEmpidLayout.setVisibility(View.GONE);
            approvPhotosLayout.setVisibility(View.GONE);
            approvFPImageLayout.setVisibility(View.GONE);

        } else {

            newEmpidLayout.setVisibility(View.GONE);
            newPhotosLayout.setVisibility(View.GONE);
        }

        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManvishCommonUtil.goToMainactivity(Statistics_Download_Activity.this);
            }
        });

        mStudentIDList=new ArrayList<>();
        studentList=new ArrayList<>();
        dbAdapter = DBAdapter.getInstance(this, MeritTrackApplication.getInstance().getDbName(), null, 1);
        setStudentList();

    }

    private void setStudentList() {


        new AsyncTask<Void, Void, Boolean>() {
            protected void onPreExecute() {
                showProgressBar("Fetching Student Data");

            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {

                    {

                        if(ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.PRETEST)) {
                            mTotalPhotos = dbAdapter.getPhotoCount
                                    (ManvishPrefConstants.SELECTED_DATE.read(),
                                            ManvishPrefConstants.SELECTED_VENUE.read(),
                                            ManvishPrefConstants.SELECTED_BATCH.read(),
                                            ManvishPrefConstants.SELECTED_CLASS.read(),
                                            StringConstants.REGISTRATION);
                            studentLinkedHashMapReg = dbAdapter.getStudentMapListFromDateVenueBatchClass
                                    (ManvishPrefConstants.SELECTED_DATE.read(),
                                            ManvishPrefConstants.SELECTED_VENUE.read(),
                                            ManvishPrefConstants.SELECTED_BATCH.read(),
                                            ManvishPrefConstants.SELECTED_CLASS.read(),
                                            StringConstants.REGISTRATION);
                        }else{
                            mTotalPhotos = dbAdapter.getPhotoCount
                                    (ManvishPrefConstants.SELECTED_DATE.read(),
                                            ManvishPrefConstants.SELECTED_VENUE.read(),
                                            ManvishPrefConstants.SELECTED_BATCH.read(),
                                            ManvishPrefConstants.SELECTED_CLASS.read(),
                                            StringConstants.VERIFICATION);

                            studentLinkedHashMapReg = dbAdapter.getStudentMapListFromDateVenueBatchClass
                                    (ManvishPrefConstants.SELECTED_DATE.read(),
                                            ManvishPrefConstants.SELECTED_VENUE.read(),
                                            ManvishPrefConstants.SELECTED_BATCH.read(),
                                            ManvishPrefConstants.SELECTED_CLASS.read(),
                                            StringConstants.VERIFICATION);

                            studentLinkedHashMapVer = dbAdapter.getStudentMapListFromDateVenueBatchClass
                                    (ManvishPrefConstants.SELECTED_DATE.read(),
                                            ManvishPrefConstants.SELECTED_VENUE.read(),
                                            ManvishPrefConstants.SELECTED_BATCH.read(),
                                            ManvishPrefConstants.SELECTED_CLASS.read(),
                                            StringConstants.VERIFICATION);
                        }
                        mTotalStudent=studentLinkedHashMapReg.size();
                        // get all
                        ManvishPrefConstants.TOTAL_STUDENT.write(mTotalStudent+"");
                    }

                    if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equals(StringConstants.PRECOUNCIL)) {

                      mTotalFP =  dbAdapter.getTemplatesListOfStudentForVerification(ManvishPrefConstants.SELECTED_DATE.read(),
                                ManvishPrefConstants.SELECTED_VENUE.read(),
                                ManvishPrefConstants.SELECTED_BATCH.read(),
                                ManvishPrefConstants.SELECTED_CLASS.read());
                    } else if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equals(StringConstants.PRETEST)){

                        mTotalFP =  dbAdapter.getAllTemplatesOfStudentsFromRegisteredTableForSessionVerification(ManvishPrefConstants.SELECTED_DATE.read(),
                                ManvishPrefConstants.SELECTED_VENUE.read(),
                                ManvishPrefConstants.SELECTED_BATCH.read(),
                                ManvishPrefConstants.SELECTED_CLASS.read());
                    }

                    if( studentLinkedHashMapReg!=null && studentLinkedHashMapReg.size()!=0 || studentLinkedHashMapVer!=null && studentLinkedHashMapVer.size()!=0 ) {
                        return true;
                    }else{
                        return false;
                    }



                } catch (Exception e) {
                    Log.e("MANVISH",
                            "Exception in parsing file"
                                    + e.getMessage());

                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean success) {

                mStudentIDList.clear();
                   if(success){

                    for (Map.Entry<String, Student> ee : studentLinkedHashMapReg.entrySet()) {

                        String key = ee.getKey();
                        Student std = ee.getValue();

                            if(std.getVerStatus().equalsIgnoreCase(""+StringConstants.STUDENT_VERIFIED)){
                                mNoOfVerSucc++;
                            }

                            if(std.getVerStatus().equalsIgnoreCase(""+StringConstants.STUDENT_VERIFICATION_FAILED)){
                                mNoOfVerFailed++;
                            }



                            if(std.getRegStatus().equalsIgnoreCase(""+StringConstants.STUDENT_REGISTERED)){
                                mNoOfRegSucc++;
                            }

                            if(std.getRegStatus().equalsIgnoreCase(""+StringConstants.STUDENT_REGISTRATION_FAILED)){
                                mNoOfRegFailed++;
                            }

                        studentList.add(std);
                        mStudentIDList.add(std.getStudentID());
                    }

                    newEmpidsDownload.setText(mStudentIDList.size()+"");
                    approvEmpidsDownload.setText(mStudentIDList.size()+"");
                    newPhotosDownload.setText(mTotalPhotos+"");
                    approvPhotosDownload.setText(mTotalPhotos+"");

                    if(mTotalFP!=StringConstants.FINGER_TEMPLATE_COUNT_ERROR) {
                        approvFPImagesDownload.setText(mTotalFP + "");
                    }else{
                        approvFPImagesDownload.setText("COUNT ERROR");
                    }
                }else{
                    new ManvishAlertDialog(Statistics_Download_Activity.this,
                            "CONTACT ADMIN","NO DATA AVAILABLE .PLEASE CONTACT ADMIN").showAlertDialog();
                }

                dismissProgressBar();
            }

        }.execute();
    }


    ProgressDialog mProgressDialog;
    private void showProgressBar(String msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
        }


        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    private void dismissProgressBar() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setHeading();
        //setStudentList();
    }
}
