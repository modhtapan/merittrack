package com.manvish.merittrack.StatisticsActivity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.util.Log;
import com.manvish.merittrack.R;
import com.manvish.merittrack.activities.BaseActivity;
import com.manvish.merittrack.adapters.DateListAdapter;
import com.manvish.merittrack.adapters.EventCodeListAdapter;
import com.manvish.merittrack.adapters.EventListAdapter;
import com.manvish.merittrack.adapters.StatisticAdapter.Statistics_DateAdapter;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.utils.ManvishCommonUtil;

import java.util.ArrayList;
import java.util.LinkedList;





// MenuActivity for showing all menu lists
public class Statistics_Date extends BaseActivity {


    ArrayList<String> mDateList;
    DBAdapter dbAdapter;
    Button buttonhome;
    ListView menuListView;
    Statistics_DateAdapter dateListAdapter;
    TextView tveventname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date);

        tveventname=(TextView)findViewById(R.id.tvEventName);
        buttonhome=(Button)findViewById(R.id.btnHome);
        tveventname.setText(ManvishPrefConstants.SELECTED_EVENT_CODE.read());

        dbAdapter = DBAdapter.getInstance(this, MeritTrackApplication.getInstance().getDbName(), null, 1);
        menuListView = (ListView) findViewById(android.R.id.list);

        if(ManvishPrefConstants.IS_TEST.read()){
            dateListAdapter = new Statistics_DateAdapter(null);
            menuListView.setAdapter(dateListAdapter);
        }else {
            setDateList();
        }

        // finish menu activity
        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                // ManvishCommonUtil.goToMainactivity(SelectDateActivity.this);
            }
        });

        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManvishCommonUtil.goToMainactivity(Statistics_Date
                        .this);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        setHeading();
    }


    private void setDateList() {


        new AsyncTask<Void, Void, Boolean>() {
            protected void onPreExecute() {
                showProgressBar("Fetching Exam Dates");

            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {

                    mDateList=dbAdapter.getUniqueDateListFromStudentTable();

                    if(mDateList.size()>0) {
                        return true;
                    }else{
                        return false;
                    }

                } catch (Exception e) {
                    Log.e("MANVISH",
                            "Exception in parsing file"
                                    + e.getMessage());

                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean success) {

                if(success){
                    dateListAdapter = new Statistics_DateAdapter(mDateList);
                    menuListView.setAdapter(dateListAdapter);
                }

                dismissProgressBar();
            }

        }.execute();
    }

    ProgressDialog mProgressDialog;
    private void showProgressBar(String msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
        }


        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    private void dismissProgressBar() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(dbAdapter!=null){
            dbAdapter.closeDatabase();
        }
    }
}
