package com.manvish.merittrack.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.manvish.merittrack.R;
import com.manvish.merittrack.activities.EventCodesActivity;
import com.manvish.merittrack.activities.RegVerButtonActivity;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.utils.ManvishCommonUtil;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * MenuListAdapter for menu listview adapter
 */
public class EventCodeListAdapter extends BaseAdapter{

    private LinkedHashMap<String,String> eventMap;
    private Context context;



    public EventCodeListAdapter(Context context ,LinkedHashMap<String,String> eventMap){
        this.eventMap=eventMap;
        this.context=context;
    }


    @Override
    public int getCount() {

        // This list is hard coded for demo purpose .

        if(ManvishPrefConstants.IS_TEST.read()){
            return StringConstants.EVENT_CODES_TEST.length;
        }else {
            if(eventMap!=null) {
                return eventMap.size();
            } else {
                return 0;
            }
        }
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Context context = parent.getContext();

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_menu, null);

            viewHolder = new ViewHolder();

            viewHolder.menuButton = (Button) convertView.findViewById(R.id.itemMenuButton);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // set each event item

        if(ManvishPrefConstants.IS_TEST.read()) {

            viewHolder.menuButton.setText(StringConstants.EVENT_CODES_TEST[position]);
        }else {


            for(Map.Entry<String, String> entry: eventMap.entrySet()) {
               // System.out.println(entry.getKey());
                eventIDList.add(entry.getKey());
            }
            viewHolder.menuButton.setText(eventIDList.get(position));
        }

        viewHolder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent eventCodeIntent = new Intent(context, EventCodesActivity.class);

                    // Authenticate with password ,before going to next activity

                if(ManvishPrefConstants.IS_TEST.read()) {
                    MeritTrackApplication.getInstance().setDbName(StringConstants.TEST_DB);
                    ManvishPrefConstants.SELECTED_EVENT_CODE.write(StringConstants.EVENT_CODES_TEST[position]);
                    // go to
                    Intent intent=new Intent(context, RegVerButtonActivity.class);
                    context.startActivity(intent);
                }else{

                    ManvishPrefConstants.SELECTED_EVENT_CODE.write(eventIDList.get(position));
                    ManvishCommonUtil.showEnterPasswordDialog((Activity)context,eventIDList.get(position),eventMap.get(eventIDList.get(position)));
                }


            }
        });


        return convertView;

    }
    LinkedList<String> eventIDList=new LinkedList<>();
    private class ViewHolder {

        Button menuButton;
    }
}
