package com.manvish.merittrack.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.manvish.merittrack.R;
import com.manvish.merittrack.activities.EventCodesActivity;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.utils.ManvishCommonUtil;

import org.apache.commons.io.FileUtils;

import java.io.File;

/**
 * MenuListAdapter for menu listview adapter
 */
public class EventListAdapter extends BaseAdapter{


    @Override
    public int getCount() {

        if(ManvishPrefConstants.IS_TEST.read()){

            return StringConstants.EVENT_ITEMS_TEST.length;
        }else {
            return StringConstants.EVENT_ITEMS.length;
        }
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Context context = parent.getContext();

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_menu, null);

            viewHolder = new ViewHolder();

            viewHolder.menuButton = (Button) convertView.findViewById(R.id.itemMenuButton);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // set each menu item
        if(ManvishPrefConstants.IS_TEST.read()){
            viewHolder.menuButton.setText(StringConstants.EVENT_ITEMS_TEST[position]);
        }else{
            viewHolder.menuButton.setText(StringConstants.EVENT_ITEMS[position]);
        }


        viewHolder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent eventCodeIntent = new Intent(context, EventCodesActivity.class);
                switch (position){


                    case 0:
                        //this is for pre-test // stage 1

                        ManvishPrefConstants.SELECTED_EVENT_STAGE.write(StringConstants.EVENT_ITEMS[position]);
                        eventCodeIntent.putExtra(StringConstants.KEY_STAGE, "1");
                        context.startActivity(eventCodeIntent);

                        break;

                    case 1:
                        // This is for Post-Test //Upload Test Data
                        ManvishPrefConstants.SELECTED_EVENT_STAGE.write(StringConstants.EVENT_ITEMS[position]);
                        eventCodeIntent.putExtra(StringConstants.KEY_STAGE, "1");
                        context.startActivity(eventCodeIntent);
                        break;

                    case 2:
                        //This is for Pre_Counciling
                        ManvishPrefConstants.SELECTED_EVENT_STAGE.write(StringConstants.EVENT_ITEMS[position]);
                        eventCodeIntent.putExtra(StringConstants.KEY_STAGE, "3");
                        context.startActivity(eventCodeIntent);
                        break;

                    case 3:
                        //This is for Post_Counciling // Upload council data
                        ManvishPrefConstants.SELECTED_EVENT_STAGE.write(StringConstants.EVENT_ITEMS[position]);
                        eventCodeIntent.putExtra(StringConstants.KEY_STAGE, "3");
                        context.startActivity(eventCodeIntent);
                        break;

                   /* case 4:
                        //This is for Post_Test backup
                        ManvishPrefConstants.SELECTED_EVENT_STAGE.write(StringConstants.EVENT_ITEMS[position]);
                        eventCodeIntent.putExtra(StringConstants.KEY_STAGE, "1");
                        context.startActivity(eventCodeIntent);
                        break;

                    case 5:
                        //This is for Post_Counciling backup
                        ManvishPrefConstants.SELECTED_EVENT_STAGE.write(StringConstants.EVENT_ITEMS[position]);
                        eventCodeIntent.putExtra(StringConstants.KEY_STAGE, "3");
                        context.startActivity(eventCodeIntent);
                        break;*/

                    case 4:

                        File storageDir=null;
                        File primaryDir = new File(StringConstants.USB_BACKUP_PATH_PRIMARY);
                        boolean isPrimary = false;
                        try{

                            try {

                                Log.e("size", primaryDir.listFiles().length + "");
                                isPrimary = true;
                                storageDir = primaryDir;

                                //This is for Post_Test USB backup
                                ManvishPrefConstants.SELECTED_EVENT_STAGE.write(StringConstants.EVENT_ITEMS[position]);
                                eventCodeIntent.putExtra(StringConstants.KEY_STAGE, "1");
                                context.startActivity(eventCodeIntent);


                            } catch(Exception e){

                                isPrimary = false;
                            }

                            if(!isPrimary) {

                                File secondryDir = new File(StringConstants.USB_BACKUP_PATH_SECONDRY);
                                try{
                                    Log.e("size", secondryDir.listFiles().length + "");

                                    //This is for Post_Test USB backup
                                     ManvishPrefConstants.SELECTED_EVENT_STAGE.write(StringConstants.EVENT_ITEMS[position]);
                                     eventCodeIntent.putExtra(StringConstants.KEY_STAGE, "1");
                                     context.startActivity(eventCodeIntent);

                                    storageDir = secondryDir;
                                }catch(Exception e){

                                    ManvishCommonUtil.showCustomToast((Activity) context,"Pendrive not detected");
                                 //   Toast.makeText(context,"Pendrive not detected",Toast.LENGTH_SHORT).show();

                                }
                            }


                        } catch(Exception e) {
                            Log.e("error",e.toString());
                            ManvishCommonUtil.showCustomToast((Activity) context,"Pendrive not detected");

                            //  Toast.makeText(context,"Pendrive not detected",Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case 5:

                        File storageCouncilDir=null;
                        File primaryCouncilDir = new File(StringConstants.USB_BACKUP_PATH_PRIMARY);
                        boolean isCouncilPrimary = false;
                        try{

                            try {

                                Log.e("size", primaryCouncilDir.listFiles().length + "");
                                isPrimary = true;
                                storageCouncilDir = primaryCouncilDir;

                                //This is for Post_Counciling USB backup
                                ManvishPrefConstants.SELECTED_EVENT_STAGE.write(StringConstants.EVENT_ITEMS[position]);
                                eventCodeIntent.putExtra(StringConstants.KEY_STAGE, "3");
                                context.startActivity(eventCodeIntent);

                            } catch(Exception e){

                                isPrimary = false;
                            }

                            if(!isPrimary) {

                                File secondryDir = new File(StringConstants.USB_BACKUP_PATH_SECONDRY);
                                try{
                                    Log.e("size", secondryDir.listFiles().length + "");

                                    //This is for Post_Counciling USB backup
                                    ManvishPrefConstants.SELECTED_EVENT_STAGE.write(StringConstants.EVENT_ITEMS[position]);
                                    eventCodeIntent.putExtra(StringConstants.KEY_STAGE, "3");
                                    context.startActivity(eventCodeIntent);

                                    storageCouncilDir = secondryDir;
                                }catch(Exception e){
                                    ManvishCommonUtil.showCustomToast((Activity) context,"Pendrive not detected");

                                    // Toast.makeText(context,"Pendrive not detected",Toast.LENGTH_SHORT).show();

                                }
                            }


                        } catch(Exception e) {
                            Log.e("error",e.toString());
                            ManvishCommonUtil.showCustomToast((Activity) context,"Pendrive not detected");

                            //  Toast.makeText(context,"Pendrive not detected",Toast.LENGTH_SHORT).show();
                        }


                        break;
                }
            }
        });


        return convertView;

    }

    private class ViewHolder {

        Button menuButton;
    }
}
