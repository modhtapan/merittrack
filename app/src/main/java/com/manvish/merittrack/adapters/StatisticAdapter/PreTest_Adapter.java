package com.manvish.merittrack.adapters.StatisticAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.content.Intent;

import com.manvish.merittrack.R;
import com.manvish.merittrack.StatisticsActivity.Statistics_Date;
import com.manvish.merittrack.StatisticsActivity.Statistics_PreTest;
import com.manvish.merittrack.StatisticsActivity.Statistics_RegVer;
import com.manvish.merittrack.activities.EventActivity;
import com.manvish.merittrack.activities.EventCodesActivity;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;

/**
 * Created by Hitesh on 4/13/2016.
 */
public class PreTest_Adapter extends BaseAdapter {
    @Override
    public int getCount() {
        return StringConstants.STATISTICS_PRE_EVENT_DATE.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Context context = parent.getContext();

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_menu, null);

            viewHolder = new ViewHolder();

            viewHolder.menuButton = (Button) convertView.findViewById(R.id.itemMenuButton);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // set each menu item
        viewHolder.menuButton.setText(StringConstants.STATISTICS_PRE_EVENT_DATE[position]);

        viewHolder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // Intent eventCodeIntent = new Intent(context, EventCodesActivity.class);
                Intent intent = new Intent(context, Statistics_Date.class);
                switch (position){


                    case 0:
                        //this is for pre-test
                        ManvishPrefConstants.SELECTED_EVENT_STAGE.write(StringConstants.STATISTICS_PRE_EVENT_DATE[position]);
                        context.startActivity(intent);

                        break;

                    case 1:
                        // This is for Post-Test
                        ManvishPrefConstants.SELECTED_EVENT_STAGE.write(StringConstants.STATISTICS_PRE_EVENT_DATE[position]);
                        context.startActivity(intent);
                        break;

                }
            }
        });


        return convertView;

    }

    private class ViewHolder {

        Button menuButton;
       }
}
