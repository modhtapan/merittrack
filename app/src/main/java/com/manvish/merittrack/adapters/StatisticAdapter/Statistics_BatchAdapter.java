package com.manvish.merittrack.adapters.StatisticAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.manvish.merittrack.R;
import com.manvish.merittrack.StatisticsActivity.Statistics_ClassroomActivity;
import com.manvish.merittrack.activities.ClassRoomActivity;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;

import java.util.ArrayList;

/**
 * MenuListAdapter for menu listview adapter
 */
public class Statistics_BatchAdapter extends BaseAdapter{

    ArrayList<String> batchList;
    public Statistics_BatchAdapter(ArrayList<String> batchList){
        this.batchList=batchList;
    }

    @Override
    public int getCount() {

        // This list is hard coded for demo purpose .
        if(ManvishPrefConstants.IS_TEST.read()){
            return StringConstants.BATCH_LIST_TEST.length;
        }else{
            return batchList.size(); // get data from database
        }

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Context context = parent.getContext();

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_menu, null);

            viewHolder = new ViewHolder();

            viewHolder.menuButton = (Button) convertView.findViewById(R.id.itemMenuButton);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if(ManvishPrefConstants.IS_TEST.read()){
            viewHolder.menuButton.setText(StringConstants.BATCH_LIST_TEST[position]);
        }else{
            viewHolder.menuButton.setText(batchList.get(position));
        }


        viewHolder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(ManvishPrefConstants.IS_TEST.read()){
                    ManvishPrefConstants.SELECTED_BATCH.write(StringConstants.BATCH_LIST_TEST[position]);
                }else{
                    ManvishPrefConstants.SELECTED_BATCH.write(batchList.get(position));
                }



                Intent intent=new Intent(context,Statistics_ClassroomActivity.class);
                context.startActivity(intent);

                // Authenticate with password ,before going to next activity



            }
        });


        return convertView;

    }

    private class ViewHolder {

        Button menuButton;
    }
}