package com.manvish.merittrack.adapters.StatisticAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.manvish.merittrack.R;
import com.manvish.merittrack.StatisticsActivity.Statistics_RegVer;
import com.manvish.merittrack.activities.EventCodesActivity;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;

/**
 * Created by Hitesh on 4/13/2016.
 */
public class Statistics_SessionAdapter extends BaseAdapter {
    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Context context = parent.getContext();

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_menu, null);

            viewHolder = new ViewHolder();

            viewHolder.menuButton = (Button) convertView.findViewById(R.id.itemMenuButton);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // set each menu item
        viewHolder.menuButton.setText(StringConstants.STATISTICS_ITEMS[position]);

        viewHolder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent eventCodeIntent = new Intent(context, EventCodesActivity.class);
                Intent intent = new Intent(context, Statistics_RegVer.class);
                switch (position){


                    case 0:
                        //this is for pre-test
                        ManvishPrefConstants.SELECTED_EVENT_STAGE.write(StringConstants.STATISTICS_ITEMS[position]);
                        context.startActivity(intent);

                        break;

                    case 1:
                        // This is for Post-Test
                        ManvishPrefConstants.SELECTED_EVENT_STAGE.write(StringConstants.STATISTICS_ITEMS[position]);
                        context.startActivity(eventCodeIntent);
                        break;

                }
            }
        });


        return convertView;

    }

    private class ViewHolder {

        Button menuButton;
    }
}
