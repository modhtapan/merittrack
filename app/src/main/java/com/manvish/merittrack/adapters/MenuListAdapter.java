package com.manvish.merittrack.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.manvish.merittrack.R;
import com.manvish.merittrack.StatisticsActivity.StatisticsActivity;
import com.manvish.merittrack.activities.DeviceDetailsActivity;
import com.manvish.merittrack.activities.DiagnosticActivity;
import com.manvish.merittrack.activities.EventActivity;
import com.manvish.merittrack.activities.MiFaunActivity;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.utils.ManvishCommonUtil;
import com.manvish.merittrack.utils.ParseAupdZipFile;
import com.manvish.merittrack.view.ManvishAlertDialog;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * MenuListAdapter for menu listview adapter
 */
public class MenuListAdapter extends BaseAdapter{

    Context context;

    @Override
    public int getCount() {
        return StringConstants.MENU_ITEMS.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        context = parent.getContext();

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_menu, null);

            viewHolder = new ViewHolder();

            viewHolder.menuButton = (Button) convertView.findViewById(R.id.itemMenuButton);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // set each menu item
        viewHolder.menuButton.setText(StringConstants.MENU_ITEMS[position]);

        viewHolder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (position){

                    case 0:

                        Intent eventIntent = new Intent(context, EventActivity.class);
                        context.startActivity(eventIntent);

                        break;
                    case 1:
                        //This flow is for testing and practice mode
                        ManvishPrefConstants.IS_TEST.write(true);
                        Intent testIntent = new Intent(context, EventActivity.class);
                        context.startActivity(testIntent);

                        break;

                    case 2:
                        //This flow is for testing and practice mode
                        showDiagDialog();

                        break;
                    case 3:
                        //GO TO SETTINGS
                        Intent settings = new Intent(context, MiFaunActivity.class);
                        context.startActivity(settings);
                        break;

                    // go to Device details Activity
                    case 4:
                        Intent intent = new Intent(context, DeviceDetailsActivity.class);
                        context.startActivity(intent);
                        break;

                    case 5:
                        Intent statisticsIntent = new Intent(context, StatisticsActivity.class);
                        context.startActivity(statisticsIntent);
                        break;

                    case 6:
                        showEnterPasswordDialog((Activity) context, "1234");
                        break;

                    case 7:
                        showEnterPasswordToLock((Activity) context, "manvish1234");
                        break;

                    case 8:
                        copyDataFromPendrive();
                        break;



                }
            }
        });


        return convertView;

    }

    private void copyDataFromPendrive() {

        if(getPendriveStorageDirectory()!=null){

            //Copy data from pendrive download folder
            File srcDownloadFolderPath = new File(getPendriveStorageDirectory().getAbsolutePath()+"/miFaun/Download");

            if(srcDownloadFolderPath.exists()){
                //copy the file from pendrive to internal memory
                File destinationFolderPath = new File(Environment.getExternalStorageDirectory()+"/MeritTrack/Download");
                if(!destinationFolderPath.exists()){
                    destinationFolderPath.mkdir() ;
                    System.out.println("new folder created");

                }

                copyFolderInBackground(srcDownloadFolderPath,destinationFolderPath);
            }else{
                Toast.makeText(context,"Folder not found to copy",Toast.LENGTH_LONG).show();
            }
        } else {

            //Check if folder is there ..save it to database ==testing purpose .
            parseAndInsertDataIntoDatabase();
        }
    }

    private class ViewHolder {

        Button menuButton;
    }



    public void showEnterPasswordDialog(final Activity context, final String password) {
        final Dialog dialog = new Dialog(context);
        Log.d("Password","pwd:"+password);
        ManvishPrefConstants.PROJECT_PASSWORD.write(password);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.refresh_dialog);
        dialog.setCancelable(false);
        final EditText etAccessCode = (EditText) dialog
                .findViewById(R.id.et_access_code);
        Button btnAccess = (Button) dialog.findViewById(R.id.btnAccess);
        Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });

        btnAccess.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d("Password ::",password);//93368868

                if (etAccessCode.getText().toString()
                        .equalsIgnoreCase(password)) {
                    updateFirmWare();

                } else {
                    new ManvishAlertDialog(context,
                            "Wrong access code",
                            "Please enter correct access code")
                            .showAlertDialog();
                }

            }
        });

        dialog.show();

    }

    public void showEnterPasswordToLock(final Activity context, final String password) {
        final Dialog dialog = new Dialog(context);
        Log.d("Password","pwd:"+password);
        ManvishPrefConstants.PROJECT_PASSWORD.write(password);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.refresh_dialog);
        dialog.setCancelable(false);
        final EditText etAccessCode = (EditText) dialog
                .findViewById(R.id.et_access_code);
        Button btnAccess = (Button) dialog.findViewById(R.id.btnAccess);
        Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });

        btnAccess.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d("Password ::",password);//manvish1234

                if (etAccessCode.getText().toString()
                        .equalsIgnoreCase(password)) {
                    lockApp(context);

                } else {
                    new ManvishAlertDialog(context,
                            "Wrong access code",
                            "Please enter correct access code")
                            .showAlertDialog();
                }

            }
        });

        dialog.show();

    }

    private void lockApp(Activity avt){
        ManvishPrefConstants.IS_LOCKED.write(true);
        ManvishCommonUtil.goToMainactivity(avt);
    }



    public void updateFirmWare(){

        try {

            File storageDir=null;
            //File storageDir = new File("/storage/usbotg/");
            File primaryDir = new File(StringConstants.USB_BACKUP_PATH_PRIMARY);
            boolean isPrimary = false;
                try {

                    Log.e("size", primaryDir.listFiles().length + "");
                    isPrimary = true;
                    storageDir = primaryDir;

                } catch(Exception e){

                    isPrimary = false;
                }

                if(!isPrimary) {

                    File secondryDir = new File(StringConstants.USB_BACKUP_PATH_SECONDRY);
                    try{
                        Log.e("size", secondryDir.listFiles().length + "");

                        storageDir = secondryDir;
                    }catch(Exception e){
                        Toast.makeText(context,"Pendrive not detected",Toast.LENGTH_SHORT).show();

                    }
                }

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(new File(storageDir.getAbsolutePath() +
                    "/miFaun/" + "mifaun.apk")), "application/vnd.android.package-archive");


            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }catch(Exception e){
         e.printStackTrace();

        }
    }
    private void showDiagDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(
                context).create();

        // Setting Dialog Title
        alertDialog.setTitle("Diagnosis Device ");

        // Setting Dialog Message
        alertDialog.setMessage("This is a quick diagnosis of Camera and Finger sensor .");


        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Intent testCamSensor = new Intent(context, DiagnosticActivity.class);
                context.startActivity(testCamSensor);

            }
        });


        // Showing Alert Message
        alertDialog.show();
    }

    public static String getAppLabel(PackageManager pm, String pathToApk) {
        PackageInfo packageInfo = pm.getPackageArchiveInfo(pathToApk, 0);

        if (Build.VERSION.SDK_INT >= 8) {
            // those two lines do the magic:
            packageInfo.applicationInfo.sourceDir = pathToApk;
            packageInfo.applicationInfo.publicSourceDir = pathToApk;
        }

        CharSequence label = pm.getApplicationLabel(packageInfo.applicationInfo);
        return label != null ? label.toString() : null;
    }

    private File getPendriveStorageDirectory(){

        boolean isPendrivePresnt=false;
       // boolean isPrimary;
        File storageCouncilDir=null;
        File primaryCouncilDir = new File(StringConstants.USB_BACKUP_PATH_PRIMARY);
        boolean isCouncilPrimary = false;
        try{

            try {

                Log.e("size", primaryCouncilDir.listFiles().length + "");
                isPendrivePresnt = true;
                storageCouncilDir = primaryCouncilDir;

                //This is for Post_Counciling USB backup
            } catch(Exception e){

                isPendrivePresnt = false;
            }

            if(!isPendrivePresnt) {

                File secondryDir = new File(StringConstants.USB_BACKUP_PATH_SECONDRY);
                try{
                    Log.e("size", secondryDir.listFiles().length + "");
                    storageCouncilDir = secondryDir;
                    isPendrivePresnt=true;
                }catch(Exception e){
                    ManvishCommonUtil.showCustomToast((Activity) context,"Pendrive not detected");

                    // Toast.makeText(context,"Pendrive not detected",Toast.LENGTH_SHORT).show();
                    isPendrivePresnt=false;
                }
            }


        } catch(Exception e) {
            Log.e("error",e.toString());
            ManvishCommonUtil.showCustomToast((Activity) context,"Pendrive not detected");
            isPendrivePresnt=false;
            //  Toast.makeText(context,"Pendrive not detected",Toast.LENGTH_SHORT).show();
        }

     return storageCouncilDir;
    }

    private void copyFolderInBackground(final File sourceFile,final File destinationFile){

        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    FileUtils.copyDirectory(sourceFile,destinationFile);
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }

            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showProgressBar("Copying file .Please wait");
            }

            @Override
            protected void onProgressUpdate(Void... values) {
                super.onProgressUpdate(values);
            }

            @Override
            protected void onPostExecute(Boolean copyStatus) {
                super.onPostExecute(copyStatus);
                if (copyStatus) {

                    ManvishCommonUtil.showCustomToast((Activity) context,"Copy success");
                    //Start inserting data in to database.
                    parseAndInsertDataIntoDatabase();
                } else {
                    ManvishCommonUtil.showCustomToast((Activity) context,"Copy failed");
                    dismissProgressBar();
                }
               // dismissProgressBar();
            }
        }.execute();
    }


    ProgressDialog mProgressDialog;
    private int mProgressDialogstatus=0;
    private Handler mProgressDialogHandler = new Handler();
    private void showProgressBar(String msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
        }


        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    private void dismissProgressBar() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

     private void parseAndInsertDataIntoDatabase(){

         new AsyncTask<Void, Void, Boolean>() {
             protected void onPreExecute() {
                 dismissProgressBar();
                 showProgressBar(" Updating Database ,please Wait ");

             }

             @Override
             protected Boolean doInBackground(Void... params) {
                 try {

                     ParseAupdZipFile parseAUPDAndSave = new ParseAupdZipFile(
                             context, Environment.getExternalStorageDirectory() + "/MeritTrack/Download/"
                             + "AUPD.zip"); // This is not needed here,Can be removed after server fix .

                     String unZipedfolderPath=Environment.getExternalStorageDirectory()+"/Merittrack/Download/";
                     return parseAUPDAndSave.parseDataFromFolder(unZipedfolderPath);

                 } catch (Exception e) {
                     Log.e("MANVISH",
                             "Exception in parsing file"
                                     + e.getMessage());
                     // dismissProgressBar();
                     return false;
                 }

             }

             @Override
             protected void onPostExecute(Boolean success) {


                 if (success) {
                     ManvishCommonUtil.showCustomToast((Activity) context, "Data saved successfully");
                     dismissProgressBar();

                 } else {

                     dismissProgressBar();
                     ManvishCommonUtil.showCustomToast((Activity) context, "Data  save went wrong.");

                 }
             }

         }.execute();
     }
}
