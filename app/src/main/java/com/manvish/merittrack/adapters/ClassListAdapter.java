package com.manvish.merittrack.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.manvish.merittrack.R;
import com.manvish.merittrack.activities.RegisterOrVerificationActivity;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.view.ManvishAlertDialog;

import java.util.ArrayList;

/**
 * MenuListAdapter for menu listview adapter
 */
public class ClassListAdapter extends BaseAdapter{

   ArrayList<String> classList;
    public  ClassListAdapter(ArrayList<String> classList){
        this.classList=classList;
    }
    @Override
    public int getCount() {

        if(ManvishPrefConstants.IS_TEST.read()){
            return StringConstants.CLASS_LIST_TEST.length;
        }else{
            if(classList!=null) {
                return classList.size();
            }else {
                return 0;
            }
        }
        // This list is hard coded for demo purpose .

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Context context = parent.getContext();

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_menu, null);

            viewHolder = new ViewHolder();

            viewHolder.menuButton = (Button) convertView.findViewById(R.id.itemMenuButton);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if(ManvishPrefConstants.IS_TEST.read()){
            viewHolder.menuButton.setText(StringConstants.CLASS_LIST_TEST[position]);
        }else{
            viewHolder.menuButton.setText(classList.get(position));
        }
        // set each event item


        viewHolder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(ManvishPrefConstants.IS_TEST.read()){
                    ManvishPrefConstants.SELECTED_CLASS.write(StringConstants.CLASS_LIST_TEST[position]);
                }else{
                    ManvishPrefConstants.SELECTED_CLASS.write(classList.get(position));
                }





                String selectedEventStage=ManvishPrefConstants.SELECTED_EVENT_STAGE.read();
                Intent registerImtent=new Intent(context, RegisterOrVerificationActivity.class);

                switch (selectedEventStage){


                    case StringConstants.PRETEST:

                        // For pretest ,we Can do both Registration and verification

                        context.startActivity(registerImtent);



                        break;

                    case StringConstants.PRECOUNCIL:

                        // For pre councelling ,We can do only Verification
                        context.startActivity(registerImtent);

                        break;



                }

            }
        });


        return convertView;

    }

    private class ViewHolder {

        Button menuButton;
    }
}
