package com.manvish.merittrack.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.manvish.merittrack.Model.Student;
import com.manvish.merittrack.R;
import com.manvish.merittrack.activities.CameraActivity;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.utils.ManvishCommonUtil;
import com.manvish.merittrack.view.ManvishAlertDialog;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

//import com.manvish.examattendance.activity.FingerRegistrationActivity;

public class IDListGridAdapter extends BaseAdapter {

    private Context mContext;

    String REG_CATEGORY;
    boolean mIsSelected = true;

    List<Student> studentDetailsList;
    DBAdapter dbAdapter;

    public IDListGridAdapter(Context c,
                             List<Student> studentDetailsList) {
        mContext = c;


        this.studentDetailsList=studentDetailsList;
        dbAdapter = DBAdapter.getInstance(c, MeritTrackApplication.getInstance().getDbName(), null, 1);
    }

    @Override
    public int getCount() {
        return studentDetailsList.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        final Context context = parent.getContext();
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_random_button, null);
            viewHolder = new ViewHolder();
            viewHolder.button = (Button) convertView.findViewById(R.id.button);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        int regOrVerStatus=0;

        // Switch case to get regOrVerStatus
        switch(ManvishPrefConstants.SELECTED_REG_OR_VER.read()){

            case StringConstants.VERIFICATION:

                if(ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.PRETEST)) {
                    // For Pre test only ,u should consider session code
                    //String sessionCodes = studentDetailsList.get(position).getSessionCode();

                    String sessionCodeAndverStatus=studentDetailsList.get(position).getSessionCode();
                    String[] sessCodeVerStatArray=sessionCodeAndverStatus.split(",");

                    if(sessCodeVerStatArray.length!=0) {

                        for (String codever : sessCodeVerStatArray) {
                            //Every code ver comes with Sessioncode-verstatus
                            // So first separate ,them and compare with Current choosen sessionCode to display the status

                            String[] codeverArray = codever.split(":");
                            if (ManvishPrefConstants.SELECTED_SESSION_CODE.read().equalsIgnoreCase(codeverArray[0])) {

                                if(codeverArray.length==2) {
                                    if (codeverArray[1].equalsIgnoreCase("" + StringConstants.STUDENT_REGISTERED)) {
                                        regOrVerStatus = StringConstants.STUDENT_REGISTERED;
                                    }
                                    if (codeverArray[1].equalsIgnoreCase("" + StringConstants.STUDENT_REGISTRATION_FAILED)) {
                                        regOrVerStatus = StringConstants.STUDENT_REGISTRATION_FAILED;
                                    }
                                }
                            }

                        }
                    }

                }else if(ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.PRECOUNCIL)) {
                    // For Pre councilling ,u no need to worry about session code
                    regOrVerStatus = Integer.parseInt(studentDetailsList.get(position).getVerStatus());
                }
                break;
            case StringConstants.OFFLINE_VERIFICATION:

                break;
            case StringConstants.REGISTRATION:
                regOrVerStatus = Integer.parseInt(studentDetailsList.get(position).getRegStatus());
                break;
        }


        // Switch case to check regOrVerStatus

        switch (regOrVerStatus) {
            case StringConstants.STUDENT_REGISTERED:

                    viewHolder.button.setBackgroundColor(mContext.getResources().getColor(
                            R.color.green_manvish));

                break;
            case StringConstants.STUDENT_NOT_REGISTERED:
                viewHolder.button.setBackgroundColor(mContext.getResources().getColor(
                        R.color.dark_grey));
                break;
            case StringConstants.STUDENT_REGISTRATION_FAILED:
                viewHolder.button.setBackgroundColor(mContext.getResources().getColor(
                        R.color.red));
                break;
            default:
                break;
        }

        viewHolder.button.setText(studentDetailsList.get(position).getStudentID());

        viewHolder.button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                /*if(mIsSelected)*/ {

                    Student student=studentDetailsList.get(position);
                    mIsSelected = false;
                    // Check ,if the ID is Already registered or verified ,if SO ,show Already registered Dialog .
                    int regOrVerStatus = 0;

                    switch (ManvishPrefConstants.SELECTED_REG_OR_VER.read()) {

                        case StringConstants.VERIFICATION:
                            //regOrVerStatus = Integer.parseInt(studentDetailsList.get(position).getVerStatus());

                            if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("1")) {

                                String sessionCodeAndverStatus=student.getSessionCode();
                                String[] sessCodeVerStatArray=sessionCodeAndverStatus.split(",");

                                if(sessCodeVerStatArray.length!=0) {

                                    for (String codever : sessCodeVerStatArray) {

                                        String[] codeverArray = codever.split(":");
                                        if (ManvishPrefConstants.SELECTED_SESSION_CODE.read().equalsIgnoreCase(codeverArray[0])) {

                                            if(codeverArray.length==2) {
                                                if (codeverArray[1].equalsIgnoreCase("" + StringConstants.STUDENT_REGISTERED)) {
                                                    regOrVerStatus = StringConstants.STUDENT_REGISTERED;
                                                }
                                                if (codeverArray[1].equalsIgnoreCase("" + StringConstants.STUDENT_REGISTRATION_FAILED)) {
                                                    regOrVerStatus = StringConstants.STUDENT_REGISTRATION_FAILED;
                                                }
                                            }
                                        }

                                    }
                                }
                            } else {
                                regOrVerStatus = Integer.parseInt(student.getVerStatus());
                            }

                            // here register means just a flag which represents both registration and verification
                            if (regOrVerStatus == StringConstants.STUDENT_REGISTERED) {
                                //showAlreadyRegisteredDialog(" Already Verified ");
                                new ManvishAlertDialog((Activity) context, "Verified", "This student is already verified .").showAlertDialog();
                            } else {

                                //If student is not registered, do not display verification screen.
                                // Display "NO DATA FOUND, Please register before verification"
                                // when student id (icon)block has been touched
                                ArrayList<byte[]> templatebyteList = null;
                                if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("1")) {
                                    //showProgressBar("Fetching student data");
                                    templatebyteList = dbAdapter.getTemplatesOfStudentFromRegisteredTableForSessionVerification(ManvishPrefConstants.SELECTED_DATE.read(),
                                            ManvishPrefConstants.SELECTED_VENUE.read(), ManvishPrefConstants.SELECTED_BATCH.read(),
                                            ManvishPrefConstants.SELECTED_CLASS.read(), studentDetailsList.get(position).getStudentID());
                                    student.setTemplatebyteList(templatebyteList);
                                    //dismissProgressBar();

                                } else if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("3")) {
                                    //showProgressBar("Fetching student data");
                                    templatebyteList = dbAdapter.getTemplatesOfStudentForVerification(ManvishPrefConstants.SELECTED_DATE.read(),
                                            ManvishPrefConstants.SELECTED_VENUE.read(), ManvishPrefConstants.SELECTED_BATCH.read(),
                                            ManvishPrefConstants.SELECTED_CLASS.read(), studentDetailsList.get(position).getStudentID());

                                    student.setTemplatebyteList(templatebyteList);
                                   // dismissProgressBar();
                                }

                                if (templatebyteList != null && templatebyteList.size() != 0) {


                                    Intent cameraIntent = new Intent(mContext, CameraActivity.class);
                                    cameraIntent.putExtra(StringConstants.KEY_STUDENT_ID, studentDetailsList.get(position).getStudentID());
                                    mContext.startActivity(cameraIntent);
                                } else {

                                    new ManvishAlertDialog((Activity) context,
                                            "NO DATA FOUND", "Please register before verification .").showAlertDialog();

                                }


                            }
                            break;

                        case StringConstants.OFFLINE_VERIFICATION:


                            ArrayList<byte[]> templatebyteList = null;
                            if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("1")) {

                                templatebyteList = dbAdapter.getTemplatesOfStudentFromRegisteredTableForSessionVerification(ManvishPrefConstants.SELECTED_DATE.read(),
                                        ManvishPrefConstants.SELECTED_VENUE.read(), ManvishPrefConstants.SELECTED_BATCH.read(),
                                        ManvishPrefConstants.SELECTED_CLASS.read(), studentDetailsList.get(position).getStudentID());

                            } else if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("3")) {
                                templatebyteList = dbAdapter.getTemplatesOfStudentForVerification(ManvishPrefConstants.SELECTED_DATE.read(),
                                        ManvishPrefConstants.SELECTED_VENUE.read(), ManvishPrefConstants.SELECTED_BATCH.read(),
                                        ManvishPrefConstants.SELECTED_CLASS.read(), studentDetailsList.get(position).getStudentID());
                            }

                            if (templatebyteList != null && templatebyteList.size() != 0) {


                                Intent cameraIntent = new Intent(mContext, CameraActivity.class);
                                cameraIntent.putExtra(StringConstants.KEY_STUDENT_ID, studentDetailsList.get(position).getStudentID());
                                mContext.startActivity(cameraIntent);
                            } else {

                                new ManvishAlertDialog((Activity) context,
                                        "NO DATA FOUND", "Please register before verification .").showAlertDialog();

                            }

                            break;

                        case StringConstants.REGISTRATION:
                            regOrVerStatus = Integer.parseInt(studentDetailsList.get(position).getRegStatus());
                            if (regOrVerStatus == StringConstants.STUDENT_REGISTERED || regOrVerStatus == StringConstants.STUDENT_REGISTRATION_FAILED) {
                                showAlreadyRegisteredDialog(" Already Registered ", studentDetailsList.get(position).getStudentID());
                            } else {

                                ManvishCommonUtil.showStudentDetails(studentDetailsList.get(position), (Activity) context);
                            }
                            break;
                    }
                }

            }

        });

                return  convertView;
    }

    @Override
    public Object getItem(int position) {

        return studentDetailsList.get(position);
    }

    private static class ViewHolder {
        Button button;
    }


    public  void showAlreadyRegisteredDialog(String msg,final String studentID) {
        final Dialog dialog = new Dialog(mContext);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_already_registered);
        TextView tvDialogMsg=(TextView)dialog.findViewById(R.id.tv_dialog_msg);
        tvDialogMsg.setText(msg);
        Button btnOverwrite,btnDelete,btnQuit;

        btnOverwrite=(Button)dialog.findViewById(R.id.btn_over_write);
        btnDelete=(Button)dialog.findViewById(R.id.btn_delete);
        btnQuit=(Button)dialog.findViewById(R.id.btn_quit);

        btnOverwrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // If we select overwrite, delete the existing reg details of student and go for fresh registration of that student

                // Delete registered data
                dbAdapter.deleteRegisteredData(ManvishPrefConstants.SELECTED_DATE.read(),
                        ManvishPrefConstants.SELECTED_VENUE.read(),
                        ManvishPrefConstants.SELECTED_BATCH.read(),
                        ManvishPrefConstants.SELECTED_CLASS.read(),studentID);

                //Update Student table
                dbAdapter.updatestudentRegStatus(ManvishPrefConstants.SELECTED_DATE.read(),
                        ManvishPrefConstants.SELECTED_VENUE.read(),
                        ManvishPrefConstants.SELECTED_BATCH.read(),
                        ManvishPrefConstants.SELECTED_CLASS.read(),studentID,"0");

               String mapKey=ManvishPrefConstants.SELECTED_DATE.read()+
                        ManvishPrefConstants.SELECTED_VENUE.read()+
                        ManvishPrefConstants.SELECTED_BATCH.read()+
                        ManvishPrefConstants.SELECTED_CLASS.read()+
                        studentID;
                LinkedHashMap<String,Student> studentLinkedHashMap=MeritTrackApplication.getInstance().getStudentListMap();
                Student student=studentLinkedHashMap.get(mapKey);
                student.setRegStatus("0");
                notifyDataSetChanged();
                //Go for fresh registration
                Intent cameraIntent = new Intent(mContext, CameraActivity.class);
                cameraIntent.putExtra(StringConstants.KEY_STUDENT_ID, studentID);
                mContext.startActivity(cameraIntent);
                dialog.dismiss();

            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // delete the student reg details with confirmation
                showConfirmationDeleteDialog(studentID);

                dialog.dismiss();

            }
        });
        btnQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        dialog.setCancelable(true);

        dialog.show();

    }

    private void showConfirmationDeleteDialog(final String studentID){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                mContext);

        alertDialog.setTitle("Are you sure ?");
        alertDialog.setMessage("Do you really want to delete existing registration.");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {

        // Delete registered data
                dbAdapter.deleteRegisteredData(ManvishPrefConstants.SELECTED_DATE.read(),
                        ManvishPrefConstants.SELECTED_VENUE.read(),
                        ManvishPrefConstants.SELECTED_BATCH.read(),
                        ManvishPrefConstants.SELECTED_CLASS.read(),studentID);

                //Update Student table
                dbAdapter.updatestudentRegStatus(ManvishPrefConstants.SELECTED_DATE.read(),
                        ManvishPrefConstants.SELECTED_VENUE.read(),
                        ManvishPrefConstants.SELECTED_BATCH.read(),
                        ManvishPrefConstants.SELECTED_CLASS.read(),studentID,"0");

                String mapKey=ManvishPrefConstants.SELECTED_DATE.read()+
                        ManvishPrefConstants.SELECTED_VENUE.read()+
                        ManvishPrefConstants.SELECTED_BATCH.read()+
                        ManvishPrefConstants.SELECTED_CLASS.read()+
                        studentID;
                LinkedHashMap<String,Student> studentLinkedHashMap=MeritTrackApplication.getInstance().getStudentListMap();
                Student student=studentLinkedHashMap.get(mapKey);
                student.setRegStatus("0");
                notifyDataSetChanged();
                dialog.cancel();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event

                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void setSelected(boolean isSelected){
        mIsSelected = isSelected;
    }

    public boolean getSelected() {
        return mIsSelected;
    }


    ProgressDialog mProgressDialog;
    private int mProgressDialogstatus=0;
    private Handler mProgressDialogHandler = new Handler();
    private void showProgressBar(String msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
        }


        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    private void dismissProgressBar() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }


}