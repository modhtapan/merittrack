package com.manvish.merittrack.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.manvish.merittrack.R;
import com.manvish.merittrack.activities.ClassRoomActivity;
import com.manvish.merittrack.activities.SelectDateActivity;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;

import java.util.ArrayList;

/**
 * MenuListAdapter for menu listview adapter
 */
public class SessionCodeListAdapter extends BaseAdapter{

    ArrayList<String> sessionCodeList;
     public SessionCodeListAdapter(ArrayList<String> sessionCodeList){
        this.sessionCodeList =sessionCodeList;
    }

    @Override
    public int getCount() {

            return sessionCodeList.size(); // get data from database


    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Context context = parent.getContext();

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_menu, null);

            viewHolder = new ViewHolder();

            viewHolder.menuButton = (Button) convertView.findViewById(R.id.itemMenuButton);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


            viewHolder.menuButton.setText(sessionCodeList.get(position));



        viewHolder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    ManvishPrefConstants.SELECTED_SESSION_CODE.write(sessionCodeList.get(position));
                    Intent intent=new Intent(context,SelectDateActivity.class);
                    context.startActivity(intent);
            }
        });


        return convertView;

    }

    private class ViewHolder {

        Button menuButton;
    }
}
