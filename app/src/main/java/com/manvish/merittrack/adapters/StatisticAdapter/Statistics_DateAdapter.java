package com.manvish.merittrack.adapters.StatisticAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.manvish.merittrack.R;
import com.manvish.merittrack.StatisticsActivity.Statistics_VenueActivity;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;

import java.util.ArrayList;

/**
 * MenuListAdapter for menu listview adapter
 */
public class Statistics_DateAdapter extends BaseAdapter{

    ArrayList<String> dateList;
    public Statistics_DateAdapter(ArrayList<String> dateList){
        this.dateList=dateList;
    }

    @Override
    public int getCount() {


        if(ManvishPrefConstants.IS_TEST.read()){
            // This list is hard coded for demo purpose .
            return StringConstants.DATE_LIST_TEST.length;
        }else{
            // This list is hard coded for demo purpose .
            return dateList.size();
        }

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Context context = parent.getContext();

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_menu, null);

            viewHolder = new ViewHolder();

            viewHolder.menuButton = (Button) convertView.findViewById(R.id.itemMenuButton);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // set each event item
        if(ManvishPrefConstants.IS_TEST.read()) {
            viewHolder.menuButton.setText(StringConstants.DATE_LIST_TEST[position]);
        }else {
            viewHolder.menuButton.setText(dateList.get(position));
        }

        viewHolder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(ManvishPrefConstants.IS_TEST.read()) {
                    ManvishPrefConstants.SELECTED_DATE.write(StringConstants.DATE_LIST[position]);
                }else{
                    ManvishPrefConstants.SELECTED_DATE.write(dateList.get(position));
                }

                String selectedEventStage=ManvishPrefConstants.SELECTED_EVENT_STAGE.read();
                Intent venueIntent=new Intent(context, Statistics_VenueActivity.class);


                switch (selectedEventStage){


                    case StringConstants.PRETEST:

                        context.startActivity(venueIntent);

                        break;

                    case StringConstants.PRECOUNCIL:

                        context.startActivity(venueIntent);

                        break;

                }



            }
        });


        return convertView;

    }

    private class ViewHolder {

        Button menuButton;
    }
}
