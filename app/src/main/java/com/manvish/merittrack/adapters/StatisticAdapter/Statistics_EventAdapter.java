package com.manvish.merittrack.adapters.StatisticAdapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.manvish.merittrack.Model.Project;
import com.manvish.merittrack.R;
import com.manvish.merittrack.StatisticsActivity.Statistics_Date;
import com.manvish.merittrack.activities.RegVerButtonActivity;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.db.DBAdapter;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * MenuListAdapter for menu listview adapter
 */
public class Statistics_EventAdapter extends BaseAdapter{

    private LinkedHashMap<String,String> eventMap;
    private Context context;
    private ProgressDialog progressDialog;

    public Statistics_EventAdapter(Context context, LinkedHashMap<String,String> eventMap){

        this.eventMap=eventMap;
        this.context=context;
    }

    @Override
    public int getCount() {

        // This list is hard coded for demo purpose .

        if(ManvishPrefConstants.IS_TEST.read()){
            return StringConstants.EVENT_CODES_TEST.length;
        }else {
            return eventMap.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Context context = parent.getContext();

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_menu, null);

            viewHolder = new ViewHolder();

            viewHolder.menuButton = (Button) convertView.findViewById(R.id.itemMenuButton);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // set each event item

        if(ManvishPrefConstants.IS_TEST.read()) {

            viewHolder.menuButton.setText(StringConstants.EVENT_CODES_TEST[position]);
        }else {


            for(Map.Entry<String, String> entry: eventMap.entrySet()) {
                // System.out.println(entry.getKey());
                eventIDList.add(entry.getKey());
            }
            viewHolder.menuButton.setText(eventIDList.get(position));
        }

        viewHolder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // Authenticate with password ,before going to next activity

                if(ManvishPrefConstants.IS_TEST.read()) {
                    ManvishPrefConstants.SELECTED_EVENT_CODE.write(StringConstants.EVENT_CODES_TEST[position]);
                    MeritTrackApplication.getInstance().setDbName(StringConstants.TEST_DB);
                    // go to
                    Intent intent=new Intent(context, RegVerButtonActivity.class);
                    context.startActivity(intent);
                }else{

                    ManvishPrefConstants.SELECTED_EVENT_CODE.write(eventIDList.get(position));
                    gotoDate(context);

                }


            }
        });


        return convertView;

    }
    LinkedList<String> eventIDList=new LinkedList<>();
    private class ViewHolder {

        Button menuButton;
    }
    private void gotoDate(final Context context) {

            if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("1")) {
                String dbName = ManvishPrefConstants.SELECTED_EVENT_CODE.read() + "T" + ".db";
                ManvishPrefConstants.DB_NAME.write(dbName);
                MeritTrackApplication.getInstance().setDbName(dbName);
            } else {
                String dbName = ManvishPrefConstants.SELECTED_EVENT_CODE.read() + "C" + ".db";
                ManvishPrefConstants.DB_NAME.write(dbName);
                MeritTrackApplication.getInstance().setDbName(dbName);
            }

       // MeritTrackApplication.getInstance().setDbName(MeritTrackApplication.getInstance().getDbName());

        new AsyncTask<Void, Void, Boolean>() {
            protected void onPreExecute() {

                progressDialog = new ProgressDialog(context);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setMessage("Please wait..");
                progressDialog.setCancelable(false);
                progressDialog.show();

            }

            @Override
            protected Boolean doInBackground(Void... params) {

                DBAdapter dbAdapter = DBAdapter.getInstance(context, MeritTrackApplication.getInstance().getDbName(), null, 1);

                Project project = dbAdapter.getProjectDetails();
                if(project!=null) {
                    ManvishPrefConstants.FINGERCONFIG.write(project.getFingerConfig());
                    ManvishPrefConstants.EXTRAIMAGES.write(project.getExtraImages());
                    ManvishPrefConstants.STAGE.write(project.getStage());
                }

                return true;

            }

            @Override
            protected void onPostExecute(Boolean success) {


                // go to
                progressDialog.dismiss();
                    Intent intent = new Intent(context, Statistics_Date.class);
                    context.startActivity(intent);

            }

        }.execute();
    }
}
