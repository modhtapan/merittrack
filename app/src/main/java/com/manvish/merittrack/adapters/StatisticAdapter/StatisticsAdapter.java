package com.manvish.merittrack.adapters.StatisticAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.manvish.merittrack.R;
import com.manvish.merittrack.StatisticsActivity.Statistics_RegVer;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;

/**
 * MenuListAdapter for menu listview adapter
 */
public class StatisticsAdapter extends BaseAdapter{


    @Override
    public int getCount() {
        return StringConstants.STATISTICS_ITEMS.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Context context = parent.getContext();

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_menu, null);

            viewHolder = new ViewHolder();

            viewHolder.menuButton = (Button) convertView.findViewById(R.id.itemMenuButton);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // set each menu item
        viewHolder.menuButton.setText(StringConstants.STATISTICS_ITEMS[position]);

        viewHolder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, Statistics_RegVer.class);

                switch (position){

                    case 0:
                        //this is for pre-test
                        ManvishPrefConstants.SELECTED_STATS.write(StringConstants.STATISTICS_ITEMS[position]);
                        ManvishPrefConstants.STAGE.write("1");
                        context.startActivity(intent);
                        break;

                    case 1:
                        // This is for Pre-councilling
                        ManvishPrefConstants.SELECTED_STATS.write(StringConstants.STATISTICS_ITEMS[position]);
                        ManvishPrefConstants.STAGE.write("3");
                        context.startActivity(intent);
                        break;
                }
            }
        });


        return convertView;

    }

    private class ViewHolder {

        Button menuButton;
    }
}
