package com.manvish.merittrack.adapters.StatisticAdapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.manvish.merittrack.R;
import com.manvish.merittrack.StatisticsActivity.Statistics_BatchActivity;
import com.manvish.merittrack.activities.BatchActivity;
import com.manvish.merittrack.activities.ClassRoomActivity;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;

import java.util.ArrayList;

/**
 * MenuListAdapter for menu listview adapter
 */
public class Statistics_VenueAdapter extends BaseAdapter{

    ArrayList<String> venueCodeList;
    public Statistics_VenueAdapter(ArrayList<String> venueCodeList){
        this.venueCodeList=venueCodeList;
    }

    @Override
    public int getCount() {

        // This list is hard coded for demo purpose .
        if(ManvishPrefConstants.IS_TEST.read()){
            return StringConstants.VENUE_LIST_TEST.length;
        }else {
            return venueCodeList.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Context context = parent.getContext();

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_menu, null);

            viewHolder = new ViewHolder();

            viewHolder.menuButton = (Button) convertView.findViewById(R.id.itemMenuButton);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if(ManvishPrefConstants.IS_TEST.read()){
            // set each event item
            viewHolder.menuButton.setText(StringConstants.VENUE_LIST_TEST[position]);
        }else{
            // set each event item
            viewHolder.menuButton.setText(venueCodeList.get(position));
        }


        viewHolder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(ManvishPrefConstants.IS_TEST.read()){
                    // set each event item
                    ManvishPrefConstants.SELECTED_VENUE.write(StringConstants.VENUE_LIST_TEST[position]);
                }else{
                    // set each event item
                    ManvishPrefConstants.SELECTED_VENUE.write(venueCodeList.get(position));
                }


                Intent intent=new Intent(context,Statistics_BatchActivity.class);
                context.startActivity(intent);

                // Authenticate with password ,before going to next activity



            }
        });


        return convertView;

    }

    private class ViewHolder {

        Button menuButton;
    }
}
