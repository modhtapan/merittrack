package com.manvish.merittrack.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.manvish.merittrack.R;
import com.manvish.merittrack.activities.UploadBackupActivity;
import com.manvish.merittrack.activities.VenueActivity;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.utils.ManvishCommonUtil;
import com.manvish.merittrack.view.ManvishAlertDialog;

import java.util.ArrayList;

/**
 * MenuListAdapter for menu listview adapter
 */
public class DateListAdapter extends BaseAdapter{

    ArrayList<String> dateList;
    DBAdapter dbAdapter;
    public DateListAdapter(ArrayList<String> dateList,DBAdapter dbAdapter){
        this.dateList=dateList;
        this.dbAdapter=dbAdapter;
    }

    int i=0;
    @Override
    public int getCount() {


        if(ManvishPrefConstants.IS_TEST.read()){
            // This list is hard coded for demo purpose .
            return StringConstants.DATE_LIST_TEST.length;
        }else{
            // This list is hard coded for demo purpose .
            if(dateList!=null) {
                return dateList.size();
            } else {
                return 0;
            }
        }

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Context context = parent.getContext();

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_menu, null);

            viewHolder = new ViewHolder();

            viewHolder.menuButton = (Button) convertView.findViewById(R.id.itemMenuButton);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // set each event item
        if(ManvishPrefConstants.IS_TEST.read()) {
            viewHolder.menuButton.setText(StringConstants.DATE_LIST_TEST[position]);
        }else {
            viewHolder.menuButton.setText(dateList.get(position));
        }

        viewHolder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(ManvishPrefConstants.IS_TEST.read()) {
                    ManvishPrefConstants.SELECTED_DATE.write(StringConstants.DATE_LIST_TEST[position]);
                }else{
                    ManvishPrefConstants.SELECTED_DATE.write(dateList.get(position));
                }

                String selectedEventStage=ManvishPrefConstants.SELECTED_EVENT_STAGE.read();
                Intent venueIntent=new Intent(context, VenueActivity.class);
                Intent postTestOrCouncilIntent=new Intent(context, UploadBackupActivity.class);

                switch (selectedEventStage){


                    case StringConstants.PRETEST:
                        i++;

                        // Check if for that Date ,Data is uploaded or not ,If data is Uploaded ,
                        // don't go to next Venue  Screen on one click ,but if he clicks second time
                        // ask for password and go to next screen ,only for pre-test

                        if(ManvishPrefConstants.IS_TEST.read()){
                            context.startActivity(venueIntent);
                        }else{

                            int uploadedRegDataCount=dbAdapter.getUploadedRegDataCountFromStudentTable(dateList.get(position));
                            int uploadVerDataCount=dbAdapter.getUploadedVerDataCountFromStudentTable(dateList.get(position));

                            if(uploadedRegDataCount>0 || uploadVerDataCount>0){

                                if(i > 1){ // means date is clicked 2 times
                                // ask for password  and allow him to go to VenueScreen .
                                    ManvishCommonUtil.showEnterPasswordToActivateAfterUpload((Activity)context,
                                            ManvishPrefConstants.PROJECT_PASSWORD.read());
                                }else{
                                 // Show dialog saying Data is already Uploaded
                                    new ManvishAlertDialog((Activity)context,"Info","Activity is closed for this Date ..").showAlertDialog();
                                }
                            }else{
                                // go to venue screen
                                context.startActivity(venueIntent);
                            }

                        }




                        break;
                    case StringConstants.POSTTEST:

                        context.startActivity(postTestOrCouncilIntent);

                        break;
                    case StringConstants.PRECOUNCIL:

                        context.startActivity(venueIntent);

                        break;
                    case StringConstants.POSTCOUNCIL:

                        context.startActivity(postTestOrCouncilIntent);

                        break;
                    case StringConstants.BACKUP_USB_POSTTEST:

                        context.startActivity(postTestOrCouncilIntent);

                        break;
                    case StringConstants.BACKUP_USB_POSTCOUNCILING:

                        context.startActivity(postTestOrCouncilIntent);

                        break;
                }



            }
        });


        return convertView;

    }

    private class ViewHolder {

        Button menuButton;
    }
}
