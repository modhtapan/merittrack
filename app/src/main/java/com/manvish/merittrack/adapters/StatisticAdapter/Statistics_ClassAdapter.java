package com.manvish.merittrack.adapters.StatisticAdapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.manvish.merittrack.R;
import com.manvish.merittrack.StatisticsActivity.Statistics_Download_Activity;
import com.manvish.merittrack.StatisticsActivity.Statistics_Uploade_Activity;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;

import java.util.ArrayList;

/**
 * MenuListAdapter for menu listview adapter
 */
public class Statistics_ClassAdapter extends BaseAdapter{

    ArrayList<String> classList;
    public  Statistics_ClassAdapter(ArrayList<String> classList){
        this.classList=classList;
    }
    @Override
    public int getCount() {

        if(ManvishPrefConstants.IS_TEST.read()){
            return StringConstants.CLASS_LIST_TEST.length;
        }else{
            return classList.size();
        }
        // This list is hard coded for demo purpose .

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Context context = parent.getContext();

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.item_menu, null);

            viewHolder = new ViewHolder();

            viewHolder.menuButton = (Button) convertView.findViewById(R.id.itemMenuButton);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if(ManvishPrefConstants.IS_TEST.read()){
            viewHolder.menuButton.setText(StringConstants.CLASS_LIST_TEST[position]);
        }else{
            viewHolder.menuButton.setText(classList.get(position));
        }
        // set each event item


        viewHolder.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(ManvishPrefConstants.IS_TEST.read()){
                    ManvishPrefConstants.SELECTED_CLASS.write(StringConstants.CLASS_LIST_TEST[position]);
                }else{
                    ManvishPrefConstants.SELECTED_CLASS.write(classList.get(position));
                }

                if(ManvishPrefConstants.SELECTED_STATS.read().equalsIgnoreCase("DOWNLOAD STATS")){

                    Intent downloadIntent = new Intent(context, Statistics_Download_Activity.class);
                    context.startActivity(downloadIntent);

                } else {

                    Intent registerImtent = new Intent(context, Statistics_Uploade_Activity.class);
                    context.startActivity(registerImtent);
                }
            }
        });


        return convertView;

    }

    private class ViewHolder {

        Button menuButton;
    }
}
