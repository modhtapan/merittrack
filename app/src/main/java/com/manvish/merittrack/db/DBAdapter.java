package com.manvish.merittrack.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.manvish.merittrack.Model.Config;
import com.manvish.merittrack.Model.Project;
import com.manvish.merittrack.Model.RegistredData;
import com.manvish.merittrack.Model.Student;
import com.manvish.merittrack.Model.Venue;
import com.manvish.merittrack.Model.VerData;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.modelupload.ImageWrite;
import com.manvish.merittrack.modelupload.TemplateWrite;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

/**
 * Created by satya on 2/24/2016.
 */
public class DBAdapter implements DBConstant {

    /**
     * Please make sure to update the Database version number if you make any
     * changes otherwise it may crash With DBAdapter you can make normal query
     * also Help to interact with Database Note: Projection: It indicates the
     * resuletent column Selection: eg Condition id=? Selection args : String
     * array of values
     */

    private static DBAdapter dbAdater;
    private DBHelper dbHelper;

    private DBAdapter() {

    }

    private DBAdapter(Context context, String dbName, SQLiteDatabase.CursorFactory factory, int version) {
        dbHelper = new DBHelper(context, dbName, factory, StringConstants.DB_VERSION_CODE); // Hardcode the version here
    }

    public static DBAdapter getInstance() {
        if (null == dbAdater) {
            dbAdater = new DBAdapter();
        }
        return dbAdater;
    }

    public static DBAdapter getInstance(Context context, String dbName, SQLiteDatabase.CursorFactory factory, int version) {
        if (null == dbAdater) {
            dbAdater = new DBAdapter(context, dbName, factory, StringConstants.DB_VERSION_CODE);
        }
        return dbAdater;
    }


    public void insertCommon(String projectCode,String ProjectPassword,String projectName,String projectStage) {
            ContentValues cv = new ContentValues();
            cv.put(PROJECT_CODE,projectCode);
            cv.put(PROJECT_STAGE, projectStage);
            cv.put(PROJECT_PASSWORD, ProjectPassword);
            dbHelper.insertData(TABLE_COMMON, cv);
    }

    public void insertConfigData(Config config,String transactionID) {


        if (config != null) {
            ContentValues cv = new ContentValues();
            cv.put(TRANSACTION_ID,transactionID);

            ManvishPrefConstants.transiction_id.write(transactionID);
            cv.put(COMPANY_DETAILS, config.getCompanyDetails());

            ManvishPrefConstants.COMPANY_LOGO.write(config.getLogo());
            cv.put(LOGO, config.getLogo());

            ManvishPrefConstants.THREAD_SCHEDULING.write(config.getThreadScheduling());
            cv.put(THREADSCHEDULING, config.getThreadScheduling());

            dbHelper.insertData(TABLE_PROJECT_CONFIG, cv);
        }

    }

    public Config getConfigData() {
        Config config=null;
        Cursor cursor = dbHelper.queryData(TABLE_PROJECT_CONFIG,null,null,null,null);

        if (null != cursor && cursor.moveToFirst()) {
            do {
                config=new Config();
                String company_details = cursor.getString(cursor.getColumnIndex(COMPANY_DETAILS));
                String logo = cursor.getString(cursor.getColumnIndex(LOGO));
                String threadScheduling=cursor.getString(cursor.getColumnIndex(THREADSCHEDULING));
                config.setCompanyDetails(company_details);
                config.setLogo(logo);
                config.setThreadScheduling(threadScheduling);
            } while (cursor.moveToNext());
        }

        return config;
    }

    public void insertProjectDetails(Project project) {


        if (project != null) {
            ContentValues cv = new ContentValues();
            cv.put(PROJECT_CODE,project.getProjectCode());
            cv.put(PROJECT_NAME, project.getProjectName());
            cv.put(PROJECT_PASSWORD, project.getProjectPassword());
            cv.put(FINGER_CONFIG, project.getFingerConfig());
            cv.put(EXTRA_IMAGES, project.getExtraImages());
            cv.put(START_DATE, project.getStartDate());
            cv.put(END_DATE, project.getEndDate());
            cv.put(CAPTURE_PHOTO_FLAG, project.getCapturePhoto());
            cv.put(CAPTURE_IMAGE_FLAG, project.getCaptureImage());
            cv.put(NO_VER_SESSION, project.getNoOfVerSession());
            cv.put(STAGE, project.getStage());
            dbHelper.insertData(TABLE_PROJECT_DETAILS, cv);
        }

    }

    public Project getProjectDetails() {
        Project project=null;
        Cursor cursor = dbHelper.queryData(TABLE_PROJECT_DETAILS,null,null,null,null);

        if (null != cursor && cursor.moveToFirst()) {
            do {
                project=new Project();
                String project_code = cursor.getString(cursor.getColumnIndex(PROJECT_CODE));
                project.setProjectCode(project_code);
                String project_name = cursor.getString(cursor.getColumnIndex(PROJECT_NAME));
                project.setProjectName(project_name);
                String project_password = cursor.getString(cursor.getColumnIndex(PROJECT_PASSWORD));
                project.setProjectPassword(project_password);
                String finger_config = cursor.getString(cursor.getColumnIndex(FINGER_CONFIG));
                project.setFingerConfig(finger_config);
                String extra_images = cursor.getString(cursor.getColumnIndex(EXTRA_IMAGES));
                project.setExtraImages(extra_images);
                String start_date = cursor.getString(cursor.getColumnIndex(START_DATE));
                project.setStartDate(start_date);
                String end_date = cursor.getString(cursor.getColumnIndex(END_DATE));
                project.setEndDate(end_date);

                String capture_photo_flag = cursor.getString(cursor.getColumnIndex(CAPTURE_PHOTO_FLAG));
                project.setCapturePhoto(capture_photo_flag);

                String capture_image_flag = cursor.getString(cursor.getColumnIndex(CAPTURE_IMAGE_FLAG));
                project.setCaptureImage(capture_image_flag);

                String no_of_ver_session = cursor.getString(cursor.getColumnIndex(NO_VER_SESSION));
                project.setNoOfVerSession(no_of_ver_session);

                String stage = cursor.getString(cursor.getColumnIndex(STAGE));
                project.setStage(stage);
            } while (cursor.moveToNext());
        }

        return project;
    }


    public void insertProjectVerSession(String projectCode,String noOFVerSessions,Project.Session session) {


        if (session != null) {
            ContentValues cv = new ContentValues();
            cv.put(PROJECT_CODE_SESSION,projectCode);
            cv.put(VER_SESSION_NO, noOFVerSessions);
            cv.put(SESSION_CODE, session.getSessionCode());
            cv.put(SESSION_DATE, session.getDate());

            dbHelper.insertData(TABLE_VER_SESSION, cv);
        }

    }

    public ArrayList<String>  getVerSessionCodeList(){

        ArrayList<String> sessionCodeList=new ArrayList<>();
        Cursor cursor = dbHelper.queryData(TABLE_VER_SESSION,null,null,null,null);
        if (null != cursor && cursor.moveToFirst()) {
            do {
                String sessionCode = cursor.getString(cursor.getColumnIndex(SESSION_CODE));
                String sessionDate = cursor.getString(cursor.getColumnIndex(SESSION_DATE));
                sessionCodeList.add(sessionCode);
            } while (cursor.moveToNext());
        }
        return sessionCodeList;
    }

    public ArrayList<String>  getVerSessionDate(String sessionCode){

        ArrayList<String> sessionDateList=new ArrayList<>();
        Cursor cursor = dbHelper.queryData(TABLE_VER_SESSION,null,DBConstant.SESSION_CODE + "='" + sessionCode + "'",null,null);
        if (null != cursor && cursor.moveToFirst()) {
            do {
               // String sessionDate = cursor.getString(cursor.getColumnIndex(SESSION_DATE));
                sessionDateList.add(sessionCode);
            } while (cursor.moveToNext());
        }
        return sessionDateList;
    }


    public void insertProjectVenues(String projectCode,Venue venue) {

       // dbHelper.deleteData(TABLE_PROJECT_VENUES, null, null);
        if (venue != null) {

            ContentValues cv = new ContentValues();
            cv.put(PROJECT_CODE_VENUE,projectCode);
            cv.put(VENUE_CODE, venue.getVenueCode());
            cv.put(VENUE_NAME, venue.getVenueName());
            cv.put(VENUE_NO_OF_STUDENTS, venue.getNoOfStudents());

            dbHelper.insertData(TABLE_PROJECT_VENUES, cv);
        }

    }

    public void insertStudentDetails(String projectCode,String venuecode,Student std) {


        if (std != null) {

            ContentValues cv = new ContentValues();
            cv.put(PROJECT_CODE_STUDENT,projectCode);
            cv.put(VENUE_CODE_STUDENT, venuecode);
            cv.put(STUDENT_ID, std.getStudentID());
            cv.put(STUDENT_NAME, std.getName());
            cv.put(TEST_DATE, std.getTestDate());
            cv.put(BATCH_CODE, std.getBatchCode());
            cv.put(CLASS_ROOM, std.getClassRoom());
            cv.put(PHOTO_GRAPH, std.getPhotograph());
            cv.put(REG_STATUS, 0);      // Initially "0" .. means ,not attempted for registration
            cv.put(VER_STATUS, 0);     //  Initially "0" .. means ,not attempted for verification
            cv.put(UPLOAD_FLAG_REG,0);    // 0 : means not uploaded
            cv.put(UPLOAD_FLAG_VER,0);
            cv.put(SESSION_CODE,""); // Insert empty session Code and first and update it while session verification to
           // dbHelper.insertData(TABLE_STUDENT, cv);
            dbHelper.insertStudentAndTemplateData(TABLE_STUDENT, cv);
        }

    }

    public void updateStudentDetails(String projectCode,String venuecode,Student std) {


        if (std != null) {

            ContentValues cv = new ContentValues();
            cv.put(PROJECT_CODE_STUDENT,projectCode);
            cv.put(VENUE_CODE_STUDENT, venuecode);
            cv.put(STUDENT_ID, std.getStudentID());
            cv.put(STUDENT_NAME, std.getName());
            cv.put(TEST_DATE, std.getTestDate());
            cv.put(BATCH_CODE, std.getBatchCode());
            cv.put(CLASS_ROOM, std.getClassRoom());
            cv.put(PHOTO_GRAPH, std.getPhotograph());
            cv.put(REG_STATUS, 0);   // Initially "0" .. means ,not attempted for registration
            cv.put(VER_STATUS, 0);  //  Initially "0" .. means ,not attempted for verification

            dbHelper.insertData(TABLE_STUDENT, cv);
        }

    }

    public void insertStudentDetailsBulkInsertion(String projectCode,String venuecode,ArrayList<Student> std) {


       dbHelper.insertStudentBulkData(projectCode,venuecode,std);

    }

    public void insertStudentTemplate(String projectCode,String venuecode,Student std ,Student.StudentTemplate stdTemplate) {


        if (std != null) {

            ContentValues cv = new ContentValues();
            cv.put(PROJECT_CODE,projectCode);
            cv.put(VENUE_CODE, venuecode);
            cv.put(STUDENT_ID, std.getStudentID());
            cv.put(STUDENT_NAME, std.getName());
            cv.put(TEST_DATE, std.getTestDate());
            cv.put(BATCH_CODE, std.getBatchCode());
            cv.put(CLASS_ROOM, std.getClassRoom());
            cv.put(TEMPLATE_TYPE, stdTemplate.getType());

            // Convert base64 to byte[] and save in to data base for template data pabitra
            byte[] templateData=Base64.decode(
                    stdTemplate.getData(), Base64.DEFAULT);

            cv.put(TEMPLATE_DATA, templateData);

           // dbHelper.insertData(TABLE_TEMPLATE, cv);
            dbHelper.insertStudentAndTemplateData(TABLE_TEMPLATE, cv);
        }

    }


    public void insertRegistrationDetails(String projectCode, String venuecode,
                                          String studentID, String studentName,
                                          String test_date, String batch_code,
                                          String classroom,
                                          byte[] photograph,
                                          byte[] fingerImage,
                                          String regStatus, String templateType,
                                          byte[] templateData, String reg_date,
                                          String reg_time,String latitude,String longitude) {

        //Insert upload flag as "0"
        String uploadFlag="0";
        ContentValues cv = new ContentValues();
        cv.put(PROJECT_CODE,projectCode);
        cv.put(VENUE_CODE, venuecode);
        cv.put(STUDENT_ID, studentID);
        cv.put(STUDENT_NAME, studentName);
        cv.put(TEST_DATE, test_date);
        cv.put(BATCH_CODE, batch_code);
        cv.put(CLASS_ROOM, classroom);
        cv.put(PHOTO_GRAPH, photograph);
        cv.put(FINGER_IMAGE, fingerImage);
        cv.put(REG_STATUS, regStatus);
        cv.put(TEMPLATE_TYPE, templateType);
        cv.put(TEMPLATE_DATA, templateData);
        cv.put(REGISTRATION_DATE, reg_date);
        cv.put(REGISTRATION_TIME, reg_time);
        cv.put(UPLOAD_FLAG,0);
        // cv.put(LATIT_UDE,latitude);
        // cv.put(LONGIT_UDE,longitude);

        dbHelper.insertData(TABLE_REGISTRATION, cv);
        //dbHelper.insertData(TABLE_REGISTRATION_TESTDB,cv);
    }


    public void insertRegisteredPhotograph(String projectCode, String venuecode,
                                          String studentID, String studentName,
                                          String test_date, String batch_code,
                                          String classroom,
                                          byte[] photograph, String reg_date,
                                          String reg_time) {


        ContentValues cv = new ContentValues();
        cv.put(PROJECT_CODE,projectCode);
        cv.put(VENUE_CODE, venuecode);
        cv.put(STUDENT_ID, studentID);
        cv.put(STUDENT_NAME, studentName);
        cv.put(TEST_DATE, test_date);
        cv.put(BATCH_CODE, batch_code);
        cv.put(CLASS_ROOM, classroom);
        cv.put(PHOTO_GRAPH, photograph);
        cv.put(REGISTRATION_DATE, reg_date);
        cv.put(REGISTRATION_TIME, reg_time);


        dbHelper.insertData(TABLE_REGISTERED_PHOTOGRAPH, cv);
    }



    public void insertVerificationDetails(String projectCode,String venuecode,
                                          String studentID,String studentName,
                                          String test_date,String batch_code,
                                          String classroom,
                                          String verStatus,
                                          String ver_date,
                                          String ver_time ,String session_code,String session_date,String fingerType,String latitude,String longitude) {


        ContentValues cv = new ContentValues();
        cv.put(PROJECT_CODE,projectCode);
        cv.put(VENUE_CODE, venuecode);
        cv.put(STUDENT_ID, studentID);
        cv.put(STUDENT_NAME, studentName);
        cv.put(TEST_DATE, test_date);
        cv.put(BATCH_CODE, batch_code);
        cv.put(CLASS_ROOM, classroom);

        cv.put(VER_STATUS, verStatus);

        cv.put(VER_DATE, ver_date);
        cv.put(VER_TIME, ver_time);
        cv.put(VER_SESSION_CODE,session_code);
        cv.put(VER_SESSION_DATE,session_date);
        cv.put(UPLOAD_FLAG,"0");
        cv.put(VER_FINGER_TYPE,fingerType);
         //  cv.put(LATIT_UDE,latitude);
        // cv.put(LONGIT_UDE,longitude);
        dbHelper.insertData(TABLE_VERIFICATION, cv);

    }

    public void deleteVerFailedData(String date,String venue,
                                     String batch,String classRoom,String studentID,String sessionCode){
        dbHelper.deleteData(TABLE_VERIFICATION,
                DBConstant.TEST_DATE + "='" + date + "'"+ " AND " + DBConstant.VENUE_CODE + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'"
                        + " AND " + DBConstant.STUDENT_ID + "='" + studentID + "'"+ " AND " + DBConstant.SESSION_CODE + "='" + sessionCode + "'",
                null);

    }

    public void flushData(String projectId) {

        dbHelper.deleteData(TABLE_PROJECT_DETAILS, PROJECT_CODE, new String[]{projectId});
        dbHelper.deleteData(TABLE_PROJECT_VENUES, PROJECT_CODE_VENUE, new String[]{projectId});
        dbHelper.deleteData(TABLE_VER_SESSION, PROJECT_CODE_SESSION, new String[]{projectId});
        dbHelper.deleteData(TABLE_STUDENT, PROJECT_CODE_STUDENT, new String[]{projectId});
        dbHelper.deleteData(TABLE_REGISTRATION, PROJECT_CODE, new String[]{projectId});
       // dbHelper.deleteData(TABLE_VERIFICATION, PROJECT_CODE, new String[]{projectId});
    }

     public void  closeDatabase(){
         if(dbHelper!=null)dbHelper.close();
         dbAdater=null;
     }

    public void removeProjectCodeFromCommonTable(String projectCode){
        //Remove projectCode row from Common Table

    }

    public void deleteExistingTableData(){
        dbHelper.deleteData(TABLE_PROJECT_CONFIG,null,null);
        dbHelper.deleteData(TABLE_PROJECT_DETAILS,null,null);
        dbHelper.deleteData(TABLE_VER_SESSION,null,null);
        dbHelper.deleteData(TABLE_PROJECT_VENUES,null,null);
        dbHelper.deleteData(TABLE_STUDENT,null,null);
        dbHelper.deleteData(TABLE_TEMPLATE,null,null);

    }

    public  LinkedHashMap<String,String> getEventsFromCommonDatabaseCommonTable(String stage){

        LinkedHashMap<String,String> eventSet = new  LinkedHashMap<>();
        Cursor cursor = dbHelper.queryData(TABLE_COMMON,null,DBConstant.PROJECT_STAGE + "='" + stage + "'",null,null);

        if (null != cursor && cursor.moveToFirst()) {
            do {

                String projectID = cursor.getString(cursor.getColumnIndex(PROJECT_CODE));
                String password = cursor.getString(cursor.getColumnIndex(PROJECT_PASSWORD));
                eventSet.put(projectID,password);

            } while (cursor.moveToNext());
        }

        return eventSet;
    }

    public  ArrayList<String> getUniqueDateListFromStudentTable(){

        ArrayList<String> dateList = new  ArrayList<String>();
        Cursor cursor = dbHelper.queryData(true, TABLE_STUDENT, new String[]{TEST_DATE}, null, null, TEST_DATE, null, null, null);

        if (null != cursor && cursor.moveToFirst()) {
            do {
                String date = cursor.getString(cursor.getColumnIndex(TEST_DATE));
                dateList.add(date);
            } while (cursor.moveToNext());
        }

        return dateList;
    }

    public  ArrayList<String> getUniqueBatchStudentTable(){

        ArrayList<String> batchList = new  ArrayList<String>();
        Cursor cursor = dbHelper.queryData(true, TABLE_STUDENT, new String[]{BATCH_CODE}, null, null, BATCH_CODE, null, null, null);

        if (null != cursor && cursor.moveToFirst()) {
            do {

                String batchCode = cursor.getString(cursor.getColumnIndex(BATCH_CODE));
                batchList.add(batchCode);
            } while (cursor.moveToNext());
        }

        return batchList;
    }


    public  ArrayList<String> getUniqueClassRoomListFromStudentTable(){

        ArrayList<String> classList = new  ArrayList<String>();
        Cursor cursor = dbHelper.queryData(true, TABLE_STUDENT, new String[]{CLASS_ROOM}, null, null, CLASS_ROOM, null, null, null);

        if (null != cursor && cursor.moveToFirst()) {
            do {

                String classRoom = cursor.getString(cursor.getColumnIndex(CLASS_ROOM));
                classList.add(classRoom);
            } while (cursor.moveToNext());
        }

        return classList;
    }


    public  ArrayList<String> getVenueList(){

        ArrayList<String> venueList = new  ArrayList<String>();
        Cursor cursor = dbHelper.queryData(true, TABLE_PROJECT_VENUES, new String[]{VENUE_CODE}, null, null, VENUE_CODE, null, null, null);

        if (null != cursor && cursor.moveToFirst()) {
            do {

                String venueCode = cursor.getString(cursor.getColumnIndex(VENUE_CODE));
                venueList.add(venueCode);
            } while (cursor.moveToNext());
        }

        return venueList;
    }

    public LinkedHashMap<String,Student> getStudentMapListFromDateVenueBatchClass(String date,String venue,
                                                                                  String batch,String classRoom ,String regOrVer){

        LinkedHashMap<String,Student> studentMapList=new LinkedHashMap<String,Student>();
        Student student;

        //String args[]={date,venue,batch,classRoom};

        // get cursor for Student which are not uploaded
        Cursor cursor=null;
        switch (regOrVer){

            case StringConstants.REGISTRATION:
                cursor = dbHelper.queryData(TABLE_STUDENT,
                        new String[]{STUDENT_ID,STUDENT_NAME,REG_STATUS,VER_STATUS,SESSION_CODE},
                        DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='" + date + "'"+ " AND "
                        + DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.UPLOAD_FLAG_REG + "='" + "0" + "'", null, "");

                break;

            case StringConstants.VERIFICATION:
                cursor = dbHelper.queryData(TABLE_STUDENT, new String[]{STUDENT_ID,STUDENT_NAME,REG_STATUS,VER_STATUS,SESSION_CODE}, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='"
                        + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'", null, "");

                break;

            case StringConstants.OFFLINE_VERIFICATION:
                cursor = dbHelper.queryData(TABLE_STUDENT, new String[]{STUDENT_ID,STUDENT_NAME,REG_STATUS,VER_STATUS,SESSION_CODE}, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND "
                        + DBConstant.TEST_DATE + "='" + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'", null, "");

                break;
        }

        if (null != cursor && cursor.moveToFirst()) {
            do {
                student=new Student();

                String id = cursor.getString(cursor.getColumnIndex(STUDENT_ID));
                student.setStudentID(id);

                String name = cursor.getString(cursor.getColumnIndex(STUDENT_NAME));
                student.setName(name);

                //String photograph = cursor.getString(cursor.getColumnIndex(PHOTO_GRAPH));
                //student.setPhotograph(photograph);

                String regStatus = cursor.getString(cursor.getColumnIndex(REG_STATUS));
                student.setRegStatus(regStatus);

                String verStatus = cursor.getString(cursor.getColumnIndex(VER_STATUS));
                student.setVerStatus(verStatus);

                String sessionCode = cursor.getString(cursor.getColumnIndex(SESSION_CODE));
                student.setSessionCode(sessionCode);
                //studentList.add(student);
                studentMapList.put(date+venue+batch+classRoom+id,student);
            } while (cursor.moveToNext());
        }

        return studentMapList;
    }



    public int getPhotoCount(String date,String venue, String batch,String classRoom ,String regOrVer){

        LinkedHashMap<String,Student> studentMapList=new LinkedHashMap<String,Student>();
        Student student;

        //String args[]={date,venue,batch,classRoom};

        // get cursor for Student which are not uploaded
        Cursor cursor=null;
        switch (regOrVer){

            case StringConstants.REGISTRATION:
                cursor = dbHelper.queryData(TABLE_STUDENT,  new String[]{DBConstant.PHOTO_GRAPH}, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='" + date + "'"+ " AND "
                        + DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.UPLOAD_FLAG_REG + "='" + "0" + "'"+ " AND " + DBConstant.PHOTO_GRAPH + "!='" + null + "'"
                        + " AND " + DBConstant.PHOTO_GRAPH + "!='" + "" + "'", null, "");

                break;

            case StringConstants.VERIFICATION:
                cursor = dbHelper.queryData(TABLE_STUDENT,  new String[]{DBConstant.PHOTO_GRAPH}, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='"
                        + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.PHOTO_GRAPH + "!='" + null + "'"
                        + " AND " + DBConstant.PHOTO_GRAPH + "!='" + "" + "'", null, "");

                break;

            case StringConstants.OFFLINE_VERIFICATION:
                cursor = dbHelper.queryData(TABLE_STUDENT, new String[]{DBConstant.PHOTO_GRAPH}, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND "
                        + DBConstant.TEST_DATE + "='" + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.PHOTO_GRAPH + "!='" + null + "'"
                        + " AND " + DBConstant.PHOTO_GRAPH + "!='" + "" + "'", null, "");

                break;
        }

//        int i=0;
//        if (null != cursor && cursor.moveToFirst()) {
//            do {
//
//
//
//                String photograph = cursor.getString(cursor.getColumnIndex(PHOTO_GRAPH));
//                //student.setPhotograph(photograph);
//                if(!TextUtils.isEmpty(photograph)){
//                   i++;
//                }
//
//            } while (cursor.moveToNext());
//        }

        if(cursor==null){
            return 0;
        }

        return cursor.getCount();
    }

    public LinkedHashMap<String,Student> getStudentMapListFromDateVenueBatchClassForDtatistics(String date,String venue,
                                                                                  String batch,String classRoom ,String regOrVer){

        LinkedHashMap<String,Student> studentMapList=new LinkedHashMap<String,Student>();
        studentMapList.clear();
        Student student;

        //String args[]={date,venue,batch,classRoom};

        // get cursor for Student which are not uploaded
        Cursor cursor=null;
        // Do not consider upload flag here ,show all datas
        switch (regOrVer){

            case StringConstants.REGISTRATION:
                cursor = dbHelper.queryData(TABLE_STUDENT, new String[]{STUDENT_ID,STUDENT_NAME,REG_STATUS,VER_STATUS,SESSION_CODE}, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='" + date + "'"+ " AND "
                        + DBConstant.CLASS_ROOM + "='" + classRoom + "'", null, "");

                break;

            case StringConstants.VERIFICATION:
                cursor = dbHelper.queryData(TABLE_STUDENT, new String[]{STUDENT_ID,STUDENT_NAME,REG_STATUS,VER_STATUS,SESSION_CODE}, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='"
                        + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'", null, "");

                break;

            case StringConstants.OFFLINE_VERIFICATION:
                cursor = dbHelper.queryData(TABLE_STUDENT, new String[]{STUDENT_ID,STUDENT_NAME,REG_STATUS,VER_STATUS,SESSION_CODE}, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND "
                        + DBConstant.TEST_DATE + "='" + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'", null, "");

                break;
        }

        if (null != cursor && cursor.moveToFirst()) {
            do {
                student=new Student();

                String id = cursor.getString(cursor.getColumnIndex(STUDENT_ID));
                student.setStudentID(id);

                String name = cursor.getString(cursor.getColumnIndex(STUDENT_NAME));
                student.setName(name);

                //String photograph = cursor.getString(cursor.getColumnIndex(PHOTO_GRAPH));
                //student.setPhotograph(photograph);

                String regStatus = cursor.getString(cursor.getColumnIndex(REG_STATUS));
                student.setRegStatus(regStatus);

                String verStatus = cursor.getString(cursor.getColumnIndex(VER_STATUS));
                student.setVerStatus(verStatus);

                String sessionCode = cursor.getString(cursor.getColumnIndex(SESSION_CODE));
                student.setSessionCode(sessionCode);
                //studentList.add(student);
                studentMapList.put(date+venue+batch+classRoom+id,student);
            } while (cursor.moveToNext());
        }

        return studentMapList;
    }




    public String getStudentphotographFromID(String date,String venue, String batch,
                                                                    String classRoom ,String regOrVer,String studentID){

        LinkedHashMap<String,Student> studentMapList=new LinkedHashMap<String,Student>();
        studentMapList.clear();
        Student student;

        //String args[]={date,venue,batch,classRoom};

        // get cursor for Student which are not uploaded
        Cursor cursor=null;
        // Do not consider upload flag here ,show all datas
        switch (regOrVer){

            case StringConstants.REGISTRATION:
                cursor = dbHelper.queryData(TABLE_STUDENT, null, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='" + date + "'"+ " AND "
                        + DBConstant.CLASS_ROOM + "='" + classRoom + "'"
                        + DBConstant.STUDENT_ID + "='" + studentID + "'", null, "");

                break;

            case StringConstants.VERIFICATION:
                cursor = dbHelper.queryData(TABLE_STUDENT, null, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='"
                        + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'"
                        + DBConstant.STUDENT_ID + "='" + studentID + "'", null, "");

                break;

            case StringConstants.OFFLINE_VERIFICATION:
                cursor = dbHelper.queryData(TABLE_STUDENT, null, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND "
                        + DBConstant.TEST_DATE + "='" + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'"
                        + DBConstant.STUDENT_ID + "='" + studentID + "'", null, "");

                break;
        }

        String photograph=null;
        if (null != cursor && cursor.moveToFirst()) {
            do {




                photograph = cursor.getString(cursor.getColumnIndex(PHOTO_GRAPH));



            } while (cursor.moveToNext());
        }

        return photograph;
    }

    public int getUploadedStudentCount(String date,String venue,String batch,String classRoom ,String regOrVer){



        //String args[]={date,venue,batch,classRoom};

        // get cursor for Student which are  uploaded
        Cursor cursor=null;
        switch (regOrVer){

            case StringConstants.REGISTRATION:
                cursor = dbHelper.queryData(TABLE_STUDENT, null, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='" + date + "'"+ " AND "
                        + DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.UPLOAD_FLAG_REG + "='" + "1" + "'", null, "");

                break;

            case StringConstants.VERIFICATION:
                cursor = dbHelper.queryData(TABLE_STUDENT, null, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='"
                        + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.UPLOAD_FLAG_VER + "='" + "1" + "'", null, "");

                break;

            case StringConstants.OFFLINE_VERIFICATION:
                cursor = dbHelper.queryData(TABLE_STUDENT, null, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND "
                        + DBConstant.TEST_DATE + "='" + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.UPLOAD_FLAG_VER + "='" + "1" + "'", null, "");

                break;
        }

        if (null != cursor) {
         return  cursor.getCount();
        }

        return 0;
    }

    public int getUploadedStudentCountForAllSession(String date,String venue,String batch,String classRoom ,String regOrVer){



        //String args[]={date,venue,batch,classRoom};

        // get cursor for Student which are  uploaded
        Cursor cursor=null;
        switch (regOrVer){


            case StringConstants.VERIFICATION:
                cursor = dbHelper.queryData(TABLE_STUDENT, null, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='"
                        + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.UPLOAD_FLAG_VER + "='" + "1" + "'", null, "");

                break;

            case StringConstants.OFFLINE_VERIFICATION:
                cursor = dbHelper.queryData(TABLE_STUDENT, null, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND "
                        + DBConstant.TEST_DATE + "='" + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.UPLOAD_FLAG_VER + "='" + "1" + "'", null, "");

                break;
        }

        int uploadVerDataForAllSessions=0;

        if (null != cursor && cursor.moveToFirst()) {
            do {
                String uploadedSessionCodes=cursor.getString(cursor.getColumnIndex(SESSION_CODE));

               if(!TextUtils.isEmpty(uploadedSessionCodes)) {
                   String sessions[] = uploadedSessionCodes.split(",");
                   int count = sessions.length;
                   uploadVerDataForAllSessions = uploadVerDataForAllSessions + count;
               }
            } while (cursor.moveToNext());
        }


        return uploadVerDataForAllSessions;
    }


    public void updatestudentRegStatus(String date,String venue,
                               String batch,String classRoom,String studentID,String regStatusCode) {

        ContentValues cv = new ContentValues();


        cv.put(REG_STATUS, regStatusCode);

//        dbHelper.updateData(TABLE_STUDENT, cv, DBConstant.TEST_DATE + "=" + "'" + date + "'"
//                + DBConstant.VENUE_CODE_STUDENT + "=" + "'" + venue + "'"
//                + DBConstant.BATCH_CODE + "=" + "'" + batch + "'"
//                + DBConstant.CLASS_ROOM + "=" + "'" + classRoom + "'"
//                + DBConstant.STUDENT_ID + "=" + "'" + studentID + "'", null);

        dbHelper.updateData(TABLE_STUDENT,
                cv,
                DBConstant.TEST_DATE + " = ? AND " + DBConstant.VENUE_CODE_STUDENT + " = ? AND " + DBConstant.BATCH_CODE + " = ? AND " + DBConstant.CLASS_ROOM + " = ? AND " + DBConstant.STUDENT_ID + " = ?",
                new String[]{date, venue,batch,classRoom,studentID});

    }

    public void updatestudentRegUploadStatus(String date,String venue,
                                       String batch,String classRoom,String studentID,String uploadFlag) {

        ContentValues cv = new ContentValues();
        cv.put(UPLOAD_FLAG_REG, uploadFlag);
        dbHelper.updateData(TABLE_STUDENT,
                cv,
                DBConstant.TEST_DATE + " = ? AND " + DBConstant.VENUE_CODE_STUDENT + " = ? AND " + DBConstant.BATCH_CODE + " = ? AND " + DBConstant.CLASS_ROOM + " = ? AND " + DBConstant.STUDENT_ID + " = ?",
                new String[]{date, venue,batch,classRoom,studentID});

    }

    public void updatestudentVerUploadStatus(String date,String venue,
                                             String batch,String classRoom,String studentID,String uploadFlag) {

        ContentValues cv = new ContentValues();
        cv.put(UPLOAD_FLAG_VER, uploadFlag);
        dbHelper.updateData(TABLE_STUDENT,
                cv,
                DBConstant.TEST_DATE + " = ? AND " + DBConstant.VENUE_CODE_STUDENT + " = ? AND " + DBConstant.BATCH_CODE + " = ? AND " + DBConstant.CLASS_ROOM + " = ? AND " + DBConstant.STUDENT_ID + " = ?",
                new String[]{date, venue,batch,classRoom,studentID});

    }

    public void updatestudentVerStatus(String date,String venue,
                                       String batch,String classRoom,String studentID,String verStatusCode,String sessionCode) {

        ContentValues cv = new ContentValues();
        cv.put(VER_STATUS, verStatusCode);
        cv.put(SESSION_CODE, sessionCode);

//        dbHelper.updateData(TABLE_STUDENT, cv, DBConstant.TEST_DATE + "=" + "'" + date + "'"
//                + DBConstant.VENUE_CODE_STUDENT + "=" + "'" + venue + "'"
//                + DBConstant.BATCH_CODE + "=" + "'" + batch + "'"
//                + DBConstant.CLASS_ROOM + "=" + "'" + classRoom + "'"
//                + DBConstant.STUDENT_ID + "=" + "'" + studentID + "'", null);

        dbHelper.updateData(TABLE_STUDENT,
                cv,
                DBConstant.TEST_DATE + " = ? AND " + DBConstant.VENUE_CODE_STUDENT + " = ? AND " + DBConstant.BATCH_CODE + " = ? AND " + DBConstant.CLASS_ROOM + " = ? AND " + DBConstant.STUDENT_ID + " = ?",
                new String[]{date, venue,batch,classRoom,studentID});

    }

    public ArrayList<byte[]> getTemplatesOfStudentForVerification(String date,String venue,
                                                                  String batch,String classRoom,String studentID){

        ArrayList<byte[]> templateList=new ArrayList<>();


        Cursor cursor = dbHelper.queryData(TABLE_TEMPLATE, new String[]{TEMPLATE_DATA},
                DBConstant.VENUE_CODE + "='" + venue + "'"
                + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='" + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.STUDENT_ID + "='" + studentID + "'", null, "");

        byte[] templateData=null;

        if (null != cursor && cursor.moveToFirst()) {
            do {
                templateData = cursor.getBlob(cursor.getColumnIndex(TEMPLATE_DATA));
                    if(templateData!=null && templateData.length!=0){
                        templateList.add(templateData);
                    }

            } while (cursor.moveToNext());
        }

        return templateList;
    }

    public int getTemplatesListOfStudentForVerification(String date,String venue,
                                                                  String batch,String classRoom){

        ArrayList<byte[]> templateList=new ArrayList<>();


        Cursor cursor = dbHelper.queryData(TABLE_TEMPLATE, new String[]{TEMPLATE_DATA}, DBConstant.VENUE_CODE + "='" + venue + "'"
                + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='" + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'", null, "");
       if(cursor!=null){
           return cursor.getCount();
       }else{
           return  StringConstants.FINGER_TEMPLATE_COUNT_ERROR;
       }

    }


    public ArrayList<byte[]> getTemplatesOfStudentFromRegisteredTableForSessionVerification(String date,String venue,
                                                                  String batch,String classRoom,String studentID){

        ArrayList<byte[]> templateList=new ArrayList<>();


        Cursor cursor = dbHelper.queryData(TABLE_REGISTRATION, new String[]{TEMPLATE_DATA}, DBConstant.VENUE_CODE + "='" + venue + "'"
                + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='" + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.STUDENT_ID + "='" + studentID + "'", null, "");


        if (null != cursor && cursor.moveToFirst()) {
            do {
                byte[] templateData = cursor.getBlob(cursor.getColumnIndex(TEMPLATE_DATA));

                if(templateData!=null && templateData.length!=0) {
                    templateList.add(templateData);
                }
            } while (cursor.moveToNext());
        }

        return templateList;
    }

    public int getAllTemplatesOfStudentsFromRegisteredTableForSessionVerification(String date,String venue,
                                                                                            String batch,String classRoom){

        ArrayList<byte[]> templateList=new ArrayList<>();


        Cursor cursor = dbHelper.queryData(TABLE_REGISTRATION, new String[]{TEMPLATE_DATA}, DBConstant.VENUE_CODE + "='" + venue + "'"
                + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='" + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'", null, "");

        if(cursor!=null){
            return cursor.getCount();
        }else{
            return StringConstants.FINGER_TEMPLATE_COUNT_ERROR;
        }
    }



    public ArrayList<TemplateWrite> getTemplateToUpload(String date, String venue,
                                                        String batch, String classRoom, String studentID){


        TemplateWrite uploadTemplate=null;
        ArrayList<TemplateWrite> uploadTemplateArrayList=new ArrayList<>();
        Cursor cursor = dbHelper.queryData(TABLE_REGISTRATION, new String[]{TEMPLATE_DATA,TEMPLATE_TYPE,REG_STATUS}, DBConstant.VENUE_CODE + "='" + venue + "'"
                + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='" + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.STUDENT_ID + "='" + studentID + "'", null, "");


        if (null != cursor && cursor.moveToFirst()) {
            do {

                uploadTemplate=new TemplateWrite();

                byte[] templateData = cursor.getBlob(cursor.getColumnIndex(TEMPLATE_DATA));
                if(templateData==null){
                    String fileDataStrin="";
                    uploadTemplate.settData(fileDataStrin);
                }else{
                    String fileDataString = Base64.encodeToString(templateData,Base64.DEFAULT);
                    uploadTemplate.settData(fileDataString);
                }


                String templateType = cursor.getString(cursor.getColumnIndex(TEMPLATE_TYPE));
                uploadTemplate.setFingerType(templateType);

                String status = cursor.getString(cursor.getColumnIndex(REG_STATUS));
                uploadTemplate.setRegStatus(status);


                //Ver Status should be offline Ver Status ,
                // Right now ,We are not considering that ,So putting a hardcode value of "0"
                // "o" means not attempted
                uploadTemplate.setVerStatus("0");

                uploadTemplateArrayList.add(uploadTemplate);

            } while (cursor.moveToNext());
        }

        return uploadTemplateArrayList;
    }


    public ArrayList<ImageWrite> getFingerImageToUpload(String date, String venue,
                                                        String batch, String classRoom, String studentID){



        ImageWrite uploadFingerImage=null;
        ArrayList<ImageWrite> uploadFingerArrayList=new ArrayList<>();
        Cursor cursor = dbHelper.queryData(TABLE_REGISTRATION, null, DBConstant.VENUE_CODE + "='" + venue + "'"
                + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='" + date + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.STUDENT_ID + "='" + studentID + "'", null, "");


        if (null != cursor && cursor.moveToFirst()) {
            do {

                uploadFingerImage=new ImageWrite();


                byte[] templateData = cursor.getBlob(cursor.getColumnIndex(FINGER_IMAGE));

                if(templateData==null){


                    uploadFingerImage.setiData("");
                }else{

                    String fileDataString = Base64.encodeToString(templateData,Base64.DEFAULT);
                    uploadFingerImage.setiData(fileDataString);
                }


                String templateType = cursor.getString(cursor.getColumnIndex(TEMPLATE_TYPE));
                uploadFingerImage.setiType(templateType);

                String status = cursor.getString(cursor.getColumnIndex(REG_STATUS));
                uploadFingerImage.setRegStatus(status);

                uploadFingerImage.setVerStatus("0");

                uploadFingerArrayList.add(uploadFingerImage);
            } while (cursor.moveToNext());
        }

        return uploadFingerArrayList;
    }


    public ArrayList<RegistredData> getRegDataForPostTest(String date){
        // for stage 1 ,reg data is also called ENR data
        ArrayList<RegistredData> registredDataArrayList= new ArrayList<>();
        // Get ResgisteredData for a date ,for which students are not uploaded to server
        String uploadFlag="0"; //0 : means not uploaded

        Cursor cursor = dbHelper.queryData(TABLE_REGISTRATION, null,
                DBConstant.TEST_DATE + "='" + date + "'"+ " AND " + DBConstant.UPLOAD_FLAG + "='" + uploadFlag + "'",
                null, "");

       /* String CREATE_TABLE_REGISTRATION = "create table " + TABLE_REGISTRATION + " ( " + PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , " + PROJECT_CODE + " varchar ,"
                + VENUE_CODE+ " varchar ," + STUDENT_ID + " varchar ," + STUDENT_NAME + " varchar" +
                "," + TEST_DATE + " varchar" +
                "," + BATCH_CODE + " varchar" +
                "," + CLASS_ROOM + " varchar ," + PHOTO_GRAPH + " varchar" +" varchar ," + FINGER_IMAGE +
                "," + REG_STATUS + " varchar ," + TEMPLATE_TYPE + " varchar," + TEMPLATE_DATA + " varchar,
                " + REGISTRATION_DATE + " varchar," + REGISTRATION_TIME + " varchar," + UPLOAD_FLAG + " varchar) ";
*/

        RegistredData regData;
        if (null != cursor && cursor.moveToFirst()) {
            do {
                regData=new RegistredData();

                String venueCode=cursor.getString(cursor.getColumnIndex(VENUE_CODE));
                regData.setVenueCode(venueCode);

                String studentID=cursor.getString(cursor.getColumnIndex(STUDENT_ID));
                regData.setStudentID(studentID);
                String studentName=cursor.getString(cursor.getColumnIndex(STUDENT_NAME));
                regData.setStudentName(studentName);
                String testDate=cursor.getString(cursor.getColumnIndex(TEST_DATE));
                regData.setTestDate(testDate);
                String studentBatchCode=cursor.getString(cursor.getColumnIndex(BATCH_CODE));
                regData.setBatchCode(studentBatchCode);
                String studentClassroom=cursor.getString(cursor.getColumnIndex(CLASS_ROOM));
                regData.setClassRoom(studentClassroom);
                //String studentID=cursor.getString(cursor.getColumnIndex(PHOTO_GRAPH)); // get photograph from phtograph Table
                byte[] fingerImage=cursor.getBlob(cursor.getColumnIndex(FINGER_IMAGE));
                regData.setFingerImage(fingerImage);
                String regStatus=cursor.getString(cursor.getColumnIndex(REG_STATUS));
                regData.setRegStatus(regStatus);
                String tempType=cursor.getString(cursor.getColumnIndex(TEMPLATE_TYPE));
                regData.setTemplateType(tempType);
                byte[] tempData=cursor.getBlob(cursor.getColumnIndex(TEMPLATE_DATA));
                regData.setTemplateData(tempData);
                String regDate=cursor.getString(cursor.getColumnIndex(REGISTRATION_DATE));
                regData.setRegDate(regDate);
                String regTime=cursor.getString(cursor.getColumnIndex(REGISTRATION_TIME));
                regData.setRegTime(regTime);
                registredDataArrayList.add(regData);
            } while (cursor.moveToNext());
        }

        return registredDataArrayList;
    }



    public ArrayList<String> getRegStudentUniqueDta(String date, boolean isBackup){
        // for stage 1 ,reg data is also called ENR data
        ArrayList<String> studentSet= new ArrayList<>();
        // Get ResgisteredData for a date ,for which students are not uploaded to server
        String uploadFlag="0"; //0 : means not uploaded

        Cursor cursor;
        if(!isBackup) {
            cursor = dbHelper.queryData(TABLE_REGISTRATION, new String[]{VENUE_CODE,STUDENT_ID,STUDENT_NAME,TEST_DATE,BATCH_CODE,CLASS_ROOM,REGISTRATION_DATE,REGISTRATION_TIME},
                    DBConstant.TEST_DATE + "='" + date + "'" + " AND " + DBConstant.UPLOAD_FLAG + "='" + uploadFlag + "'",
                    null, "");
        } else {
            cursor = dbHelper.queryData(TABLE_REGISTRATION, null,
                    DBConstant.TEST_DATE + "='" + date + "'" ,
                    null, "");
        }


        if (null != cursor && cursor.moveToFirst()) {
            do {

                String venueCode=cursor.getString(cursor.getColumnIndex(VENUE_CODE));
                String studentID=cursor.getString(cursor.getColumnIndex(STUDENT_ID));
                String studentName=cursor.getString(cursor.getColumnIndex(STUDENT_NAME));
                String testDate=cursor.getString(cursor.getColumnIndex(TEST_DATE));
                String studentBatchCode=cursor.getString(cursor.getColumnIndex(BATCH_CODE));
                String studentClassroom=cursor.getString(cursor.getColumnIndex(CLASS_ROOM));
                String regDate=cursor.getString(cursor.getColumnIndex(REGISTRATION_DATE));
                String regTime=cursor.getString(cursor.getColumnIndex(REGISTRATION_TIME));
                studentSet.add(venueCode+","+studentID+","+studentName+","+testDate+","+studentBatchCode+","+studentClassroom+","+regDate+","+regTime);

            } while (cursor.moveToNext());
        }

        ArrayList studentList = new ArrayList(new LinkedHashSet(
                studentSet));

        return studentList;
    }

    public byte[] getRegPhotoForPostTest(String date,String venue,
                                       String batch,String classRoom,String studentID){
        // for stage 1 ,get registered photograph

        byte[] photograph=null;
        Cursor cursor = dbHelper.queryData(TABLE_REGISTERED_PHOTOGRAPH, new String[]{PHOTO_GRAPH}, DBConstant.VENUE_CODE + "='" + venue + "'"
                + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='" + date + "'"+ " AND " +
                DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.STUDENT_ID + "='" + studentID + "'", null, "");


        if (null != cursor && cursor.moveToFirst()) {
            do {
                photograph = cursor.getBlob(cursor.getColumnIndex(PHOTO_GRAPH));
            } while (cursor.moveToNext());
        }

      return photograph;
    }

    public String getStudentPhotograph(String date,String venue,
                                         String batch,String classRoom,String studentID){
        // for stage 1 ,get registered photograph

        String photograph=null;
        Cursor cursor = dbHelper.queryData(TABLE_STUDENT, new String[]{PHOTO_GRAPH}, DBConstant.VENUE_CODE_STUDENT + "='" + venue + "'"
                + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='" + date + "'"+ " AND " +
                DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.STUDENT_ID + "='" + studentID + "'", null, "");


        if (null != cursor && cursor.moveToFirst()) {
            do {
                photograph = cursor.getString(cursor.getColumnIndex(PHOTO_GRAPH));
                Log.d("Photographvalue", "getStudentPhotograph:"+photograph);
            } while (cursor.moveToNext());
        }

        return photograph;
    }

    public ArrayList<VerData> getAccessLogForPostCouncil(String date, boolean isBackup){ // Get verification Data
        // for stage 1 ,ver data is also called AccessLog data

        ArrayList<VerData> verDataArrayList=new ArrayList<>();
        // Get ResgisteredData for a date ,for which students are not uploaded to server
        String uploadFlag="0"; //0 : means not uploaded

        Cursor cursor;
        if(!isBackup) {
            cursor = dbHelper.queryData(TABLE_VERIFICATION, null,
                    DBConstant.TEST_DATE + "='" + date + "'" + " AND " + DBConstant.UPLOAD_FLAG + "='" + uploadFlag + "'",
                    null, "");
        } else {
            cursor = dbHelper.queryData(TABLE_VERIFICATION, null,
                    DBConstant.TEST_DATE + "='" + date + "'",
                    null, "");
        }


        VerData verData;
        if (null != cursor && cursor.moveToFirst()) {
            do {
                verData=new VerData();
                String studentID=cursor.getString(cursor.getColumnIndex(STUDENT_ID));
                verData.setStudentID(studentID);
                String venueCode=cursor.getString(cursor.getColumnIndex(VENUE_CODE));
                verData.setVenueCode(venueCode);

                String studentName=cursor.getString(cursor.getColumnIndex(STUDENT_NAME));
                verData.setStudentName(studentName);
                String testDate=cursor.getString(cursor.getColumnIndex(TEST_DATE));
                verData.setTestDate(testDate);
                String studentBatchCode=cursor.getString(cursor.getColumnIndex(BATCH_CODE));
                verData.setBatchCode(studentBatchCode);
                String studentClassroom=cursor.getString(cursor.getColumnIndex(CLASS_ROOM));
                verData.setClassRoom(studentClassroom);
                //String studentID=cursor.getString(cursor.getColumnIndex(PHOTO_GRAPH)); // get photograph from phtograph Table

                String verStatus=cursor.getString(cursor.getColumnIndex(VER_STATUS));
                verData.setVerStatus(verStatus);

                String verDate=cursor.getString(cursor.getColumnIndex(VER_DATE));
                verData.setVerDate(verDate);
                String verTime=cursor.getString(cursor.getColumnIndex(VER_TIME));
                verData.setVerTime(verTime);

                String sessionCode=cursor.getString(cursor.getColumnIndex(SESSION_CODE));
                verData.setSessionCode(sessionCode);

                String fingerType=cursor.getString(cursor.getColumnIndex(VER_FINGER_TYPE));
                verData.setFingerType(fingerType);

                verDataArrayList.add(verData);
            } while (cursor.moveToNext());
        }

        return verDataArrayList;

    }

    public void deleteRegisteredData(String date,String venue,
                                  String batch,String classRoom,String studentID){
        dbHelper.deleteData(TABLE_REGISTRATION,
                DBConstant.TEST_DATE + "='" + date + "'"+ " AND " + DBConstant.VENUE_CODE + "='" + venue + "'"
                        + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.CLASS_ROOM + "='" + classRoom + "'"
                        + " AND " + DBConstant.STUDENT_ID + "='" + studentID + "'",
                null);

    }

    public int getUploadedRegDataCountFromStudentTable(String date){
        int uploadedDataCount=0;

        // get student count for whom data is uploaded
      Cursor  cursor = dbHelper.queryData(TABLE_STUDENT, null,
                DBConstant.TEST_DATE + "='" + date + "'" + " AND " + DBConstant.UPLOAD_FLAG_REG + "='" + "1" + "'",
                null, "");
        if(cursor!=null){
            uploadedDataCount=cursor.getCount();
        }

    return uploadedDataCount;
    }

    public int getUploadedVerDataCountFromStudentTable(String date){
        int uploadedDataCount=0;

        // get student count for whom data is uploaded
        Cursor  cursor = dbHelper.queryData(TABLE_STUDENT, new String[]{DBConstant.UPLOAD_FLAG_VER},
                DBConstant.TEST_DATE + "='" + date + "'" + " AND " + DBConstant.UPLOAD_FLAG_VER + "='" + "1" + "'",
                null, "");
        if(cursor!=null){
            uploadedDataCount=cursor.getCount();
        }

        return uploadedDataCount;
    }

//    String DIAG_DATE="DIAG_DATE";
//    String DIAG_TIME="DIAG_TIME";
//    String DIAG_LONGITUDE="DIAG_LONGITUDE";
//    String DIAG_LATITUDE="DIAG_LATITUDE";
//    String DIAG_PHOTOGRAPH="DIAG_PHOTO";
//    String DIAG_FINGER_IMAGE="DIAG_FINGER_IMAGE";
//    String DIAG_FINGER_TEMPLATE="DIAG_FINGER_TEMPLATE";
//    String DIAG_REG_STATUS="DIAG_REG_STATUS";
//    String DIAG_VER_STATUS="DIAG_VER_STATUS";

    public void insertDataToDiagTable(String diag_date,String diag_time,String diag_longi,String diag_lati,
                                      byte[] diag_photo,byte[] diag_f_image,byte[] diag_f_template,String diag_reg_status,
                                      String diag_ver_status ){

        ContentValues cv = new ContentValues();
        cv.put(DIAG_DATE,diag_date);
        cv.put(DIAG_TIME, diag_time);
        cv.put(DIAG_LONGITUDE, diag_longi);
        cv.put(DIAG_LATITUDE, diag_lati);
        cv.put(DIAG_PHOTOGRAPH, diag_photo);
        cv.put(DIAG_FINGER_IMAGE, diag_f_image);
        cv.put(DIAG_FINGER_TEMPLATE, diag_f_template);
        cv.put(DIAG_REG_STATUS, diag_reg_status);
        cv.put(DIAG_VER_STATUS, diag_ver_status);
        dbHelper.insertData(TABLE_DIAGNOSTIC, cv);
    }

    public void deleteProjectDataFromCommanDatabase(String projectId){

        dbHelper.deleteData(TABLE_COMMON,
                DBConstant.PROJECT_CODE + "='" + projectId + "'",
                null);
    }

    public int getVerSuccCountForAllSession(String date,String venue,String batch,String classRoom){
        Cursor cursor = dbHelper.queryData(TABLE_VERIFICATION, null, DBConstant.VENUE_CODE + "='" + venue + "'"
                + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='" + date + "'"+ " AND "
                + DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.VER_STATUS + "='" + "1" + "'", null, "");
        if(cursor!=null){
            return cursor.getCount();
        }else{
            return 0;
        }
    }

    public int getVerFailCountForAllSession(String date,String venue,String batch,String classRoom){
        Cursor cursor = dbHelper.queryData(TABLE_VERIFICATION, null, DBConstant.VENUE_CODE + "='" + venue + "'"
                + " AND " + DBConstant.BATCH_CODE + "='" + batch + "'"+ " AND " + DBConstant.TEST_DATE + "='" + date + "'"+ " AND "
                + DBConstant.CLASS_ROOM + "='" + classRoom + "'"+ " AND " + DBConstant.VER_STATUS + "='" + "-1" + "'", null, "");


        if(cursor!=null){
            return cursor.getCount();
        }else{
            return 0;
        }
    }

}
