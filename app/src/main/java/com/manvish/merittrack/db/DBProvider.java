package com.manvish.merittrack.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;


/**
 * Created by satya on 2/24/2016.
 */
public class DBProvider extends ContentProvider implements DBConstant {

    private static UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(AUTHORITY, TABLE_MASTER_DATA, CODE_MASTER_DATA_TABLE);
        // uriMatcher.addURI(AUTHORITY, TABLE_OPERATOR_DATA, CODE_OPERATOR_DATA_TABLE);
    }

    private SQLiteDatabase getWritableDb() {
//        return DBHelper.getInstance(getContext()).getWritableDatabase();
        return null;
    }

    private SQLiteDatabase getReadableDb() {
//        return DBHelper.getInstance(getContext()).getReadableDatabase();
        return null;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public boolean onCreate() {
//        DBHelper.getInstance(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(getTableName(uri));
        Cursor cursor = queryBuilder.query(getReadableDb(), projection, selection, selectionArgs, null, null, sortOrder);
        if (!cursor.isClosed()) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getWritableDb();
        int uriCode = uriMatcher.match(uri);
        Uri contentUri = null;
        String tableName = getTableName(uri);
        switch (uriCode) {
            case CODE_MASTER_DATA_TABLE:
                contentUri = MASTER_DATA_TABLE_URI;
                break;
//            case CODE_OPERATOR_DATA_TABLE:
//                contentUri = OPERATOR_DATA_TABLE_URI;
//                break;
            default:
                throw new IllegalArgumentException("Unknown Table");
        }
        long rowId = db.insert(tableName, null, values);
        if (rowId > 0) {
            Uri rowUri = ContentUris.withAppendedId(contentUri, rowId);
            getContext().getContentResolver().notifyChange(rowUri, null);
            return rowUri;
        } else {
            throw new IllegalArgumentException("Database insertion failed");
        }

    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int numInserted = 0;
        String table = getTableName(uri);
        SQLiteDatabase sqlDB = getWritableDb();
        sqlDB.beginTransaction();
        numInserted = sqlDB.update(table, values, selection, selectionArgs);
        sqlDB.setTransactionSuccessful();
        getContext().getContentResolver().notifyChange(uri, null);

        sqlDB.endTransaction();
        return numInserted;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        int numInserted = 0;
        String table = getTableName(uri);
        SQLiteDatabase sqlDB = getWritableDb();
        sqlDB.beginTransaction();
        try {
            for (ContentValues cv : values) {
                long newID = sqlDB.insertOrThrow(table, null, cv);
                if (newID <= 0) {
                    throw new SQLException("Failed to insert row into " + uri);
                }
            }
            sqlDB.setTransactionSuccessful();
            getContext().getContentResolver().notifyChange(uri, null);
            numInserted = values.length;
        } catch (Exception e) {
            throw new SQLException("Database operation failed");
        } finally {
            sqlDB.endTransaction();
        }

        return numInserted;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int numberofRwEfferected = 0;
        SQLiteDatabase db = getWritableDb();
        String tableName = getTableName(uri);
        db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + tableName
                + "'");
        numberofRwEfferected = db.delete(tableName, selection, selectionArgs);
        if (numberofRwEfferected > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
//        if(tableName.equals(TABLE_DEMO)){
//
//        }else{
//            if (numberofRwEfferected > 0) {
//                getContext().getContentResolver().notifyChange(uri, null);
//            }
//        }
        return numberofRwEfferected;
    }

    private String getTableName(Uri uri) {
        int uriCode = uriMatcher.match(uri);
        String tableName = null;
        switch (uriCode) {
            case CODE_MASTER_DATA_TABLE:
                tableName = TABLE_MASTER_DATA;
                break;
//            case CODE_OPERATOR_DATA_TABLE:
//                tableName = TABLE_OPERATOR_DATA;
//                break;
            default:
                throw new IllegalArgumentException("Unknown Table");
        }
        return tableName;
    }
}
