package com.manvish.merittrack.db;

import android.net.Uri;

/**
 * Created by satya on 2/24/2016.
 */
public interface DBConstant {
    String DB_NAME = "manvishdb";
    int DB_VERSION = 1;
    String DB_LOG_TAG = "MeritTrack_DB";
    String AUTHORITY = "sathya.android.com.sampleapp.provider";
    String BASE_URI = "content://" + AUTHORITY + "/";

    /**
     * Table common field
     */

    String PRIMARY_KEY = "_id";

    /**
     * demo Table
     */

    // This is for no use ,can remove after concerning SATHYA .
    int CODE_MASTER_DATA_TABLE = 100;
    String TABLE_MASTER_DATA = "master_data";

    String PROJECT_ID = "project_id";
    String VANUE_CODE = "vanue_code";
    String LOGIN_ID = "vanue_id";
    String PASSWORD = "password";

    String CREATE_MASTER_DATA_TABLE = "create table " + TABLE_MASTER_DATA + " ( " + PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , " + PROJECT_ID + " varchar ," + VANUE_CODE
            + " varchar ," + LOGIN_ID + " varchar ," + PASSWORD + " varchar ) ";
    Uri MASTER_DATA_TABLE_URI = Uri.parse(BASE_URI + TABLE_MASTER_DATA);

    /**
     * demo Table
     */


    // Common Database , for storing
     String PROJECT_CODE = "project_code";
     String PROJECT_PASSWORD = "project_password";
     String PROJECT_NAME="project_name";
     String PROJECT_STAGE="project_stage";
     String UPLOAD_FLAG="upload_flag";


     String TABLE_COMMON = "common";
     String CREATE_TABLE_COMMON = "create table " + TABLE_COMMON + " ( " + PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , " + PROJECT_CODE + " varchar ,"
            + PROJECT_PASSWORD + " varchar," + PROJECT_STAGE + " varchar ) ";


    // Creating Tables for MeritTrack

     // Columns for Project Configuration Table
    //  This table is common for all projects ,so everytime we get data ,delete data from this table and again install .

    String TABLE_PROJECT_CONFIG = "project_config";
    String TRANSACTION_ID = "transaction_id";
    String COMPANY_DETAILS = "company_details";
    String LOGO = "logo";
    String THREADSCHEDULING = "thread_scheduling";

    String CREATE_TABLE_PROJECT_CONFIG = "create table " + TABLE_PROJECT_CONFIG + " ( " + PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , " + TRANSACTION_ID + " varchar ,"
            + COMPANY_DETAILS + " varchar ," + LOGO + " varchar ," + THREADSCHEDULING + " varchar) ";


    // This Table contains specific project related data .
    String TABLE_PROJECT_DETAILS = "project_details";
    String FINGER_CONFIG = "finger_config";
    String EXTRA_IMAGES = "extra_images";
    String START_DATE = "start_date";
    String END_DATE = "end_date";
    String CAPTURE_PHOTO_FLAG = "capture_photo_flag"; // comes in Y OR N
    String CAPTURE_IMAGE_FLAG = "capture_image_flag"; // capture image flag
    String NO_VER_SESSION = "no_ver_session"; // capture image flag
    String STAGE = "stage";


    String CREATE_TABLE_PROJECT_DETAILS = "create table " + TABLE_PROJECT_DETAILS + " ( " + PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , " + PROJECT_CODE + " varchar ,"
            + PROJECT_NAME + " varchar ," + PROJECT_PASSWORD + " varchar ," + FINGER_CONFIG + " varchar" +
            "," + EXTRA_IMAGES + " varchar" +
            "," + START_DATE + " varchar" +
            "," + END_DATE + " varchar" +
            "," + CAPTURE_PHOTO_FLAG + " varchar" +
            "," + CAPTURE_IMAGE_FLAG + " varchar" +
            "," + NO_VER_SESSION + " varchar" +
            "," + STAGE + " varchar) ";


    // This table contains ,Verification Session related data ,if that is present in a Project .

    String TABLE_VER_SESSION = "table_ver_session";
    String PROJECT_CODE_SESSION= "project_code_session";
    String VER_SESSION_NO = "no_of_ver_sessions";
    String SESSION_CODE = "session_code";
    String SESSION_DATE = "session_date";

    String CREATE_TABLE_VER_SESSION= "create table " + TABLE_VER_SESSION + " ( " + PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , " + PROJECT_CODE_SESSION + " varchar ,"
            + VER_SESSION_NO + " varchar ," + SESSION_CODE + " varchar ," + SESSION_DATE + " varchar) ";



    // This table contains ,Verification Session related data ,if that is present in a Project .

    String TABLE_PROJECT_VENUES = "project_venues";
    String PROJECT_CODE_VENUE= "project_code_venue";
    String VENUE_CODE = "VENUE_code";
    String VENUE_NAME = "VENUE_NAME";
    String VENUE_NO_OF_STUDENTS = "venue_no_of_students";

    String CREATE_TABLE_PROJECT_VENUES = "create table " + TABLE_PROJECT_VENUES + " ( " + PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , " + PROJECT_CODE_VENUE + " varchar ,"
            + VENUE_CODE + " varchar ," + VENUE_NAME + " varchar ," + VENUE_NO_OF_STUDENTS + " varchar) ";



   // TABLE STUDENT DETAILS


    String TABLE_STUDENT = "student";
    String PROJECT_CODE_STUDENT= "project_code_student";
    String VENUE_CODE_STUDENT = "venue_code_student";
    String STUDENT_ID = "student_id";
    String STUDENT_NAME = "student_name";
    String TEST_DATE="test_date";
    String BATCH_CODE="batch_code";
    String CLASS_ROOM="class_room";
    String PHOTO_GRAPH="photograph"; // if present ,Generally at time of registartion ,we will not get photograph
    String REG_STATUS="reg_status";
    String VER_STATUS="ver_status";
    String UPLOAD_FLAG_REG = "UPLOAD_FLAG_REG";
    String UPLOAD_FLAG_VER = "UPLOAD_FLAG_VER";
    //String  LATIT_UDE="latit_ude";
    //String LONGIT_UDE="longit_ude";

    String CREATE_TABLE_STUDENT = "create table " + TABLE_STUDENT + " ( " + PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , " + PROJECT_CODE_STUDENT + " varchar ,"
            + VENUE_CODE_STUDENT + " varchar ," + STUDENT_ID + " varchar ," + STUDENT_NAME + " varchar" +
            "," + TEST_DATE + " varchar" +
            "," + BATCH_CODE + " varchar" +
            "," + CLASS_ROOM + " varchar ," + PHOTO_GRAPH + " BLOB" +
            "," + REG_STATUS + " varchar" +
            "," + VER_STATUS + " varchar" +
            "," + UPLOAD_FLAG_REG + " varchar" +
            "," + UPLOAD_FLAG_VER + " varchar" +
            "," + SESSION_CODE + " varchar)";

    // Template Table ,Generally this table takes data for councillinng purpose ..

    String TABLE_TEMPLATE = "student_template";
    String TEMPLATE_TYPE="TEMPLATE_TYPE";
    String TEMPLATE_DATA="TEMPLATE_DATA";

    String CREATE_TABLE_TEMPLATE = "create table " + TABLE_TEMPLATE + " ( " + PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , " + PROJECT_CODE + " varchar ,"
            + VENUE_CODE + " varchar ," + STUDENT_ID + " varchar ," + STUDENT_NAME + " varchar" +
            "," + TEST_DATE + " varchar" +
            "," + BATCH_CODE + " varchar" +
            "," + CLASS_ROOM + " varchar ," + TEMPLATE_TYPE + " varchar," + TEMPLATE_DATA + " varchar) ";

    // Registration table ,it stores data after registartion ..

    String TABLE_REGISTRATION ="registration";


    String REGISTRATION_DATE="REGISTRATION_DATE";
    String REGISTRATION_TIME="REGISTRATION_TIME";
    String FINGER_IMAGE="FINGER_IMAGE";



    String CREATE_TABLE_REGISTRATION = "create table " + TABLE_REGISTRATION + " ( " + PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , " + PROJECT_CODE + " varchar ,"
            + VENUE_CODE+ " varchar ," + STUDENT_ID + " varchar ," + STUDENT_NAME + " varchar" +
            "," + TEST_DATE + " varchar" +
            "," + BATCH_CODE + " varchar" +
            "," + CLASS_ROOM + " varchar ," + PHOTO_GRAPH + " varchar" +" varchar ," + FINGER_IMAGE +
            "," + REG_STATUS + " varchar ," + TEMPLATE_TYPE + " varchar," + TEMPLATE_DATA + " varchar," + REGISTRATION_DATE + " varchar," + REGISTRATION_TIME + " varchar," + UPLOAD_FLAG + " varchar)";





    String TABLE_REGISTERED_PHOTOGRAPH ="registered_photograph";

    String CREATE_TABLE_REGISTERED_PHOTOGRAPH = "create table " + TABLE_REGISTERED_PHOTOGRAPH + " ( " + PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , " + PROJECT_CODE + " varchar ,"
            + VENUE_CODE+ " varchar ," + STUDENT_ID + " varchar ," + STUDENT_NAME + " varchar" +
            "," + TEST_DATE + " varchar" +
            "," + BATCH_CODE + " varchar" +
            "," + CLASS_ROOM + " varchar ," + PHOTO_GRAPH + " varchar ," + REGISTRATION_DATE + " varchar," + REGISTRATION_TIME + " varchar," + UPLOAD_FLAG + " varchar) ";

    String TABLE_VERIFICATION ="verification";
    String VER_DATE="REGISTRATION_DATE";
    String VER_TIME="VER_TIME";
    String VER_SESSION_CODE="SESSION_CODE";
    String VER_SESSION_DATE="SESSION_DATE";
    String VER_FINGER_TYPE="VER_FINGER_TYPE";

    String CREATE_TABLE_VERIFICATION = "create table " + TABLE_VERIFICATION + " ( " + PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , " + PROJECT_CODE + " varchar ,"
            + VENUE_CODE+ " varchar ," + STUDENT_ID + " varchar ," + STUDENT_NAME + " varchar" +
            "," + TEST_DATE + " varchar" +
            "," + BATCH_CODE + " varchar" +
            "," + CLASS_ROOM + " varchar"  +
            "," + VER_STATUS + " varchar ," + VER_DATE + " varchar,"+ VER_FINGER_TYPE + " varchar," + VER_TIME + " varchar," + SESSION_CODE + " varchar," + SESSION_DATE
            + " varchar ," + UPLOAD_FLAG + " varchar) ";

    String TABLE_DIAGNOSTIC ="diagnostic";
    String DIAG_DATE="DIAG_DATE";
    String DIAG_TIME="DIAG_TIME";
    String DIAG_LONGITUDE="DIAG_LONGITUDE";
    String DIAG_LATITUDE="DIAG_LATITUDE";
    String DIAG_PHOTOGRAPH="DIAG_PHOTO";
    String DIAG_FINGER_IMAGE="DIAG_FINGER_IMAGE";
    String DIAG_FINGER_TEMPLATE="DIAG_FINGER_TEMPLATE";
    String DIAG_REG_STATUS="DIAG_REG_STATUS";
    String DIAG_VER_STATUS="DIAG_VER_STATUS";


    String CREATE_TABLE_DIAGNOSTIC = "create table " + TABLE_DIAGNOSTIC + " ( " + PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , " + DIAG_DATE + " varchar ,"
            + DIAG_TIME+ " varchar ," + DIAG_LONGITUDE + " varchar ," + DIAG_LATITUDE + " varchar" +
            "," + DIAG_PHOTOGRAPH + " varchar" +
            "," + DIAG_FINGER_IMAGE + " varchar" +
            "," + DIAG_FINGER_TEMPLATE + " varchar"  +
            "," + DIAG_REG_STATUS + " varchar ," + DIAG_VER_STATUS + " varchar,"+ UPLOAD_FLAG + " varchar) ";

}
