package com.manvish.merittrack.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.os.Environment;

import com.manvish.merittrack.Model.Student;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by satya on 2/24/2016.
 */
public class DBHelper extends SQLiteOpenHelper implements DBConstant {

    private static DBHelper helper;
    SQLiteDatabase db;

   /* Environment.getExternalStorageDirectory()
            + File.separator + FILE_DIR
    + File.separator + DATABASE_NAME*/

    public DBHelper(Context context, String dbName, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, Environment.getExternalStorageDirectory()
                + File.separator + "MeritTrack"
                + File.separator + dbName, null, version);
    }

    public static DBHelper getInstance(Context context, String dbName, SQLiteDatabase.CursorFactory factory, int version) {
        if (null == helper) {
            helper = new DBHelper(context, dbName, null, version);
        }
        return helper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        db.execSQL(CREATE_TABLE_PROJECT_CONFIG);
        db.execSQL(CREATE_TABLE_PROJECT_DETAILS);
        db.execSQL(CREATE_TABLE_VER_SESSION);//This is for specific projects
        db.execSQL(CREATE_TABLE_PROJECT_VENUES);
        db.execSQL(CREATE_TABLE_STUDENT);
        db.execSQL(CREATE_TABLE_TEMPLATE);
        db.execSQL(CREATE_TABLE_REGISTRATION);

        db.execSQL(CREATE_TABLE_COMMON);
        db.execSQL(CREATE_TABLE_VERIFICATION);
        db.execSQL(CREATE_TABLE_REGISTERED_PHOTOGRAPH);
        db.execSQL(CREATE_TABLE_DIAGNOSTIC);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " +  CREATE_TABLE_PROJECT_CONFIG);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_PROJECT_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_VER_SESSION);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_PROJECT_VENUES);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_STUDENT);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_TEMPLATE);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_REGISTRATION);

        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_COMMON);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_VERIFICATION);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_DIAGNOSTIC);
        onCreate(db);
    }

    public void insertData(String tableName, ContentValues values) {
        SQLiteDatabase db = this.getWritableDatabase();
        long rowId = db.insert(tableName, null, values);
        db.close();
    }

    public void insertStudentAndTemplateData(String tableName, ContentValues values) {
        if(db!=null && db.isOpen()){

        }else{
            db = this.getWritableDatabase();
        }
        long rowId = db.insert(tableName, null, values);
        //db.close();
    }


    public void insertStudentBulkData(String projectCode,String venueCode ,ArrayList<Student> studentArrayList) {
        String sql = "INSERT INTO " + DBHelper.TABLE_STUDENT
                + " VALUES (?,?,?,?,?,?,?,?,?,?,?);";


//        String PROJECT_CODE_STUDENT= "project_code_student";
//        String VENUE_CODE_STUDENT = "venue_code_student";
//        String STUDENT_ID = "student_id";
//        String STUDENT_NAME = "student_name";
//        String TEST_DATE="test_date";
//        String BATCH_CODE="batch_code";
//        String CLASS_ROOM="class_room";
//        String PHOTO_GRAPH="photograph"; // if present ,Generally at time of registartion ,we will not get photograph
//        String REG_STATUS="reg_status";
//        String VER_STATUS="ver_status";

        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteStatement statement = db.compileStatement(sql);
        try {

            db.beginTransaction();
            for (int idx = 0; idx < studentArrayList.size(); idx++) {
                statement.clearBindings();
                statement.bindDouble(1, idx);
                statement.bindString(2, projectCode);
                statement.bindString(3, venueCode);
                statement.bindString(4, studentArrayList.get(idx).getStudentID());
                statement.bindString(5, studentArrayList.get(idx).getName());
                statement.bindString(6, studentArrayList.get(idx).getTestDate());
                statement.bindString(7, studentArrayList.get(idx).getBatchCode());
                statement.bindString(8, studentArrayList.get(idx).getClassRoom());
                statement.bindString(9, studentArrayList.get(idx).getPhotograph());
                statement.bindString(10, "0");
                statement.bindString(11, "0");
                statement.execute();

            }

        }catch(Exception e){
            e.printStackTrace();
        }finally {
            db.setTransactionSuccessful();
            db.endTransaction();
            //db.close();
        }
    }

    public Cursor queryData(String tableName, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(tableName, projection, selection, selectionArgs, null, null, sortOrder);
        return cursor;
    }

    public Cursor queryData(boolean distinct, String table,
                            String[] columns, String selection,
                            String[] selectionArgs, String groupBy,
                            String having, String orderBy, String limit) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(distinct, table, columns, selection, selectionArgs, groupBy, having,orderBy,limit);
        return cursor;
    }

    public int deleteData(String tableName, String selection, String[] selectionArgs) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(tableName, selection, selectionArgs);
    }

    public int updateData(String tableName, ContentValues values, String selection, String[] selectionArgs) {
        int numInsrted = 0;
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        numInsrted = sqlDB.update(tableName, values, selection, selectionArgs);
        return numInsrted;
    }

}
