package com.manvish.merittrack.utils;

import android.content.Context;
import android.text.TextUtils;

import com.manvish.merittrack.Model.Config;
import com.manvish.merittrack.Model.Flush;
import com.manvish.merittrack.Model.FlushProjectCode;
import com.manvish.merittrack.Model.Project;
import com.manvish.merittrack.Model.Root;
import com.manvish.merittrack.Model.Student;
import com.manvish.merittrack.Model.Venue;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;


/**
 * Created by pabitra on 3/9/16.
 */
public class ParseXml {


    Root rootModel;
    Config config;
    Project project;
    Flush flush;
    FlushProjectCode flushProjectCode;
    Venue venue;
    Student student;
    Student.StudentTemplate template;

    ArrayList<Venue> venueList = new ArrayList<Venue>();
    ArrayList<Student> studentList = new ArrayList<Student>();
    ArrayList<Student.StudentTemplate> templateList;
    ArrayList<FlushProjectCode> flushProjectCodeArrayList;
    Project.Session session;

    LinkedList<Project.Session> versessionList;
    String text;

    Context mContext;

    public ParseXml(Context mContext) {
        this.mContext = mContext;
    }

    public Root parse(InputStream is) {
        XmlPullParserFactory factory = null;
        XmlPullParser parser = null;
        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            parser = factory.newPullParser();

            parser.setInput(is, null);

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = parser.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagname.equalsIgnoreCase("root")) {
                            // create a new instance of employee
                            rootModel = new Root();
                        }

                        if (tagname.equalsIgnoreCase("config")) {
                            // create a new instance of employee
                            config = new Config();
                        }

                        if (tagname.equalsIgnoreCase("project")) {
                            // create a new instance of employee
                            project = new Project();
                        }

                        if (tagname.equalsIgnoreCase("transactionId")) {
                            rootModel.setTransactionID(parser.nextText());

                        }

                        if (tagname.equalsIgnoreCase("Flush")) {
                            flush=new Flush();
                            flushProjectCodeArrayList=new ArrayList<>();

                        }

                        if(tagname.equalsIgnoreCase("flushCode")){
                            flushProjectCode=new FlushProjectCode();
                            flushProjectCode.setProjectCode(parser.nextText());

                            flushProjectCodeArrayList.add(flushProjectCode);
                        }
                        // companyDetails
                        if (tagname.equalsIgnoreCase("companyDetails")) {
                            config.setCompanyDetails(parser.nextText());
                        }
                        // logo
                        if (tagname.equalsIgnoreCase("logo")) {
                            config.setLogo(parser.nextText());
                        }
                        // threadScheduling
                        if (tagname.equalsIgnoreCase("threadScheduling")) {
                            config.setThreadScheduling(parser.nextText());
                        }

                        if (tagname.equalsIgnoreCase("projCode")) {
                            project.setProjectCode(parser.nextText());
                        }
                        // // adminLogin

                        if (tagname.equalsIgnoreCase("ProjectName")) {
                            project.setProjectName(parser.nextText());
                        }
                        if (tagname.equalsIgnoreCase("projectPassword")) {
                            project.setProjectPassword(parser.nextText());
                        }
                        if (tagname.equalsIgnoreCase("fingConfig")) {
                            project.setFingerConfig(parser.nextText());
                        }
                        if (tagname.equalsIgnoreCase("extraImages")) {
                            project.setExtraImages(parser.nextText());
                        }
                        if (tagname.equalsIgnoreCase("startDate")) {
                            project.setStartDate(parser.nextText());
                        }
                        if (tagname.equalsIgnoreCase("enddate")) {
                            project.setEndDate(parser.nextText());
                        }
                        if (tagname.equalsIgnoreCase("capturephoto")) {

                            if(TextUtils.isEmpty(parser.nextText())) {
                                project.setCapturePhoto(parser.nextText());
                            }else{
                                project.setCapturePhoto(" ");
                            }
                        }
                        if (tagname.equalsIgnoreCase("captureimage")) {
                            project.setCaptureImage(parser.nextText());
                        }

                        if (tagname.equalsIgnoreCase("Stage")) {
                            project.setStage(parser.nextText());
                        }
                        if (tagname.equalsIgnoreCase("verSessions")) {
                            project.setNoOfVerSession(parser.nextText());
                        }

                        if (tagname.equalsIgnoreCase("listOfVerSessions")) {

                            versessionList=new LinkedList<>();

                        }
                        if(tagname.equalsIgnoreCase("Session")){
                            session =new Project().new Session();
                        }
                        else if(session!=null){

                            if(tagname.equalsIgnoreCase("code")){
                                session.setSessionCode(parser.nextText());
                            }

                            if(tagname.equalsIgnoreCase("date")){
                                session.setDate(parser.nextText());
                            }
                        }


                        // VenueParse

                        if (tagname.equalsIgnoreCase("Venue")) {
                            venue = new Venue();
                        } else if (venue != null) {
                            if (tagname.equalsIgnoreCase("venueCode")) {
                                venue.setVenueCode(parser.nextText());
                            }


                            if (tagname.equalsIgnoreCase("noOfStudents")) {
                                venue.setNoOfStudents(parser.nextText());
                            }
                            if (tagname.equalsIgnoreCase("venueName")) {
                                venue.setVenueName(parser.nextText());
                            }

                        }

                        //iniitialize studentlist here
                        if (tagname.equalsIgnoreCase("StudentDetails")) {

                            studentList = new ArrayList<Student>();
                        }

                        // For student
                        if (tagname.equalsIgnoreCase("student")) {
                            student = new Student();
                            templateList = new ArrayList<Student.StudentTemplate>();

                        } else if (student != null) {

                            if (tagname.equalsIgnoreCase("StudentId")) {

                                student.setStudentID(parser.nextText());

                            } else if (tagname.equalsIgnoreCase("name")) {
                                student.setName(parser.nextText());
                            } else if (tagname.equalsIgnoreCase("testdate")) {
                                student.setTestDate(parser.nextText());
                            } else if (tagname.equalsIgnoreCase("batchCode")) {
                                student.setBatchCode(parser.nextText());
                            } else if (tagname.equalsIgnoreCase("classRoom")) {
                                student.setClassRoom(parser.nextText());
                            } else if (tagname.equalsIgnoreCase("photograph")) {
                                student.setPhotograph(parser.nextText());
                            }

                            if (tagname.equalsIgnoreCase("template")) {
                                template = new Student().new StudentTemplate();
                            } else if (template != null) {
                                if (tagname.equalsIgnoreCase("type")) {
                                    template.setType(parser.nextText());
                                }

                                if (tagname.equalsIgnoreCase("tData")) {
                                    template.setData(parser.nextText());
                                }

                            }
                        }

                        // TemplateParse ========



                        break;

                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;

                    case XmlPullParser.END_TAG:

                        if (tagname.equalsIgnoreCase("Flush")) {
                            flush.setFlushProjectCodeList(flushProjectCodeArrayList);

                        }

                        if (tagname.equalsIgnoreCase("Session")) {
                            versessionList.add(session);

                        }

                        if (tagname.equalsIgnoreCase("template")) {
                            templateList.add(template);

                        }

                        if (tagname.equalsIgnoreCase("student")) {
                            student.setStudentTemplateList(templateList);

                            studentList.add(student);
                        }

                        if (tagname.equalsIgnoreCase("Venue")) {

                            venue.setStudentList(studentList);
                            venueList.add(venue);
                        }

                        if (tagname.equalsIgnoreCase("Project")) {
                            project.setLisOfVerSession(versessionList);
                            project.setVenueList(venueList);
                        }

                        if (tagname.equalsIgnoreCase("root")) {
                            // add employee object to list
                            rootModel.setFlush(flush);
                            rootModel.setConfig(config);
                            rootModel.setProject(project);
                        }

                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return rootModel;
    }


}
