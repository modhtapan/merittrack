package com.manvish.merittrack.utils;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.usb.UsbManager;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.manvish.merittrack.Model.Config;
import com.manvish.merittrack.Model.Flush;
import com.manvish.merittrack.Model.FlushProjectCode;
import com.manvish.merittrack.Model.Project;
import com.manvish.merittrack.Model.Student;
import com.manvish.merittrack.Model.Venue;
import com.manvish.merittrack.R;
import com.manvish.merittrack.activities.CameraActivity;
import com.manvish.merittrack.activities.MainActivity;
import com.manvish.merittrack.activities.RegVerButtonActivity;
import com.manvish.merittrack.activities.SelectDateActivity;
import com.manvish.merittrack.activities.VenueActivity;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.http.ShellInterface;
import com.manvish.merittrack.view.ManvishAlertDialog;
import com.morpho.morphosmart.sdk.ErrorCodes;

import org.apache.commons.io.FileUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


public class ManvishCommonUtil {


   private static String zip_password="manvish1234";
   private static String originalFilePath=null;

    public ManvishCommonUtil() {

    }

    public static String getSerialNo() {
        String serialNo = "";

        serialNo = android.os.Build.SERIAL;

        return serialNo;
    }


    public static void deleteTable(Class c) {

        new Delete().from(c).execute();
    }

    public static void makeTrueFullScreen(Activity avt) {
        avt.getWindow()
                .getDecorView()
                .setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    public static String getModelNo() {
        return android.os.Build.MODEL;
    }


    public static boolean isWifiOn(Context context) {
        WifiManager wifiManager = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        return wifiManager.isWifiEnabled();
    }

    public static final String http = "http://";
    String portNumber = ":9095";
    public static final String Service = "/4001/";

    public static String getBaseURL(String ip, String portNo) {

        portNo = ":" + portNo;
        String baseURL = http + ip + portNo + Service;
        return baseURL;
    }

    public static String getCurrentDate() {
        // String lCurrentDate = "";
        Date now = new Date();
        int hours = now.getHours();
        hours = (hours > 12) ? hours - 12 : hours;
        int minutes = now.getMinutes();
        int h1 = hours / 10;
        int h2 = hours - h1 * 10;
        int m1 = minutes / 10;
        int m2 = minutes - m1 * 10;

        Calendar calendar = Calendar.getInstance();
        Log.d("hi", String.format("%d:%d -> %d %d: %d %d", hours, minutes, h1,
                h2, m1, m2));
        Date date = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        return dateFormat.format(date);
    }

    public static String getCurrentTime() {
        // String lCurrentDate = "";
        Date now = new Date();
        int hours = now.getHours();
        hours = (hours > 12) ? hours - 12 : hours;
        int minutes = now.getMinutes();
        int h1 = hours / 10;
        int h2 = hours - h1 * 10;
        int m1 = minutes / 10;
        int m2 = minutes - m1 * 10;

        Calendar calendar = Calendar.getInstance();
        Log.d("hi", String.format("%d:%d -> %d %d: %d %d", hours, minutes, h1,
                h2, m1, m2));
        Date date = calendar.getTime();
        DateFormat homeDateFormat = new SimpleDateFormat("HH:mm:ss");

        return homeDateFormat.format(date);
    }

    public static String getCurrentDateTime() {

        String currentDateTime = null;
        Date now = new Date();
        int hours = now.getHours();
        hours = (hours > 12) ? hours - 12 : hours;
        int minutes = now.getMinutes();
        int h1 = hours / 10;
        int h2 = hours - h1 * 10;
        int m1 = minutes / 10;
        int m2 = minutes - m1 * 10;

        Calendar calendar = Calendar.getInstance();
        Log.d("hi", String.format("%d:%d -> %d %d: %d %d", hours, minutes, h1,
                h2, m1, m2));
        Date date = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return dateFormat.format(date);
    }


    public static void showEnterPasswordDialog(final Activity context, final String eventCode, final String password) {
        final Dialog dialog = new Dialog(context);
        Log.d("Password", "pwd:" + password);
        ManvishPrefConstants.PROJECT_PASSWORD.write(password);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.refresh_dialog);
        dialog.setCancelable(false);
        final EditText etAccessCode = (EditText) dialog
                .findViewById(R.id.et_access_code);
        Button btnAccess = (Button) dialog.findViewById(R.id.btnAccess);
        Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });

        btnAccess.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d("Password ::", password);//93368868

                if (etAccessCode.getText().toString()
                        .equalsIgnoreCase(password)) {


                    if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("1") ||
                            ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.PRETEST)) {

                        String dbName = eventCode.trim()+ "T" + ".db";
                        ManvishPrefConstants.DB_NAME.write(dbName);
                        MeritTrackApplication.getInstance().setDbName(dbName);

                    } else {
                        String dbName = eventCode.trim()+ "C" + ".db";
                        ManvishPrefConstants.DB_NAME.write(dbName);
                        MeritTrackApplication.getInstance().setDbName(dbName);
                    }
                    new AsyncTask<Void, Void, Boolean>() {
                        protected void onPreExecute() {
                            ManvishPrefConstants.SELECTED_EVENT_CODE.write(eventCode);

                        }

                        @Override
                        protected Boolean doInBackground(Void... params) {

                            DBAdapter dbAdapter = DBAdapter.getInstance(context, MeritTrackApplication.getInstance().getDbName(), null, 1);

                            Project project = dbAdapter.getProjectDetails();

                            if(project!=null) {
                                ManvishPrefConstants.FINGERCONFIG.write(project.getFingerConfig());
                                ManvishPrefConstants.EXTRAIMAGES.write(project.getExtraImages());
                                ManvishPrefConstants.STAGE.write(project.getStage());

                                Config config = dbAdapter.getConfigData();
                                if(config!=null){
                                    ManvishPrefConstants.THREAD_SCHEDULING.write(config.getThreadScheduling());
                                    ManvishPrefConstants.COMPANY_LOGO.write(config.getLogo());
                                    ManvishPrefConstants.COMPANY_NAME.write(config.getCompanyDetails());
                                }

                            }
                            return true;

                        }

                        @Override
                        protected void onPostExecute(Boolean success) {


                            // go to

                            if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.POSTTEST)
                                    || ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.POSTCOUNCIL) || ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.BACKUP_USB_POSTTEST)
                                    || ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.BACKUP_USB_POSTCOUNCILING)) {

                                Intent intent = new Intent(context, SelectDateActivity.class);
                                context.startActivity(intent);
                            } else {
                                Intent intent = new Intent(context, RegVerButtonActivity.class);
                                context.startActivity(intent);
                            }

                            dialog.dismiss();

                        }

                    }.execute();
                } else {
                    // Toast.makeText(getApplicationContext(),
                    // "Incorrect access code", Toast.LENGTH_LONG).show();
                    new ManvishAlertDialog(context,
                            "Wrong access code",
                            "Please enter correct access code")
                            .showAlertDialog();
                }

            }
        });

        dialog.show();

    }


    public static ProgressDialog showProgressDialog(Context context, String msg) {

        ProgressDialog mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();

        return mProgressDialog;
    }

    public static void dismissProgressDialog(ProgressDialog mProgressDialog) {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    // copy source file to desination file
    public static boolean copyFile(File sourceFile, File destinationFile) {
        if (sourceFile.exists()) {
            try {
                InputStream in = new FileInputStream(sourceFile);
                OutputStream out = new FileOutputStream(destinationFile);

                // Copy the bits from instream to outstream
                byte[] buf = new byte[1024];
                int len;

                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }

                in.close();
                out.close();

                return true;
            } catch (Exception e) {
                System.out.println("Exception Copy:"+e.toString());
                return false;
            }

        } else {
            System.out.println("Exception Copy: Source not exists");
            return false;
        }
    }


    public static void zip(String[] _files, String zipFileName) {
        try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(zipFileName);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
                    dest));
            byte data[] = new byte[4096];

            for (int i = 0; i < _files.length; i++) {
                Log.v("Compress", "Adding: " + _files[i]);
                FileInputStream fi = new FileInputStream(_files[i]);
                origin = new BufferedInputStream(fi, 4096);

                ZipEntry entry = new ZipEntry(_files[i].substring(_files[i].lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;

                while ((count = origin.read(data, 0, 4096)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* public static void zipFileWithPassword(String[] originalFilePath, String zipFilePath) {
        //originalFilePath=Environment.getExternalStorageDirectory()+"/MeritTrack/Upload/"+ManvishPrefConstants.SERIAL_NO.read()+"_"+ManvishPrefConstants.SELECTED_EVENT_CODE.read()+"_"+ManvishPrefConstants.SELECTED_DATE.read() + "_PT.zip";
       // zipFilePath=Environment.getExternalStorageDirectory()+"/MeritTrack/Upload/"+ManvishPrefConstants.SERIAL_NO.read()+"_"+ManvishPrefConstants.SELECTED_EVENT_CODE.read()+"_"+ManvishPrefConstants.SELECTED_DATE.read() + "_PT.zip";

        // String zip_password= null;
        try {
            ZipFile zipFile = new ZipFile(zipFilePath);
            if(zipFile.isEncrypted()){
                zipFile.setPassword(zip_password);
            }
            zipFile.extractAll(String.valueOf(originalFilePath));
        }catch(ZipException e){
            e.printStackTrace();
        }



    }*/

    //method globally declared for home button to goto main activity
    public static void goToMainactivity(Activity avt) {
        Intent intent = new Intent(avt, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        avt.startActivity(intent);
        avt.finish();
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap retVal;

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

        return retVal;
    }

    private static final int SHORT_DELAY = 1000; // 2 seconds
    public static void showCustomToast(Activity avt, String msg) {

        LayoutInflater inflater = avt.getLayoutInflater();

        View customToastroot = inflater.inflate(R.layout.mycustom_toast, null);

        TextView tvToastMsg = (TextView) customToastroot.findViewById(R.id.textView1);
        tvToastMsg.setText(msg);

        Toast customtoast = new Toast(avt);

        customtoast.setView(customToastroot);
        customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        customtoast.setDuration(Toast.LENGTH_SHORT);
        customtoast.show();

    }

    public static void showCustomToastForNetwork(Activity avt, String msg) {

        LayoutInflater inflater = avt.getLayoutInflater();

        View customToastroot = inflater.inflate(R.layout.mycustom_toast, null);

        TextView tvToastMsg = (TextView) customToastroot.findViewById(R.id.textView1);
        tvToastMsg.setText(msg);

        Toast customtoast = new Toast(avt);

        customtoast.setView(customToastroot);
        customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        customtoast.setDuration(Toast.LENGTH_LONG);
        customtoast.show();

    }

    public static void showCustomToastForBackupPosttestandPostCouncil(Activity avt, String msg) {

        //int SHORT_DELAY = 2000;
        LayoutInflater inflater = avt.getLayoutInflater();

        View customToastroot = inflater.inflate(R.layout.mycustom_toast, null);

        TextView tvToastMsg = (TextView) customToastroot.findViewById(R.id.textView1);
        tvToastMsg.setText(msg);

        Toast customtoast = new Toast(avt);

        customtoast.setView(customToastroot);
        customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        customtoast.setDuration(Toast.LENGTH_SHORT);
        customtoast.show();

    }

    /**
     * set server time to device
     *
     * @param time
     */
    public static void setTime(long time) {
        if (ShellInterface.isSuAvailable()) {
            ShellInterface.runCommand("chmod 666 /dev/alarm");
            SystemClock.setCurrentTimeMillis(time);
            ShellInterface.runCommand("chmod 664 /dev/alarm");
        }
    }

    public static void showStudentDetails(final Student std, final Activity avt) {
        // show photograph ,ID and student Name before going for registration
        final Dialog dialog = new Dialog(avt);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_student_details);
        dialog.setCancelable(false);

        TextView tvStudentID, tvStudentName;
        Button btnCancel, btnOk;
        ImageView mIvStdPic;
        btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btnOk = (Button) dialog.findViewById(R.id.btn_ok);
        tvStudentID = (TextView) dialog.findViewById(R.id.tv_student_id);
        tvStudentName = (TextView) dialog.findViewById(R.id.tv_student_name);
        mIvStdPic = (ImageView) dialog.findViewById(R.id.iv_student_pic);
        DBAdapter dbAdapter=DBAdapter.getInstance(avt, MeritTrackApplication.getInstance().getDbName(), null, 1);
        String photograph=dbAdapter.getStudentPhotograph(ManvishPrefConstants.SELECTED_DATE.read(),
                ManvishPrefConstants.SELECTED_VENUE.read(),
                ManvishPrefConstants.SELECTED_BATCH.read(),
                ManvishPrefConstants.SELECTED_CLASS.read(),
                std.getStudentID());
        if (!TextUtils.isEmpty(photograph)) {

            try {
                byte[] decodedString = Base64.decode(photograph, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory
                        .decodeByteArray(decodedString, 0,
                                decodedString.length);
                mIvStdPic.setImageBitmap(decodedByte);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        tvStudentID.setText(std.getStudentID());
        tvStudentName.setText(std.getName());

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent cameraIntent = new Intent(avt, CameraActivity.class);
                cameraIntent.putExtra(StringConstants.KEY_STUDENT_ID, std.getStudentID());
                avt.startActivity(cameraIntent);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public static void showEnterPasswordToActivateAfterUpload(final Activity context, final String password) {
        final Dialog dialog = new Dialog(context);
        Log.d("Password", "pwd:" + password);
        ManvishPrefConstants.PROJECT_PASSWORD.write(password);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.refresh_dialog);
        dialog.setCancelable(false);
        final EditText etAccessCode = (EditText) dialog
                .findViewById(R.id.et_access_code);
        Button btnAccess = (Button) dialog.findViewById(R.id.btnAccess);
        Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });

        btnAccess.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d("Password ::", password);//93368868
                dialog.dismiss();
                if (etAccessCode.getText().toString()
                        .equalsIgnoreCase(password)) {
                    // Go to Venue Activity
                    Intent venueIntent = new Intent(context, VenueActivity.class);
                    context.startActivity(venueIntent);

                } else {

                    new ManvishAlertDialog(context,
                            "Wrong access code",
                            "Please enter correct access code")
                            .showAlertDialog();
                }

            }
        });

        dialog.show();

    }


    public static String getDateCurrentTimeZone(long timestamp) {
        try {
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date currenTimeZone = (Date) calendar.getTime();
            return sdf.format(currenTimeZone);
        } catch (Exception e) {
        }
        return "";
    }

    public static byte[] rotateByteArrayImage(byte[] originalImageByte, int angle) {

        // byte[] rotatedImage=null;

        Bitmap bmp = BitmapFactory.decodeByteArray(originalImageByte, 0, originalImageByte.length);

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        bmp = Bitmap.createBitmap(bmp, 0, 0,
                bmp.getWidth(), bmp.getHeight(), matrix, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 60, stream);
        byte[] flippedImageByteArray = stream.toByteArray();

        return flippedImageByteArray;
    }

    public static boolean isSystemApp(String packageName, Context context) {
        try {
            // Get packageinfo for target application
            PackageManager mPackageManager = context.getPackageManager();
            PackageInfo targetPkgInfo = mPackageManager.getPackageInfo(
                    packageName, PackageManager.GET_SIGNATURES);
            // Get packageinfo for system package
            PackageInfo sys = mPackageManager.getPackageInfo(
                    "android", PackageManager.GET_SIGNATURES);
            // Match both packageinfo for there signatures
            return (targetPkgInfo != null && targetPkgInfo.signatures != null && sys.signatures[0]
                    .equals(targetPkgInfo.signatures[0]));
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static Bitmap getBitmapFromRawImage(byte[] rawImage) {

        byte[] Bits = new byte[rawImage.length * 4];

        int i;
        for (i = 0; i < rawImage.length; i++) {
            Bits[i * 4] = Bits[i * 4 + 1] = Bits[i * 4 + 2] = rawImage[i];

            // Invert the source bits
            Bits[i * 4 + 3] = -1;
        }

        Bitmap bm = Bitmap.createBitmap(256, 400, Bitmap.Config.ALPHA_8);
        bm.copyPixelsFromBuffer(ByteBuffer.wrap(Bits));

        return bm;

    }


    public static byte[] getByteFImageWithHeader(byte[] rawImage) {

        byte[] Bits = new byte[rawImage.length * 4];

        int i;
        for (i = 0; i < rawImage.length; i++) {
            Bits[i * 4] = Bits[i * 4 + 1] = Bits[i * 4 + 2] = rawImage[i];

            // Invert the source bits
            Bits[i * 4 + 3] = -1;
        }

        return Bits;

    }

    public static boolean isUSBConnected(Context context) {

        Intent intent = ((Activity) context).getIntent();
        if (intent != null) {
            Log.d("onResume", "intent: " + intent.toString());
            Log.e("onResume", "actiob: " +intent.getAction());
            if (intent.getAction()!=null && intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static boolean isExternalStorageAvailable() {

        String state = Environment.getExternalStorageState();
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Something else is wrong. It may be one of many other states, but
            // all we need
            // to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }

        if (mExternalStorageAvailable == true
                && mExternalStorageWriteable == true) {
            return true;
        } else {
            return false;
        }
    }


    public static boolean hasStorage(boolean requireWriteAccess) {
        //TODO: After fix the bug,  add "if (VERBOSE)" before logging errors.
        String state = Environment.getExternalStorageState();
        Log.v("has", "storage state is " + state);

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            if (requireWriteAccess) {
                boolean writable = checkFsWritable();
                Log.v("has", "storage writable is " + writable);
                return writable;
            } else {
                return true;
            }
        } else if (!requireWriteAccess && Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    private static boolean checkFsWritable() {
        // Create a temporary file to see whether a volume is really writeable.
        // It's important not to put it in the root directory which may have a
        // limit on the number of files.
        String directoryName = Environment.getExternalStorageDirectory().toString() + "/DCIM";
        File directory = new File(directoryName);
        if (!directory.isDirectory()) {
            if (!directory.mkdirs()) {
                return false;
            }
        }
        return directory.canWrite();
    }


    public static void flushData(Flush flush,Context mContext){
        ArrayList<FlushProjectCode> flushProjectCodeList = flush.getFlushProjectCodeList();

        if (flushProjectCodeList.size() > 0) {
            DBAdapter dbAdapterCommon = DBAdapter.getInstance(mContext, "common.db", null, 1); // Common is the common databse name
            for (FlushProjectCode flushProjectCode : flushProjectCodeList) {

                String projectCodeToFlush = flushProjectCode.getProjectCode();
                dbAdapterCommon.deleteProjectDataFromCommanDatabase(projectCodeToFlush);
                ArrayList<Venue> venueList = null;

               /* if (project != null) {
                    venueList = project.getVenueList();
                }*/

               /* if (venueList != null && venueList.size() > 0) {
                    // Do not consider flush //  Means they are sending some data to us
                } else {
*/
                    try {
                        // delete the directory from SD-CARD
                        try {
                            FileUtils.deleteDirectory(new File("/mnt/sdcard2/MeritTracSDBackUp/" + projectCodeToFlush));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // else delete the project
                        File flushFile1 = new File(Environment.getExternalStorageDirectory()
                                + File.separator + "MeritTrack"
                                + File.separator + projectCodeToFlush + "T" + ".db");

                        File flushFile2 = new File(Environment.getExternalStorageDirectory()
                                + File.separator + "MeritTrack"
                                + File.separator + projectCodeToFlush + "C" + ".db");
                        // Delete project Related Data from Upload folder and Offiine Upload folder .
                        //String  flushFile3 = Environment.getExternalStorageDirectory()+"/MeritTrack/Upload/"+ManvishPrefConstants.SERIAL_NO.read()+"_"+ManvishPrefConstants.SELECTED_EVENT_CODE.read()+"_"+ManvishPrefConstants.SELECTED_DATE.read() + "_PT.zip";
                        //File flushFile3 =new File(filepath);

                        File f = new File(Environment.getExternalStorageDirectory()+"/MeritTrack/Upload/");
                        File file[] = f.listFiles();

                        if (file != null && file.length != 0) {

                            for (int i = 0; i < file.length; i++) {

                               File uploadFile=file[i];
                                String uploadFileName=uploadFile.getName();
                                if(uploadFileName.contains("flushProjectCode")){

                                    try {
                                        uploadFile.delete();
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }


                                }
                            }
                        }

                        File f1=new File(Environment.getExternalStorageDirectory()+"/MeritTrack/Upload_Offline/");
                        File file1[] = f1.listFiles();

                        if (file1 != null && file1.length != 0) {

                            for (int i = 0; i < file1.length; i++) {

                                File uploadFile1=file1[i];
                                String uploadFileName1=uploadFile1.getName();
                                if(uploadFileName1.contains("flushProjectCode")){

                                    try {
                                        uploadFile1.delete();
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }


                                }
                            }
                        }




                        if (flushFile1.exists()) {
                            flushFile1.delete();
                        }

                        if (flushFile2.exists()) {
                            flushFile2.delete();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
           // }
            dbAdapterCommon.closeDatabase();
        }
    }

    public static void  deleteExistingProjectData(String projectId,Context mContext) {

        if (projectId != null) {
           // String projectId = project.getProjectCode();
            DBAdapter dbAdapter = DBAdapter.getInstance(mContext, "common.db", null, 1); // Common is the common databse name
           // Delete project data for the ,same project if exists
            dbAdapter.deleteProjectDataFromCommanDatabase(projectId);

            // Delete  project Databse files  ,same project if exists
                File projectData1 = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "MeritTrack"
                        + File.separator + projectId + "T" + ".db");

                if (projectData1.exists()) {
                    // take backup for registration  and  verification then delete the DB ..
                    File from = new File(Environment.getExternalStorageDirectory()
                            + File.separator + "MeritTrack"
                            + File.separator + projectId + "T" + ".db");

                   /* File to = new File(Environment.getExternalStorageDirectory()
                            + File.separator + "MeritTrack_backup"
                            + File.separator + System.currentTimeMillis() / 100 + "" + projectId + "T" + ".db");*/

                    try {
                        //FileUtils.moveFile(from, to); // Move to separate Backup Folder ..
                        FileUtils.forceDelete(from);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }



                File projectData2 = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "MeritTrack"
                        + File.separator + projectId + "C" + ".db");

                if (projectData2.exists()) {
                    File from = new File(Environment.getExternalStorageDirectory()
                            + File.separator + "MeritTrack"
                            + File.separator + projectId + "C" + ".db");

                   /* File to = new File(Environment.getExternalStorageDirectory()
                            + File.separator + "MeritTrack_backup"
                            + File.separator + System.currentTimeMillis() / 100 + "" + projectId + "C" + ".db");*/
                    try {
                       // FileUtils.moveFile(from, to); // Move to separate Backup Folder ..
                        FileUtils.forceDelete(from);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            dbAdapter.closeDatabase();
        }
    }

    public static void insertConfigData(Config config,String transictionId,Context mContext){


        DBAdapter dbAdapterProject = DBAdapter.getInstance(mContext,"common.db", null, 1);
        dbAdapterProject.insertConfigData(config,transictionId);
        dbAdapterProject.closeDatabase();
    }

    public static void insertSessionDetails(Project project,Context mContext){

        // Save project_codes to common Databse
        DBAdapter dbAdapterCommon = DBAdapter.getInstance(mContext,"common.db", null, 1);
        dbAdapterCommon.insertCommon(project.getProjectCode(), project.getProjectPassword(), project.getProjectName(), project.getStage());
        dbAdapterCommon.closeDatabase();

        String dbName=null;
        if(project.getStage().equalsIgnoreCase("1")){
            dbName=project.getProjectCode()+"T"+".db";
        }

        if(project.getStage().equalsIgnoreCase("3")){
            dbName=project.getProjectCode()+"C"+".db";
        }

        DBAdapter dbAdapterProject = DBAdapter.getInstance(mContext,dbName, null, 1);

        LinkedList<Project.Session> verSessionList=project.getLisOfVerSession();

        // insert sessions if exist

        if(verSessionList!=null && verSessionList.size()!=0 ) {
            for (Project.Session session : verSessionList) {

                dbAdapterProject.insertProjectVerSession(project.getProjectCode(), project.getNoOfVerSession(), session);
            }
        }
        dbAdapterProject.closeDatabase();
    }

    public static void insertProjectDetails(Project project,Context mContext){

        // Save project_codes to common Databse
        DBAdapter dbAdapterCommon = DBAdapter.getInstance(mContext,"common.db", null, 1);
        dbAdapterCommon.insertCommon(project.getProjectCode(), project.getProjectPassword(), project.getProjectName(), project.getStage());
        dbAdapterCommon.closeDatabase();

        String dbName=null;
        if(project.getStage().equalsIgnoreCase("1")){
            dbName=project.getProjectCode()+"T"+".db";
        }

        if(project.getStage().equalsIgnoreCase("3")){
            dbName=project.getProjectCode()+"C"+".db";
        }

        DBAdapter dbAdapterProject = DBAdapter.getInstance(mContext,dbName, null, 1);
        dbAdapterProject.insertProjectDetails(project);
        dbAdapterProject.closeDatabase();
    }

    public static void insertVenueDetails(Project project,Venue venue,Context mContext){

        String dbName=null;
        if(project.getStage().equalsIgnoreCase("1")){
            dbName=project.getProjectCode()+"T"+".db";
        }

        if(project.getStage().equalsIgnoreCase("3")){
            dbName=project.getProjectCode()+"C"+".db";
        }

        DBAdapter dbAdapterProject = DBAdapter.getInstance(mContext,dbName, null, 1);
        dbAdapterProject.insertProjectVenues(project.getProjectCode(),venue);

        dbAdapterProject.closeDatabase();
    }

    public static void insertStudentDetails(Project project,Student student,Context mContext){

        String dbName=null;
        if(project.getStage().equalsIgnoreCase("1")){
            dbName=project.getProjectCode()+"T"+".db";
        }

        if(project.getStage().equalsIgnoreCase("3")){
            dbName=project.getProjectCode()+"C"+".db";
        }

        DBAdapter dbAdapterProject = DBAdapter.getInstance(mContext,dbName, null, 1);
        dbAdapterProject.insertStudentDetails(project.getProjectCode(),student.getVenueCode(),student);

        if(null!= student.getStudentTemplateList() && student.getStudentTemplateList().size()!=0){

            for(Student.StudentTemplate stdTemplate:student.getStudentTemplateList()){


                dbAdapterProject.insertStudentTemplate(project.getProjectCode(),student.getVenueCode(),student,stdTemplate);
            }
        }

        dbAdapterProject.closeDatabase();
    }

    public static void insertStudentDetailsEffectively(Project project,Student student,DBAdapter dbAdapterProject){
        dbAdapterProject.insertStudentDetails(project.getProjectCode(),student.getVenueCode(),student);

        if(null!= student.getStudentTemplateList() && student.getStudentTemplateList().size()!=0){

            for(Student.StudentTemplate stdTemplate:student.getStudentTemplateList()){


                dbAdapterProject.insertStudentTemplate(project.getProjectCode(),student.getVenueCode(),student,stdTemplate);
            }
        }

        //dbAdapterProject.closeDatabase();
    }


    public static boolean initializeSensor(Activity avt,InitializeSensor mSensor){

        boolean isUSBPermissionGranted=mSensor.hasUSBPermission();
        if(isUSBPermissionGranted){
            String sensorName= mSensor.getSensorName();

            if(sensorName!=null){
                if(mSensor.connect(sensorName)){

                return true;
                }else{
                    new ManvishAlertDialog(avt,"Sensor Error","Try after some time").showAlertDialog();

                }
            }
        }
        return false;
    }

    private void copyDataInBackground(){

    }

    @SuppressLint("DefaultLocale")
    public static String convertToInternationalMessage(Activity avt,int iCodeError,
                                                int iInternalError) {
        switch (iCodeError) {
            case ErrorCodes.MORPHO_OK:
                return avt.getString(R.string.MORPHO_OK);
            case ErrorCodes.MORPHOERR_INTERNAL:
                return avt.getString(R.string.MORPHOERR_INTERNAL);
            case ErrorCodes.MORPHOERR_PROTOCOLE:
                return avt.getString(R.string.MORPHOERR_PROTOCOLE);
            case ErrorCodes.MORPHOERR_CONNECT:
                return avt.getString(R.string.MORPHOERR_CONNECT);
            case ErrorCodes.MORPHOERR_CLOSE_COM:
                return avt.getString(R.string.MORPHOERR_CLOSE_COM);
            case ErrorCodes.MORPHOERR_BADPARAMETER:
                return avt.getString(R.string.MORPHOERR_BADPARAMETER);
            case ErrorCodes.MORPHOERR_MEMORY_PC:
                return avt.getString(R.string.MORPHOERR_MEMORY_PC);
            case ErrorCodes.MORPHOERR_MEMORY_DEVICE:
                return avt.getString(R.string.MORPHOERR_MEMORY_DEVICE);
            case ErrorCodes.MORPHOERR_NO_HIT:
                return avt.getString(R.string.MORPHOERR_NO_HIT);
            case ErrorCodes.MORPHOERR_STATUS:
                return avt.getString(R.string.MORPHOERR_STATUS);
            case ErrorCodes.MORPHOERR_DB_FULL:
                return avt.getString(R.string.MORPHOERR_DB_FULL);
            case ErrorCodes.MORPHOERR_DB_EMPTY:
                return avt.getString(R.string.MORPHOERR_DB_EMPTY);
            case ErrorCodes.MORPHOERR_ALREADY_ENROLLED:
                return avt.getString(R.string.MORPHOERR_ALREADY_ENROLLED);
            case ErrorCodes.MORPHOERR_BASE_NOT_FOUND:
                return avt.getString(R.string.MORPHOERR_BASE_NOT_FOUND);
            case ErrorCodes.MORPHOERR_BASE_ALREADY_EXISTS:
                return avt.getString(R.string.MORPHOERR_BASE_ALREADY_EXISTS);
            case ErrorCodes.MORPHOERR_NO_ASSOCIATED_DB:
                return avt.getString(R.string.MORPHOERR_NO_ASSOCIATED_DB);
            case ErrorCodes.MORPHOERR_NO_ASSOCIATED_DEVICE:
                return avt.getString(R.string.MORPHOERR_NO_ASSOCIATED_DEVICE);
            case ErrorCodes.MORPHOERR_INVALID_TEMPLATE:
                return avt.getString(R.string.MORPHOERR_INVALID_TEMPLATE);
            case ErrorCodes.MORPHOERR_NOT_IMPLEMENTED:
                return avt.getString(R.string.MORPHOERR_NOT_IMPLEMENTED);
            case ErrorCodes.MORPHOERR_TIMEOUT:
                return avt.getString(R.string.MORPHOERR_TIMEOUT);
            case ErrorCodes.MORPHOERR_NO_REGISTERED_TEMPLATE:
                return avt.getString(R.string.MORPHOERR_NO_REGISTERED_TEMPLATE);
            case ErrorCodes.MORPHOERR_FIELD_NOT_FOUND:
                return avt.getString(R.string.MORPHOERR_FIELD_NOT_FOUND);
            case ErrorCodes.MORPHOERR_CORRUPTED_CLASS:
                return avt.getString(R.string.MORPHOERR_CORRUPTED_CLASS);
            case ErrorCodes.MORPHOERR_TO_MANY_TEMPLATE:
                return avt.getString(R.string.MORPHOERR_TO_MANY_TEMPLATE);
            case ErrorCodes.MORPHOERR_TO_MANY_FIELD:
                return avt.getString(R.string.MORPHOERR_TO_MANY_FIELD);
            case ErrorCodes.MORPHOERR_MIXED_TEMPLATE:
                return avt.getString(R.string.MORPHOERR_MIXED_TEMPLATE);
            case ErrorCodes.MORPHOERR_CMDE_ABORTED:
                return avt.getString(R.string.MORPHOERR_CMDE_ABORTED);
            case ErrorCodes.MORPHOERR_INVALID_PK_FORMAT:
                return avt.getString(R.string.MORPHOERR_INVALID_PK_FORMAT);
            case ErrorCodes.MORPHOERR_SAME_FINGER:
                return avt.getString(R.string.MORPHOERR_SAME_FINGER);
            case ErrorCodes.MORPHOERR_OUT_OF_FIELD:
                return avt.getString(R.string.MORPHOERR_OUT_OF_FIELD);
            case ErrorCodes.MORPHOERR_INVALID_USER_ID:
                return avt.getString(R.string.MORPHOERR_INVALID_USER_ID);
            case ErrorCodes.MORPHOERR_INVALID_USER_DATA:
                return avt.getString(R.string.MORPHOERR_INVALID_USER_DATA);
            case ErrorCodes.MORPHOERR_FIELD_INVALID:
                return avt.getString(R.string.MORPHOERR_FIELD_INVALID);
            case ErrorCodes.MORPHOERR_USER_NOT_FOUND:
                return avt.getString(R.string.MORPHOERR_USER_NOT_FOUND);
            case ErrorCodes.MORPHOERR_COM_NOT_OPEN:
                return avt.getString(R.string.MORPHOERR_COM_NOT_OPEN);
            case ErrorCodes.MORPHOERR_ELT_ALREADY_PRESENT:
                return avt.getString(R.string.MORPHOERR_ELT_ALREADY_PRESENT);
            case ErrorCodes.MORPHOERR_NOCALLTO_DBQUERRYFIRST:
                return avt.getString(R.string.MORPHOERR_NOCALLTO_DBQUERRYFIRST);
            case ErrorCodes.MORPHOERR_USER:
                return avt.getString(R.string.MORPHOERR_USER);
            case ErrorCodes.MORPHOERR_BAD_COMPRESSION:
                return avt.getString(R.string.MORPHOERR_BAD_COMPRESSION);
            case ErrorCodes.MORPHOERR_SECU:
                return avt.getString(R.string.MORPHOERR_SECU);
            case ErrorCodes.MORPHOERR_CERTIF_UNKNOW:
                return avt.getString(R.string.MORPHOERR_CERTIF_UNKNOW);
            case ErrorCodes.MORPHOERR_INVALID_CLASS:
                return avt.getString(R.string.MORPHOERR_INVALID_CLASS);
            case ErrorCodes.MORPHOERR_USB_DEVICE_NAME_UNKNOWN:
                return avt.getString(R.string.MORPHOERR_USB_DEVICE_NAME_UNKNOWN);
            case ErrorCodes.MORPHOERR_CERTIF_INVALID:
                return avt.getString(R.string.MORPHOERR_CERTIF_INVALID);
            case ErrorCodes.MORPHOERR_SIGNER_ID:
                return avt.getString(R.string.MORPHOERR_SIGNER_ID);
            case ErrorCodes.MORPHOERR_SIGNER_ID_INVALID:
                return avt.getString(R.string.MORPHOERR_SIGNER_ID_INVALID);
            case ErrorCodes.MORPHOERR_FFD:
                return avt.getString(R.string.MORPHOERR_FFD);
            case ErrorCodes.MORPHOERR_MOIST_FINGER:
                return avt.getString(R.string.MORPHOERR_MOIST_FINGER);
            case ErrorCodes.MORPHOERR_NO_SERVER:
                return avt.getString(R.string.MORPHOERR_NO_SERVER);
            case ErrorCodes.MORPHOERR_OTP_NOT_INITIALIZED:
                return avt.getString(R.string.MORPHOERR_OTP_NOT_INITIALIZED);
            case ErrorCodes.MORPHOERR_OTP_PIN_NEEDED:
                return avt.getString(R.string.MORPHOERR_OTP_PIN_NEEDED);
            case ErrorCodes.MORPHOERR_OTP_REENROLL_NOT_ALLOWED:
                return avt.getString(R.string.MORPHOERR_OTP_REENROLL_NOT_ALLOWED);
            case ErrorCodes.MORPHOERR_OTP_ENROLL_FAILED:
                return avt.getString(R.string.MORPHOERR_OTP_ENROLL_FAILED);
            case ErrorCodes.MORPHOERR_OTP_IDENT_FAILED:
                return avt.getString(R.string.MORPHOERR_OTP_IDENT_FAILED);
            case ErrorCodes.MORPHOERR_NO_MORE_OTP:
                return avt.getString(R.string.MORPHOERR_NO_MORE_OTP);
            case ErrorCodes.MORPHOERR_OTP_NO_HIT:
                return avt.getString(R.string.MORPHOERR_OTP_NO_HIT);
            case ErrorCodes.MORPHOERR_OTP_ENROLL_NEEDED:
                return avt.getString(R.string.MORPHOERR_OTP_ENROLL_NEEDED);
            case ErrorCodes.MORPHOERR_DEVICE_LOCKED:
                return avt.getString(R.string.MORPHOERR_DEVICE_LOCKED);
            case ErrorCodes.MORPHOERR_DEVICE_NOT_LOCK:
                return avt.getString(R.string.MORPHOERR_DEVICE_NOT_LOCK);
            case ErrorCodes.MORPHOERR_OTP_LOCK_GEN_OTP:
                return avt.getString(R.string.MORPHOERR_OTP_LOCK_GEN_OTP);
            case ErrorCodes.MORPHOERR_OTP_LOCK_SET_PARAM:
                return avt.getString(R.string.MORPHOERR_OTP_LOCK_SET_PARAM);
            case ErrorCodes.MORPHOERR_OTP_LOCK_ENROLL:
                return avt.getString(R.string.MORPHOERR_OTP_LOCK_ENROLL);
            case ErrorCodes.MORPHOERR_FVP_MINUTIAE_SECURITY_MISMATCH:
                return avt.getString(R.string.MORPHOERR_FVP_MINUTIAE_SECURITY_MISMATCH);
            case ErrorCodes.MORPHOERR_FVP_FINGER_MISPLACED_OR_WITHDRAWN:
                return avt.getString(R.string.MORPHOERR_FVP_FINGER_MISPLACED_OR_WITHDRAWN);
            case ErrorCodes.MORPHOERR_LICENSE_MISSING:
                return avt.getString(R.string.MORPHOERR_LICENSE_MISSING);
            case ErrorCodes.MORPHOERR_CANT_GRAN_PERMISSION_USB:
                return avt.getString(R.string.MORPHOERR_CANT_GRAN_PERMISSION_USB);
            default:
                return String.format("Unknown error %d, Internal Error = %d",
                        iCodeError, iInternalError);
        }
    }

}
