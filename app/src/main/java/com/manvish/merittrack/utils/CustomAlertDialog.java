package com.manvish.merittrack.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.manvish.merittrack.R;


public class CustomAlertDialog {


    public static void displayAlertDialog(final Activity contxt, final String msgTitle, String dispMsg) {
        final Dialog custDialg = new Dialog(contxt, R.style.PauseDialog) {
            @Override
            public boolean onTouchEvent(MotionEvent event) {
                /** Tap anywhere to close dialog. */
                this.dismiss();
                return true;
            }
        };
        custDialg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        custDialg.setCancelable(true);
        custDialg.setContentView(R.layout.cust_alert_dialog);
        custDialg.show();

        LinearLayout layout = (LinearLayout) custDialg.findViewById(R.id.ll_total_layout);
        /**
         * To avoid dismissal of dialog onTouch inside visible area
         */

        ((TextView) custDialg.findViewById(R.id.tv_cust_title)).setText("" + msgTitle);
        ((TextView) custDialg.findViewById(R.id.tv_cust_alert_msg)).setText("" + dispMsg);

        layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }// End of onClick
        });// End of layout

        ((Button) custDialg.findViewById(R.id.btn_cust_dlg_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                custDialg.dismiss();
                if (msgTitle.equalsIgnoreCase("No License")) {
                    contxt.finish();
                }
            }//End of onClick()
        });//End of dialogButtonExport
    }//End of exportDialog()

}

