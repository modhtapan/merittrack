package com.manvish.merittrack.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Xml;


import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.modelupload.AcsStudentWrite;
import com.manvish.merittrack.modelupload.EnrStudentWrite;
import com.manvish.merittrack.modelupload.ImageWrite;
import com.manvish.merittrack.modelupload.RootModel;
import com.manvish.merittrack.modelupload.TemplateWrite;


import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import org.apache.commons.io.FileUtils;
import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class ManvishFileUtils {

    public static ArrayList<String> noticeeSupportedVideoFormat = new ArrayList<String>();

    static {
        noticeeSupportedVideoFormat.add("mp4");
        noticeeSupportedVideoFormat.add("3gp");
        noticeeSupportedVideoFormat.add("webm");

    }

    public static byte[] getBytesFromBitmap(Bitmap bitmap) {
        int bytes = bitmap.getByteCount();
        ByteBuffer buffer = ByteBuffer.allocate(bytes); // Create
        // a
        // new
        // buffer
        bitmap.copyPixelsToBuffer(buffer); // Move the byte data
        // to
        // the buffer

        byte[] array = buffer.array();
        return array;
    }

    // convert from bitmap to byte array
    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    public static Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }
    public static Bitmap getVideoThumbnail(String filePath, int width,
                                           int height) {
        Bitmap image = ThumbnailUtils.createVideoThumbnail(filePath,
                MediaStore.Video.Thumbnails.MINI_KIND);
        if (image == null) {
            return null;
        }
        Bitmap thumbnail = ThumbnailUtils
                .extractThumbnail(image, width, height);
        if (thumbnail != image) {
            image.recycle();
        }
        return thumbnail;
    }

    public static String getMediaPathFromUri(Context context, Uri mediaUri) {
        if (mediaUri == null)
            return null;

        String scheme = mediaUri.getScheme();
        if (scheme == null) {
            return null;
        }
        if (scheme.equalsIgnoreCase(ContentResolver.SCHEME_FILE)) {
            try {
                String string = mediaUri.toString();
                if (string != null) {
                    string = string.replace(" ", "%20");
                }
                return new File(new URI(string)).getAbsolutePath();
            } catch (Exception e) {
                return null;
            }
        }

        String[] projection = new String[]{MediaStore.Images.Media.DATA};

        String path = null;
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(mediaUri, projection,
                    "", null, "");
            if (cursor == null || cursor.getCount() == 0) {
                Log.d(StringConstants.TAG, "cursor is null or empty");
                return null;
            }

            if (cursor.moveToFirst()) {
                path = cursor.getString(cursor
                        .getColumnIndex(MediaStore.Images.Media.DATA));
                Log.d(StringConstants.TAG, "path from URI: " + path);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        if (path == null) {

            Log.d(StringConstants.TAG, "path from URI: null");
        }

        return path;
    }

    public static Bitmap getImage(String filePath, int width, int height) {

        if (filePath == null) {
            return null;
        }
        String mimeType = getMimeTypeFromFileName(filePath);
        if (mimeType == null) {
            return null;
        }
        if (noticeeSupportedVideoFormat.contains(mimeType)) {
            return getVideoThumbnail(filePath, width, height);
        } else {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, o);
            int origSize = (o.outHeight < o.outWidth ? o.outHeight : o.outWidth);
            int scale = Math.round((float) origSize / (float) width);
            if (scale == 0) {
                scale = 1;
            }

            BitmapFactory.Options loadOpt = new BitmapFactory.Options();
            loadOpt.inSampleSize = scale;
            loadOpt.inJustDecodeBounds = false;

            return adjustRotation(BitmapFactory.decodeFile(filePath, loadOpt),
                    filePath);
        }
    }

    private static Bitmap adjustRotation(Bitmap inBitmap, String filePath) {
        int angle = getAngle(filePath);

        if (angle == 0)
            return inBitmap;

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        Bitmap outBitmap = Bitmap.createBitmap(inBitmap, 0, 0,
                inBitmap.getWidth(), inBitmap.getHeight(), matrix, true);

        if (inBitmap != outBitmap) {
            inBitmap.recycle();
        }

        return outBitmap;
    }

    private static int getAngle(String filePath) {
        int orientation;

        try {
            ExifInterface exif = new ExifInterface(filePath);
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
        } catch (IOException e) {
            Log.d(StringConstants.TAG, "File Not Found");
            return 0;
        }

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return 90;

            case ExifInterface.ORIENTATION_ROTATE_180:
                return 180;

            case ExifInterface.ORIENTATION_ROTATE_270:
                return 270;

            default:
                return 0;
        }
    }

    public static String getMimeTypeFromFileName(String fileName) {
        String ext = getFileExtensionFromFileName(fileName);
        if (ext == null)
            return null;
        else {
            return ext;
        }

    }

    public static String getFileExtensionFromFileName(String fileName) {
        if (fileName == null || !fileName.contains(".")) {
            return null;
        }
        return fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase(
                Locale.getDefault());
    }

    public static String getnoticeeCameraFilePath(String fileName) {
        File noticeeCameraDir = getnoticeeCameraDir();
        if (noticeeCameraDir == null)
            return null;

        noticeeCameraDir.getAbsolutePath();
        String filePath = noticeeCameraDir.getAbsolutePath() + fileName;
        File newFile = new File(filePath);
        try {
            System.out.println("Creating File: " + filePath);
            newFile.createNewFile();
        } catch (Exception e) {
            return null;
        }

        if (!newFile.exists()) {

            Log.d("noticeeFileUtils", "ERROR - Could not create file");

            return null;
        }
        Log.d("noticeeFileUtils", "File successfully created");
        return filePath;
    }

    private static final String CAMERA_STORAGE_PATH = getSdcardPath() + "/dcim";

    public static File getnoticeeCameraDir() {
        if (!Environment.MEDIA_MOUNTED.equals(Environment
                .getExternalStorageState())) {
            Log.d("noticeeFileUtils", "Media Not Mounted");
            return null;
        }

        File noticeeCameraDir = new File(CAMERA_STORAGE_PATH + "/" + "noticee");

        if (!noticeeCameraDir.mkdirs()) {
            if (!noticeeCameraDir.exists()) {

                System.out.println("Could not create noticee Camera directory");

                return null;
            }
        }

        return noticeeCameraDir;
    }

    public static String getSdcardPath() {
        return Environment.getExternalStorageDirectory().toString();
    }

    public static String getVideoFilePath() {
        String lVideoFilePath = null;
        String title = null;
        String fileName = null;
        String ts = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ROOT)
                .format(new Date());

        title = "noticee_VID_" + ts;
        fileName = "/" + title + ".mp4";

        lVideoFilePath = getnoticeeCameraFilePath(fileName);
        if (lVideoFilePath == null) {
            return null;
        }
        return lVideoFilePath;
    }



    public static File createSyncFileInSDCard(String fileName) {

        File f = null;
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            // handle case of no SDCARD present
        } else {
            String dir = Environment.getExternalStorageDirectory()
                    + File.separator + "Meritrack";
            // create folder
            File folder = new File(dir); // folder name
            folder.mkdirs();

            // create file
            File file = new File(dir, fileName);
            return file;
        }
        return f;

    }

    public static File createEnrollmentFileInSDCard(String fileName) {

        File f = null;
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            // handle case of no SDCARD present
        } else {
            String dir = Environment.getExternalStorageDirectory()
                    + File.separator + "EnrollMentXML";
            // create folder
            File folder = new File(dir); // folder name
            folder.mkdirs();

            // create file
            File file = new File(dir, fileName);
            return file;
        }
        return f;

    }


    public static FileInputStream unzipAndGetInputStream(String sourcePath) {

        String destinationPath = Environment.getExternalStorageDirectory()+"/Merittrack/";
        FileInputStream is = null;
        String path = null;
        try {
            ZipFile zipFile = new ZipFile(sourcePath);
            zipFile.extractAll(destinationPath);
        } catch (ZipException e) {
            e.printStackTrace();
        }

        try {
            is = new FileInputStream(destinationPath + File.separator + ManvishPrefConstants.SERIAL_NO.read() + ".xml");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return is;
    }

    public static String unzipAndGetFolderPath(String sourcePath) {

        String destinationPath = Environment.getExternalStorageDirectory()+"/Merittrack/Download/";
        FileInputStream is = null;
        String path = null;
        try {
            ZipFile zipFile = new ZipFile(sourcePath);
            zipFile.extractAll(destinationPath);
            //After extracting all delete the zip file
            FileUtils.forceDelete(new File(sourcePath));
            return destinationPath;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isFileExists(String fileName) {
        String dir = Environment.getExternalStorageDirectory() + File.separator
                + "MeritrackXML";
        // create folder
        File folder = new File(dir); // folder name
        folder.mkdirs();

        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                System.out.println("File " + listOfFiles[i].getName());

                if (listOfFiles[i].getName().equalsIgnoreCase(fileName)) {
                    return true;
                }
            } else if (listOfFiles[i].isDirectory()) {
                System.out.println("Directory " + listOfFiles[i].getName());
                return false;
            }
        }
        return false;
    }


    // pre test uploadData
    public static String getUploadPreTestXml(RootModel rootModel) throws Exception {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();

        xmlSerializer.setOutput(writer);
        // start DOCUMENT
        xmlSerializer.startDocument("UTF-8", true);

        xmlSerializer.startTag("",
                StringConstants.XML_ROOT);

        xmlSerializer.startTag("",
                StringConstants.XML_PROJECT_DETAILS);

        xmlSerializer.startTag("",
                StringConstants.XML_PROJECT_CODE);

        xmlSerializer.text(rootModel.getProjectDetails().getProjCode());

        xmlSerializer.endTag("",
                StringConstants.XML_PROJECT_CODE);

        xmlSerializer.startTag("",
                StringConstants.XML_DATE);

        xmlSerializer.text(ManvishPrefConstants.SELECTED_DATE.read());

        xmlSerializer.endTag("",
                StringConstants.XML_DATE);

        xmlSerializer.startTag("",
                StringConstants.XML_VENUE_CODE);

        xmlSerializer.text(rootModel.getProjectDetails().getVenueCode());

        xmlSerializer.endTag("",
                StringConstants.XML_VENUE_CODE);


        xmlSerializer.startTag("",
                StringConstants.XML_STAGE);

        xmlSerializer.text(rootModel.getProjectDetails().getStage());

        xmlSerializer.endTag("",
                StringConstants.XML_STAGE);

        xmlSerializer.endTag("", StringConstants.XML_PROJECT_DETAILS);

        if (rootModel.getEnrData() != null) {

            xmlSerializer.startTag("",
                    StringConstants.XML_ENR_DATA);

            for (EnrStudentWrite studentWrite : rootModel.getEnrData().getStudent()) {

                xmlSerializer.startTag("",
                        StringConstants.XML_STUDENT);

                xmlSerializer.startTag("",
                        StringConstants.XML_STUDENT_ID);

                xmlSerializer.text(studentWrite.getStudentId());

                xmlSerializer.endTag("",
                        StringConstants.XML_STUDENT_ID);

                xmlSerializer.startTag("",
                        StringConstants.XML_UNIT_ID);

                xmlSerializer.text(studentWrite.getUnitId());

                xmlSerializer.endTag("",
                        StringConstants.XML_UNIT_ID);

                xmlSerializer.startTag("",
                        StringConstants.XML_PROJECT_CODE);

                xmlSerializer.text(studentWrite.getProjCode());

                xmlSerializer.endTag("",
                        StringConstants.XML_PROJECT_CODE);

                xmlSerializer.startTag("",
                        StringConstants.XML_REG_DATE);
                xmlSerializer.text(studentWrite.getRegDate());
                xmlSerializer.endTag("",
                        StringConstants.XML_REG_DATE);


                xmlSerializer.startTag("",
                        StringConstants.XML_REG_TIME);
                xmlSerializer.text(studentWrite.getRegTime());
                xmlSerializer.endTag("",
                        StringConstants.XML_REG_TIME);


                int i=0;
                for (TemplateWrite templateWrite : studentWrite.getTemplate()) {
                    xmlSerializer.startTag("",
                            StringConstants.XML_TEMPLATE);

                    xmlSerializer.startTag("",
                            StringConstants.XML_FINGER_TYPE);

                    int fingerType=Integer.parseInt(templateWrite.getFingerType().trim());
                    if(i>1){
                        int multi=i/2;
                        xmlSerializer.text(fingerType+10*multi+"");
                    }else{
                        xmlSerializer.text(templateWrite.getFingerType());
                    }


                    xmlSerializer.endTag("",
                            StringConstants.XML_FINGER_TYPE);

                    i++;
                    xmlSerializer.startTag("",
                            StringConstants.XML_TDATA);
                    xmlSerializer.text(templateWrite.gettData());

                    xmlSerializer.endTag("",
                            StringConstants.XML_TDATA);

                    xmlSerializer.startTag("",
                            StringConstants.XML_REG_STATUS);

                    if(templateWrite.getRegStatus().equalsIgnoreCase("-1")){
                        xmlSerializer.text("F");
                    }else if(templateWrite.getRegStatus().equalsIgnoreCase("1")){
                        xmlSerializer.text("R");
                    }
                   // xmlSerializer.text(templateWrite.getRegStatus());
                    xmlSerializer.endTag("",
                            StringConstants.XML_REG_STATUS);

                    xmlSerializer.startTag("",
                            StringConstants.XML_VER_STATUS);

                    xmlSerializer.text("F"); // We are not capturing anything for Offline verification now ..
                    xmlSerializer.endTag("",
                            StringConstants.XML_VER_STATUS);


                    xmlSerializer.endTag("",
                            StringConstants.XML_TEMPLATE);

                }

                int j=0;
                for (ImageWrite imageWrite : studentWrite.getImage()) {
                    xmlSerializer.startTag("",
                            StringConstants.XML_IMAGE);

                    xmlSerializer.startTag("",
                            StringConstants.XML_ITYPE);
                    int fingerType=Integer.parseInt(imageWrite.getiType().trim());
                    if(j>1){
                        int multi=j/2;
                        xmlSerializer.text(fingerType+10*multi+"");
                    }else{
                        xmlSerializer.text(imageWrite.getiType());
                    }

                    j++;
                    xmlSerializer.endTag("",
                            StringConstants.XML_ITYPE);

                    xmlSerializer.startTag("",
                            StringConstants.XML_IDATA);

                    xmlSerializer.text(imageWrite.getiData());
                    xmlSerializer.endTag("",
                            StringConstants.XML_IDATA);

                    xmlSerializer.startTag("",
                            StringConstants.XML_REG_STATUS);

                    if(imageWrite.getRegStatus().trim().equalsIgnoreCase("-1")){
                        xmlSerializer.text("F");
                    }else if(imageWrite.getRegStatus().trim().equalsIgnoreCase("1")){
                        xmlSerializer.text("R");
                    }

                   // xmlSerializer.text(imageWrite.getRegStatus());
                    xmlSerializer.endTag("",
                            StringConstants.XML_REG_STATUS);

                    xmlSerializer.startTag("",
                            StringConstants.XML_VER_STATUS);

                   /* if(imageWrite.getVerStatus().trim().equalsIgnoreCase("-1")){
                        xmlSerializer.text("F");
                    }else if(imageWrite.getVerStatus().trim().equalsIgnoreCase("1")){
                        xmlSerializer.text("S");
                    }*/
                    xmlSerializer.text("F"); // hard codeed for this ,we are not cnsidering offline verification ..

                    xmlSerializer.endTag("",
                            StringConstants.XML_VER_STATUS);

                    xmlSerializer.endTag("",
                            StringConstants.XML_IMAGE);

                }

                xmlSerializer.startTag("",
                        StringConstants.XML_PHOTOGRAPH);
                xmlSerializer.text(studentWrite.getPhotograph());
                xmlSerializer.endTag("",
                        StringConstants.XML_PHOTOGRAPH);


                xmlSerializer.endTag("",
                        StringConstants.XML_STUDENT);
            }

            xmlSerializer.endTag("",
                    StringConstants.XML_ENR_DATA);
        }

        if (rootModel.getAcslog() != null) {

            xmlSerializer.startTag("",
                    StringConstants.XML_ACS_LOG);

            for (AcsStudentWrite studentWrite : rootModel.getAcslog().getStudent()) {

                xmlSerializer.startTag("",
                        StringConstants.XML_STUDENT);

                xmlSerializer.startTag("",
                        StringConstants.XML_STUDENT_ID);

                xmlSerializer.text(studentWrite.getStudentId());

                xmlSerializer.endTag("",
                        StringConstants.XML_STUDENT_ID);

                xmlSerializer.startTag("",
                        StringConstants.XML_UNIT_ID);

                xmlSerializer.text(studentWrite.getUnitId());

                xmlSerializer.endTag("",
                        StringConstants.XML_UNIT_ID);

                xmlSerializer.startTag("",
                        StringConstants.XML_PROJECT_CODE);

                xmlSerializer.text(studentWrite.getProjCode());

                xmlSerializer.endTag("",
                        StringConstants.XML_PROJECT_CODE);


                xmlSerializer.startTag("",
                        StringConstants.XML_VER_DATE);
                xmlSerializer.text(studentWrite.getVerDate());
                xmlSerializer.endTag("",
                        StringConstants.XML_VER_DATE);


                xmlSerializer.startTag("",
                        StringConstants.XML_VER_TIME);
                xmlSerializer.text(studentWrite.getVerTime());
                xmlSerializer.endTag("",
                        StringConstants.XML_VER_TIME);


                xmlSerializer.startTag("",
                        StringConstants.XML_IN_OUT);
                xmlSerializer.text(studentWrite.getInOut());
                xmlSerializer.endTag("",
                        StringConstants.XML_IN_OUT);


                xmlSerializer.startTag("",
                        StringConstants.XML_STATUS);

                if(studentWrite.getStatus().trim().equalsIgnoreCase("-1")){
                    xmlSerializer.text("F");
                }else if(studentWrite.getStatus().trim().equalsIgnoreCase("1")){
                    xmlSerializer.text("S");
                }

              //  xmlSerializer.text(studentWrite.getStatus());
                xmlSerializer.endTag("",
                        StringConstants.XML_STATUS);

                xmlSerializer.startTag("",
                        StringConstants.XML_FINGER_TYPE);
                xmlSerializer.text(studentWrite.getFingerType());
                xmlSerializer.endTag("",
                        StringConstants.XML_FINGER_TYPE);

                xmlSerializer.startTag("",
                        StringConstants.XML_SESSION_CODE);
                xmlSerializer.text(studentWrite.getSessionCode());
                xmlSerializer.endTag("",
                        StringConstants.XML_SESSION_CODE);


                xmlSerializer.endTag("",
                        StringConstants.XML_STUDENT);
            }

            xmlSerializer.endTag("",
                    StringConstants.XML_ACS_LOG);
        }

        xmlSerializer.endTag("", StringConstants.XML_ROOT);

        // end DOCUMENT
        xmlSerializer.endDocument();

        return writer.toString();
    }

    }




