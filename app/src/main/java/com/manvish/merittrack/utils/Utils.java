package com.manvish.merittrack.utils;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.manvish.merittrack.constants.ManvishPrefConstants;

/**
 * Utils class for utility of this app
 */
public class Utils {


    // get mac address of device
    public static String getMacAddress(Context context) {
        WifiManager wimanager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        String macAddress = wimanager.getConnectionInfo().getMacAddress();
        if (macAddress == null) {
            macAddress = "Device don't have mac address or wi-fi is disabled";
        }
        return macAddress;
    }

    public static String getSerialId(Context context){
        TelephonyManager tManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        return tManager.getDeviceId();
    }


    public static String availMacAddress(Context context){
        String address= "" ;
        WifiManager wifiManager1 = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);


        try {
            if (wifiManager1.isWifiEnabled()) {
                // WIFI ALREADY ENABLED. GRAB THE MAC ADDRESS HERE
                WifiInfo info = wifiManager1.getConnectionInfo();
                address = info.getMacAddress();
                ManvishPrefConstants.MAC_ADDRESS.write(address);
              //  Toast.makeText(context, "WifiNetwork is already avilable  grab the macaddress", Toast.LENGTH_SHORT).show();
            } else {
                // ENABLE THE WIFI FIRST
                wifiManager1.setWifiEnabled(true);

                // WIFI IS NOW ENABLED. GRAB THE MAC ADDRESS HERE
                WifiInfo info = wifiManager1.getConnectionInfo();
                address = info.getMacAddress();
                ManvishPrefConstants.MAC_ADDRESS.write(address);
                //return  address;
               // Toast.makeText(context, "WifiNetwork is now avilable  grab the macaddress", Toast.LENGTH_SHORT).show();
                wifiManager1.setWifiEnabled(false);
            }
            return address;
        }catch(Exception e){
           e.printStackTrace();
        }

return  address;

 }


}
