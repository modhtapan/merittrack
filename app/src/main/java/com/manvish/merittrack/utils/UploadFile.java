package com.manvish.merittrack.utils;

import android.content.Context;
import android.util.Log;


import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;

import net.gotev.uploadservice.MultipartUploadRequest;

import java.io.File;

/**
 * UploadFile to upload file to server
 */
public class UploadFile {


    public static void uploadMultipart(Context context,final String id, String filePath) {

        String BASE_URL ;
        if(ManvishPrefConstants.SERVER_IP.read()==null){

            ManvishPrefConstants.SERVER_IP.write(StringConstants.AUPD_BASE_URL);
            ManvishPrefConstants.SERVER_PORT.write(StringConstants.AUPD_BASE_PORT);
            BASE_URL = StringConstants.AUPD_HTTP+StringConstants.AUPD_BASE_URL+":"+StringConstants.AUPD_BASE_PORT+"/"+StringConstants.SERVICE_NAME+"/";

        } else {
            String IPAddress= ManvishPrefConstants.SERVER_IP.read();
            String port = ManvishPrefConstants.SERVER_PORT.read();
            BASE_URL = StringConstants.AUPD_HTTP + IPAddress + ":" + port + "/" + StringConstants.SERVICE_NAME + "/";
        }

        String command=null;

        File uploadZipFile=new File(filePath);
        String fileName=uploadZipFile.getName();
        String fileNameArray[]=fileName.split("_");
        String projectCode=fileNameArray[1];

        if (filePath.contains("PT")) { // /storage/sdcard0/MeritTrack/Upload_Offline/616001452_2614_20-05-2016_PT.zip
            command= "RDATA";
        } else{
            command="CDATA";
        }

         String url = BASE_URL+command;

        try {
            Log.d("upload","multipart");
            new MultipartUploadRequest(context, id, url)

                    .addParameter("UNITID", ManvishPrefConstants.SERIAL_NO.read()) // Value is hardCoded
                    .addParameter("PROJECTID", projectCode)
                    .addParameter("TIME", ManvishCommonUtil.getCurrentTime())
                    .addParameter("DATE", ManvishCommonUtil.getCurrentDate())
                    .addParameter("MODEL", "2")
                    .addParameter("LONGITUDE", "2")
                    .addParameter("LATITUDE", "2")
                    .addParameter("SIZE", String.valueOf(new File(filePath).length()))
                    //.addParameter("DATA",fileData)
                    .addFileToUpload(filePath, "file")
                    .startUpload();
            Log.d("upload","multipart start");
        } catch (Exception exc) {
            Log.e("AndroidUploadService", exc.getMessage(), exc);
        }
    }
}
