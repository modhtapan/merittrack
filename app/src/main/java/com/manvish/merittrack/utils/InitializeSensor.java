package com.manvish.merittrack.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.manvish.merittrack.Model.RegOrVerFingerDetails;
import com.manvish.merittrack.Model.Student;
import com.manvish.merittrack.R;
import com.manvish.merittrack.activities.MorphoSampleActivity;
import com.manvish.merittrack.activities.RegisterOrVerificationActivity;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.view.ManvishAlertDialog;
import com.morpho.android.usb.USBManager;
import com.morpho.morphosample.info.CaptureInfo;
import com.morpho.morphosample.info.MorphoInfo;
import com.morpho.morphosample.info.ProcessInfo;
import com.morpho.morphosample.info.VerifyInfo;
import com.morpho.morphosample.info.subtype.AuthenticationMode;
import com.morpho.morphosample.info.subtype.CaptureType;
import com.morpho.morphosmart.sdk.CallbackMask;
import com.morpho.morphosmart.sdk.CallbackMessage;
import com.morpho.morphosmart.sdk.Coder;
import com.morpho.morphosmart.sdk.CompressionAlgorithm;
import com.morpho.morphosmart.sdk.DetectionMode;
import com.morpho.morphosmart.sdk.EnrollmentType;
import com.morpho.morphosmart.sdk.ErrorCodes;
import com.morpho.morphosmart.sdk.FalseAcceptanceRate;
import com.morpho.morphosmart.sdk.LatentDetection;
import com.morpho.morphosmart.sdk.MorphoDatabase;
import com.morpho.morphosmart.sdk.MorphoDevice;
import com.morpho.morphosmart.sdk.MorphoImage;
import com.morpho.morphosmart.sdk.MorphoWakeUpMode;
import com.morpho.morphosmart.sdk.ResultMatching;
import com.morpho.morphosmart.sdk.StrategyAcquisitionMode;
import com.morpho.morphosmart.sdk.Template;
import com.morpho.morphosmart.sdk.TemplateFVP;
import com.morpho.morphosmart.sdk.TemplateFVPType;
import com.morpho.morphosmart.sdk.TemplateList;
import com.morpho.morphosmart.sdk.TemplateType;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by pabitra on 4/16/16.
 */
public class InitializeSensor implements Observer{


   // private int currentCaptureBitmapId = 0;
    private boolean isCaptureVerif = false;
    private Handler mHandler = new Handler();
    String strMessage = new String();
    private int index;
    private MorphoDatabase morphoDatabase;
    private View mInflaterView;
    private Bitmap mFinalCapturedImageBitmap;

    MorphoImage morphoImage;

    byte[] mTemplateData;

    DBAdapter dbAdapter;


    static MorphoDevice morphoDevice;
    String sensorName;

    Context context;
    public InitializeSensor(Context context){

        this.context=context;
    }




    public void initSensor(){

        if (USBManager.getInstance().isDevicesHasPermission() == true) {

        } else {
            grantPermission();
        }

    }

    public boolean hasUSBPermission(){
        if (USBManager.getInstance().isDevicesHasPermission() == true) {
            return true;
        }else {
            return false;
        }
    }

    public void grantPermission() {
        USBManager.getInstance().initialize(context,
                "com.morpho.morphosample.USB_ACTION");
    }


    public String getSensorName() {

        morphoDevice = new MorphoDevice();
        Integer nbUsbDevice = new Integer(0);
        if(morphoDevice!=null) {
            int ret = morphoDevice.initUsbDevicesNameEnum(nbUsbDevice);
            if (ret == ErrorCodes.MORPHO_OK) {

                if (nbUsbDevice > 0) {
                    sensorName = morphoDevice.getUsbDeviceName(0);

                    if (sensorName != null) {
                    /* Obulesu code for reverse finger */
                       return sensorName;

                    } else {
                        Toast.makeText(context,
                                "No device found", Toast.LENGTH_LONG).show();
                        return null;
                    }
                }else{
                    //Sensor name is null
                    return null;
                }
            } else {
                Toast.makeText(context, ErrorCodes.getError(ret,
                        morphoDevice.getInternalError()), Toast.LENGTH_LONG).show();
                return null;
            }
        }else{

            return null;
        }

    }


    private boolean setParameter(MorphoDevice morphoDevice) {

        byte[] sensorWindowPosition = morphoDevice.getConfigParam(MorphoDevice.CONFIG_SENSOR_WIN_POSITION_TAG);
        int position = 0;
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@Step 1 " + sensorWindowPosition[0]);
        if (sensorWindowPosition != null) {
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@Step 1 " + sensorWindowPosition[0]);
            position = sensorWindowPosition[0];
            if (position != 3) {
                byte[] fpOrient = new byte[1];
                fpOrient[0] = 0x03;
                int ret1 = morphoDevice.setConfigParam(MorphoDevice.CONFIG_SENSOR_WIN_POSITION_TAG, fpOrient);
                if (ret1 != ErrorCodes.MORPHO_OK) {

                    System.out.println("failed setting reverse params");
                } else {
                    int  ret = morphoDevice.rebootSoft(30, this);
                    morphoDevice.resumeConnection(30, this);
                    System.out.println("Succes setting reverse paramas");
                    return true;
                }
            }else{
                return true;
            }
        }
        return false;
    }

    public boolean connect(String sensorName) {

       // morphoDevice=ProcessInfo.getInstance().getMorphoDevice();

        if(morphoDevice!=null) {
            int ret = morphoDevice.openUsbDevice(sensorName, 50);
            // ret is an integer ,
            // if It is  0 ,then device successfully opened .

            if (ret != ErrorCodes.MORPHO_OK) {
                closeSensor();
                return false;
            } else {
                ProcessInfo.getInstance().setMSOSerialNumber(sensorName);
                String productDescriptor = morphoDevice.getProductDescriptor();

            /*  Need to understand this below piece of code */
                java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(
                        productDescriptor, "\n");
                if (tokenizer.hasMoreTokens()) {
                    String l_s_current = tokenizer.nextToken();
                    if (l_s_current.contains("FINGER VP")
                            || l_s_current.contains("FVP")) {
                        MorphoInfo.m_b_fvp = true;
                    }
                }

            /*  -----------------  */

                if (ret != ErrorCodes.MORPHO_OK) {

                    ManvishCommonUtil.showCustomToast((Activity) context, "Device is Busy ,Try Again");
                    closeDevice(morphoDevice);
                } else {
                        morphoDevice.closeDevice();

                        sensorName = ProcessInfo.getInstance().getMSOSerialNumber();
                        ret = morphoDevice.openUsbDevice(sensorName, 0);
                        if (ret == ErrorCodes.MORPHO_OK) {
                            boolean isParamSet=setParameter(morphoDevice);

                            if(isParamSet){
                                ProcessInfo.getInstance().setMorphoDevice(morphoDevice);
                                return true;
                            }else{
                                return false;
                            }

                        } else {
                            ManvishCommonUtil.showCustomToast((Activity) context, "Device is Busy ,Try Again");
                            closeSensor();
                            return false;
                        }

                }
            }
            return false;
        }else{

            Log.d(StringConstants.TAG,"Morphodevice is null");
            return false;
        }

    }


   /* public void startSensor(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                onStartStop();
            }
        }, 500);
    }*/
    public void resumeSensor(){

        initSensor();
    }

    public void closeDevice(MorphoDevice morphoDevice) {
        try {

            morphoDevice.closeDevice();
            morphoDevice=null;

        } catch (Exception e) {
            Log.e("ERROR", e.toString());
        } finally {
            ProcessInfo.getInstance().setMorphoDevice(null);
        }
    }

    public void closeSensor(){

        try {
        if (morphoDevice != null && ProcessInfo.getInstance().isStarted()) {
            morphoDevice.cancelLiveAcquisition();
            morphoDevice.closeDevice();
            if(ProcessInfo.getInstance()!=null) {
                ProcessInfo.getInstance().setMorphoDevice(null);
            }
            Thread.sleep(2000);
        }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

    }


    public void stopProcess(){
            ProcessInfo.getInstance().setStarted(false);
            if (ProcessInfo.getInstance().isCommandBioStart()) {
                ProcessInfo.getInstance().getMorphoDevice().cancelLiveAcquisition();
            }
           try {
            Thread.sleep(500);
           } catch (InterruptedException e) {
            e.printStackTrace();
          }
    }



    private void notifyEndProcess() {
        mHandler.post(new Runnable() {
            @Override
            public synchronized void run() {
                try {

                    stopProcess();
                } catch (Exception e) {
                    Log.d("notifyEndProcess", "" + e.getMessage());
                }
            }
        });

    }

    @Override
    public synchronized void update(Observable o, Object arg) {
        try {
            // convert the object to a callback back message.
            CallbackMessage message = (CallbackMessage) arg;

            int type = message.getMessageType();

            switch (type) {

                case 1:
                    // message is a command.
                    Integer command = (Integer) message.getMessage();
                    mHandler.post(new Runnable() {
                        @Override
                        public synchronized void run() {
                            //updateSensorMessage(strMessage);
                        }
                    });

                    break;
                case 2:
                    // message is a low resolution image, display it.
                    byte[] image = (byte[]) message.getMessage();

                    morphoImage = MorphoImage.getMorphoImageFromLive(image);
                    int imageRowNumber = morphoImage.getMorphoImageHeader()
                            .getNbRow();
                    int imageColumnNumber = morphoImage.getMorphoImageHeader()
                            .getNbColumn();
                    final Bitmap imageBmp = Bitmap.createBitmap(imageColumnNumber,
                            imageRowNumber, Bitmap.Config.ALPHA_8);

                    CompressionAlgorithm al=morphoImage.getCompressionAlgorithm();
                    al.getCode();
                    System.out.println("compression code=="+ al.getCode());
                    imageBmp.copyPixelsFromBuffer(ByteBuffer.wrap(
                            morphoImage.getImage(), 0,
                            morphoImage.getImage().length));

                    mFinalCapturedImageBitmap = imageBmp;

                    mHandler.post(new Runnable() {
                        @Override
                        public synchronized void run() {
                            updateImage(imageBmp);
                        }
                    });
                    break;
                case 3:
                    // message is the coded image quality.
                    final Integer quality = (Integer) message.getMessage();
                    mHandler.post(new Runnable() {
                        @Override
                        public synchronized void run() {
                            //updateSensorProgressBar(quality);
                        }
                    });
                    break;
                // case 4:
                // byte[] enrollcmd = (byte[]) message.getMessage();
            }
        } catch (Exception e) {
            //alert(e.getMessage());
        }
    }

    private void updateImage(Bitmap bitmap) {
        try {
            mFinalCapturedImageBitmap = bitmap;
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void captureFinger(){

           // morphoDeviceCapture(this);
    }

    private void verifyFinger(){
        // For Verification
        VerifyInfo veriFyInstance = VerifyInfo.getInstance();
        veriFyInstance.setAuthenticationMode(AuthenticationMode.File);
        veriFyInstance.setFileName("Filename");
        MorphoInfo verifyInfo = veriFyInstance;
    }



    private void stop() {
        ProcessInfo.getInstance().setStarted(false);

        try {
            if (ProcessInfo.getInstance().isCommandBioStart()) {
                if(ProcessInfo.getInstance().getMorphoDevice()!=null) {
                    ProcessInfo.getInstance().getMorphoDevice().cancelLiveAcquisition();
                }
            }
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    ArrayList<byte[]> templatebyteList=null;
    public RegOrVerFingerDetails verifyFinger(final Observer observer) {

        stop();
        ///////////////////
        VerifyInfo veriFyInstance = VerifyInfo.getInstance();
        veriFyInstance.setAuthenticationMode(AuthenticationMode.File);
        veriFyInstance.setFileName("Filename");
        MorphoInfo verifyInfo = veriFyInstance;
        /////////////////// can be removed later

        final RegOrVerFingerDetails regOrVerFingerDetails=new RegOrVerFingerDetails();

        if(!ManvishPrefConstants.IS_DIAGNOSTIC.read()) {

            if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("1")) {

                templatebyteList = dbAdapter.getTemplatesOfStudentFromRegisteredTableForSessionVerification(ManvishPrefConstants.SELECTED_DATE.read(),
                        ManvishPrefConstants.SELECTED_VENUE.read(), ManvishPrefConstants.SELECTED_BATCH.read(),
                        ManvishPrefConstants.SELECTED_CLASS.read(), ((Activity)context).getIntent().getStringExtra(StringConstants.KEY_STUDENT_ID));

            } else if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("3")) {
                templatebyteList = dbAdapter.getTemplatesOfStudentForVerification(ManvishPrefConstants.SELECTED_DATE.read(),
                        ManvishPrefConstants.SELECTED_VENUE.read(), ManvishPrefConstants.SELECTED_BATCH.read(),
                        ManvishPrefConstants.SELECTED_CLASS.read(), ((Activity)context).getIntent().getStringExtra(StringConstants.KEY_STUDENT_ID));
            }
        }else{
            templatebyteList=new ArrayList<>();
            templatebyteList.add(MeritTrackApplication.getInstance().getDiagonsticTemplate());
        }
        try {

                    Template template;
                    TemplateFVP templateFVP = new TemplateFVP();

                    TemplateList templateList = new TemplateList();

                    //pabitra
                    Log.d("PROCESS","I am inside command thread");
                    for (byte[] templatebyte : templatebyteList) {

                        template = new Template();
                        template.setData(templatebyte);
                        // Manually set template type
                        template.setTemplateType((TemplateType) TemplateType.MORPHO_PK_ISO_FMR);

                        templateList.putTemplate(template);

                    }

                    int timeOut = 10; //pabitra change
                    int far = FalseAcceptanceRate.MORPHO_FAR_5;
                    Coder coderChoice = Coder.MORPHO_DEFAULT_CODER;
                    int detectModeChoice = DetectionMode.MORPHO_VERIF_DETECT_MODE
                            .getValue();
                    int matchingStrategy = 0;

                    int callbackCmd = ProcessInfo.getInstance()
                            .getCallbackCmd();

                    callbackCmd &= ~CallbackMask.MORPHO_CALLBACK_ENROLLMENT_CMD
                            .getValue();

                    ResultMatching resultMatching = new ResultMatching();
/*
                    int ret = morphoDevice
                            .setStrategyAcquisitionMode(ProcessInfo
                                    .getInstance().getStrategyAcquisitionMode());*/

            int ret = morphoDevice
                    .setStrategyAcquisitionMode(StrategyAcquisitionMode.MORPHO_ACQ_EXPERT_MODE);



           /* int ret = morphoDevice
                    .setStrategyAcquisitionMode(ProcessInfo
                            .getInstance().getStrategyAcquisitionMode());*/
            
                    if (ret == 0) {
                        Log.d("PROCESS","I am inside command ret == 0");
                        System.out.println("list of templates=="+templateList);
                        try {
                            ret = morphoDevice.verify(timeOut, far, coderChoice,
                                    detectModeChoice, matchingStrategy,
                                    templateList, callbackCmd, observer,
                                    resultMatching);
                        }catch(Exception e){
                            System.out.println("exception"+e.toString());
                            //alert("FAIL");
                        }

                    }

                    ProcessInfo.getInstance().setCommandBioStart(false);

                    //getAndWriteFFDLogs();

                    String message = "PROCESS";

                    if (ret == ErrorCodes.MORPHO_OK) {
                        message = "Matching Score = "
                                + resultMatching.getMatchingScore()
                                + "\nPK Number = "
                                + resultMatching.getMatchingPKNumber();
                        Log.d("Matching score == ", "PROCESS"+message);

                        //StringConstants.STUDENT_REGISTERED ,these are flags for  both verification and registraion as well
                        //So dont be confused .


                        if (resultMatching.getMatchingScore() > 1000) {

                                    regOrVerFingerDetails.setRetvalue(0);
                                    regOrVerFingerDetails.setEnternalError(morphoDevice.getInternalError());
                                    regOrVerFingerDetails.setFingerImage(mFinalCapturedImageBitmap);
                                    regOrVerFingerDetails.setFingerTemplate(mTemplateData);
                            return regOrVerFingerDetails;
                        }else{

                            regOrVerFingerDetails.setRetvalue(-1);
                            regOrVerFingerDetails.setEnternalError(morphoDevice.getInternalError());
                            regOrVerFingerDetails.setFingerImage(mFinalCapturedImageBitmap);
                            regOrVerFingerDetails.setFingerTemplate(mTemplateData);
                            return regOrVerFingerDetails;
                        }
                    }else {
                        final String msg = message;
                        final int l_ret = ret;
                        final int internalError = morphoDevice.getInternalError();

                        regOrVerFingerDetails.setRetvalue(l_ret);
                        regOrVerFingerDetails.setEnternalError(internalError);
                        regOrVerFingerDetails.setFingerImage(mFinalCapturedImageBitmap);
                        regOrVerFingerDetails.setFingerTemplate(mTemplateData);

                        Log.d("PROCESS","I else of ret==morpho OHK");
                        return regOrVerFingerDetails;
                    }
             //       notifyEndProcess();



        } catch (Exception e) {
           // alert(e.getMessage());
            Log.d("PROCESS","exception");
            regOrVerFingerDetails.setRetvalue(-1);
            regOrVerFingerDetails.setEnternalError(100);// Arbitary no defined by pabitra
            regOrVerFingerDetails.setFingerImage(mFinalCapturedImageBitmap);
            regOrVerFingerDetails.setFingerTemplate(mTemplateData);
            return regOrVerFingerDetails;
        }
    }

    public RegOrVerFingerDetails captureFinger(final Observer observer){

        CaptureInfo.getInstance().setLatentDetect(true);
        CaptureInfo.getInstance().setCaptureType(CaptureType.Verif);
        CaptureInfo.getInstance().setTemplateType(
                TemplateType.MORPHO_PK_ISO_FMR);
        CaptureInfo.getInstance().setTemplateFVPType(
                TemplateFVPType.MORPHO_NO_PK_FVP);

        ProcessInfo.getInstance().setCommandBioStart(true);
        ProcessInfo.getInstance().setStarted(true);


       final RegOrVerFingerDetails regOrVerFingerDetails=new RegOrVerFingerDetails();

        index = 0;
        isCaptureVerif = false;
        final TemplateList templateList = new TemplateList();
        int acquisitionThreshold = 0;
        int advancedSecurityLevelsRequired=255;
        int maxSizeTemplate = 255;
        int  detectModeChoice = DetectionMode.MORPHO_ENROLL_DETECT_MODE
                .getValue();
        boolean exportFVP = false, exportFP = false;
        int timeout =20;
        int nbFinger=1;
        morphoDevice=ProcessInfo.getInstance().getMorphoDevice();
        int ret = morphoDevice.capture(timeout, acquisitionThreshold,
                advancedSecurityLevelsRequired, nbFinger,
                TemplateType.MORPHO_PK_ISO_FMR, TemplateFVPType.MORPHO_NO_PK_FVP, maxSizeTemplate,
                EnrollmentType.ONE_ACQUISITIONS,  LatentDetection.LATENT_DETECT_ENABLE, Coder.MORPHO_DEFAULT_CODER,
                detectModeChoice,
                CompressionAlgorithm.MORPHO_NO_COMPRESS, 0,
                templateList, CallbackMask.MORPHO_CALLBACK_IMAGE_CMD.getValue(), observer);
        ProcessInfo.getInstance().setCommandBioStart(false);

        //getAndWriteFFDLogs();

        String message = "";

            if (ret == ErrorCodes.MORPHO_OK) {
                int NbTemplateFVP = templateList.getNbFVPTemplate();
                int NbTemplate = templateList.getNbTemplate();
                if (MorphoInfo.m_b_fvp) {
                    if (NbTemplateFVP > 0) {
                        TemplateFVP t = templateList.getFVPTemplate(0);
                        message += "Advanced Security Levels Compatibility: "
                                + (t.getAdvancedSecurityLevelsCompatibility() == true ? "Yes"
                                : "NO") + "\n";
                        for (int i = 0; i < NbTemplateFVP; i++) {
                            t = templateList.getFVPTemplate(i);
                            message += "Finger #" + (i + 1)
                                    + " - Quality Score: "
                                    + t.getTemplateQuality() + "\n";
                        }
                    }
                } else {
                    if (NbTemplate > 0) {
                        for (int i = 0; i < NbTemplateFVP; i++) {
                            Template t = templateList.getTemplate(i);
                            message += "Finger #" + (i + 1)
                                    + " - Quality Score: "
                                    + t.getTemplateQuality() + "\n";
                        }
                    }
                }


            }
        final String alertMessage = message;
        final int internalError = morphoDevice.getInternalError();
        final int retvalue = ret;
        regOrVerFingerDetails.setEnternalError(internalError);
        regOrVerFingerDetails.setFingerImage(mFinalCapturedImageBitmap);
        regOrVerFingerDetails.setFingerTemplate(mTemplateData);
        regOrVerFingerDetails.setRetvalue(retvalue);
        //  notifyEndProcess();

        return regOrVerFingerDetails;
    }



}
