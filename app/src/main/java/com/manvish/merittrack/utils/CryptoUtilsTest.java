package com.manvish.merittrack.utils;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.os.Build;

import java.io.File;

/**
 * A tester for the CryptoUtils class.
 *
 * @author www.codejava.net
 */

/**
 * A tester for the CryptoUtils class.
 * @author www.codejava.net
 *
 */
public class CryptoUtilsTest {
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void encryptFile(String filePath, String rootPath, String fileName) {
        String key = "mifaunmanvish005";
        File inputFile = new File(filePath + "\\" + fileName + ".zip");
        File encryptedFile = new File(rootPath + "\\encryptFile\\" + fileName + ".zip");

        try {

            System.out.println("Ens");
            CryptoUtils.encrypt(key, inputFile, encryptedFile);
            System.out.println("Ens");
        } catch (MediaCodec.CryptoException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void deCryptFile(String encFilePath, String decFilePath) {
        String key = "mifaunmanvish005";
        File inputFile = new File(encFilePath);
        File decryptedFile = new File(decFilePath);
        try {

            System.out.println("Ens");
            CryptoUtils.decrypt(key, inputFile, decryptedFile);
            System.out.println("Ens");
        } catch (MediaCodec.CryptoException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
}