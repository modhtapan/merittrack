package com.manvish.merittrack.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import android.widget.Toast;

import com.activeandroid.util.Log;
import com.manvish.merittrack.Model.Flush;
import com.manvish.merittrack.Model.FlushProjectCode;
import com.manvish.merittrack.Model.Project;
import com.manvish.merittrack.Model.Root;
import com.manvish.merittrack.Model.Student;
import com.manvish.merittrack.Model.Venue;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.db.DBAdapter;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;

public class ParseAupdZipFile {

    private Context mContext;
    private String mZipFilePath;
    private String mInternalFileLocation;

    private Root mRMAllXmlDataModel;
    String tag = "ParseAupdZipFile";
    int i = 0;

    public ParseAupdZipFile(Context mContext, String mZipFilePath) {
        this.mContext = mContext;
        this.mZipFilePath = mZipFilePath;

    }

    public ParseAupdZipFile(Context mContext) {
        this.mContext = mContext;
    }

    public boolean parseZipfile() {

        boolean isParsedAndSaved = false;
        //InputStream is = getXmlStreamToParse();

        //Decrypt the original zip file here
        CryptoUtils.decrypt("mifaunmanvish005", new File(mZipFilePath), new File(mZipFilePath));
        // InputStream is = ManvishFileUtils.unzipAndGetInputStream(mZipFilePath);

        String unZipedfolderPath=ManvishFileUtils.unzipAndGetFolderPath(mZipFilePath);
        DBAdapter closeDBAdapter=DBAdapter.getInstance();
        if(closeDBAdapter!=null){
            closeDBAdapter.closeDatabase();
        }
        // return new ParseAndSaveXmlData(mContext).parse(is);

        File f = new File(unZipedfolderPath);
        File file[] = f.listFiles();

        if (file != null && file.length != 0) {
            Log.d("Files", "Size: " + file.length);
            for (int i = 0; i < file.length; i++) {
                Log.d("Files", "FileName:" + file[i].getName());

                if(file[i].getName().contains("config")){
                    isParsedAndSaved=insertConfigFile(file[i]);
                    if(file.length==1 || !isParsedAndSaved){
                       return isParsedAndSaved; //If only one file is there inside folder ,just return
                    }
                }else {
                    Log.d("NO CONFIG","NO CONFIG FILE");
                }
            }
        } else {
            Toast.makeText(mContext,"Empty folder",Toast.LENGTH_SHORT).show();
            return false;
        }

        //Now check if more files are available after deleting the config file
        //Get all files from folder

        File allFiles[]=f.listFiles(); // get all files after deleting the config file

        if(allFiles!=null && allFiles.length!=0){

            for (int i = 0; i < allFiles.length; i++) {
                Log.d("Files", "FileName:" + allFiles[i].getName());
                InputStream is= null;
                try {
                    is = new FileInputStream(allFiles[i]); // Convert file to inputstream for xml parsing
                    isParsedAndSaved=new ParseAndSaveLargeXmlData(mContext).parse(is,allFiles[i].getAbsolutePath());// insert the config file first
                    FileUtils.forceDelete(allFiles[i]);// delete the file now
                    if(!isParsedAndSaved){
                        break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    isParsedAndSaved=false;
                    break;
                }
            }
        }

        return isParsedAndSaved;
    }

    public boolean parseDataFromFolder(String zipedfolderPath) {

        CryptoUtils.decrypt("mifaunmanvish005", new File(mZipFilePath), new File(mZipFilePath));
        // InputStream is = ManvishFileUtils.unzipAndGetInputStream(mZipFilePath);

        String unZipedfolderPath = ManvishFileUtils.unzipAndGetFolderPath(mZipFilePath);

        boolean isParsedAndSaved = false;
        DBAdapter closeDBAdapter=DBAdapter.getInstance();
        if(closeDBAdapter!=null){
            closeDBAdapter.closeDatabase();
        }
        // return new ParseAndSaveXmlData(mContext).parse(is);

        File f = new File(unZipedfolderPath);
        File file[] = f.listFiles();

        if (file != null && file.length != 0) {
            Log.d("Files", "Size: " + file.length);
            for (int i = 0; i < file.length; i++) {
                Log.d("Files", "FileName:" + file[i].getName());

                if (file[i].getName().contains(StringConstants.CONFIG)) {
                    isParsedAndSaved=insertConfigFile(file[i]);
                    if(file.length==1 || !isParsedAndSaved){
                        return isParsedAndSaved; //If only one file is there inside folder ,just return
                    }
                }else {
                    Log.d("NO CONFIG","NO CONFIG FILE");
                }
            }
        } else {
            Toast.makeText(mContext,"Empty folder",Toast.LENGTH_SHORT).show();
            return false;
        }

        //Now check if more files are available after deleting the config file
        //Get all files from folder

        File allFiles[]=f.listFiles(); // get all files after deleting the config file

        if(allFiles!=null && allFiles.length!=0){

            for (int i = 0; i < allFiles.length; i++) {
                Log.d("Files", "FileName:" + allFiles[i].getName());
                InputStream is= null;
                try {
                    is = new FileInputStream(allFiles[i]); // Convert file to inputstream for xml parsing
                    isParsedAndSaved=new ParseAndSaveLargeXmlData(mContext).parse(is,allFiles[i].getAbsolutePath());// insert the config file first
                    FileUtils.forceDelete(allFiles[i]);// delete the file now
                    if(!isParsedAndSaved){
                        break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    isParsedAndSaved=false;
                    break;
                }
            }
        }

        return isParsedAndSaved;
    }



    private boolean insertConfigFile(File file){

        boolean isConfigInsertSuccess=false;

        InputStream is= null;
        try {
            is = new FileInputStream(file); // Convert file to inputstream for xml parsing
            isConfigInsertSuccess=new ParseAndSaveLargeXmlData(mContext).parse(is,file.getAbsolutePath());// insert the config file first
            FileUtils.forceDelete(file);// delete the file now
            //isConfigInsertSuccess=true;
        } catch (Exception e) {
            e.printStackTrace();
            isConfigInsertSuccess=false;
        }

        return isConfigInsertSuccess;
    }

    private InputStream getXmlStreamToParseForDemo() {
        InputStream is = null;

        AssetManager assetManager;
        assetManager = mContext.getAssets();
        try {
            is = assetManager.open("download_data.xml");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return is;
    }

    private boolean isFileExist(String filePath) {
        File zipFile = new File(filePath);
        return zipFile.exists();
    }




}
