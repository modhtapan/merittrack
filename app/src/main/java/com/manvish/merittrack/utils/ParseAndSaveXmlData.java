package com.manvish.merittrack.utils;

import android.content.Context;
import android.text.TextUtils;

import com.manvish.merittrack.Model.Config;
import com.manvish.merittrack.Model.Flush;
import com.manvish.merittrack.Model.FlushProjectCode;
import com.manvish.merittrack.Model.Project;
import com.manvish.merittrack.Model.Root;
import com.manvish.merittrack.Model.Student;
import com.manvish.merittrack.Model.Venue;
import com.manvish.merittrack.constants.ManvishPrefConstants;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by pabitra on 3/9/16.
 */
public class ParseAndSaveXmlData {


    Root rootModel;
    Config config;
    Project project;
    Flush flush;
    FlushProjectCode flushProjectCode;
    Venue venue;
    Student student;
    Student.StudentTemplate template;
     String transictionId;


    //ArrayList<Venue> venueList = new ArrayList<Venue>();

    ArrayList<Student.StudentTemplate> templateList;
    ArrayList<FlushProjectCode> flushProjectCodeArrayList;
    Project.Session session;

    LinkedList<Project.Session> versessionList;
    String text;

    Context mContext;

    public ParseAndSaveXmlData(Context mContext) {
        this.mContext = mContext;
    }

    public boolean parse(InputStream is) {
        XmlPullParserFactory factory = null;
        XmlPullParser parser = null;
        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            parser = factory.newPullParser();

            parser.setInput(is, null);

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = parser.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagname.equalsIgnoreCase("root")) {
                            // create a new instance of employee
                            rootModel = new Root();
                        }

                        if (tagname.equalsIgnoreCase("config")) {
                            // create a new instance of employee
                            config = new Config();
                        }

                        if (tagname.equalsIgnoreCase("project")) {
                            // create a new instance of employee
                            project = new Project();
                        }

                        if (tagname.equalsIgnoreCase("transactionId")) {
                           // rootModel.setTransactionID(parser.nextText());
                            transictionId=parser.nextText();
                            ManvishPrefConstants.transiction_id.write(transictionId);
                        }

                        if (tagname.equalsIgnoreCase("Flush")) {
                            flush=new Flush();
                            flushProjectCodeArrayList=new ArrayList<>();

                        }

                        if(tagname.equalsIgnoreCase("flushCode")){
                            flushProjectCode=new FlushProjectCode();
                            flushProjectCode.setProjectCode(parser.nextText());
                        }
                        // companyDetails
                        if (tagname.equalsIgnoreCase("companyDetails")) {
                            config.setCompanyDetails(parser.nextText());
                        }
                        // logo
                        if (tagname.equalsIgnoreCase("logo")) {
                            config.setLogo(parser.nextText());
                        }
                        // threadScheduling
                        if (tagname.equalsIgnoreCase("threadScheduling")) {
                            config.setThreadScheduling(parser.nextText());

                            //getting configValue ends here ..
                            // So insert data into database here
                        }

                        if (tagname.equalsIgnoreCase("projCode")) {
                            String projectCode=parser.nextText();
                            //IF same project code data exists ,take a backup and delete those data
                            ManvishCommonUtil.deleteExistingProjectData(projectCode,mContext);
                            project.setProjectCode(projectCode);
                        }
                        // // adminLogin

                        if (tagname.equalsIgnoreCase("ProjectName")) {
                            project.setProjectName(parser.nextText());
                        }
                        if (tagname.equalsIgnoreCase("projectPassword")) {
                            project.setProjectPassword(parser.nextText());
                        }
                        if (tagname.equalsIgnoreCase("fingConfig")) {
                            project.setFingerConfig(parser.nextText());
                        }
                        if (tagname.equalsIgnoreCase("extraImages")) {
                            project.setExtraImages(parser.nextText());
                        }
                        if (tagname.equalsIgnoreCase("startDate")) {
                            project.setStartDate(parser.nextText());
                        }
                        if (tagname.equalsIgnoreCase("enddate")) {
                            project.setEndDate(parser.nextText());
                        }
                        if (tagname.equalsIgnoreCase("capturephoto")) {

                            if(TextUtils.isEmpty(parser.nextText())) {
                                project.setCapturePhoto(parser.nextText());
                            }else{
                                project.setCapturePhoto(" ");
                            }
                        }
                        if (tagname.equalsIgnoreCase("captureimage")) {
                            project.setCaptureImage(parser.nextText());
                        }

                        if (tagname.equalsIgnoreCase("Stage")) {
                            project.setStage(parser.nextText());


                        }
                        if (tagname.equalsIgnoreCase("verSessions")) {
                            project.setNoOfVerSession(parser.nextText());
                        }

                        if (tagname.equalsIgnoreCase("listOfVerSessions")) {

                            versessionList=new LinkedList<>();

                        }
                        if(tagname.equalsIgnoreCase("Session")){
                            session =new Project().new Session();
                        }
                        else if(session!=null){

                            if(tagname.equalsIgnoreCase("code")){
                                session.setSessionCode(parser.nextText());
                            }

                            if(tagname.equalsIgnoreCase("date")){
                                session.setDate(parser.nextText());
                            }
                        }


                        // VenueParse

                        if (tagname.equalsIgnoreCase("Venue")) {
                            venue = new Venue();
                        } else if (venue != null) {
                            if (tagname.equalsIgnoreCase("venueCode")) {
                                venue.setVenueCode(parser.nextText());
                            }


                            if (tagname.equalsIgnoreCase("noOfStudents")) {
                                venue.setNoOfStudents(parser.nextText());
                            }
                            if (tagname.equalsIgnoreCase("venueName")) {
                                venue.setVenueName(parser.nextText());
                            }

                        }

                        //iniitialize studentlist here
                        if (tagname.equalsIgnoreCase("StudentDetails")) {

                           // studentList = new ArrayList<Student>();
                        }

                        // For student
                        if (tagname.equalsIgnoreCase("student")) {
                            student = new Student();
                            templateList = new ArrayList<Student.StudentTemplate>();

                        } else if (student != null) {

                            if (tagname.equalsIgnoreCase("StudentId")) {

                                student.setStudentID(parser.nextText());

                            } else if (tagname.equalsIgnoreCase("name")) {
                                student.setName(parser.nextText());
                            } else if (tagname.equalsIgnoreCase("testdate")) {
                                student.setTestDate(parser.nextText());
                            } else if (tagname.equalsIgnoreCase("batchCode")) {
                                student.setBatchCode(parser.nextText());
                            } else if (tagname.equalsIgnoreCase("classRoom")) {
                                student.setClassRoom(parser.nextText());
                            } else if (tagname.equalsIgnoreCase("photograph")) {
                                student.setPhotograph(parser.nextText());
                            }
                            student.setProjectCode(project.getProjectCode());
                            student.setVenueCode(venue.getVenueCode()); //need to rewrite this code


                            if (tagname.equalsIgnoreCase("template")) {
                                template = new Student().new StudentTemplate();
                            } else if (template != null) {
                                if (tagname.equalsIgnoreCase("type")) {
                                    template.setType(parser.nextText());
                                }

                                if (tagname.equalsIgnoreCase("tData")) {
                                    template.setData(parser.nextText());
                                }

                            }
                        }

                        // TemplateParse ========



                        break;

                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;

                    case XmlPullParser.END_TAG:

                        if (tagname.equalsIgnoreCase("config")) {

                            ManvishCommonUtil.insertConfigData(config,transictionId,mContext);
                        }
                        if (tagname.equalsIgnoreCase("flushcode")) {
                            flushProjectCodeArrayList.add(flushProjectCode);
                        }

                        if (tagname.equalsIgnoreCase("Flush")) {
                            flush.setFlushProjectCodeList(flushProjectCodeArrayList);
                            ManvishCommonUtil.flushData(flush,mContext); // Flush the data here
                        }

                        if (tagname.equalsIgnoreCase("Session")) {
                            versessionList.add(session);

                        }

                        if (tagname.equalsIgnoreCase("listOfVerSessions")) {
                           project.setLisOfVerSession(versessionList);

                        }


                        if (tagname.equalsIgnoreCase("transactionId")) {
                           // transictionId=rootModel.getTransactionID();
                            //ManvishPrefConstants.transiction_id.write(transictionId);

                        }

                        if (tagname.equalsIgnoreCase("template")) {
                            templateList.add(template);

                        }

                        if (tagname.equalsIgnoreCase("student")) {
                            student.setStudentTemplateList(templateList);
                            ManvishCommonUtil.insertStudentDetails(project,student,mContext);
                           // studentList.add(student);
                        }

                        if (tagname.equalsIgnoreCase("Venue")) {

                           // venue.setStudentList(studentList);
                            ManvishCommonUtil.insertVenueDetails(project,venue,mContext);
                           // venueList.add(venue);
                        }

                        if (tagname.equalsIgnoreCase("Project")) {


                           // project.setLisOfVerSession(versessionList);
                           // project.setVenueList(venueList);
                            ManvishCommonUtil.insertProjectDetails(project,mContext);
                            ManvishCommonUtil.insertSessionDetails(project,mContext);
                        }

                        if (tagname.equalsIgnoreCase("root")) {
                            // add employee object to list
                           // rootModel.setFlush(flush);
                           // rootModel.setConfig(config);
                           // rootModel.setProject(project);
                        }

                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }

        } catch (XmlPullParserException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }




}
