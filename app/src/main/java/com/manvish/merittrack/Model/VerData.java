package com.manvish.merittrack.Model;

/**
 * Created by pabitra on 4/23/16.
 */
public class VerData {

//
//    String CREATE_TABLE_VERIFICATION = "create table " + TABLE_VERIFICATION + " ( " + PRIMARY_KEY + "
// INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , " + PROJECT_CODE + " varchar ,"
//            + VENUE_CODE+ " varchar ," + STUDENT_ID + " varchar ," + STUDENT_NAME + " varchar" +
//            "," + TEST_DATE + " varchar" +
//            "," + BATCH_CODE + " varchar" +
//            "," + CLASS_ROOM + " varchar"  +
//            "," + VER_STATUS + " varchar ," + VER_DATE + " varchar," + VER_TIME + " varchar," + SESSION_CODE + " varchar," + SESSION_DATE + " varchar ," + UPLOAD_FLAG + " varchar) ";


      String studentID;
      String studentName;
      String testDate;
        String batchCode;
        String classRoom;
        String verStatus;
        String verDate;
        String verTime;
        String sessionCode;
    String sessionDate;
    String fingerType;
    String venueCode;

    public String getVenueCode() {
        return venueCode;
    }

    public void setVenueCode(String venueCode) {
        this.venueCode = venueCode;
    }

    public String getFingerType() {
        return fingerType;
    }

    public void setFingerType(String fingerType) {
        this.fingerType = fingerType;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getTestDate() {
        return testDate;
    }

    public void setTestDate(String testDate) {
        this.testDate = testDate;
    }

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getVerStatus() {
        return verStatus;
    }

    public void setVerStatus(String verStatus) {
        this.verStatus = verStatus;
    }

    public String getVerDate() {
        return verDate;
    }

    public void setVerDate(String verDate) {
        this.verDate = verDate;
    }

    public String getVerTime() {
        return verTime;
    }

    public void setVerTime(String verTime) {
        this.verTime = verTime;
    }

    public String getSessionCode() {
        return sessionCode;
    }

    public void setSessionCode(String sessionCode) {
        this.sessionCode = sessionCode;
    }

    public String getSessionDate() {
        return sessionDate;
    }

    public void setSessionDate(String sessionDate) {
        this.sessionDate = sessionDate;
    }
}
