package com.manvish.merittrack.Model;

/**
 * Created by pabitra on 4/22/16.
 */
public class RegistredData {

//    String CREATE_TABLE_REGISTRATION = "create table " + TABLE_REGISTRATION + " ( " + PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , " + PROJECT_CODE + " varchar ,"
//            + VENUE_CODE+ " varchar ," + STUDENT_ID + " varchar ," + STUDENT_NAME + " varchar" +
//            "," + TEST_DATE + " varchar" +
//            "," + BATCH_CODE + " varchar" +
//            "," + CLASS_ROOM + " varchar ," + PHOTO_GRAPH + " varchar" +" varchar ," + FINGER_IMAGE +
//            "," + REG_STATUS + " varchar ," + TEMPLATE_TYPE + " varchar," + TEMPLATE_DATA + " varchar," + REGISTRATION_DATE + " varchar," + REGISTRATION_TIME + " varchar," + UPLOAD_FLAG + " varchar) ";

    String projectCode;
    String venueCode;
    String studentID;
    String studentName;
    String testDate;

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getVenueCode() {
        return venueCode;
    }

    public void setVenueCode(String venueCode) {
        this.venueCode = venueCode;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getTestDate() {
        return testDate;
    }

    public void setTestDate(String testDate) {
        this.testDate = testDate;
    }

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public byte[] getFingerImage() {
        return fingerImage;
    }

    public void setFingerImage(byte[] fingerImage) {
        this.fingerImage = fingerImage;
    }

    public String getTemplateType() {
        return templateType;
    }

    public void setTemplateType(String templateType) {
        this.templateType = templateType;
    }

    public byte[] getTemplateData() {
        return templateData;
    }

    public void setTemplateData(byte[] templateData) {
        this.templateData = templateData;
    }

    public String getRegStatus() {
        return regStatus;
    }

    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getRegTime() {
        return regTime;
    }

    public void setRegTime(String regTime) {
        this.regTime = regTime;
    }

    String batchCode;
    String classRoom;
    byte[] fingerImage;
    String templateType;
    byte[] templateData;
    String regStatus;
    String regDate;
    String regTime;
}
