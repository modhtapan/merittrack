package com.manvish.merittrack.Model;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by pabitra on 4/9/16.
 */
public class Project {

    String projectCode;
    String projectName;
    String projectPassword;
    String fingerConfig; // it's a comma separated string of integers ,split it to extract values .
    String extraImages; // it's a comma separated string of integers ,split it to extract values .
    String startDate;
    String endDate;

    public ArrayList<Venue> getVenueList() {
        return venueList;
    }

    public void setVenueList(ArrayList<Venue> venueList) {
        this.venueList = venueList;
    }

    ArrayList<Venue> venueList;

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectPassword() {
        return projectPassword;
    }

    public void setProjectPassword(String projectPassword) {
        this.projectPassword = projectPassword;
    }

    public String getFingerConfig() {
        return fingerConfig;
    }

    public void setFingerConfig(String fingerConfig) {
        this.fingerConfig = fingerConfig;
    }

    public String getExtraImages() {
        return extraImages;
    }

    public void setExtraImages(String extraImages) {
        this.extraImages = extraImages;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCapturePhoto() {
        return capturePhoto;
    }

    public void setCapturePhoto(String capturePhoto) {
        this.capturePhoto = capturePhoto;
    }

    public String getCaptureImage() {
        return captureImage;
    }

    public void setCaptureImage(String captureImage) {
        this.captureImage = captureImage;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getNoOfVerSession() {
        return noOfVerSession;
    }

    public void setNoOfVerSession(String noOfVerSession) {
        this.noOfVerSession = noOfVerSession;
    }

    String capturePhoto;
    String captureImage;
    String stage;
    String noOfVerSession;// it's a integer ,so convert it while determinig .
    LinkedList<Session> lisOfVerSession;

    public LinkedList<Session> getLisOfVerSession() {
        return lisOfVerSession;
    }

    public void setLisOfVerSession(LinkedList<Session> lisOfVerSession) {
        this.lisOfVerSession = lisOfVerSession;
    }

   public class Session{

        String sessionCode;
        String date;

        public String getSessionCode() {
            return sessionCode;
        }

        public void setSessionCode(String sessionCode) {
            this.sessionCode = sessionCode;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
    }
}
