package com.manvish.merittrack.Model;

import android.graphics.Bitmap;

/**
 * (C) Koninklijke Philips N.V., 2015.
 * All rights reserved.
 */
public class RegOrVerFingerDetails {

    Bitmap fingerImage;
    byte[] fingerTemplate;
    boolean isRegistered;
    boolean isVerified;
    int  enternalError;
    int retvalue;


    public int getRetvalue() {
        return retvalue;
    }

    public void setRetvalue(final int retvalue) {
        this.retvalue = retvalue;
    }

    public int getEnternalError() {
        return enternalError;
    }

    public void setEnternalError(final int enternalError) {
        this.enternalError = enternalError;
    }

    public Bitmap getFingerImage() {
        return fingerImage;
    }

    public void setFingerImage(final Bitmap fingerImage) {
        this.fingerImage = fingerImage;
    }

    public byte[] getFingerTemplate() {
        return fingerTemplate;
    }

    public void setFingerTemplate(final byte[] fingerTemplate) {
        this.fingerTemplate = fingerTemplate;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public void setRegistered(final boolean registered) {
        isRegistered = registered;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified(final boolean verified) {
        isVerified = verified;
    }



}
