package com.manvish.merittrack.Model;

import java.util.ArrayList;

/**
 * Created by pabitra on 4/9/16.
 */
public class Flush {

    ArrayList<FlushProjectCode> flushProjectCodeList;

    public ArrayList<FlushProjectCode> getFlushProjectCodeList() {
        return flushProjectCodeList;
    }

    public void setFlushProjectCodeList(ArrayList<FlushProjectCode> flushProjectCodeList) {
        this.flushProjectCodeList = flushProjectCodeList;
    }
}
