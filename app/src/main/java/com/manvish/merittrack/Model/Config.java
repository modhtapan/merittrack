package com.manvish.merittrack.Model;

/**
 * Created by pabitra on 4/9/16.
 */
public class Config {

    String companyDetails;
    String logo;

    public String getCompanyDetails() {
        return companyDetails;
    }

    public void setCompanyDetails(String companyDetails) {
        this.companyDetails = companyDetails;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getThreadScheduling() {
        return threadScheduling;
    }

    public void setThreadScheduling(String threadScheduling) {
        this.threadScheduling = threadScheduling;
    }

    String threadScheduling;
}
