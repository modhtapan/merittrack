package com.manvish.merittrack.Model;

/**
 * Created by pabitra on 4/9/16.
 */
public class Data {
    Project project;

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
