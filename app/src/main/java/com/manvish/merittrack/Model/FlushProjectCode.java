package com.manvish.merittrack.Model;

/**
 * Created by pabitra on 4/9/16.
 */
public class FlushProjectCode {

    String projectCode;

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }
}
