package com.manvish.merittrack.Model;

import java.util.ArrayList;

/**
 * Created by pabitra on 4/9/16.
 */
public class Student {

    String studentID;
    String name;
    String testDate;
    String regStatus;
    String verStatus;
    String sessionCode;

    public ArrayList<byte[]> getTemplatebyteList() {
        return templatebyteList;
    }

    public void setTemplatebyteList(final ArrayList<byte[]> templatebyteList) {
        this.templatebyteList = templatebyteList;
    }

    ArrayList<byte[]> templatebyteList;

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(final String projectCode) {
        this.projectCode = projectCode;
    }

    String projectCode;
    String venueCode;

    public String getVenueCode() {
        return venueCode;
    }

    public void setVenueCode(final String venueCode) {
        this.venueCode = venueCode;
    }

    public String getSessionCode() {
        return sessionCode;
    }

    public void setSessionCode(String sessionCode) {
        this.sessionCode = sessionCode;
    }

    public String getRegStatus() {
        return regStatus;
    }

    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    public String getVerStatus() {
        return verStatus;
    }

    public void setVerStatus(String verStatus) {
        this.verStatus = verStatus;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTestDate() {
        return testDate;
    }

    public void setTestDate(String testDate) {
        this.testDate = testDate;
    }

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getPhotograph() {
        return photograph;
    }

    int photoCount;

    public int getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(int photoCount) {
        this.photoCount = photoCount;
    }

    public void setPhotograph(String photograph) {
        this.photograph = photograph;
    }

    public ArrayList<StudentTemplate> getStudentTemplateList() {
        return StudentTemplateList;
    }

    public void setStudentTemplateList(ArrayList<StudentTemplate> studentTemplateList) {
        StudentTemplateList = studentTemplateList;
    }

    String batchCode;
    String classRoom;
    String photograph; // It's a base64 String format from server
    ArrayList<StudentTemplate> StudentTemplateList; // For pretest Template will not come so ,Handle it ..



    public class StudentTemplate {
        String type;
        String data;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }
    }
}
