package com.manvish.merittrack.application;

import android.app.Application;
import android.content.Context;
import android.provider.Settings;

import com.crashlytics.android.Crashlytics;
import com.manvish.merittrack.Model.Student;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.utils.ManvishCommonUtil;

import java.util.LinkedHashMap;

import io.fabric.sdk.android.Fabric;

public class MeritTrackApplication extends Application {

    private static MeritTrackApplication thisInstance;
    private static Context context;


    String dbName;
    public static int mLowLevel=0;

    public byte[] getDiagonsticTemplate() {
        return diagonsticTemplate;
    }

    public void setDiagonsticTemplate(byte[] diagonsticTemplate) {
        this.diagonsticTemplate = diagonsticTemplate;
    }

    byte[] diagonsticTemplate;

    public String getDbName() {

        if(dbName==null){
            dbName=ManvishPrefConstants.DB_NAME.read();
        }
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public static synchronized MeritTrackApplication getInstance() {
        return thisInstance;
    }

    private LinkedHashMap<String,Student> studentListMap;

    public  LinkedHashMap<String,Student> getStudentListMap() {
        return studentListMap;
    }

    public  void setStudentListMap(LinkedHashMap<String,Student> studentListMap) {
       this.studentListMap = studentListMap;
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        MeritTrackApplication.context = getApplicationContext();
        thisInstance = this;
        ManvishPrefConstants.SERIAL_NO.write(ManvishCommonUtil.getSerialNo());
        ManvishPrefConstants.IS_FIRSTTIME_LUNCH.write(true);
        enableUnknownSourceInstallation();

    }

    public static Context getAppContext() {
        return MeritTrackApplication.context;
    }

    private void enableUnknownSourceInstallation(){

        try {
            Settings.Global.putInt(getContentResolver(), Settings.Global.INSTALL_NON_MARKET_APPS, 1);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
