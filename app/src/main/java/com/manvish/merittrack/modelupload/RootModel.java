package com.manvish.merittrack.modelupload;


public class RootModel {

    private Acslog acslog;
    private ProjectDetails projectDetails;
    private EnrData enrData;

    public Acslog getAcslog() {
        return acslog;
    }

    public void setAcslog(Acslog acslog) {
        this.acslog = acslog;
    }

    public ProjectDetails getProjectDetails() {
        return projectDetails;
    }

    public void setProjectDetails(ProjectDetails projectDetails) {
        this.projectDetails = projectDetails;
    }

    public EnrData getEnrData() {
        return enrData;
    }

    public void setEnrData(EnrData enrData) {
        this.enrData = enrData;
    }
}
