package com.manvish.merittrack.modelupload;


import java.util.List;

public class EnrData {

    private List<EnrStudentWrite> student;

    public List<EnrStudentWrite> getStudent() {
        return student;
    }

    public void setStudent(List<EnrStudentWrite> student) {
        this.student = student;
    }
}
