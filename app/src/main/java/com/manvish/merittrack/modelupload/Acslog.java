package com.manvish.merittrack.modelupload;


import java.util.List;

public class Acslog {

    private List<AcsStudentWrite> student;

    public List<AcsStudentWrite> getStudent() {
        return student;
    }

    public void setStudent(List<AcsStudentWrite> student) {
        this.student = student;
    }
}
