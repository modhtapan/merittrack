package com.manvish.merittrack.modelupload;

/**
 * Created by tapan on 12/3/16.
 */
public class TemplateWrite {

    private String fingerType;
    private String tData;
    private String regStatus;
    private String verStatus;

    public String getFingerType() {
        return fingerType;
    }

    public void setFingerType(String fingerType) {
        this.fingerType = fingerType;
    }

    public String gettData() {
        return tData;
    }

    public void settData(String tData) {
        this.tData = tData;
    }

    public String getRegStatus() {
        return regStatus;
    }

    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    public String getVerStatus() {
        return verStatus;
    }

    public void setVerStatus(String verStatus) {
        this.verStatus = verStatus;
    }
}
