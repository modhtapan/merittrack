package com.manvish.merittrack.modelupload;

import java.util.List;

/**
 * Created by tapan on 12/3/16.
 */
public class EnrStudentWrite {

    private String studentId;
    private String unitId;
    private String projCode;
    private String regDate;
    private String regTime;
    private String photograph;

    private List<TemplateWrite> template;
    private List<ImageWrite> image;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getProjCode() {
        return projCode;
    }

    public void setProjCode(String projCode) {
        this.projCode = projCode;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getRegTime() {
        return regTime;
    }

    public void setRegTime(String regTime) {
        this.regTime = regTime;
    }

    public String getPhotograph() {
        return photograph;
    }

    public void setPhotograph(String photograph) {
        this.photograph = photograph;
    }

    public List<TemplateWrite> getTemplate() {
        return template;
    }

    public void setTemplate(List<TemplateWrite> template) {
        this.template = template;
    }

    public List<ImageWrite> getImage() {
        return image;
    }

    public void setImage(List<ImageWrite> image) {
        this.image = image;
    }
}
