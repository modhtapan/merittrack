package com.manvish.merittrack.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DownloadIntentService extends IntentService {

    public static final int REQUEST_URL_AUPD = 1001;// AUPD
    public static final int REQUEST_URL_TIME = 1002;// AUPD TIME

    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;
    private static final String TAG = DownloadIntentService.class.getName();
    private String URL_TAG;
    private byte[] data = null;

    public DownloadIntentService() {
        super(DownloadIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.d(TAG, "Service Started!");

        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        String url = intent.getStringExtra("url");
        URL_TAG = intent.getStringExtra(StringConstants.URL_TAG_KEY);
        data = intent.getByteArrayExtra("DATA");
        Bundle bundle = new Bundle();

        if (!TextUtils.isEmpty(url)) {
            /* Update UI: Download Service is Running */
			/* Service Started */
            receiver.send(STATUS_RUNNING, Bundle.EMPTY);

            try {
                byte[] results = downloadData(url, data);

				/* Sending result back to activity */
                if (null != results && results.length > 0) {
                    bundle.putByteArray("result", results);
                    bundle.putString(StringConstants.URL_TAG_KEY, URL_TAG);
                    Log.d("DEBUG", "print tag==" + URL_TAG);
                    receiver.send(STATUS_FINISHED, bundle);
                } else {

                    // because for url ta acknoledgement ,its comes null or 0
                    // also
                    if (URL_TAG.equalsIgnoreCase(StringConstants.URL_TAG_AKN)) {

                        bundle.putByteArray("result", results);
                        bundle.putString(StringConstants.URL_TAG_KEY, URL_TAG);
                        Log.d("DEBUG", "print tag==" + URL_TAG);
                        receiver.send(STATUS_FINISHED, bundle);
                    } else {
                        bundle.putString(Intent.EXTRA_TEXT, "SERVER Error");
                        bundle.putString(StringConstants.URL_TAG_KEY, URL_TAG);
                        Log.d("DEBUG", "print tag==nulll" + URL_TAG);
                        receiver.send(STATUS_ERROR, bundle);
                    }
                }
            } catch (Exception e) {
                Log.d("DEBUG", "print tag==fail" + URL_TAG);
				/* Sending error message back to activity */

                bundle.putString(Intent.EXTRA_TEXT, e.toString());
                bundle.putString(StringConstants.URL_TAG_KEY, URL_TAG);
                receiver.send(STATUS_ERROR, bundle);
            }
        }
        Log.d(TAG, "Service Stopping!");
        this.stopSelf();
    }

    private byte[] downloadData(String requestUrl, byte[] data) {

        try {

            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
            HttpConnectionParams.setSoTimeout(httpParameters, 20000);

            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpPost httppost = new HttpPost(requestUrl);
            httppost.setHeader("content-type",
                    "application/x-www-form-urlencoded");

            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);
            // Every input is String format
            nameValuePairs.add(new BasicNameValuePair("UNITID",
                    ManvishPrefConstants.SERIAL_NO.read()));
            nameValuePairs.add(new BasicNameValuePair("TIME", "07:15:10"));
            nameValuePairs.add(new BasicNameValuePair("DATE", "17/12/2015"));
            nameValuePairs.add(new BasicNameValuePair("MODEL",
                    StringConstants.MODEL_NO));
            nameValuePairs.add(new BasicNameValuePair("LONGITUDE", "-"));
            nameValuePairs.add(new BasicNameValuePair("LATITUDE", "-"));

            if (URL_TAG.equalsIgnoreCase(StringConstants.URL_TAG_AUPD)) {
                // AUPD no buffer(data)
                nameValuePairs.add(new BasicNameValuePair("SIZE", "0"));
                nameValuePairs.add(new BasicNameValuePair("DATA", "NA"));
            }
            if (URL_TAG.equalsIgnoreCase(StringConstants.URL_TAG_TIME)) {
                // TIME no buffer (data)
                nameValuePairs.add(new BasicNameValuePair("SIZE", "0"));
                nameValuePairs.add(new BasicNameValuePair("DATA", "NA"));
            }
            if (URL_TAG.equalsIgnoreCase(StringConstants.URL_TAG_AKN)) {
                // AKnowledgement = SUCC = Transaction-ID ,buffer(data)
                nameValuePairs.add(new BasicNameValuePair("SIZE", "20"));

                System.out.println("print transaction ID"
                        + ManvishPrefConstants.transiction_id.read());

                // Sending side
                byte[] transiction_idByte = ManvishPrefConstants.transiction_id
                        .read().getBytes("UTF-8");
                String transictionIDBase64 = Base64.encodeToString(
                        transiction_idByte, Base64.DEFAULT);

                nameValuePairs.add(new BasicNameValuePair("DATA",
                        transictionIDBase64));
            }
            if (URL_TAG.equalsIgnoreCase(StringConstants.URL_TAG_ENRL)) {
                // Enrollment == xml to byte[] to Bae64 format
                nameValuePairs.add(new BasicNameValuePair("SIZE", ""
                        + data.length));
                String base64String = Base64.encodeToString(data,
                        Base64.DEFAULT);
                nameValuePairs
                        .add(new BasicNameValuePair("DATA", base64String));
            }

            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = httpclient.execute(httppost);

            if (response != null) {
                StatusLine stLine = response.getStatusLine();
                int StatusCode = stLine.getStatusCode();
                // Check for 200 (success)

				/* 200 represents HTTP OK */
                if (StatusCode == 200) {

                    System.out.println("Print status Code==" + StatusCode);
                    byte[] lAUPDHEaderByte = null;
                    try {
                        HttpEntity httpEntity = response.getEntity();
                        if (httpEntity != null) {
                            lAUPDHEaderByte = saveHttpEnityDataTofileAndgetAUPDHeader(httpEntity);
                            // saveHttpEnityDataTofile(httpEntity);
                            //b = Base64.decode(IOUtils.toString(httpEntity.getContent()), Base64.DEFAULT);

                            //byte[] first48Byte=getFirst48Byte();
                        }
                        return lAUPDHEaderByte;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                } else {
                    return null;
                    // System.out.println("print status code==" + StatusCode);
                    // throw new DownloadException("Failed to fetch data!!");

                }
            } else {
                System.out.println("null value");
                return null;
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    BufferedInputStream bis = null;
    int ret_read = 0;
    byte[] buff = null;

    public byte[] saveHttpEnityDataTofileAndgetAUPDHeader(HttpEntity httpEntity) {

        File folderMeritTrak = new File(Environment.getExternalStorageDirectory() + "/MeritTrack");
        if (!folderMeritTrak.exists()) {
            folderMeritTrak.mkdir();
            System.out.println("new MeritTrak folder created");
        }

        File aupdHeaderFile = new File(Environment.getExternalStorageDirectory() + "/MeritTrack/header.txt");

        File folder = new File(Environment.getExternalStorageDirectory() + "/MeritTrack/Download");
        if (!folder.exists()) {
            folder.mkdir();
            System.out.println("new folder created");
        }
        final File xmlDataFile = new File(Environment.getExternalStorageDirectory() + "/MeritTrack/Download/" + "AUPD.zip");

        try {
            bis = new BufferedInputStream(httpEntity.getContent());

            OutputStream header = new FileOutputStream(aupdHeaderFile);
            int readsize = 1024 * 1024;
            buff = new byte[readsize];

            //Block for reading header
            ret_read = bis.read(buff, 0, 48);
            header.write(buff, 0, 48);
            Log.d("header size", Integer.toString(buff.length));
            Log.d("header size", buff.toString());
            header.flush();
            header.close();
            byte[] headerByte = getByteFromFile(aupdHeaderFile);
            final long lAUPDFileSize = getAUPDFileSizeFromHeader(headerByte);

            //Block for reading zip
            if (lAUPDFileSize > 0) {

                OutputStream output = null;

                output = new FileOutputStream(xmlDataFile);

                while ((ret_read = bis.read(buff)) != -1) {
                    output.write(buff, 0, ret_read);
                    Log.d("Read", Integer.toString(ret_read));
                }
                output.flush();
                output.close();
                bis.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getByteFromFile(aupdHeaderFile);
    }

    private long getAUPDFileSizeFromHeader(final byte[] aupdHeader) {

        byte[] aupdHeaderZipFileSize = Arrays.copyOfRange(aupdHeader,
                11 + 11 + 11 + 11, 11 + 11 + 11 + 11 + 4);

        long zipFileSize = byteToInt(aupdHeaderZipFileSize, 4);
        return zipFileSize;
    }

    private byte[] getByteFromFile(File AUPDHeaderFile) {

        try {
            return FileUtils.readFileToByteArray(AUPDHeaderFile);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public long byteToInt(byte[] bytes, int length) {
        int val = 0;
        if (length > 4)
            throw new RuntimeException("Too big to fit in int");
        for (int i = 0; i < length; i++) {
            val = val << 8;
            val = val | (bytes[i] & 0xFF);
        }
        return val;
    }


}