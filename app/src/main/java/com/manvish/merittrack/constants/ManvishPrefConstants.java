package com.manvish.merittrack.constants;

public class ManvishPrefConstants {

    // sample to follow to declare shared preference constants ==



    public static final  ManvishPreference<String> LATITUDE=new ManvishPreference<String>
            ("latitude",null,ManvishPreference.stringHandler);

    public static final  ManvishPreference<String> LONGITUDE=new ManvishPreference<String>
            ("longitude",null,ManvishPreference.stringHandler);

    public static final ManvishPreference<String> MAC_ADDRESS = new ManvishPreference<String>(
            "macaddress", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> SERVER_IP = new ManvishPreference<String>(
            "serverip", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> SERVER_PORT = new ManvishPreference<String>(
            "serverport", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> transiction_id = new ManvishPreference<String>(
            "transictionid", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> STAGE = new ManvishPreference<String>(
            "stage", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> THREAD_SCHEDULING = new ManvishPreference<String>(
            "threadscheduling", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> SERIAL_NO = new ManvishPreference<String>(
            "serialno", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> UNIT_ID = new ManvishPreference<String>(
            "unitid", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<Boolean> LOG_IN_FLAG = new ManvishPreference<Boolean>(
            "loginflag", false, ManvishPreference.booleanHandler);

    public static final ManvishPreference<Boolean> IS_GPRS_ENABLE = new ManvishPreference<Boolean>(
            "isgprsenabled", false, ManvishPreference.booleanHandler);


    public static final ManvishPreference<String> FINGERCONFIG = new ManvishPreference<String>(
            "fingerConfig", "", ManvishPreference.stringHandler);

    public static final ManvishPreference<String> EXTRAIMAGES = new ManvishPreference<String>(
            "extraimages", "", ManvishPreference.stringHandler);

    // determine ,it's PRETEST ,POSTTEST,PRECOUNCIL,POSTCOUNCIL
    public static final ManvishPreference<String> SELECTED_EVENT_STAGE = new ManvishPreference<String>(
            "event_stage", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> SELECTED_STATS = new ManvishPreference<String>(
            "stats", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> SELECTED_REG_OR_VER = new ManvishPreference<String>(
            "regorver", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> SELECTED_EVENT_CODE = new ManvishPreference<String>(
            "event_code", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> SELECTED_DATE = new ManvishPreference<String>(
            "selecteddate", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> SELECTED_VENUE = new ManvishPreference<String>(
            "selectedvenue", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> SELECTED_BATCH = new ManvishPreference<String>(
            "selectedbatch", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> SELECTED_CLASS = new ManvishPreference<String>(
            "selectedclass", null, ManvishPreference.stringHandler);

    //COMPANY_LOGO

    public static final ManvishPreference<String> COMPANY_LOGO = new ManvishPreference<String>(
            "company_logo", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> COMPANY_NAME = new ManvishPreference<String>(
            "company_name", "", ManvishPreference.stringHandler);

    public static final ManvishPreference<Boolean> IS_TEST = new ManvishPreference<Boolean>(
            "is_test", false, ManvishPreference.booleanHandler);

    public static final ManvishPreference<String> FINGERPRINTBASE64STRING = new ManvishPreference<String>(
            "fingerprintbase64string", "", ManvishPreference.stringHandler);

    public static final ManvishPreference<String> SELECTED_SESSION_CODE = new ManvishPreference<String>(
            "selectedsessioncode", "", ManvishPreference.stringHandler);

    public static final ManvishPreference<String> SELECTED_SESSION_DATE = new ManvishPreference<String>(
            "selectedsessiondate", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> TOTAL_STUDENT = new ManvishPreference<String>(
            "totalStudent", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> TOTAL_STUDENT_REG_VER_SUCC = new ManvishPreference<String>(
            "totalStudentregversucc", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> TOTAL_STUDENT_REG_VER_FAIL = new ManvishPreference<String>(
            "totalStudentregverfail", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> DB_NAME = new ManvishPreference<String>(
            "db_name", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<String> PROJECT_PASSWORD = new ManvishPreference<String>(
            "project_password", null, ManvishPreference.stringHandler);

    public static final ManvishPreference<Boolean> IS_DIAGNOSTIC = new ManvishPreference<Boolean>(
            "is_diagnostic", false, ManvishPreference.booleanHandler);

    public static final ManvishPreference<Boolean> IS_LOCKED = new ManvishPreference<Boolean>(
            "is_locked", false, ManvishPreference.booleanHandler);

    public static final ManvishPreference<Boolean> IS_FIRSTTIME_LUNCH= new ManvishPreference<Boolean>(
            "first_time_lunch", false, ManvishPreference.booleanHandler);

}
