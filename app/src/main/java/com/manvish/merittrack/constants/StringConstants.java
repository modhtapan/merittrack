package com.manvish.merittrack.constants;

import java.util.regex.Pattern;

/**
 * StringConstants for constants to use in app
 */
public class StringConstants {


    public static String TAG="MERITTRAC";
    // menu_items array for menuactivity lists
    public static final String[] MENU_ITEMS = {
            "EVENT ACTIVITY",
            "TEST ACTIVITY",
            "DIAG REPORT",
            "MIFAUN ACTIVITY",
            "VIEW MIFAUN DETAILS",
            "STATISTICS",
            "UPDATE FIRMWARE",
            "LOCK APP",
            "Copy data"

    };


/*    public static final String IPADDRESS_PATTERN = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";*/

    //For Battery purpose
    public static final String BATTERY_LEVEL_CHANGE = "BATTERY_LEVEL_CHANGE";

    public static final Pattern PATTERN = Pattern.compile(
            "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");


    public static final String[] EVENT_ITEMS = {
            "Pre Test",
            "Post Test",
            "Pre Counseling",
            "Post Counseling",
           /* "Backup Post Test",
            "Backup Post Counseling",*/
            "USB Backup Post Test",
            "USB Backup Post Counseling"

    };

    public static final String[] EVENT_CODES = {
            "9210",
            "9310",
            "2101",
            "1100",
            "1120",

    };


    public static final String[] DATE_LIST = {
            "10-04-2016",
            "12-04-2016",
            "14-04-2016",

    };

    public static final String[] BATCH_LIST = {
            "BATCH-1",
            "BATCH-2",
            "BATCH-3",

    };

    public static final String[] VENUE_LIST = {
            "VENUE-1",
            "VENUE-2",
            "VENUE-3",

    };

    public static final String[] CLASS_LIST = {
            "CLASS-1",
            "CLASS-2",
            "CLASS-3",

    };


    // For Test Activity ===



    public static final String[] EVENT_ITEMS_TEST = {
            "Pre Test",

    };

    public static final String[] EVENT_CODES_TEST = {
            "EVENT-1",


    };

    public static final String[] DATE_LIST_TEST = {
            "DATE-1",


    };

    public static final String[] BATCH_LIST_TEST = {
            "BATCH-1",


    };

    public static final String[] VENUE_LIST_TEST = {
            "VENUE-1",

    };

    public static final String[] CLASS_LIST_TEST = {
            "CLASS-1",


    };

    //STATISTICS MENU LIST ITEM
    public static final String[] STATISTICS_ITEMS = {
            "REG/VER STATS",
            "DOWNLOAD STATS",

    };

    public static final String [] REG_VER_STATS_ITEMS= {
            "Pre Test",
            "Pre Counseling",

    };
    public static final  String [] STATISTICS_EVENT = {
            "EVENT-1",

    };
    public static final  String [] STATISTICS_PRE_EVENT_DATE = {
            "DATE-1",
            "DATE-2"

    };
    public static final  String [] STATISTICS_PRE_EVENT_VENUE= {
            "VENUE",


    };
    public static final  String [] STATISTICS_PRE_EVENT_BATCH = {
            "BARCH-1",
            "BATCH-2",

    };
    public static final  String [] STATISTICS_PRE_EVENT_CLASSROOM = {
            "CLASS-1",
            "CLASS-2",

    };

    public static final  String [] STATISTICS_PRE_EVENT_SESSION = {
            "SESSION-1",
            "SESSION-2",

    };


//    public static String ACTION_FINGER_CAPTURED = "com.manvish.android.FINGER_CAPTURED_ACTION";
//    public static String ACTION_AUPD_DOWNLOAD = "com.manvish.android.AUPD_DOWNLOAD_ACTION";
//
//    public static String KEY_INTENT_SUCCESS = "SUCCESS";
//    public static String KEY_INTENT_BITMAP = "BITMAP";
//    public static String VALUE_FAIL = "FAIL";
//
//    public static String SHARED_PREFERENCE_FILE_NAME="merittracksharedfile";

    //========= from Morpho sample project ================
    // Splash screen timer
    public static int SPLASH_TIME_OUT = 2000;
    public static String STAGE = "STAGE";
    public static String STAGE1 = "STAGE1";
    public static String STAGE2 = "STAGE2";
    public static String STAGE3 = "STAGE3";
    public static String STAGE4 = "STAGE4";

   // public static String TAG = "EXAMATTENDANCE";
    public static String SELECTED_ID = "SELECTED_ID";
    public static String CAPTURED_IMAGE_PATH = "CAPTURED_IMAGE_PATH";
    public static String MANVISH_FOLDER = "MANVISH";
    public static String SHARED_PREFERENCE_FILE_NAME = "MANVISH_PREF_FILE";
    public static String FINAL_SUBMISSION_FLAG_YES = "Y";
    public static String FINAL_SUBMISSION_FLAG_NO = "N";

    public static String KEY_ROLL_NO = "KEY_ROLL_NO";
    public static String KEY_REG_STATUS = "KEY_REG_STATUS";


    // Start tags should be end tag for xml writing
    public static String XML_START_TAG_STUDENT_DETAILS = "STUDENT_DETAILS";
    public static String XML_END_TAG_STUDENT_DETAILS = "STUDENT_DETAILS";

    public static String XML_START_TAG_STUDENT = "STUDENT";
    public static String XML_END_TAG_STUDENT = "STUDENT";

    public static String XML_TAG_HALL_TKT_NO = "HALL_TKT_NO";
    public static String XML_TAG_VENUE_CODE = "VENUE_CODE";
    public static String XML_TAG_CLASS_ROOM = "CLASS_ROOM";
    public static String XML_TAG_PROJECT_CODE = "PROJECT_CODE";

    public static String XML_TAG_REG_STATUS = "REG_STATUS" + "";

    public static String MENU_NAMES[] = {"SPOT REGISTRATION",
            "UNIT DETAILS", "NETWORK SETTINGS",
            "PRACTICE MODE", "DOWNLOAD", "UPLOAD", "DATA BACKUP", "DOWNLOAD DATA"};


    public static String[] TABS = {"Random", "Class Room", "Reconciliation",
            "Final Submission", "Dash Board"};
    public static String CONFIG = "config";

    public static String getFileNameForManvishMedia(String aMediaType) {
        String mediaFileName = "MANVISH_" + System.currentTimeMillis();
        mediaFileName = mediaFileName + ".jpg";
        return mediaFileName;
    }

    // Morpho sensor
    public static int NO_OF_FINGERS = 1;
    // Capture Verify button
    // check Latent Detect
    // public static String FP_TEMPLATE_TYPE = "Morpho_pk_iso_FMR";
    // public static String FPV_TEMPLATE_TYPE = "Morpho_No_pk_fvp";
    public static String ACTION_FINGER_CAPTURED = "com.manvish.android.FINGER_CAPTURED_ACTION";
    public static String ACTION_AUPD_DOWNLOAD = "com.manvish.android.AUPD_DOWNLOAD_ACTION";
    public static String KEY_INTENT_SUCCESS = "SUCCESS";
    public static String KEY_INTENT_BITMAP = "BITMAP";
    public static String KEY_INTENT_TEMPLATE = "TEMPLATE";
    public static String KEY_INTENT_VENUECODE = "VENUE_CODE";

    public static String VALUE_FAIL = "FAIL";
    public static String VALUE_SUCC = "SUCC";
    public static int DELAY_TIME = 1200;
    public static long DELAY_AUPD_COMMAND_INTERVAL = 1000 * 60 * 1;

    public static int FINGER_CAPTURED_REQUEST_CODE = 11;

    public static String SERVICE_NAME = "4001";
    public static String AUPD_BASE_URL = "59.90.224.106";
    public static String AUPD_BASE_PORT = "8181";
    public static String AUPD_HTTP = "http://";
    //production
    //public static String AUPD_BASE_URL = "http://192.168.1.6:9095/4001/";
    //Testing
    //public static String AUPD_BASE_URL = "http://192.168.1.203:9095/4001/";

//	public static String AUPD_BASE_URL = "http://192.168.1.15:9095/4001/";

//	public static String AUPD_URL =AUPD_BASE_URL+"AUPDB";
//	public static String AUPD_URL_TIME =AUPD_BASE_URL+"TIME";
//	public static String AUPD_URL_ENROLLMENT =AUPD_BASE_URL+"ENROLLMENT";
//	public static String AUPD_URL_ACKNOWLEDGEMENT =AUPD_BASE_URL+"SUCC";


    public static String AUPD_URL = "AUPDB";
    public static String AUPD_URL_TIME = "TIME";
    public static String AUPD_URL_ENROLLMENT = "ENROLLMENT";
    public static String AUPD_URL_ACKNOWLEDGEMENT = "SUCC";


    public static String URL_TAG_AUPD = "URL_TAG_AUPD";
    public static String URL_TAG_TIME = "URL_TAG_TIME";
    public static String URL_TAG_AKN = "URL_TAG_AKN";
    public static String URL_TAG_ENRL = "URL_TAG_ENRL";

    public static String URL_TAG_KEY = "URL_TAG_KEY";
    public static String HTTP_COLLON = "http://";

    //Using these below variables for verification as well .

    public static final int STUDENT_REGISTERED = 1;
    public static final int STUDENT_NOT_REGISTERED = 0;
    public static final int STUDENT_REGISTRATION_FAILED = -1;

    public static final int STUDENT_VERIFIED = 1;
    public static final int STUDENT_NOT_VERIFIED = 0;
    public static final int STUDENT_VERIFICATION_FAILED = -1;


    //public static final String MODEL_NUMBER="miFaun-MI005";

    public static final String student_registration_start = "Start";

    public static final int student_camera = 1;

    public static final String MODEL_NO = "miFaun-MI005";

    public static final int REQUEST_CODE_REGISTRAION = 100;

    public static String STUDENT_TO_REGISTER = "STUDENT_TO_REGISTER";

    public static String SPOT_REGISTER = "spot_register";

    public static String HALL_TKT_NO_TO_REGISTER = "HALL_TKT_NO_TO_REGISTER";

    public static String OPERATOR_PHOTOGRAPH = "OPERATOR_PHOTOGRAPH";

    // Finger config constants

    public static final int FINGER_RIGHT_THUMB = 1;
    public static final int FINGER_RIGHT_INDEX = 2;
    public static final int FINGER_RIGHT_MIDDLE = 3;
    public static final int FINGER_RIGHT_RING = 4;
    public static final int FINGER_RIGHT_LITTLE = 5;

    public static final int FINGER_LEFT_THUMB = 6;
    public static final int FINGER_LEFT_INDEX = 7;
    public static final int FINGER_LEFT_MIDDLE = 8;
    public static final int FINGER_LEFT_RING = 9;
    public static final int FINGER_LEFT_LITTLE = 10;

    public static final String CR = "COUNCIL_REGISTRATION";
    public static final String R = "REGISTRATION";
    public static final String VR = "VERIFICATION";
    public static final String O = "OPERATOR";

    //public static String REGISTRATION_KEY = "REGISTRATION_KEY";

    //public static String COUNCIL_ACTIVITY_KEY = "COUNCIL_ACTIVITY_KEY";

    public static final int DB_VERSION_CODE = 1;

    //tapan xml create constants
    //tapan xml create constants
    public static String XML_ROOT = "root";

    public static String XML_PROJECT_DETAILS = "projectDetails";

    public static String XML_PROJECT_CODE = "projCode";

    public static String XML_DATE = "Date";

    public static String XML_VENUE_CODE = "venueCode";

    public static String XML_STAGE = "stage";

    public static String XML_ENR_DATA = "enrData";

    public static String XML_ENR_DATA_END = "</enrData>";

    public static String XML_ACS_LOG = "acslog";

    public static String XML_ACS_LOG_END = "</acslog>";

    public static String XML_STUDENT = "student";

    public static String XML_UNIT_ID = "unitId";
    public static String XML_STUDENT_ID = "StudentId";
    public static String XML_REG_DATE = "regDate";
    public static String XML_REG_TIME = "regTime";
    public static String XML_BATCH_ID = "batchId";
    public static String XML_TEMPLATE = "template";
    public static String XML_FINGER_TYPE = "fingerType";
    public static String XML_TDATA = "tData";
    public static String XML_REG_STATUS = "regStatus";
    public static String XML_VER_STATUS = "verStatus";
    public static String XML_IMAGE = "image";
    public static String XML_ITYPE = "iType";
    public static String XML_IDATA = "iData";
    public static String XML_PHOTOGRAPH = "photograph";
    public static String XML_VER_DATE = "verDate";
    public static String XML_VER_TIME = "verTime";
    public static String XML_IN_OUT = "inOut";
    public static String XML_STATUS = "status";
    public static String XML_SESSION_CODE = "sessionCode";

    public static String PROJECT_STAGE_1 = "1";
    public static String PROJECT_STAGE_2 = "2";
    public static String PROJECT_STAGE_3 = "3";
    public static String PROJECT_STAGE_4 = "4";

    public static String REG_CATEGORY_KEY;

    public static String CAT_RANDOM = "RANDOM";
    public static String CAT_CLASSROOM = "CLASSROOM";
    public static String CAT_RECOUNCIL = "RECOUNCIL";
    public static String CAT_SPOT = "SPOT";

    public static String USB_BACKUP = "miFaunBackup";

    public static String USB_BACKUP_PATH_SECONDRY = "/mnt/media_rw/usbotg";

    public static String USB_BACKUP_PATH_PRIMARY = "/storage/usbotg";

    //==============



    //Project stages
    //"PRE-TEST",

    public static final String PRETEST="Pre Test";
    public static final String POSTTEST="Post Test";
    public static final String PRECOUNCIL="Pre Counseling";
    public static final String POSTCOUNCIL="Post Counseling";
    public static final String BACKUP_POSTTEST="Backup Post Test";
    public static final String BACKUP_POSTCOUNCILING="Backup Post Counseling";
    public static final String BACKUP_USB_POSTTEST="USB Backup Post Test";
    public static final String BACKUP_USB_POSTCOUNCILING="USB Backup Post Counseling";


    public static final String REGISTRATION = "REGISTRATION";
    public static final String VERIFICATION = "VERIFICATION";

    public static final String OFFLINE_VERIFICATION = "OFFLINE_VERIFICATION";

    public static final String KEY_STUDENT_ID = "key_student_id";

    public static final String KEY_STAGE = "key_stage";

    public static final String ACTIVITY_TYPE = "activity_type";

    public static final String STATISTICS = "STATISTICS";

    public static final String OFFLINE_UPLOAD_PATH = "/sdcard/MeritTrack/Upload";

    // Interval time in ms for cache memory
    public static final int INTERVAL_TIME = 120000;

    public static final int DOWNLOAD_TIME_INTERVAL = 2*60*1000;

    public static final String root_tag_end="</root>";
    public static final String xml_header_tag="<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>";
    public static final int FINGER_TEMPLATE_COUNT_ERROR=-1;

    public static final String TEST_DB = "test.db";//created the final String
}
