package com.manvish.merittrack.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.inputmethodservice.KeyboardView;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.activeandroid.util.Log;
import com.manvish.merittrack.Model.Student;
import com.manvish.merittrack.R;
import com.manvish.merittrack.adapters.IDListGridAdapter;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.utils.ManvishCommonUtil;
import com.manvish.merittrack.view.CustomKeyboard;
import com.manvish.merittrack.view.ManvishAlertDialog;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by pabitra on 4/6/16.
 */
public class RegisterOrVerificationActivity extends BaseActivity {

    KeyboardView mKeyBoardView;
    GridView mGridView;
    IDListGridAdapter idListGridAdapter;

    AutoCompleteTextView mEtStudentID;
    ArrayList<Student> studentList; // specific to a date,batch,venue and classRoom
    DBAdapter dbAdapter;
    LinkedHashMap<String, Student> studentLinkedHashMap;
    TextView mTvSelectedEventCode, mTvSelectedVenueName,
            mTvSelectedBatchID, mTvSelectedDate,
            mSelectedClassName, mTvRollNoRange,
            mTvAttendanceSuccessRatio, mTvRegFailure, mTVRegOrVer, mTvEventStage;

    int mTotalStudent, mNoOfRegOrVerFailed, mNoOfRegOrVerSucc;
    String mStartRollNo, mEndRollNo;

    ArrayList<String> mStudentIDList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_register);
        studentLinkedHashMap = new LinkedHashMap<>();
        mStudentIDList = new ArrayList<>();
        mStudentIDList.clear();
        studentList = new ArrayList<>();

        dbAdapter=DBAdapter.getInstance();
        dbAdapter.closeDatabase();

        dbAdapter = DBAdapter.getInstance(this, MeritTrackApplication.getInstance().getDbName(), null, 1);

        Student student;

        if(ManvishPrefConstants.IS_FIRSTTIME_LUNCH.read() && ManvishPrefConstants.IS_TEST.read()) {

           ManvishPrefConstants.IS_FIRSTTIME_LUNCH.write(false);
            dbAdapter.deleteExistingTableData();

            for (int i = 0; i < 50; i++) {
                student = new Student();
                student.setStudentID("test-" + i);
                student.setName("test-name-" + i);
                student.setRegStatus("0");
                student.setVerStatus("0");
                student.setTestDate(ManvishPrefConstants.SELECTED_DATE.read());
                student.setVenueCode( ManvishPrefConstants.SELECTED_VENUE.read());
                student.setBatchCode( ManvishPrefConstants.SELECTED_BATCH.read());
                student.setClassRoom( ManvishPrefConstants.SELECTED_CLASS.read());

                dbAdapter.insertStudentDetails("test", ManvishPrefConstants.SELECTED_VENUE.read(), student);

            }
        }

        initializeViews();

        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);

        CustomKeyboard mCustomKeyboard = new CustomKeyboard(this, viewGroup,
                R.id.keyboardview, R.xml.qwerty_numbers);

    }

    public void cancel(View v) {
        finish();
    }



    @Override
    protected void onResume() {
        super.onResume();
        setHeading();
        if (idListGridAdapter != null) {
            idListGridAdapter.setSelected(true);
        }
        mNoOfRegOrVerFailed = 0;
        mNoOfRegOrVerSucc = 0;

        // if(!ManvishPrefConstants.IS_TEST.read()) {
        if (null != MeritTrackApplication.getInstance().getStudentListMap()) {
            if (MeritTrackApplication.getInstance().getStudentListMap().size() != 0) {

                studentLinkedHashMap = MeritTrackApplication.getInstance().getStudentListMap();
                studentList.clear(); //Clear the student list before adding

                for (Map.Entry<String, Student> ee : MeritTrackApplication.getInstance()
                        .getStudentListMap().entrySet()) {

                    String key = ee.getKey();
                    Student std = ee.getValue();

                    if (ManvishPrefConstants.SELECTED_REG_OR_VER.read().equalsIgnoreCase(StringConstants.VERIFICATION)
                            || ManvishPrefConstants.SELECTED_REG_OR_VER.read().equalsIgnoreCase(StringConstants.OFFLINE_VERIFICATION)) {

                           /* if(std.getVerStatus().equalsIgnoreCase(""+StringConstants.STUDENT_REGISTERED)){
                                mNoOfRegOrVerSucc++;
                            }

                            if(std.getVerStatus().equalsIgnoreCase(""+StringConstants.STUDENT_REGISTRATION_FAILED)){
                                mNoOfRegOrVerFailed++;
                            }*/
                        if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.PRECOUNCIL)) {
                            if (std.getVerStatus().equalsIgnoreCase("" + StringConstants.STUDENT_REGISTERED)) {
                                mNoOfRegOrVerSucc++;
                            }

                            if (std.getVerStatus().equalsIgnoreCase("" + StringConstants.STUDENT_REGISTRATION_FAILED)) {
                                mNoOfRegOrVerFailed++;
                            }
                        } else {
                            if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.PRETEST)) {

                                String sessionCodeAndverStatus = std.getSessionCode();
                                String[] sessCodeVerStatArray = sessionCodeAndverStatus.split(",");
                                if (sessCodeVerStatArray.length != 0) {

                                    for (String codever : sessCodeVerStatArray) {
                                        //Every code ver comes with Sessioncode-verstatus
                                        // So first separate ,them and compare with Current choosen sessionCode to display the status

                                        String[] codeverArray = codever.split(":");
                                        if (codeverArray.length == 2) {
                                            if (ManvishPrefConstants.SELECTED_SESSION_CODE.read().equalsIgnoreCase(codeverArray[0])) {

                                                if (codeverArray[1].equalsIgnoreCase("" + StringConstants.STUDENT_REGISTERED)) {
                                                    mNoOfRegOrVerSucc++;
                                                }
                                                if (codeverArray[1].equalsIgnoreCase("" + StringConstants.STUDENT_REGISTRATION_FAILED)) {
                                                    mNoOfRegOrVerFailed++;
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }

                    } else {

                        if (std.getRegStatus().equalsIgnoreCase("" + StringConstants.STUDENT_REGISTERED)) {
                            mNoOfRegOrVerSucc++;
                        }

                        if (std.getRegStatus().equalsIgnoreCase("" + StringConstants.STUDENT_REGISTRATION_FAILED)) {
                            mNoOfRegOrVerFailed++;
                        }
                    }
                    studentList.add(std);
                }

                mTotalStudent = studentLinkedHashMap.size();
                // get all
                ManvishPrefConstants.TOTAL_STUDENT.write(mTotalStudent + "");

                ManvishPrefConstants.TOTAL_STUDENT_REG_VER_FAIL.write("" + mNoOfRegOrVerFailed); // saving in shared preference
                ManvishPrefConstants.TOTAL_STUDENT_REG_VER_SUCC.write("" + mNoOfRegOrVerSucc);

                mStartRollNo = studentList.get(0).getStudentID();
                mEndRollNo = studentList.get(studentList.size() - 1).getStudentID();

                mTvRollNoRange.setText("RN:" + mStartRollNo + "-" + mEndRollNo);
                mTvAttendanceSuccessRatio.setText("ATN:" + mNoOfRegOrVerSucc + "/" + mTotalStudent);

                if (ManvishPrefConstants.SELECTED_REG_OR_VER.read().equalsIgnoreCase(StringConstants.VERIFICATION) ||
                        ManvishPrefConstants.SELECTED_REG_OR_VER.read().equalsIgnoreCase(StringConstants.OFFLINE_VERIFICATION)) {

                    mTvRegFailure.setText("VF: " + mNoOfRegOrVerFailed);
                } else {
                    mTvRegFailure.setText("RF: " + mNoOfRegOrVerFailed);
                }

                idListGridAdapter = new IDListGridAdapter(RegisterOrVerificationActivity.this, studentList);

                mGridView.setAdapter(idListGridAdapter);

            }
        }

        // }
    }

    private void initializeViews() {
        mKeyBoardView = (KeyboardView) findViewById(R.id.keyboardview);

        mGridView = (GridView) findViewById(R.id.grid_view);

        mEtStudentID = (AutoCompleteTextView) findViewById(R.id.et_studentID);
        mEtStudentID.clearFocus();
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, mStudentIDList);
        mEtStudentID.setAdapter(adapter);

        mTvSelectedEventCode = (TextView) findViewById(R.id.tv_event_name);
        mTvSelectedEventCode.setText(ManvishPrefConstants.SELECTED_EVENT_CODE.read());

        mTvSelectedVenueName = (TextView) findViewById(R.id.tv_venue_name);
        mTvSelectedVenueName.setText(ManvishPrefConstants.SELECTED_VENUE.read());

        mTvSelectedBatchID = (TextView) findViewById(R.id.tv_batch_id);
        mTvSelectedBatchID.setText(ManvishPrefConstants.SELECTED_BATCH.read());

        mTvSelectedDate = (TextView) findViewById(R.id.tv_selected_date);
        mTvSelectedDate.setText(ManvishPrefConstants.SELECTED_DATE.read());

        mSelectedClassName = (TextView) findViewById(R.id.tv_selected_class);
        mSelectedClassName.setText(ManvishPrefConstants.SELECTED_CLASS.read());


        mTvRollNoRange = (TextView) findViewById(R.id.tv_roll_no_range);
        mTvAttendanceSuccessRatio = (TextView) findViewById(R.id.tv_attendance_ratio);
        mTvRegFailure = (TextView) findViewById(R.id.tv_reg_failed);


        mTVRegOrVer = (TextView) findViewById(R.id.tv_reg_or_ver);
        mTVRegOrVer.setText(ManvishPrefConstants.SELECTED_REG_OR_VER.read());

        mTvEventStage = (TextView) findViewById(R.id.tv_event_stage);
        mTvEventStage.setText(ManvishPrefConstants.SELECTED_EVENT_STAGE.read());

        setStudentList();
    }


    private void setStudentList() {


        new AsyncTask<Void, Void, Boolean>() {
            protected void onPreExecute() {
                showProgressBar("Fetching Student Data");

            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {

                    if (ManvishPrefConstants.IS_TEST.read()) {

                        studentLinkedHashMap = dbAdapter.getStudentMapListFromDateVenueBatchClass
                                (ManvishPrefConstants.SELECTED_DATE.read(),
                                        ManvishPrefConstants.SELECTED_VENUE.read(),
                                        ManvishPrefConstants.SELECTED_BATCH.read(),
                                        ManvishPrefConstants.SELECTED_CLASS.read(),
                                        ManvishPrefConstants.SELECTED_REG_OR_VER.read());
                        MeritTrackApplication.getInstance().setStudentListMap(studentLinkedHashMap);
                    } else {
                        studentLinkedHashMap = dbAdapter.getStudentMapListFromDateVenueBatchClass
                                (ManvishPrefConstants.SELECTED_DATE.read(),
                                        ManvishPrefConstants.SELECTED_VENUE.read(),
                                        ManvishPrefConstants.SELECTED_BATCH.read(),
                                        ManvishPrefConstants.SELECTED_CLASS.read(),
                                        ManvishPrefConstants.SELECTED_REG_OR_VER.read());
                        mTotalStudent = studentLinkedHashMap.size();
                        // get all
                        ManvishPrefConstants.TOTAL_STUDENT.write(mTotalStudent + "");
                        MeritTrackApplication.getInstance().setStudentListMap(studentLinkedHashMap); // set it to a global variaable
                    }

                    if (MeritTrackApplication.getInstance().getStudentListMap().size() > 0) {
                        return true;
                    } else {
                        return false;
                    }

                } catch (Exception e) {
                    Log.e("MANVISH",
                            "Exception in parsing file"
                                    + e.getMessage());

                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean success) {

                mStudentIDList.clear();
                if (MeritTrackApplication.getInstance().getStudentListMap().size() != 0) {

                    //studentList

                    for (Map.Entry<String, Student> ee : MeritTrackApplication.getInstance()
                            .getStudentListMap().entrySet()) {

                        String key = ee.getKey();
                        Student std = ee.getValue();

                        if (ManvishPrefConstants.SELECTED_REG_OR_VER.read().equalsIgnoreCase(StringConstants.VERIFICATION)
                                || ManvishPrefConstants.SELECTED_REG_OR_VER.read().equalsIgnoreCase(StringConstants.OFFLINE_VERIFICATION)) {

                            if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.PRECOUNCIL)) {
                                if (std.getVerStatus().equalsIgnoreCase("" + StringConstants.STUDENT_REGISTERED)) {
                                    mNoOfRegOrVerSucc++;
                                }

                                if (std.getVerStatus().equalsIgnoreCase("" + StringConstants.STUDENT_REGISTRATION_FAILED)) {
                                    mNoOfRegOrVerFailed++;
                                }
                            } else {
                                if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.PRETEST)) {

                                    String sessionCodeAndverStatus = std.getSessionCode();
                                    String[] sessCodeVerStatArray = sessionCodeAndverStatus.split(",");
                                    if (sessCodeVerStatArray.length != 0) {

                                        for (String codever : sessCodeVerStatArray) {
                                            //Every code ver comes with Sessioncode-verstatus
                                            // So first separate ,them and compare with Current choosen sessionCode to display the status

                                            String[] codeverArray = codever.split(":");
                                            if (codeverArray.length == 2) { // This condition no need to check from later
                                                if (ManvishPrefConstants.SELECTED_SESSION_CODE.read().equalsIgnoreCase(codeverArray[0])) {

                                                    if (codeverArray[1].equalsIgnoreCase("" + StringConstants.STUDENT_REGISTERED)) {
                                                        mNoOfRegOrVerSucc++;
                                                    }
                                                    if (codeverArray[1].equalsIgnoreCase("" + StringConstants.STUDENT_REGISTRATION_FAILED)) {
                                                        mNoOfRegOrVerFailed++;
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }

                        } else {

                            if (std.getRegStatus().equalsIgnoreCase("" + StringConstants.STUDENT_REGISTERED)) {
                                mNoOfRegOrVerSucc++;
                            }

                            if (std.getRegStatus().equalsIgnoreCase("" + StringConstants.STUDENT_REGISTRATION_FAILED)) {
                                mNoOfRegOrVerFailed++;
                            }
                        }
                        studentList.add(std);
                        mStudentIDList.add(std.getStudentID());
                    }

                    ManvishPrefConstants.TOTAL_STUDENT_REG_VER_FAIL.write("" + mNoOfRegOrVerFailed); // saving in shared preference
                    ManvishPrefConstants.TOTAL_STUDENT_REG_VER_SUCC.write("" + mNoOfRegOrVerSucc);

                    mStartRollNo = studentList.get(0).getStudentID();
                    mEndRollNo = studentList.get(studentList.size() - 1).getStudentID();

                    mTvRollNoRange.setText("RN:" + mStartRollNo + "-" + mEndRollNo);
                    mTvAttendanceSuccessRatio.setText("ATN:" + mNoOfRegOrVerSucc + "/" + mTotalStudent);

                    if (ManvishPrefConstants.SELECTED_REG_OR_VER.read().equalsIgnoreCase(StringConstants.VERIFICATION) ||
                            ManvishPrefConstants.SELECTED_REG_OR_VER.read().equalsIgnoreCase(StringConstants.OFFLINE_VERIFICATION)) {

                        mTvRegFailure.setText("VF: " + mNoOfRegOrVerFailed);
                    } else {
                        mTvRegFailure.setText("RF: " + mNoOfRegOrVerFailed);
                    }


                    idListGridAdapter = new IDListGridAdapter(RegisterOrVerificationActivity.this, studentList);

                    mGridView.setAdapter(idListGridAdapter);

                } else {
//                    new ManvishAlertDialog(RegisterOrVerificationActivity.this,
//                            "CONTACT ADMIN","NO DATA AVAILABLE .PLEASE CONTACT ADMIN").showAlertDialog();
                }

                dismissProgressBar();
            }

        }.execute();
    }

    ProgressDialog mProgressDialog;

    private void showProgressBar(String msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
        }


        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    private void dismissProgressBar() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    public void enter(View v) {

        boolean isIdFound = false;

        for (Student std : studentList) {

            if (std.getStudentID().equalsIgnoreCase(mEtStudentID.getText().toString())) {
                isIdFound = true;
                // Check ,if the ID is Already registered ,if SO ,show Already registered Dialog .
                int regOrVerStatus = 0;

                switch (ManvishPrefConstants.SELECTED_REG_OR_VER.read()) {

                    case StringConstants.VERIFICATION:


                        if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("1")) {

                            String sessionCodeAndverStatus = std.getSessionCode();
                            String[] sessCodeVerStatArray = sessionCodeAndverStatus.split(",");

                            if (sessCodeVerStatArray.length != 0) {

                                for (String codever : sessCodeVerStatArray) {
                                    //Every code ver comes with Sessioncode-verstatus
                                    // So first separate ,them and compare with Current choosen sessionCode to display the status

                                    String[] codeverArray = codever.split(":");
                                    if (ManvishPrefConstants.SELECTED_SESSION_CODE.read().equalsIgnoreCase(codeverArray[0])) {

                                        if (codeverArray.length == 2) {
                                            if (codeverArray[1].equalsIgnoreCase("" + StringConstants.STUDENT_REGISTERED)) {
                                                regOrVerStatus = StringConstants.STUDENT_REGISTERED;
                                            }
                                            if (codeverArray[1].equalsIgnoreCase("" + StringConstants.STUDENT_REGISTRATION_FAILED)) {
                                                regOrVerStatus = StringConstants.STUDENT_REGISTRATION_FAILED;
                                            }
                                        }
                                    }

                                }
                            }
                        } else {
                            regOrVerStatus = Integer.parseInt(std.getVerStatus());
                        }


                        // here register means just a flag which represents both registration and verification
                        if (regOrVerStatus == StringConstants.STUDENT_REGISTERED) {
                            //showAlreadyRegisteredDialog(" Already Verified ");
                            new ManvishAlertDialog(this, "Verified", "This student is already verified .").showAlertDialog();
                        } else {

                            //If student is not registered, do not display verification screen.
                            // Display "NO DATA FOUND, Please register before verification"
                            // when student id (icon)block has been touched
                            ArrayList<byte[]> templatebyteList = null;
                            if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("1")) {
                                showProgressBar("Fetching student data");
                                templatebyteList = dbAdapter.getTemplatesOfStudentFromRegisteredTableForSessionVerification(ManvishPrefConstants.SELECTED_DATE.read(),
                                        ManvishPrefConstants.SELECTED_VENUE.read(), ManvishPrefConstants.SELECTED_BATCH.read(),
                                        ManvishPrefConstants.SELECTED_CLASS.read(), std.getStudentID());
                                std.setTemplatebyteList(templatebyteList);
                                dismissProgressBar();
                            } else if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("3")) {
                                showProgressBar("Fetching student data");
                                templatebyteList = dbAdapter.getTemplatesOfStudentForVerification(ManvishPrefConstants.SELECTED_DATE.read(),
                                        ManvishPrefConstants.SELECTED_VENUE.read(), ManvishPrefConstants.SELECTED_BATCH.read(),
                                        ManvishPrefConstants.SELECTED_CLASS.read(), std.getStudentID());
                                dismissProgressBar();
                                std.setTemplatebyteList(templatebyteList);
                            }

                            if (templatebyteList != null && templatebyteList.size() != 0) {
                                Intent cameraIntent = new Intent(this, CameraActivity.class);
                                cameraIntent.putExtra(StringConstants.KEY_STUDENT_ID, std.getStudentID());
                                startActivity(cameraIntent);
                            } else {
                                new ManvishAlertDialog(this,
                                        "NO DATA FOUND", "Please register before verification .").showAlertDialog();
                            }


                        }
                        break;

                    case StringConstants.OFFLINE_VERIFICATION:


                        ArrayList<byte[]> templatebyteList = null;
                        if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("1")) {

                            templatebyteList = dbAdapter.getTemplatesOfStudentFromRegisteredTableForSessionVerification(ManvishPrefConstants.SELECTED_DATE.read(),
                                    ManvishPrefConstants.SELECTED_VENUE.read(), ManvishPrefConstants.SELECTED_BATCH.read(),
                                    ManvishPrefConstants.SELECTED_CLASS.read(), std.getStudentID());

                        } else if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("3")) {
                            templatebyteList = dbAdapter.getTemplatesOfStudentForVerification(ManvishPrefConstants.SELECTED_DATE.read(),
                                    ManvishPrefConstants.SELECTED_VENUE.read(), ManvishPrefConstants.SELECTED_BATCH.read(),
                                    ManvishPrefConstants.SELECTED_CLASS.read(), std.getStudentID());
                        }

                        if (templatebyteList != null && templatebyteList.size() != 0) {


                            Intent cameraIntent = new Intent(this, CameraActivity.class);
                            cameraIntent.putExtra(StringConstants.KEY_STUDENT_ID, std.getStudentID());
                            startActivity(cameraIntent);
                        } else {

                            new ManvishAlertDialog(this,
                                    "NO DATA FOUND", "Please register before verification .").showAlertDialog();

                        }

                        break;

                    case StringConstants.REGISTRATION:
                        regOrVerStatus = Integer.parseInt(std.getRegStatus());
                        if (regOrVerStatus == StringConstants.STUDENT_REGISTERED || regOrVerStatus == StringConstants.STUDENT_REGISTRATION_FAILED) {
                            showAlreadyRegisteredDialog(" Already Registered ", std.getStudentID());
                        } else {

                            ManvishCommonUtil.showStudentDetails(std, RegisterOrVerificationActivity.this);
                        }
                        break;
                }

                break;
            }


        }
        if (!isIdFound) {
            new ManvishAlertDialog(RegisterOrVerificationActivity.this, "Student not found ", "Please check student ID ").showAlertDialog();
        }

    }


    public void showAlreadyRegisteredDialog(String msg, final String studentID) {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_already_registered);
        TextView tvDialogMsg = (TextView) dialog.findViewById(R.id.tv_dialog_msg);
        tvDialogMsg.setText(msg);
        Button btnOverwrite, btnDelete, btnQuit;

        btnOverwrite = (Button) dialog.findViewById(R.id.btn_over_write);
        btnDelete = (Button) dialog.findViewById(R.id.btn_delete);
        btnQuit = (Button) dialog.findViewById(R.id.btn_quit);

        btnOverwrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // If we select overwrite, delete the existing reg details of student and go for fresh registration of that student

                // Delete registered data
                dbAdapter.deleteRegisteredData(ManvishPrefConstants.SELECTED_DATE.read(),
                        ManvishPrefConstants.SELECTED_VENUE.read(),
                        ManvishPrefConstants.SELECTED_BATCH.read(),
                        ManvishPrefConstants.SELECTED_CLASS.read(), studentID);

                //Update Student table
                dbAdapter.updatestudentRegStatus(ManvishPrefConstants.SELECTED_DATE.read(),
                        ManvishPrefConstants.SELECTED_VENUE.read(),
                        ManvishPrefConstants.SELECTED_BATCH.read(),
                        ManvishPrefConstants.SELECTED_CLASS.read(), studentID, "0");

                String mapKey = ManvishPrefConstants.SELECTED_DATE.read() +
                        ManvishPrefConstants.SELECTED_VENUE.read() +
                        ManvishPrefConstants.SELECTED_BATCH.read() +
                        ManvishPrefConstants.SELECTED_CLASS.read() +
                        studentID;
                LinkedHashMap<String, Student> studentLinkedHashMap = MeritTrackApplication.getInstance().getStudentListMap();
                Student student = studentLinkedHashMap.get(mapKey);
                student.setRegStatus("0");
                idListGridAdapter.notifyDataSetChanged();
                //Go for fresh registration
                Intent cameraIntent = new Intent(RegisterOrVerificationActivity.this, CameraActivity.class);
                cameraIntent.putExtra(StringConstants.KEY_STUDENT_ID, studentID);
                startActivity(cameraIntent);
                dialog.dismiss();

            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // delete the student reg details with confirmation
                showConfirmationDeleteDialog(studentID);

                dialog.dismiss();

            }
        });
        btnQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        dialog.setCancelable(true);

        dialog.show();

    }

    private void showConfirmationDeleteDialog(final String studentID) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                this);

        alertDialog.setTitle("Are you sure ?");
        alertDialog.setMessage("Do you really want to delete existing registration.");

        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                // Delete registered data
                dbAdapter.deleteRegisteredData(ManvishPrefConstants.SELECTED_DATE.read(),
                        ManvishPrefConstants.SELECTED_VENUE.read(),
                        ManvishPrefConstants.SELECTED_BATCH.read(),
                        ManvishPrefConstants.SELECTED_CLASS.read(), studentID);

                //Update Student table
                dbAdapter.updatestudentRegStatus(ManvishPrefConstants.SELECTED_DATE.read(),
                        ManvishPrefConstants.SELECTED_VENUE.read(),
                        ManvishPrefConstants.SELECTED_BATCH.read(),
                        ManvishPrefConstants.SELECTED_CLASS.read(), studentID, "0");

                String mapKey = ManvishPrefConstants.SELECTED_DATE.read() +
                        ManvishPrefConstants.SELECTED_VENUE.read() +
                        ManvishPrefConstants.SELECTED_BATCH.read() +
                        ManvishPrefConstants.SELECTED_CLASS.read() +
                        studentID;
                LinkedHashMap<String, Student> studentLinkedHashMap = MeritTrackApplication.getInstance().getStudentListMap();
                Student student = studentLinkedHashMap.get(mapKey);
                student.setRegStatus("0");
                idListGridAdapter.notifyDataSetChanged();
                dialog.cancel();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event

                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // mSensor.closeSensor();
       // mSensor.closeDeviceAndFinishActivity();
        MeritTrackApplication.getInstance().getStudentListMap().clear();
        studentLinkedHashMap.clear();
        studentList.clear();
    }

    public void home(View v) {
        ManvishCommonUtil.goToMainactivity(this);
    }
}
