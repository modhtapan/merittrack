package com.manvish.merittrack.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.inputmethodservice.KeyboardView;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.DigitalClock;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.manvish.merittrack.R;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.utils.ManvishCommonUtil;
import com.manvish.merittrack.view.CustomKeyboard;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

// MiFaunActivity is settings activity
public class MiFaunActivity extends BaseActivity {

    final int DATE_PICKER_ID = 1111;
    static final int TIME_DIALOG_ID = 0;

    private SeekBar brightbar;//Seekbar object
    //Variable to store brightness value
    private int brightness;
    //Content resolver used as a handle to the system's settings
    private ContentResolver cResolver;
    //Window object, that will store a reference to the current window
    private Window window;
    //Text percentage
    TextView txtPerc;
    private int year;
    private int month;
    private int day;

    EditText editTextServer, editTextport;
    //TextView wifinetstatus;

    TextView mDatePickerTextView;
    TextView tvwificonfig;
    DigitalClock mTimePickerTextView;
    KeyboardView mKeyboardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_faun);

        Switch wifiSwitch = (Switch) findViewById(R.id.wifiSwitch);
        Switch gprsSwitch = (Switch) findViewById(R.id.gprsSwitch);
        editTextServer = (EditText) findViewById(R.id.serverIpEdit);//edittext
        editTextport = (EditText) findViewById(R.id.editTextport);//edittext
        mKeyboardView = (KeyboardView) findViewById(R.id.keyboardviewmifaun);//keyboard view
        tvwificonfig = (TextView) findViewById(R.id.Textwifi);
        // wifinetstatus=(TextView)findViewById(R.id.textwifistatus);

        //Instantiate seekbar object
        brightbar = (SeekBar) findViewById(R.id.brightbar);

        txtPerc = (TextView) findViewById(R.id.txtPercentage);


        //Get the content resolver
        cResolver = getContentResolver();

        //Get the current window
        window = getWindow();

        //Set the seekbar range between 0 and 255
        brightbar.setMax(255);
        //Set the seek bar progress to 1
        brightbar.setKeyProgressIncrement(1);


        try {
            //Get the current system brightness
            brightness = Settings.System.getInt(cResolver, Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            //Throw an error case it couldn't be retrieved
            Log.e("Error", "Cannot access system brightness");
            e.printStackTrace();
        }

        //Set the progress of the seek bar based on the system's brightness
        brightbar.setProgress(brightness);


        //Calculate the brightness percentage
        float perc = (brightness / (float) 255) * 100;
        //Set the brightness percentage
        txtPerc.setText((int) perc + " %");

        //Register OnSeekBarChangeListener, so it can actually change values
        brightbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {
                //Set the system brightness using the brightness variable value
                Settings.System.putInt(cResolver, Settings.System.SCREEN_BRIGHTNESS, brightness);
                //Get the current window attributes
                WindowManager.LayoutParams layoutpars = window.getAttributes();
                //Set the brightness of this window
                layoutpars.screenBrightness = brightness / (float) 255;
                //Apply attribute changes to this window
                window.setAttributes(layoutpars);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                //Nothing handled here
            }

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //Set the minimal brightness level
                //if seek bar is 20 or any value below
                if (progress <= 20) {
                    progress = 110;
                    //Set the brightness to 20
                    brightness = 20;
                    brightbar.setProgress(20);
                   // brightbar.setProgress(110);
                } else //brightness is greater than 20
                {
                    //Set brightness variable based on the progress bar
                    brightness = progress;
                }
                //Calculate the brightness percentage
                float perc = (brightness / (float) 255) * 100;
                //Set the brightness percentage
                txtPerc.setText((int) perc + " %");
            }
        });


        //mDateDisplay=(TextView)findViewById(R.id.textViewDate);
        // mTimeDisplay=(TextView)findViewById(R.id.timeDisplay);

        editTextServer.setText(ManvishPrefConstants.SERVER_IP.read());
        editTextport.setText(ManvishPrefConstants.SERVER_PORT.read());


        final WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        boolean wifiEnabled = wifiManager.isWifiEnabled();

        if (wifiEnabled) {

            wifiSwitch.setChecked(true);

        } else {
            wifiSwitch.setChecked(false);
        }


        wifiSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    wifiManager.setWifiEnabled(isChecked);
                    Toast.makeText(MiFaunActivity.this, "Wifi network is Turned on", Toast.LENGTH_LONG).show();

                } else {
                    wifiManager.setWifiEnabled(false);//Wifi actions will be performed
                    Toast.makeText(MiFaunActivity.this, "Wifi network is Turned off", Toast.LENGTH_LONG).show();
                }
            }


        });

        tvwificonfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                intent.putExtra("extra_prefs_show_button_bar", true);//Code to enable the system settings to previous and next buttons
                startActivity(intent);
            }
        });


        if (ManvishPrefConstants.IS_GPRS_ENABLE.read()) {

            gprsSwitch.setChecked(true);

        } else {
            gprsSwitch.setChecked(false);
        }

        gprsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                try {
                    setGprsEnabled(isChecked);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }

            }
        });

        mDatePickerTextView = (TextView) findViewById(R.id.dateTextView);
        mTimePickerTextView = (DigitalClock) findViewById(R.id.timeTextView);

        // Get current date by calender

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        // Show current date
        //updateDisplay();

        mDatePickerTextView.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(day).append("/").append(month + 1).append("/")
                .append(year).append(" "));

        mDatePickerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // On click show datepicker dialog
                showDialog(DATE_PICKER_ID);
            }
        });

        // open time picker dialog
        mTimePickerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(MiFaunActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        mTimePickerTextView.setText(selectedHour + ":" + selectedMinute);

                        SimpleDateFormat sdf = new SimpleDateFormat(
                                "dd-M-yyyy HH:mm:ss");
                        String dateInString = new StringBuilder().append(day).append("-").append(month + 1).append("-")
                                .append(year).append(" ").append(mTimePickerTextView.getText().toString() + ":00").toString();

                        Date date = null;
                        try {
                            date = sdf.parse(dateInString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        System.out.println(dateInString);
                        System.out.println("Date - Time in milliseconds : "
                                + date.getTime());
                        ManvishCommonUtil.setTime(date.getTime());

                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        // finish menu activity
        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                finish();
                /*Intent i1=new Intent(MiFaunActivity.this,MainActivity.class);
                startActivity(i1);*/
                //ManvishCommonUtil.goToMainactivity(MiFaunActivity.this);
            }
        });

        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);

        editTextServer.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_icon, 0);
        editTextport.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_icon, 0);

        editTextServer.addTextChangedListener(ipWatch);
        editTextport.addTextChangedListener(portWatch);


        CustomKeyboard mCustomKeyboard = new CustomKeyboard(this, viewGroup,
                R.id.keyboardviewmifaun, R.xml.qwerty);

        mCustomKeyboard.registerEditText(R.id.serverIpEdit);
        mCustomKeyboard.registerEditText(R.id.editTextport);


        editTextServer.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View arg0, boolean hasFocus) {
                if (hasFocus) {
                    mKeyboardView.setVisibility(View.VISIBLE);
                } else {
                    mKeyboardView.setVisibility(View.GONE);
                }

            }
        });

        editTextport.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View arg0, boolean hasFocus) {
                if (hasFocus) {
                    mKeyboardView.setVisibility(View.VISIBLE);
                } else {
                    mKeyboardView.setVisibility(View.GONE);
                }

            }
        });
//code for calling the System keyboards on the click of both the serveredit an port edit
       /* editTextServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextServer.requestFocus();
                editTextServer.setFocusableInTouchMode(true);

                InputMethodManager imm=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editTextServer,InputMethodManager.SHOW_FORCED);


            }
        });*/



       /* editTextport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextport.requestFocus();
                editTextport.setFocusableInTouchMode(true);

                InputMethodManager imm=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editTextport,InputMethodManager.SHOW_FORCED);


            }
        });*/



    }


    private void openwificonfig() {
        //Intent intent=new Intent(MiFaunActivity.this,Wificonfigactivity.class);
        Intent intent = null;
        try {
            intent = new Intent(MiFaunActivity.this, Class.forName(Settings.ACTION_WIFI_SETTINGS));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        startActivity(intent);//Now my configuration page will be displayed in the other activity
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:

                // open datepicker dialog.
                // set date picker for current date
                // add pickerListener listner to date picker
                return new DatePickerDialog(this, pickerListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            // Show selected date
            mDatePickerTextView.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-")
                    .append(year).append(" "));

            SimpleDateFormat sdf = new SimpleDateFormat(
                    "dd-M-yyyy HH:mm:ss");
            String dateInString = new StringBuilder().append(day).append("-").append(month + 1).append("-")
                    .append(year).append(" ").append(mTimePickerTextView.getText().toString() + ":00").toString();

            Date date = null;
            try {
                date = sdf.parse(dateInString);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            System.out.println(dateInString);
            System.out.println("Date - Time in milliseconds : "
                    + date.getTime());

            try {
                ManvishCommonUtil.setTime(date.getTime());
            } catch (Exception e) {
                e.printStackTrace();
            }

            //updateDisplay();
        }
    };

    // gprs connetion enable/disable
    private void setGprsEnabled(boolean enabled) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {

        final ConnectivityManager conman = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final Class conmanClass = Class.forName(conman.getClass().getName());
        final Field connectivityManagerField = conmanClass.getDeclaredField("mService");
        connectivityManagerField.setAccessible(true);
        final Object connectivityManager = connectivityManagerField.get(conman);
        final Class connectivityManagerClass = Class.forName(connectivityManager.getClass().getName());
        final Method setMobileDataEnabledMethod = connectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
        setMobileDataEnabledMethod.setAccessible(true);

        setMobileDataEnabledMethod.invoke(connectivityManager, enabled);

        ManvishPrefConstants.IS_GPRS_ENABLE.write(enabled);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // setHeading();
        setHeading();
        /*Batterylevel b1=new Batterylevel();
        b1.Batterylevelindicator();*/
        // Batterylevelindicator();
    }

    // caption text watcher
    TextWatcher ipWatch = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable arg0) {


        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {

            String ip = s.toString();
            if (ip.length() != 0) {
                if (Patterns.WEB_URL.matcher(StringConstants.AUPD_HTTP + ip).matches()) {
                    ManvishPrefConstants.SERVER_IP.write(ip);
                    editTextServer.setError(null);
                    editTextServer.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_icon, 0);
                } else {
                    editTextServer.setError("Ip Address is wrong");
                    editTextServer.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
            }
        }
    };

    // caption text watcher
    TextWatcher portWatch = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable arg0) {


        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {

            String port = s.toString();

            if (port.length() == 4) {
                ManvishPrefConstants.SERVER_PORT.write(port);
                editTextport.setError(null);
                editTextport.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.right_icon, 0);
            } else {
                editTextport.setError("Port is wrong");
                editTextport.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
        }
    };
}
