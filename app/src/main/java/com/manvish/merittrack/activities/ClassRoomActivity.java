package com.manvish.merittrack.activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.util.Log;
import com.manvish.merittrack.R;
import com.manvish.merittrack.adapters.ClassListAdapter;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.utils.ManvishCommonUtil;

import java.util.ArrayList;

// MenuActivity for showing all menu lists
public class ClassRoomActivity extends BaseActivity {


    TextView tveventname, tvselecteddate, tvselectedbatch;
    ListView menuListView;
    ClassListAdapter classListAdapter;
    ArrayList<String> classList;
    DBAdapter dbAdapter;
    Button buttonhome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classroom);

        dbAdapter = DBAdapter.getInstance(this, MeritTrackApplication.getInstance().getDbName(), null, 1);

        menuListView = (ListView) findViewById(android.R.id.list);
        buttonhome = (Button) findViewById(R.id.btnHome);
        tveventname = (TextView) findViewById(R.id.tv_event_name);
        tvselecteddate = (TextView) findViewById(R.id.tv_selected_date);
        tvselectedbatch = (TextView) findViewById(R.id.tv_selected_batch);


        tveventname.setText(ManvishPrefConstants.SELECTED_EVENT_CODE.read());
        tvselecteddate.setText(ManvishPrefConstants.SELECTED_DATE.read());
        tvselectedbatch.setText(ManvishPrefConstants.SELECTED_BATCH.read());

        if (ManvishPrefConstants.IS_TEST.read()) {
            classListAdapter = new ClassListAdapter(null);
            menuListView.setAdapter(classListAdapter);
        } else {
            setClassList();
        }


        // finish menu activity
        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                //ManvishCommonUtil.goToMainactivity(ClassRoomActivity.this);
            }
        });

        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManvishCommonUtil.goToMainactivity(ClassRoomActivity.this);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        //setHeading();
        setHeading();
        /*Batterylevel b1=new Batterylevel();
        b1.Batterylevelindicator();
*/       // Batterylevelindicator();
    }


    private void setClassList() {


        new AsyncTask<Void, Void, Boolean>() {
            protected void onPreExecute() {
                showProgressBar("Fetching Class data");

            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {

                    classList = dbAdapter.getUniqueClassRoomListFromStudentTable();

                    if (classList.size() > 0) {
                        return true;
                    } else {
                        return false;
                    }

                } catch (Exception e) {
                    Log.e("MANVISH",
                            "Exception in parsing file"
                                    + e.getMessage());

                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean success) {

                if (success) {
                    classListAdapter = new ClassListAdapter(classList);
                    menuListView.setAdapter(classListAdapter);
                }

                dismissProgressBar();
            }

        }.execute();
    }

    ProgressDialog mProgressDialog;

    private void showProgressBar(String msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
        }


        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    private void dismissProgressBar() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }
}
