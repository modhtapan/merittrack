package com.manvish.merittrack.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Base64;
import android.util.Xml;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.util.Log;
import com.manvish.merittrack.Model.VerData;
import com.manvish.merittrack.R;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.modelupload.AcsStudentWrite;
import com.manvish.merittrack.modelupload.EnrStudentWrite;
import com.manvish.merittrack.modelupload.ImageWrite;
import com.manvish.merittrack.modelupload.ProjectDetails;
import com.manvish.merittrack.modelupload.TemplateWrite;
import com.manvish.merittrack.utils.ManvishCommonUtil;
import com.manvish.merittrack.utils.UploadFile;
import com.manvish.merittrack.view.ManvishAlertDialog;

import net.gotev.uploadservice.UploadServiceBroadcastReceiver;

import org.apache.commons.io.FileUtils;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by pabitra on 4/7/16.
 */
public class UploadBackupActivity extends BaseActivity {


    TextView tvWarningMsg;
    DBAdapter dbAdapter;
    String zipFilePath;
    String offLineZipFilePath;
    ArrayList<String> studentSetData;
    List<AcsStudentWrite> acsStudentWriteArrayList;
    ArrayList<VerData> verDataArrayList;
    String source = "";
    String destination = "";
    String zip_password = "manvish1234";//password given for creating the zipfile creation


    private int mProgressbar = 0;
    private int mprogressBarStatus = 0;
    private Handler progressBarHandler = new Handler();


    //  private ProgressDialog mUploadProgressDialog;

    int stageCode;
    ProgressDialog mUploadProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbAdapter = DBAdapter.getInstance(this, MeritTrackApplication.getInstance().getDbName(), null, 1);

        setContentView(R.layout.activity_post_test_reg_cancel_warning);
        tvWarningMsg = (TextView) findViewById(R.id.tv_reg_close_warning);

        if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.POSTTEST)) {
            tvWarningMsg.setText("Warning!!! This operation closes The Registration Activity." + System.getProperty("line.separator") + "If you want to continue press Enter.");

        } else if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.POSTCOUNCIL)) {
            tvWarningMsg.setText("Warning!!! This operation closes The Verification Activity." + System.getProperty("line.separator") + "If you want to continue press Enter.");

        } else if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.BACKUP_USB_POSTTEST)) {
            tvWarningMsg.setText(ManvishPrefConstants.SELECTED_EVENT_CODE.read() + ":" + ManvishPrefConstants.SELECTED_DATE.read() + " Do you want to continue?");

        } else if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.BACKUP_POSTCOUNCILING)) {
            tvWarningMsg.setText(ManvishPrefConstants.SELECTED_EVENT_CODE.read() + ":" + ManvishPrefConstants.SELECTED_DATE.read() + " Do you want to continue?");

        }

        mUploadProgressDialog = new ProgressDialog(UploadBackupActivity.this);
        mUploadProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mUploadProgressDialog.setMessage("Uploading...");
        mUploadProgressDialog.setCancelable(false);
        mUploadProgressDialog.setProgress(0);

    }

    public void cancel(View v) {

        finish();//OnClick operations Present in the Xml layouts
    }

    public void enter(View v) {

        showConfirmDialog();//OnClick operations Present in the Xml layouts and OnEnter Use dilaog_cancel_ok.xml
    }

    public void showConfirmDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_cancel_ok);
        dialog.setCancelable(true);

        Button btnOk = (Button) dialog.findViewById(R.id.btn_ok);
        Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);

        btnOk.setOnClickListener(new View.OnClickListener() {
                                     @Override
                                     public void onClick(View v) {

                                         dialog.dismiss();

                                         if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                                                 equalsIgnoreCase(StringConstants.BACKUP_USB_POSTTEST)) {


                                             copyPostTestFile();

                                         } else if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                                                 equalsIgnoreCase(StringConstants.BACKUP_USB_POSTCOUNCILING)) {

                                             copyPostCouncilFile();

                                         } else if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                                                 equalsIgnoreCase(StringConstants.POSTTEST) || ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                                                 equalsIgnoreCase(StringConstants.POSTCOUNCIL)) {
                                             uploadData();
                                         }
                                     }
                                 }

        );

        btnCancel.setOnClickListener(new View.OnClickListener()

                                     {
                                         @Override
                                         public void onClick(View v) {

                                             // Start taking data back up ,
                                             // Preparing back up please wait ..
                                             // Preparing candidate data to upload ,please wait .
                                             // Uploaded candidate data ,go to Home Screen
                                             dialog.dismiss();
                                         }
                                     }

        );

        dialog.show();
    }


    private void copyPostTestFile() {

        File storageDir = null;//Checking the Directory
        File primaryDir = new File(StringConstants.USB_BACKUP_PATH_PRIMARY);//Hardcoded path
        boolean isPrimary = false;
        try {

            try {

                android.util.Log.e("size", primaryDir.listFiles().length + "");//get the List of Files
                isPrimary = true;//hardcoded path exists
                storageDir = primaryDir;//Now the Hardcoded Directory is StorageDirectory

                //This is for Post_Test USB backup


            } catch (Exception e) {

                isPrimary = false;
            }

            if (!isPrimary) {

                File secondryDir = new File(StringConstants.USB_BACKUP_PATH_SECONDRY);
                try {
                    android.util.Log.e("size", secondryDir.listFiles().length + "");

                    //This is for Post_Test USB backup

                    storageDir = secondryDir;//Now the Back up is created in the Secondary Storage But pins to the same path
                } catch (Exception e) {

                    ManvishCommonUtil.showCustomToastForNetwork(UploadBackupActivity.this, "Pendrive not detected");
                    //   Toast.makeText(context,"Pendrive not detected",Toast.LENGTH_SHORT).show();

                }
            }


        } catch (Exception e) {
            android.util.Log.e("error", e.toString());
            ManvishCommonUtil.showCustomToastForNetwork(UploadBackupActivity.this, "Pendrive not detected");

            //  Toast.makeText(context,"Pendrive not detected",Toast.LENGTH_SHORT).show();
        }

        if (storageDir == null) {
            return;
        }


        zipFilePath = Environment.getExternalStorageDirectory() + "/MeritTrack/Upload/" + ManvishPrefConstants.SERIAL_NO.read() + "_" + ManvishPrefConstants.SELECTED_EVENT_CODE.read() + "_" + ManvishPrefConstants.SELECTED_DATE.read() + "_PT.zip";
        final File sourceFile = new File(zipFilePath);
        if (sourceFile.exists()) {


            offLineZipFilePath = storageDir.getAbsolutePath() + "/miFaunBackup";//File will be created in the miFaunBackup  folder
            File file = new File(offLineZipFilePath);

            boolean rootFolder = false;

            if (!file.exists()) {
                rootFolder = file.mkdir();//created the Directory will be in the rootfolder
            } else {
                rootFolder = true;
            }

            if (rootFolder) {

                File nFile = new File(offLineZipFilePath + "/Backup_PostTest");
                boolean childFolder = false;

                if (!nFile.exists()) {
                    childFolder = nFile.mkdir();//Offline zip folder will be in the childfolder
                } else {
                    childFolder = true;
                }
                if (childFolder) {
                    offLineZipFilePath = offLineZipFilePath + "/Backup_PostTest/" + ManvishPrefConstants.SERIAL_NO.read() + "_" + ManvishPrefConstants.SELECTED_EVENT_CODE.read() + "_" + ManvishPrefConstants.SELECTED_DATE.read() + "_PT.zip";
                    final File destinationFile = new File(offLineZipFilePath);
                    copyFileInBackground(sourceFile, destinationFile);

                    //ManvishCommonUtil.showCustomToastForBackupPosttestandPostCouncil(UploadBackupActivity.this, "Copy in Progress!!!");

                    /*boolean status = ManvishCommonUtil.copyFile(sourceFile, destinationFile);

                    if (status) {//true
                        new ManvishAlertDialog(UploadBackupActivity.this, "Back Up status",
                                "Back up successful").showAlertDialog();
                    } else {
                        new ManvishAlertDialog(UploadBackupActivity.this, "Back Up status",
                                "Back up failure").showAlertDialog();

                    }
*/


                } else {
                    new ManvishAlertDialog(UploadBackupActivity.this, "Back Up status",
                            "Back up failure").showAlertDialog();
                }

            } else {
                new ManvishAlertDialog(UploadBackupActivity.this, "Back Up status",
                        "Back up failure").showAlertDialog();

            }
        } else {
            new ManvishAlertDialog(UploadBackupActivity.this, "Back Up status",
                    "Back up file not exists").showAlertDialog();
        }

    }



    private void copyPostCouncilFile(){

        File storageDir=null;
        File primaryDir = new File(StringConstants.USB_BACKUP_PATH_PRIMARY);
        boolean isPrimary = false;
        try{

            try {

                android.util.Log.e("size", primaryDir.listFiles().length + "");
                isPrimary = true;
                storageDir = primaryDir;

                //This is for Post_Test USB backup


            } catch(Exception e){

                isPrimary = false;
            }

            if(!isPrimary) {

                File secondryDir = new File(StringConstants.USB_BACKUP_PATH_SECONDRY);
                try{
                    android.util.Log.e("size", secondryDir.listFiles().length + "");

                    //This is for Post_Test USB backup

                    storageDir = secondryDir;
                }catch(Exception e){

                    ManvishCommonUtil.showCustomToastForNetwork(UploadBackupActivity.this,"Pendrive not detected");
                    //   Toast.makeText(context,"Pendrive not detected",Toast.LENGTH_SHORT).show();

                }
            }


        } catch(Exception e) {
            android.util.Log.e("error",e.toString());
            ManvishCommonUtil.showCustomToastForNetwork(UploadBackupActivity.this,"Pendrive not detected");

            //  Toast.makeText(context,"Pendrive not detected",Toast.LENGTH_SHORT).show();
        }

        if(storageDir==null){
            return;
        }


        zipFilePath = Environment.getExternalStorageDirectory()+"/MeritTrack/Upload/"+ManvishPrefConstants.SERIAL_NO.read()+"_"+ManvishPrefConstants.SELECTED_EVENT_CODE.read()+"_"+ManvishPrefConstants.SELECTED_DATE.read() + "_PC.zip";
        File sourceFile = new File(zipFilePath);
        if(sourceFile.exists()) {

            offLineZipFilePath = storageDir.getAbsolutePath() + "/miFaunBackup";
            File file = new File(offLineZipFilePath);

            boolean rootFolder = false;

            if(!file.exists()) {
                rootFolder = file.mkdir();

            } else {
                rootFolder = true;
            }

            if (rootFolder) {

                File nFile = new File(offLineZipFilePath + "/Backup_PostCouncil");
                boolean childFolder = false;

                if (!nFile.exists()) {
                    childFolder = nFile.mkdir();
                } else {
                    childFolder = true;
                }
                if (childFolder) {
                    offLineZipFilePath = offLineZipFilePath + "/Backup_PostCouncil/" + ManvishPrefConstants.SERIAL_NO.read() + "_" + ManvishPrefConstants.SELECTED_EVENT_CODE.read() + "_" + ManvishPrefConstants.SELECTED_DATE.read() + "_PC.zip";
                    File destinationFile = new File(offLineZipFilePath);
                    //ManvishCommonUtil.showCustomToastForBackupPosttestandPostCouncil(UploadBackupActivity.this,"Copy in Progress!!!");
                    //boolean status = ManvishCommonUtil.copyFile(sourceFile, destinationFile);
                    copyFileInBackground(sourceFile, destinationFile);
                } else {
                    new ManvishAlertDialog(UploadBackupActivity.this, "Back Up status",
                            "Back up failure").showAlertDialog();

                }
            } else {

                new ManvishAlertDialog(UploadBackupActivity.this, "Back Up status",
                        "Back up failure").showAlertDialog();

            }

        } else {

            new ManvishAlertDialog(UploadBackupActivity.this, "Back Up status",
                    "Backup file not exists").showAlertDialog();
        }
    }


    public final UploadServiceBroadcastReceiver uploadServiceBroadcastReceiver = new UploadServiceBroadcastReceiver(){


       @Override
        public void onProgress(String uploadId, int progress) {
            super.onProgress(uploadId, progress);

           try {
               dismissProgressBar();
           }catch (Exception e){
               e.printStackTrace();
           }

          try{
              Log.d("upload","progress:"+progress);
              Log.d(StringConstants.TAG,"progress");
              mUploadProgressDialog.show();
              mUploadProgressDialog.setProgress(progress);
              final int progressvalue=progress;

          }catch (Exception e){
              try {
                  mUploadProgressDialog.dismiss();
              }catch (Exception ex){
                  ex.printStackTrace();
              }
          }
        }

        @Override
        public void onError(String uploadId, Exception exception) {
            super.onError(uploadId, exception);

            try {
                dismissProgressBar();
            }catch (Exception e){
                e.printStackTrace();
            }
            try {
                Log.d("upload", "error:" + exception.toString());
                mUploadProgressDialog.dismiss();
                mUploadProgressDialog.setProgress(0);
                //ManvishCommonUtil.showCustomToast(UploadBackupActivity.this,"Server not found");
                new ManvishAlertDialog(UploadBackupActivity.this, "STATUS", "Server not found").showAlertDialog();
                //  Toast.makeText(UploadBackupActivity.this,"error"+exception.toString(),Toast.LENGTH_SHORT).show();
                Log.d(StringConstants.TAG, "Error");
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onCompleted(String uploadId, int serverResponseCode, byte[] serverResponseBody) {
            super.onCompleted(uploadId, serverResponseCode, serverResponseBody);


            String responseString=null;

            if(serverResponseBody!=null && serverResponseBody.length!=0) {
                try {

                    byte[] responseByte = Base64.decode(serverResponseBody, Base64.DEFAULT);
                    responseString = new String(responseByte);
                } catch (Exception e) {
                    responseString = null;
                    e.printStackTrace();
                }
            }

            String command=null;

            if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.POSTTEST)) {
                command= "RDATA";
            } else if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.POSTCOUNCIL)) {
                command="CDATA";
            }
           // System.out.println("responseString=="+responseString);
             //   System.out.println("length=="+responseString.length());

                Log.d("upload","complete");
                mUploadProgressDialog.setProgress(100);
                mUploadProgressDialog.dismiss();
                mUploadProgressDialog.setProgress(0);

            if(serverResponseCode == 200) {
                if(responseString!=null) {
                    if (responseString.contains("SUCC") && responseString.contains(ManvishPrefConstants.SERIAL_NO.read())
                    && responseString.contains(command)) {
                        //ManvishCommonUtil.showCustomToast(UploadBackupActivity.this,"Upload Completed");
                        new ManvishAlertDialog(UploadBackupActivity.this, "STATUS", "Data upload completed").showAlertDialog();


                        if (offLineZipFilePath != null && new File(offLineZipFilePath).exists()) {
                            // new File(mOfflineUploadPath).delete();
                            try {
                                //FileUtils.deleteDirectory(new File(offLineZipFilePath));
                                //FileUtils.forceDeleteOnExit(new File(offLineZipFilePath));
                                FileUtils.forceDelete(new File(offLineZipFilePath));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        // ManvishCommonUtil.showCustomToast(UploadBackupActivity.this,"Upload Failed");
                        new ManvishAlertDialog(UploadBackupActivity.this, "STATUS", "Data upload failed").showAlertDialog();
                    }

                }else{
                   // ManvishCommonUtil.showCustomToast(UploadBackupActivity.this, "Contact admin for upload status");
                    new ManvishAlertDialog(UploadBackupActivity.this, "STATUS", "Contact admin for upload status").showAlertDialog();
                }
            }else{
                //ManvishCommonUtil.showCustomToast(UploadBackupActivity.this,"Server not reachable");
                new ManvishAlertDialog(UploadBackupActivity.this,"STATUS","Server not reachable").showAlertDialog();
            }
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        try {
            uploadServiceBroadcastReceiver.unregister(this);
        } catch (Exception e){

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setHeading();
        /*Batterylevel b1=new Batterylevel();
        b1.Batterylevelindicator();*/
      //  Batterylevelindicator();
        try {
            uploadServiceBroadcastReceiver.register(this);
        } catch (Exception e){

        }
        }

    String filePathForUploadFile=null;

    private void uploadData() {

        new AsyncTask<Void, Void, String>() {
            protected void onPreExecute() {

                if(ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                        equalsIgnoreCase(StringConstants.POSTTEST) || ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                        equalsIgnoreCase(StringConstants.POSTCOUNCIL)) {


                    showProgressBar("Please wait preparing data");

                } else if(ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                        equalsIgnoreCase(StringConstants.BACKUP_USB_POSTTEST) || ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                        equalsIgnoreCase(StringConstants.BACKUP_USB_POSTCOUNCILING)) {

                    showProgressBar("Please wait backup data");
                }

            }

            @Override
            protected String doInBackground(Void... params) {
                String councilXMLString = null;
                try {

                    boolean isBackup = false;
                    if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.BACKUP_POSTTEST)
                            || ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.BACKUP_POSTCOUNCILING)) {

                        isBackup = true;
                    }

                    String selectedDate = ManvishPrefConstants.SELECTED_DATE.read();

                     verDataArrayList = dbAdapter.getAccessLogForPostCouncil(selectedDate,isBackup);

                    // Start taking data back up ,
                    // Preparing back up please wait ..
                    // Preparing candidate data to upload ,please wait .
                    // Uploaded candidate data ,go to Home Screen


                    if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.POSTTEST) || ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.BACKUP_POSTTEST)) {

                        //get data from registration Table(ENR) and as well as Verification TABLE (Session)
                        stageCode = 2;
                    } else if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.POSTCOUNCIL) || ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.BACKUP_POSTCOUNCILING)) {

                            //get data only from Verification TABLE
                            stageCode = 4;

                    }


                    String filePath = null;
                    String originalFilePath=null;

                    String offlineFolderPath = Environment.getExternalStorageDirectory()+"/MeritTrack/Upload_Offline";
                    File offlineFolder = new File(offlineFolderPath);
                    if(!offlineFolder.exists()){
                        offlineFolder.mkdirs();
                    }

                    if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                            equalsIgnoreCase(StringConstants.POSTTEST)) {

                        filePath = Environment.getExternalStorageDirectory()+"/MeritTrack/Upload/" + ManvishPrefConstants.SERIAL_NO.read()+ "_PT.xml";
                        zipFilePath = Environment.getExternalStorageDirectory()+"/MeritTrack/Upload/"+ManvishPrefConstants.SERIAL_NO.read()+"_"+ManvishPrefConstants.SELECTED_EVENT_CODE.read()+"_"+ManvishPrefConstants.SELECTED_DATE.read() + "_PT.zip";
                        originalFilePath=Environment.getExternalStorageDirectory()+"/MeritTrack/Upload/"+ManvishPrefConstants.SERIAL_NO.read()+"_"+ManvishPrefConstants.SELECTED_EVENT_CODE.read()+"_"+ManvishPrefConstants.SELECTED_DATE.read() + "_PT.zip";
                        offLineZipFilePath = Environment.getExternalStorageDirectory()+"/MeritTrack/Upload_Offline/"+ManvishPrefConstants.SERIAL_NO.read()+"_"+ManvishPrefConstants.SELECTED_EVENT_CODE.read()+"_"+ManvishPrefConstants.SELECTED_DATE.read() + "_PT.zip";


                    } else if(ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                            equalsIgnoreCase(StringConstants.POSTCOUNCIL)){

                        filePath = Environment.getExternalStorageDirectory()+"/MeritTrack/Upload/"+ManvishPrefConstants.SERIAL_NO.read()+"_PC.xml";
                        zipFilePath=Environment.getExternalStorageDirectory()+"/MeritTrack/Upload/"+ManvishPrefConstants.SERIAL_NO.read()+"_"+ManvishPrefConstants.SELECTED_EVENT_CODE.read()+"_"+ManvishPrefConstants.SELECTED_DATE.read() + "_PC.zip";
                        originalFilePath=Environment.getExternalStorageDirectory()+"/MeritTrack/Upload/"+ManvishPrefConstants.SERIAL_NO.read()+"_"+ManvishPrefConstants.SELECTED_EVENT_CODE.read()+"_"+ManvishPrefConstants.SELECTED_DATE.read() + "_PT.zip";
                        offLineZipFilePath = Environment.getExternalStorageDirectory()+"/MeritTrack/Upload_Offline/"+ManvishPrefConstants.SERIAL_NO.read()+"_"+ManvishPrefConstants.SELECTED_EVENT_CODE.read()+"_"+ManvishPrefConstants.SELECTED_DATE.read() + "_PC.zip";

                    } else if(ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                            equalsIgnoreCase(StringConstants.BACKUP_POSTTEST)){

                        filePath = Environment.getExternalStorageDirectory()+"/MeritTrack/Backup_PostTest/"+ManvishPrefConstants.SERIAL_NO.read()+ "_PT.xml";
                        originalFilePath=Environment.getExternalStorageDirectory()+"/MeritTrack/Upload/"+ManvishPrefConstants.SERIAL_NO.read()+"_"+ManvishPrefConstants.SELECTED_EVENT_CODE.read()+"_"+ManvishPrefConstants.SELECTED_DATE.read() + "_PT.zip";
                        zipFilePath=Environment.getExternalStorageDirectory()+"/MeritTrack/Backup_PostTest/"+ManvishPrefConstants.SERIAL_NO.read()+"_"+ManvishPrefConstants.SELECTED_EVENT_CODE.read()+"_"+ManvishPrefConstants.SELECTED_DATE.read() + "_PT.zip";

                    } else if(ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                            equalsIgnoreCase(StringConstants.BACKUP_POSTCOUNCILING)){

                        filePath = Environment.getExternalStorageDirectory()+"/MeritTrack/Backup_PostCounciling/"+ManvishPrefConstants.SERIAL_NO.read()+ "_PC.xml";
                        originalFilePath=Environment.getExternalStorageDirectory()+"/MeritTrack/Upload/"+ManvishPrefConstants.SERIAL_NO.read()+"_"+ManvishPrefConstants.SELECTED_EVENT_CODE.read()+"_"+ManvishPrefConstants.SELECTED_DATE.read() + "_PT.zip";
                        zipFilePath=Environment.getExternalStorageDirectory()+"/MeritTrack/Backup_PostCounciling/"+ManvishPrefConstants.SERIAL_NO.read()+"_"+ManvishPrefConstants.SELECTED_EVENT_CODE.read()+"_"+ManvishPrefConstants.SELECTED_DATE.read() + "_PC.zip";

                    } else if(ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                            equalsIgnoreCase(StringConstants.BACKUP_USB_POSTTEST)){


                        File storageDir=null;
                        File primaryDir = new File(StringConstants.USB_BACKUP_PATH_PRIMARY);
                        boolean isPrimary = false;
                        try{

                            try {

                                android.util.Log.e("size", primaryDir.listFiles().length + "");
                                isPrimary = true;
                                storageDir = primaryDir;

                            } catch(Exception e){

                                isPrimary = false;
                            }

                            if(!isPrimary) {

                                File secondryDir = new File(StringConstants.USB_BACKUP_PATH_SECONDRY);
                                try{
                                    android.util.Log.e("size", secondryDir.listFiles().length + "");

                                    storageDir = secondryDir;
                                }catch(Exception e){
                                    Toast.makeText(UploadBackupActivity.this,"Pendrive not detected",Toast.LENGTH_SHORT).show();

                                }
                            }



                            File baseFile = new File(storageDir.getAbsolutePath()+"/MiFaunBackup");

                            boolean success = baseFile.mkdirs();

                            File outputFile = new File(baseFile.getAbsolutePath()+"/Backup_PostTest");
                            outputFile.mkdirs();


                            filePath = outputFile.getAbsolutePath() + ManvishPrefConstants.SERIAL_NO.read() + "_PT.xml";
                            zipFilePath = outputFile.getAbsolutePath() + ManvishPrefConstants.SERIAL_NO.read() + "_" + ManvishPrefConstants.SELECTED_EVENT_CODE.read() + "_" + ManvishPrefConstants.SELECTED_DATE.read() + "_PT.zip";




                        } catch(Exception e) {
                            android.util.Log.e("error",e.toString());
                            Toast.makeText(UploadBackupActivity.this,"Pendrive not detected",Toast.LENGTH_SHORT).show();
                        }


                    } else if(ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                            equalsIgnoreCase(StringConstants.BACKUP_USB_POSTCOUNCILING)) {


                        File storageDir=null;
                        File primaryDir = new File(StringConstants.USB_BACKUP_PATH_PRIMARY);
                        boolean isPrimary = false;
                        try{

                            try {

                                android.util.Log.e("size", primaryDir.listFiles().length + "");
                                isPrimary = true;
                                storageDir = primaryDir;

                            } catch(Exception e){

                                isPrimary = false;
                            }

                            if(!isPrimary) {

                                File secondryDir = new File(StringConstants.USB_BACKUP_PATH_SECONDRY);
                                try{
                                    android.util.Log.e("size", secondryDir.listFiles().length + "");

                                    storageDir = secondryDir;
                                }catch(Exception e){
                                    Toast.makeText(UploadBackupActivity.this,"Pendrive not detected",Toast.LENGTH_SHORT).show();

                                }
                            }

                            File baseFile = new File(storageDir.getAbsolutePath()+"/MiFaunBackup");

                            boolean success = baseFile.mkdirs();

                            File outputFile = new File(baseFile.getAbsolutePath()+"/Backup_PostCounciling");

                            outputFile.mkdirs();


                            filePath = outputFile.getAbsolutePath() + ManvishPrefConstants.SERIAL_NO.read() + "_PC.xml";
                            originalFilePath=Environment.getExternalStorageDirectory()+"/MeritTrack/Upload/"+ManvishPrefConstants.SERIAL_NO.read()+"_"+ManvishPrefConstants.SELECTED_EVENT_CODE.read()+"_"+ManvishPrefConstants.SELECTED_DATE.read() + "_PT.zip";
                            zipFilePath = outputFile.getAbsolutePath() + ManvishPrefConstants.SERIAL_NO.read() + "_" + ManvishPrefConstants.SELECTED_EVENT_CODE.read() + "_" + ManvishPrefConstants.SELECTED_DATE.read() + "_PC.zip";




                        } catch(Exception e) {
                            android.util.Log.e("error",e.toString());
                            Toast.makeText(UploadBackupActivity.this,"Pendrive not detected",Toast.LENGTH_SHORT).show();
                        }
                    }



                    ProjectDetails projectDetails = new ProjectDetails();
                    projectDetails.setProjCode(ManvishPrefConstants.SELECTED_EVENT_CODE.read());
                    projectDetails.setStage("" + stageCode);
                    projectDetails.setVenueCode("  "); // Venue is not needed as we are inserting data DateWise

                    //pabitra change <code></code>

                    XmlSerializer xmlSerializer = Xml.newSerializer();
                    StringWriter writer = new StringWriter();

                    xmlSerializer.setOutput(writer);
                    // start DOCUMENT
                    xmlSerializer.startDocument("UTF-8", true);

                    xmlSerializer.startTag("",
                            StringConstants.XML_ROOT);

                    xmlSerializer.startTag("",
                            StringConstants.XML_PROJECT_DETAILS);

                    xmlSerializer.startTag("",
                            StringConstants.XML_PROJECT_CODE);

                    xmlSerializer.text(projectDetails.getProjCode());

                    xmlSerializer.endTag("",
                            StringConstants.XML_PROJECT_CODE);

                    xmlSerializer.startTag("",
                            StringConstants.XML_DATE);

                    xmlSerializer.text(ManvishPrefConstants.SELECTED_DATE.read());

                    xmlSerializer.endTag("",
                            StringConstants.XML_DATE);

                    xmlSerializer.startTag("",
                            StringConstants.XML_VENUE_CODE);

                    xmlSerializer.text(projectDetails.getVenueCode());

                    xmlSerializer.endTag("",
                            StringConstants.XML_VENUE_CODE);


                    xmlSerializer.startTag("",
                            StringConstants.XML_STAGE);

                    xmlSerializer.text(stageCode+"");

                    xmlSerializer.endTag("",
                            StringConstants.XML_STAGE);

                    xmlSerializer.endTag("", StringConstants.XML_PROJECT_DETAILS);

                    //Write to this file for testing
                    xmlSerializer.endDocument();
                    //String appendfilePath = Environment.getExternalStorageDirectory()+"/MeritTrack/Upload/" + ManvishPrefConstants.SERIAL_NO.read()+ "_PT.xml";
                    String originalString=writer.toString();
                    String finalString=originalString.replace(StringConstants.root_tag_end,"");
                    FileUtils.writeStringToFile(new File(filePath),finalString);

                    if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                            equalsIgnoreCase(StringConstants.POSTTEST) || ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                            equalsIgnoreCase(StringConstants.BACKUP_POSTTEST)) {

                        studentSetData = dbAdapter.getRegStudentUniqueDta(selectedDate, isBackup);
                        // Get students who are not uploaded ,that is Upload flag is 0

                        Log.e("student data", studentSetData.size() + ":size");

                        XmlSerializer xmlSerializer1 = Xml.newSerializer();
                        StringWriter writer1 = new StringWriter();
                        xmlSerializer1.setOutput(writer1);
                        // start DOCUMENT
                        xmlSerializer1.startDocument("UTF-8", true);
                        xmlSerializer1.startTag("",
                                StringConstants.XML_ENR_DATA);

                        xmlSerializer1.endDocument();
                        String originalString1=writer1.toString();
                        String finalString1=originalString1.replace(StringConstants.xml_header_tag,"");
                        FileUtils.writeStringToFile(new File(filePath), finalString1, true);

                        for (String stdDataString : studentSetData) {

                            String[] stddataArray = stdDataString.split(",");
                            // studentSet.add(venueCode+","+studentID+","+studentName+","+testDate+","+studentBatchCode+","+studentClassroom);
                            String venueCode = stddataArray[0];
                            String studentId = stddataArray[1];
                            String studentName = stddataArray[2];
                            String testDate = stddataArray[3];
                            String batch = stddataArray[4];
                            String classRoom = stddataArray[5];
                            String regdate = stddataArray[6];
                            String regTime = stddataArray[7];

                            // get TemplateData for a perticularStudent

                            ArrayList<TemplateWrite> templateListToUpload = dbAdapter.getTemplateToUpload(selectedDate, venueCode, batch, classRoom, studentId);
                            ArrayList<ImageWrite> fingerImageListToUpload = dbAdapter.getFingerImageToUpload(selectedDate, venueCode, batch, classRoom, studentId);

                            byte[] photoGraph = dbAdapter.getRegPhotoForPostTest(ManvishPrefConstants.SELECTED_DATE.read(), venueCode, batch, classRoom, studentId);
                            String photoString = " ";
                            if (photoGraph != null || photoGraph.length != 0) {
                                photoString = Base64.encodeToString(photoGraph, Base64.DEFAULT);
                            }

                            EnrStudentWrite studentWrite = new EnrStudentWrite();
                            studentWrite.setStudentId(studentId);
                            studentWrite.setUnitId(ManvishPrefConstants.SERIAL_NO.read());
                            studentWrite.setProjCode(ManvishPrefConstants.SELECTED_EVENT_CODE.read());
                            studentWrite.setRegDate(regdate);
                            studentWrite.setRegTime(regTime);

                            studentWrite.setPhotograph(photoString);

                            studentWrite.setTemplate(templateListToUpload);
                            studentWrite.setImage(fingerImageListToUpload);

                           // enrStudentWriteArrayList.add(enrStudentWrite);

                            XmlSerializer xmlSerializer2 = Xml.newSerializer();
                            StringWriter writer2 = new StringWriter();
                            xmlSerializer2.setOutput(writer2);
                            // start DOCUMENT
                            xmlSerializer2.startDocument("UTF-8", true);

                            xmlSerializer2.startTag("",
                                    StringConstants.XML_STUDENT);





                            xmlSerializer2.startTag("",
                                    StringConstants.XML_STUDENT_ID);

                            xmlSerializer2.text(studentWrite.getStudentId());

                            xmlSerializer2.endTag("",
                                    StringConstants.XML_STUDENT_ID);

                            xmlSerializer2.startTag("",
                                    StringConstants.XML_UNIT_ID);

                            xmlSerializer2.text(studentWrite.getUnitId());

                            xmlSerializer2.endTag("",
                                    StringConstants.XML_UNIT_ID);

                            xmlSerializer2.startTag("",
                                    StringConstants.XML_PROJECT_CODE);

                            xmlSerializer2.text(studentWrite.getProjCode());

                            xmlSerializer2.endTag("",
                                    StringConstants.XML_PROJECT_CODE);

                            xmlSerializer2.startTag("",
                                    StringConstants.XML_REG_DATE);
                            xmlSerializer2.text(studentWrite.getRegDate());
                            xmlSerializer2.endTag("",
                                    StringConstants.XML_REG_DATE);


                            xmlSerializer2.startTag("",
                                    StringConstants.XML_REG_TIME);
                            xmlSerializer2.text(studentWrite.getRegTime());
                            xmlSerializer2.endTag("",
                                    StringConstants.XML_REG_TIME);


                            int i=0;
                            for (TemplateWrite templateWrite : studentWrite.getTemplate()) {
                                xmlSerializer2.startTag("",
                                        StringConstants.XML_TEMPLATE);

                                xmlSerializer2.startTag("",
                                        StringConstants.XML_FINGER_TYPE);

                                int fingerType=Integer.parseInt(templateWrite.getFingerType().trim());
                                if(i>1){
                                    int multi=i/2;
                                    xmlSerializer2.text(fingerType+10*multi+"");
                                }else{
                                    xmlSerializer2.text(templateWrite.getFingerType());
                                }


                                xmlSerializer2.endTag("",
                                        StringConstants.XML_FINGER_TYPE);

                                i++;
                                xmlSerializer2.startTag("",
                                        StringConstants.XML_TDATA);
                                xmlSerializer2.text(templateWrite.gettData());

                                xmlSerializer2.endTag("",
                                        StringConstants.XML_TDATA);

                                xmlSerializer2.startTag("",
                                        StringConstants.XML_REG_STATUS);

                                if(templateWrite.getRegStatus().equalsIgnoreCase("-1")){
                                    xmlSerializer2.text("F");
                                }else if(templateWrite.getRegStatus().equalsIgnoreCase("1")){
                                    xmlSerializer2.text("R");
                                }
                                // xmlSerializer.text(templateWrite.getRegStatus());
                                xmlSerializer2.endTag("",
                                        StringConstants.XML_REG_STATUS);

                                xmlSerializer2.startTag("",
                                        StringConstants.XML_VER_STATUS);

                                xmlSerializer2.text("F"); // We are not capturing anything for Offline verification now ..
                                xmlSerializer2.endTag("",
                                        StringConstants.XML_VER_STATUS);


                                xmlSerializer2.endTag("",
                                        StringConstants.XML_TEMPLATE);

                            }

                            int j=0;
                            for (ImageWrite imageWrite : studentWrite.getImage()) {
                                xmlSerializer2.startTag("",
                                        StringConstants.XML_IMAGE);

                                xmlSerializer2.startTag("",
                                        StringConstants.XML_ITYPE);
                                int fingerType=Integer.parseInt(imageWrite.getiType().trim());
                                if(j>1){
                                    int multi=j/2;
                                    xmlSerializer2.text(fingerType+10*multi+"");
                                }else{
                                    xmlSerializer2.text(imageWrite.getiType());
                                }

                                j++;
                                xmlSerializer2.endTag("",
                                        StringConstants.XML_ITYPE);

                                xmlSerializer2.startTag("",
                                        StringConstants.XML_IDATA);

                                xmlSerializer2.text(imageWrite.getiData());
                                xmlSerializer2.endTag("",
                                        StringConstants.XML_IDATA);

                                xmlSerializer2.startTag("",
                                        StringConstants.XML_REG_STATUS);

                                if(imageWrite.getRegStatus().trim().equalsIgnoreCase("-1")){
                                    xmlSerializer2.text("F");
                                }else if(imageWrite.getRegStatus().trim().equalsIgnoreCase("1")){
                                    xmlSerializer2.text("R");
                                }

                                // xmlSerializer.text(imageWrite.getRegStatus());
                                xmlSerializer2.endTag("",
                                        StringConstants.XML_REG_STATUS);

                                xmlSerializer2.startTag("",
                                        StringConstants.XML_VER_STATUS);

                                xmlSerializer2.text("F"); // hard codeed for this ,we are not cnsidering offline verification ..

                                xmlSerializer2.endTag("",
                                        StringConstants.XML_VER_STATUS);

                                xmlSerializer2.endTag("",
                                        StringConstants.XML_IMAGE);

                            }

                            xmlSerializer2.startTag("",
                                    StringConstants.XML_PHOTOGRAPH);
                            xmlSerializer2.text(studentWrite.getPhotograph());
                            xmlSerializer2.endTag("",
                                    StringConstants.XML_PHOTOGRAPH);


                            xmlSerializer2.endTag("",
                                    StringConstants.XML_STUDENT);

                            xmlSerializer2.endDocument();
                            String originalString2=writer2.toString();
                            String finalString2=originalString2.replace(StringConstants.xml_header_tag,"");
                            FileUtils.writeStringToFile(new File(filePath), finalString2, true);

                        }


                    /*    XmlSerializer xmlSerializer3 = Xml.newSerializer();
                        StringWriter writer3 = new StringWriter();
                        xmlSerializer3.setOutput(writer3);
                        // start DOCUMENT
                        xmlSerializer3.startDocument("UTF-8", true);
                        xmlSerializer3.endTag("",
                                StringConstants.XML_ENR_DATA);

                        xmlSerializer3.endDocument();
                        String originalString3=writer3.toString();
                        String finalString3=originalString3.replace(StringConstants.xml_header_tag,"");*/
                        FileUtils.writeStringToFile(new File(filePath), StringConstants.XML_ENR_DATA_END, true);

                    }
                     acsStudentWriteArrayList = new ArrayList<>();
                    AcsStudentWrite studentWrite;

                    if(verDataArrayList!=null && verDataArrayList.size()!=0) {

                        XmlSerializer xmlSerializer4 = Xml.newSerializer();
                        StringWriter writer4 = new StringWriter();
                        xmlSerializer4.setOutput(writer4);
                        // start DOCUMENT
                        xmlSerializer4.startDocument("UTF-8", true);
                        xmlSerializer4.startTag("",
                                StringConstants.XML_ACS_LOG);

                        xmlSerializer4.endDocument();
                        String originalString4=writer4.toString();
                        String finalString4=originalString4.replace(StringConstants.xml_header_tag,"");
                        FileUtils.writeStringToFile(new File(filePath), finalString4, true);

                        for (VerData verData : verDataArrayList) {
                            studentWrite = new AcsStudentWrite();
                            studentWrite.setStudentId(verData.getStudentID());
                            studentWrite.setUnitId(ManvishPrefConstants.SERIAL_NO.read());
                            studentWrite.setProjCode(ManvishPrefConstants.SELECTED_EVENT_CODE.read());
                            studentWrite.setVerDate(verData.getVerDate());
                            studentWrite.setVerTime(verData.getVerTime());
                            studentWrite.setInOut("INN");
                            studentWrite.setStatus(verData.getVerStatus());
                            studentWrite.setFingerType(verData.getFingerType());
                            if (verData.getSessionCode() == null) {
                                studentWrite.setSessionCode(" ");//hardcoded
                            } else {
                                studentWrite.setSessionCode(verData.getSessionCode());//hardcoded
                            }

                            XmlSerializer xmlSerializer5 = Xml.newSerializer();
                            StringWriter writer5 = new StringWriter();
                            xmlSerializer5.setOutput(writer5);
                            // start DOCUMENT
                            xmlSerializer5.startDocument("UTF-8", true);
                            xmlSerializer5.startTag("",
                                    StringConstants.XML_STUDENT);




                            xmlSerializer5.startTag("",
                                    StringConstants.XML_STUDENT_ID);

                            xmlSerializer5.text(studentWrite.getStudentId());

                            xmlSerializer5.endTag("",
                                    StringConstants.XML_STUDENT_ID);

                            xmlSerializer5.startTag("",
                                    StringConstants.XML_UNIT_ID);

                            xmlSerializer5.text(studentWrite.getUnitId());

                            xmlSerializer5.endTag("",
                                    StringConstants.XML_UNIT_ID);

                            xmlSerializer5.startTag("",
                                    StringConstants.XML_PROJECT_CODE);

                            xmlSerializer5.text(studentWrite.getProjCode());

                            xmlSerializer5.endTag("",
                                    StringConstants.XML_PROJECT_CODE);


                            xmlSerializer5.startTag("",
                                    StringConstants.XML_VER_DATE);
                            xmlSerializer5.text(studentWrite.getVerDate());
                            xmlSerializer5.endTag("",
                                    StringConstants.XML_VER_DATE);


                            xmlSerializer5.startTag("",
                                    StringConstants.XML_VER_TIME);
                            xmlSerializer5.text(studentWrite.getVerTime());
                            xmlSerializer5.endTag("",
                                    StringConstants.XML_VER_TIME);


                            xmlSerializer5.startTag("",
                                    StringConstants.XML_IN_OUT);
                            xmlSerializer5.text(studentWrite.getInOut());
                            xmlSerializer5.endTag("",
                                    StringConstants.XML_IN_OUT);


                            xmlSerializer5.startTag("",
                                    StringConstants.XML_STATUS);

                            if(studentWrite.getStatus().trim().equalsIgnoreCase("-1")){
                                xmlSerializer5.text("F");
                            }else if(studentWrite.getStatus().trim().equalsIgnoreCase("1")){
                                xmlSerializer5.text("S");
                            }

                            //  xmlSerializer.text(studentWrite.getStatus());
                            xmlSerializer5.endTag("",
                                    StringConstants.XML_STATUS);

                            xmlSerializer5.startTag("",
                                    StringConstants.XML_FINGER_TYPE);
                            xmlSerializer5.text(studentWrite.getFingerType());
                            xmlSerializer5.endTag("",
                                    StringConstants.XML_FINGER_TYPE);

                            xmlSerializer5.startTag("",
                                    StringConstants.XML_SESSION_CODE);
                            xmlSerializer5.text(studentWrite.getSessionCode());
                            xmlSerializer5.endTag("",
                                    StringConstants.XML_SESSION_CODE);


                            xmlSerializer5.endTag("",
                                    StringConstants.XML_STUDENT);

                            xmlSerializer5.endDocument();
                            String originalString5=writer5.toString();
                            String finalString5=originalString5.replace(StringConstants.xml_header_tag,"");
                            FileUtils.writeStringToFile(new File(filePath), finalString5, true);
                        }

                       /* XmlSerializer xmlSerializer6 = Xml.newSerializer();
                        StringWriter writer6 = new StringWriter();
                        xmlSerializer6.setOutput(writer6);
                        // end DOCUMENT
                        xmlSerializer6.startDocument("UTF-8", true);
                        xmlSerializer.endTag("",
                                StringConstants.XML_ACS_LOG);
                        xmlSerializer6.endDocument();
                        String originalString6=writer6.toString();
                        String finalString6=originalString6.replace(StringConstants.xml_header_tag,"");
*/                        FileUtils.writeStringToFile(new File(filePath), StringConstants.XML_ACS_LOG_END, true);
                    }
                    System.out.println("upload");
                    FileUtils.writeStringToFile(new File(filePath),StringConstants.root_tag_end,true); // add ("</root> to close the file")
                    System.out.println("upload file create");
                   // ManvishCommonUtil.zipFileWithPassword(new String[] {originalFilePath},zipFilePath);
                    ManvishCommonUtil.zip(new String[]{filePath}, zipFilePath);
                    System.out.println("upload zip create");
                        if(ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                                equalsIgnoreCase(StringConstants.POSTTEST) || ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                                equalsIgnoreCase(StringConstants.POSTCOUNCIL)) {

                            ManvishCommonUtil.copyFile(new File(zipFilePath), new File(offLineZipFilePath));
                        }

                        return "hi";


                } catch (IOException e) {
                    //Log.e("upload",e.toString());
                    e.printStackTrace();
                    return null;
                }

            }


            @Override
            protected void onPostExecute(final String fileDataString) {
                // dismissProgressBar();

                //Toast.makeText(UploadBackupActivity.this,"I am in onPost"+councilXMLString,Toast.LENGTH_LONG).show();


                if(new File(zipFilePath).exists()){

                    //Toast.makeText(UploadBackupActivity.this,"I am in onPost not null"+new File(zipFilePath).length(),Toast.LENGTH_LONG).show();
                    Log.d(StringConstants.TAG,"I am in on Post not null");
                   try{

                       if(ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                               equalsIgnoreCase(StringConstants.POSTTEST) || ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                               equalsIgnoreCase(StringConstants.POSTCOUNCIL)) {

                           // TODO pabitra update student table, update reg table. update ver table -- fuck u

                           new AsyncTask<String, String, String>() {

                               @Override
                               protected void onPreExecute() {
                                   super.onPreExecute();
                                   try {
                                       showProgressBar("Updating database");
                                   }catch (Exception e){
                                       e.printStackTrace();
                                   }
                               }

                               @Override
                               protected String doInBackground(String... params) {



                                   if(ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                                           equalsIgnoreCase(StringConstants.POSTTEST)){
                                       //Update both UPLOAD_FLAG_REG and UPLOAD_FLAG_VAR in TABLE_STUDENT

                                       // Update in Student table for upload flag reg==

                                       if(studentSetData!=null && studentSetData.size()!=0) {
                                           for (String stdDataString : studentSetData) { // Here studentSetData are from registration table ,which are already registered .
                                               String stddataArray[] = stdDataString.split(",");
                                               String venueCode = stddataArray[0];
                                               String studentId = stddataArray[1];
                                               String studentName = stddataArray[2];
                                               String testDate = stddataArray[3];
                                               String batch = stddataArray[4];
                                               String classRoom = stddataArray[5];
                                               String uploadFlag = "1"; // Flag 1 means data is gone through upload process
                                               dbAdapter.updatestudentRegUploadStatus(testDate, venueCode, batch, classRoom, studentId, uploadFlag);
                                           }

                                       }

                                       if(verDataArrayList!=null && verDataArrayList.size()>0) {
                                           for (VerData verData : verDataArrayList) { // Here studentSetData are from registration table ,which are already registered .

                                               String venueCode = verData.getVenueCode();
                                               String studentId = verData.getStudentID();
                                               String studentName = verData.getStudentName();
                                               String testDate = verData.getTestDate();
                                               String batch = verData.getBatchCode();
                                               String classRoom = verData.getClassRoom();
                                               String uploadFlag = "1"; // Flag 1 means data is gone through upload process
                                               dbAdapter.updatestudentVerUploadStatus(testDate, venueCode, batch, classRoom, studentId, uploadFlag);
                                           }
                                       }
                                   }else{
                                       // Upload happened for post councilling ,
                                       // As councilling does not have any reg data ,so upload status of VER_Flag

                                       if(verDataArrayList!=null && verDataArrayList.size()>0) {
                                           for (VerData verData : verDataArrayList) { // Here studentSetData are from registration table ,which are already registered .

                                               String venueCode = verData.getVenueCode();
                                               String studentId = verData.getStudentID();
                                               String studentName = verData.getStudentName();
                                               String testDate = verData.getTestDate();
                                               String batch = verData.getBatchCode();
                                               String classRoom = verData.getClassRoom();
                                               String uploadFlag = "1"; // Flag 1 means data is gone through upload process
                                               dbAdapter.updatestudentVerUploadStatus(testDate, venueCode, batch, classRoom, studentId, uploadFlag);
                                           }
                                       }
                                   }

                                   return null;
                               }

                               @Override
                               protected void onPostExecute(String s) {
                                   super.onPostExecute(s);

                                   System.out.println("upload start");

                                   UploadFile.uploadMultipart(UploadBackupActivity.this, "1", zipFilePath);

                               }
                           }.execute();


                       } else if(ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                               equalsIgnoreCase(StringConstants.BACKUP_POSTTEST) || ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                               equalsIgnoreCase(StringConstants.BACKUP_POSTCOUNCILING) || ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                               equalsIgnoreCase(StringConstants.BACKUP_USB_POSTTEST) || ManvishPrefConstants.SELECTED_EVENT_STAGE.read().
                               equalsIgnoreCase(StringConstants.BACKUP_USB_POSTCOUNCILING)) {


                         //  Toast.makeText(UploadBackupActivity.this,"Backup Successful",Toast.LENGTH_SHORT).show();
                           new ManvishAlertDialog(UploadBackupActivity.this, "Back Up status",
                                   "Back up Successfull").showAlertDialog();
                           try {
                               dismissProgressBar();
                           }catch (Exception e){
                               e.printStackTrace();
                           }
                          // Log.d(StringConstants.TAG,"Back up Completed");
                       }

                    }catch(Exception e){
                       Log.d("upload","exception"+e.toString());
                        e.printStackTrace();
                        dismissProgressBar();
                    }


                }else{
                    try {
                        dismissProgressBar();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    ManvishCommonUtil.showCustomToast(UploadBackupActivity.this,"Upload file do not found,Please try after some time");
                }


            }

        }.execute();
    }

    ProgressDialog mProgressDialog;
    private int mProgressDialogstatus=0;
    private Handler mProgressDialogHandler = new Handler();
    private void showProgressBar(String msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
        }


        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    private void dismissProgressBar() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    public long byteToInt(byte[] bytes, int length) {
        int val = 0;
        if (length > 4)
            throw new RuntimeException("Too big to fit in int");
        for (int i = 0; i < length; i++) {
            val = val << 8;
            val = val | (bytes[i] & 0xFF);
        }
        return val;
    }

    private void copyFileInBackground(final File sourceFile,final File destinationFile){

        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                boolean copyStatus = ManvishCommonUtil.copyFile(sourceFile, destinationFile);
                return copyStatus;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showProgressBar("Copying file .Please wait");
            }

            @Override
            protected void onProgressUpdate(Void... values) {
                super.onProgressUpdate(values);
            }

            @Override
            protected void onPostExecute(Boolean copyStatus) {
                super.onPostExecute(copyStatus);
                if (copyStatus) {

                    new ManvishAlertDialog(UploadBackupActivity.this, "Back Up status",
                            "Back up successful").showAlertDialog();
                } else {
                    new ManvishAlertDialog(UploadBackupActivity.this, "Back Up status",
                            "Back up failure").showAlertDialog();
                }
                dismissProgressBar();
            }
        }.execute();
    }


}
