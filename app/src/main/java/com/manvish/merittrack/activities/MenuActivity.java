package com.manvish.merittrack.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.manvish.merittrack.R;
import com.manvish.merittrack.adapters.MenuListAdapter;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.utils.InitializeSensor;
import com.manvish.merittrack.utils.ManvishCommonUtil;
import com.morpho.morphosample.info.ProcessInfo;

// MenuActivity for showing all menu lists
public class MenuActivity extends BaseActivity {


    Button buttonhome;
    InitializeSensor mSensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        mSensor= new InitializeSensor(this);


        ListView menuListView = (ListView) findViewById(android.R.id.list);
        buttonhome = (Button) findViewById(R.id.btnHome);
        MenuListAdapter menuListAdapter = new MenuListAdapter();
        menuListView.setAdapter(menuListAdapter);

        // finish menu activity
        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ManvishCommonUtil.goToMainactivity(MenuActivity.this);
            }
        });

        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManvishCommonUtil.goToMainactivity(MenuActivity.this);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

       /* new Thread(new Runnable() {
            @Override
            public void run() {
                if(ProcessInfo.getInstance().getMorphoDevice()==null) {
                    ManvishCommonUtil.initializeSensor(MenuActivity.this,mSensor);
                }

            }
        });*/

        // setHeading();
        setHeading();
        /*Batterylevel b1=new Batterylevel();
        b1.Batterylevelindicator();*/
        // Batterylevelindicator();

        ManvishPrefConstants.IS_TEST.write(false);
    }

    @Override
    protected void onDestroy() {


        super.onDestroy();
    }
}
