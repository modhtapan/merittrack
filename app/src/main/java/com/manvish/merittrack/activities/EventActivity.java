package com.manvish.merittrack.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.manvish.merittrack.R;
import com.manvish.merittrack.adapters.EventListAdapter;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.utils.ManvishCommonUtil;

// MenuActivity for showing all menu lists
public class EventActivity extends BaseActivity {

    Button buttonhome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        ListView menuListView = (ListView) findViewById(android.R.id.list);
        EventListAdapter eventListAdapter = new EventListAdapter();
        menuListView.setAdapter(eventListAdapter);
        buttonhome = (Button) findViewById(R.id.btnHome);

        // finish menu activity
        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                //ManvishCommonUtil.goToMainactivity(EventActivity.this);
            }
        });

        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManvishCommonUtil.goToMainactivity(EventActivity.this);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        //setHeading();
        setHeading();
        /*Batterylevel b1=new Batterylevel();
        b1.Batterylevelindicator();
*/      //  Batterylevelindicator();

        //Close the already opened database ==

        if (ManvishPrefConstants.SELECTED_EVENT_CODE.read() != null) {
            DBAdapter dbAdapter = DBAdapter.getInstance(this, ManvishPrefConstants.SELECTED_EVENT_CODE.read().trim() + ManvishPrefConstants.STAGE.read() + ".db", null, 1);
            dbAdapter.closeDatabase();
        }

    }
}
