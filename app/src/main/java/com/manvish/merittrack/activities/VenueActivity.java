package com.manvish.merittrack.activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.util.Log;
import com.manvish.merittrack.R;
import com.manvish.merittrack.adapters.VenueListAdapter;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.utils.ManvishCommonUtil;

import java.util.ArrayList;

// MenuActivity for showing all menu lists
public class VenueActivity extends BaseActivity {


    TextView tveventname, tvselecteddate;
    ListView menuListView;
    DBAdapter dbAdapter;
    Button buttonhome;
    VenueListAdapter venueListAdapter;
    ArrayList<String> venueList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue);

        dbAdapter = DBAdapter.getInstance(this, MeritTrackApplication.getInstance().getDbName(), null, 1);
        menuListView = (ListView) findViewById(android.R.id.list);
        tveventname = (TextView) findViewById(R.id.tv_event_name);
        tvselecteddate = (TextView) findViewById(R.id.tv_selected_date);

        tveventname.setText(ManvishPrefConstants.SELECTED_EVENT_CODE.read());
        tvselecteddate.setText(ManvishPrefConstants.SELECTED_DATE.read());

        buttonhome = (Button) findViewById(R.id.btnHome);
        if (ManvishPrefConstants.IS_TEST.read()) {
            venueListAdapter = new VenueListAdapter(null);
            menuListView.setAdapter(venueListAdapter);
        } else {
            setDateList();
        }

        // finish menu activity
        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                //ManvishCommonUtil.goToMainactivity(VenueActivity.this);
            }
        });

        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManvishCommonUtil.goToMainactivity(VenueActivity.this);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        // setHeading();
        setHeading();
        /*Batterylevel b1=new Batterylevel();
        b1.Batterylevelindicator();*/
        // Batterylevelindicator();
    }

    private void setDateList() {


        new AsyncTask<Void, Void, Boolean>() {
            protected void onPreExecute() {
                showProgressBar("Fetching Exam Dates");

            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {

                    venueList = dbAdapter.getVenueList();

                    if (venueList.size() > 0) {
                        return true;
                    } else {
                        return false;
                    }

                } catch (Exception e) {
                    Log.e("MANVISH",
                            "Exception in parsing file"
                                    + e.getMessage());

                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean success) {

                if (success) {
                    venueListAdapter = new VenueListAdapter(venueList);
                    menuListView.setAdapter(venueListAdapter);
                }

                dismissProgressBar();
            }

        }.execute();
    }

    ProgressDialog mProgressDialog;

    private void showProgressBar(String msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
        }


        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    private void dismissProgressBar() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }
}
