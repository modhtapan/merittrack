package com.manvish.merittrack.activities;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.manvish.merittrack.Model.Student;
import com.manvish.merittrack.R;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.fragment.AlertDialogRadioFragment;
import com.manvish.merittrack.modelupload.AcsStudentWrite;
import com.manvish.merittrack.modelupload.Acslog;
import com.manvish.merittrack.modelupload.EnrData;
import com.manvish.merittrack.modelupload.EnrStudentWrite;
import com.manvish.merittrack.modelupload.ImageWrite;
import com.manvish.merittrack.modelupload.ProjectDetails;
import com.manvish.merittrack.modelupload.RootModel;
import com.manvish.merittrack.modelupload.TemplateWrite;
import com.manvish.merittrack.utils.InitializeSensor;
import com.manvish.merittrack.utils.ManvishCommonUtil;
import com.manvish.merittrack.utils.ManvishFileUtils;
import com.manvish.merittrack.view.ManvishAlertDialog;
import com.morpho.android.usb.USBManager;
import com.morpho.morphosample.info.ProcessInfo;

import org.apache.commons.io.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
public class CameraActivity extends BaseActivity implements
        View.OnClickListener, PictureCallback, AlertDialogRadioFragment.AlertPositiveListener {

    public static final String TAG = "CameraFragment";
    private Camera camera;

    private Button photoButton;
    private boolean isPausedCalled = false;

    private ImageView mIvFingerPrint;
    private Button mBtnCancel;
    private Button btnConfigFinger;

    private TextView mTvPlaceFinger;
    private FingureCapturedReciever mFingureCapturedReciever;


    SurfaceView surfaceView;
    TextView mTvUserId, mTvUserName;

    int onreceive;

    Student mSelectedStudent;
    String mStudentId;
    DBAdapter dbAdapter;
    String mapKey;
    private boolean iSPhotoCaptured;
    static int finger, extra;
    ImageView mIVPhotograph;
    LinkedHashMap<String, Student> studentLinkedHashMap;//String will be the mapkey(StudentID,venue etc)

    static int noOfFingerToCaptureForFirst = 1;
    static int noOfFingerToCaptureForSecond = 1;

    static int whichFingerType;

    String currentDate;
    String currentTime;

    int redColor, greencolor;
    InitializeSensor mSensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_camera);
        mSensor=new InitializeSensor(this);
        iSPhotoCaptured = false;//Initially the photocaptured will be false
        onreceive = 0;

        currentDate = getCurrentDate();
        currentTime = getCurrentTime();

        mStudentId = getIntent().getStringExtra(StringConstants.KEY_STUDENT_ID);//Get the the StudentID and Storing in the mStudentId
        mapKey = ManvishPrefConstants.SELECTED_DATE.read() +
                ManvishPrefConstants.SELECTED_VENUE.read() +
                ManvishPrefConstants.SELECTED_BATCH.read() +
                ManvishPrefConstants.SELECTED_CLASS.read() +
                mStudentId;//Storing all the StudentID with the related Studentinformation in the Mapclass

        if (USBManager.getInstance().isDevicesHasPermission() != true) {

            grantPermission();
        }

        initializeViews();//Initolize all the basic Views used in the layout.
        dbAdapter = DBAdapter.getInstance(this, MeritTrackApplication.getInstance().getDbName(), null, 1);//Get the dbInstance and  also the dbname  for this Activity
        studentLinkedHashMap = MeritTrackApplication.getInstance().getStudentListMap();

        if (studentLinkedHashMap == null) { // This should not be null ,This is

            setStudentList();
        } else {
            mSelectedStudent = MeritTrackApplication.getInstance().getStudentListMap().get(mapKey);
            mTvUserId.setText(mSelectedStudent.getStudentID());
            mTvUserName.setText(mSelectedStudent.getName());

        }

        mFingerConfigArray = ManvishPrefConstants.FINGERCONFIG.read().split(",");
        mExtraImages = ManvishPrefConstants.EXTRAIMAGES.read().split(",");

        mFingureCapturedReciever = new FingureCapturedReciever();
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mFingureCapturedReciever,
                new IntentFilter(StringConstants.ACTION_FINGER_CAPTURED));

        surfaceView = (SurfaceView) findViewById(R.id.camera_surface_view);
        // Do not On Camera for Verification
        if (ManvishPrefConstants.SELECTED_REG_OR_VER.read().equalsIgnoreCase(StringConstants.REGISTRATION)) {
            if (camera == null) {
                try {
                    camera = Camera.open();
                    // photoButton.setVisibility(View.GONE);
                } catch (Exception e) {
                    Log.e(TAG, "No camera with exception: " + e.getMessage());
                    //photoButton.setEnabled(false);
//                    Toast.makeText(this, "Please Try Again", Toast.LENGTH_LONG)
//                            .show();
                    ManvishCommonUtil.showCustomToast(CameraActivity.this, "Please Try Again");
                }
            }


            SurfaceHolder holder = surfaceView.getHolder();
            holder.addCallback(new Callback() {

                public void surfaceCreated(SurfaceHolder holder) {
                    try {
                        if (camera != null) {


                            // camera.setDisplayOrientation(90);
                            camera.setPreviewDisplay(holder);
                            camera.setParameters(getCameraParams(camera));

                            if (getResources().getConfiguration().orientation !=
                                    Configuration.ORIENTATION_LANDSCAPE) {
                                getCameraParams(camera).set("orientation", "portrait");
                                // For Android Version 2.2 and above
                                camera.setDisplayOrientation(90);
                                // For Android Version 2.0 and above
                                getCameraParams(camera).setRotation(90);
                            }

                            Camera.Parameters params = camera.getParameters();

                            // Check what resolutions are supported by your camera
                            List<Camera.Size> sizes = params
                                    .getSupportedPictureSizes();

                            // Iterate through all available resolutions and choose
                            // one.
                            // The chosen resolution will be stored in mSize.
                            params.setPictureSize(sizes.get(0).width,
                                    sizes.get(0).height);
                            camera.setParameters(params);
                            camera.startPreview();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Error setting up preview", e);
                        new ManvishCommonUtil().showCustomToast(CameraActivity.this, "Cmera not opening,try once more .");
                    }
                }

                public void surfaceChanged(SurfaceHolder holder, int format,
                                           int width, int height) {
                    // nothing to do here
                }

                public void surfaceDestroyed(SurfaceHolder holder) {
                    // nothing here
                }

            });

        } else {
            findViewById(R.id.tvPhotoSection).setVisibility(View.GONE);
            surfaceView.setVisibility(View.GONE);
            photoButton.setText("VERIFY FINGER");
            btnConfigFinger.setVisibility(View.INVISIBLE);
            mIVPhotograph.setVisibility(View.VISIBLE);

            if (!ManvishPrefConstants.SELECTED_REG_OR_VER.read().equalsIgnoreCase(StringConstants.REGISTRATION)) {

                mIVPhotograph.setVisibility(View.VISIBLE);

                if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("1")) {
                    // For stage 1 ,means Registration or Pretest ,get photograph which are recently captured,
                    // while going for session Verification

                    byte[] photo = dbAdapter.getRegPhotoForPostTest(ManvishPrefConstants.SELECTED_DATE.read(), ManvishPrefConstants.SELECTED_VENUE.read(),
                            ManvishPrefConstants.SELECTED_BATCH.read(), ManvishPrefConstants.SELECTED_CLASS.read()
                            , mSelectedStudent.getStudentID());
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(photo,
                            0, photo.length);
                    try {
                        if (photo != null && photo.length >= 10) {

                            //decodedByte = ManvishCommonUtil.rotateImage(decodedByte, 90);
                            mIVPhotograph.setImageBitmap(decodedByte);
                        }
                    } catch (Exception e) {
                        mIVPhotograph.setImageBitmap(decodedByte);
                    }

                } else if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("3")) {

                    if (mSelectedStudent != null) {

                        String photograph= dbAdapter.getStudentPhotograph(ManvishPrefConstants.SELECTED_DATE.read(),
                                ManvishPrefConstants.SELECTED_VENUE.read(),
                                ManvishPrefConstants.SELECTED_BATCH.read(),
                                ManvishPrefConstants.SELECTED_CLASS.read(),
                                mStudentId);
                        if (!TextUtils.isEmpty(photograph)) {


                            try {
                                byte[] decodedString = Base64.decode(photograph
                                       , Base64.DEFAULT
                                        );
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,
                                        0, decodedString.length);
                                mIVPhotograph.setImageBitmap(decodedByte);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

            }

        }

        if (ManvishPrefConstants.IS_TEST.read()) btnConfigFinger.setVisibility(View.INVISIBLE);

        photoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String buttonText = photoButton.getText().toString();

                if (buttonText.contains("PHOTO")) {
                    photoButton.setEnabled(false);
                    if (camera == null)
                        return;

                    try {
                        camera.takePicture(null, null, CameraActivity.this);
                    } catch (Exception e) {
                        e.printStackTrace();

                        ManvishCommonUtil.showCustomToast(CameraActivity.this, "Try after some time");
                        finish();
                    }

                } else if (buttonText.contains("CAPTURE")) {
                    photoButton.setEnabled(false);
                    startConnection();
                } else if (buttonText.contains("VERIFY")) {
                    startConnection();
                    photoButton.setEnabled(false);
                }

            }
        });

    }


    public void grantPermission() {
        USBManager.getInstance().initialize(this,
                "com.morpho.morphosample.USB_ACTION");
    }

    private Camera.Parameters getCameraParams(Camera mCamera) {
        Camera.Parameters params = mCamera.getParameters();
        params.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
        params.setPictureSize(140, 240);

        List<String> focusModes = params.getSupportedFocusModes();

        if (focusModes.contains(Camera.Parameters.FLASH_MODE_AUTO)) {
            params.setFocusMode(Camera.Parameters.FLASH_MODE_AUTO);
        } else if (focusModes
                .contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }


        params.setExposureCompensation(0);
        params.setPictureFormat(ImageFormat.JPEG);
        params.setJpegQuality(100);


        return params;
    }

    @Override
    public void onResume() {
        super.onResume();
        setHeading();
        /*Batterylevel b1=new Batterylevel();
        b1.Batterylevelindicator();*/
        // Batterylevelindicator();


        if (camera == null) {

            try {
                if (isPausedCalled) {
                    camera = Camera.open();
                    // photoButton.setEnabled(true);
                    isPausedCalled = false;
                }

                // photoButton.setEnabled(true);
            } catch (Exception e) {
                Log.i(TAG, "No camera: " + e.getMessage());
                // photoButton.setEnabled(false);
//                Toast.makeText(this, "No camera detected", Toast.LENGTH_LONG)
//                        .show();
                ManvishCommonUtil.showCustomToast(CameraActivity.this, "No camera detected ,Try once more");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, 500);
            }
        }

    }

    private void startConnection() {

        ArrayList<byte[]> templatebyteList = null;

        switch (ManvishPrefConstants.SELECTED_REG_OR_VER.read()) {
            case StringConstants.VERIFICATION:

                photoButton.setText("VERIFY FINGER");
                mTvPlaceFinger.setText("PLACE FINGER");
                mTvPlaceFinger.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startConnectionFragment();

                    }
                }, 500);

                break;

            case StringConstants.OFFLINE_VERIFICATION:

                photoButton.setText("VERIFY FINGER");
                mTvPlaceFinger.setText("PLACE FINGER");
                mTvPlaceFinger.setVisibility(View.VISIBLE);

                if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("1")) {

                    /*templatebyteList = dbAdapter.getTemplatesOfStudentFromRegisteredTableForSessionVerification(ManvishPrefConstants.SELECTED_DATE.read(),
                            ManvishPrefConstants.SELECTED_VENUE.read(), ManvishPrefConstants.SELECTED_BATCH.read(),
                            ManvishPrefConstants.SELECTED_CLASS.read(), mStudentId);*/
                    templatebyteList=mSelectedStudent.getTemplatebyteList();

                } else if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("3")) {
                    /*templatebyteList = dbAdapter.getTemplatesOfStudentForVerification(ManvishPrefConstants.SELECTED_DATE.read(),
                            ManvishPrefConstants.SELECTED_VENUE.read(), ManvishPrefConstants.SELECTED_BATCH.read(),
                            ManvishPrefConstants.SELECTED_CLASS.read(), mStudentId);
                    */
                    templatebyteList=mSelectedStudent.getTemplatebyteList();
                }

                if (templatebyteList != null && templatebyteList.size() != 0) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startConnectionFragment();

                        }
                    }, 500);
                } else {

                    CameraActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            new ManvishAlertDialog(CameraActivity.this,
                                    "NO TEMPLATE FOUND", "Please register finger before verification .").showAlertDialog();
                        }
                    });

                }

                break;

            case StringConstants.REGISTRATION:

                photoButton.setText("CAPTURE FINGER");

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startConnectionFragment();

                    }
                }, 500);

                CameraActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                    }
                });


                break;
        }


    }
    // mBtnCancel.setVisibility(View.GONE);
    //mBtnCancel.setEnabled(false);


    private void startConnectionFragment() {

        Intent dialogActivity = new Intent(this, MorphoSampleActivity.class);
        dialogActivity.putExtra(StringConstants.KEY_STUDENT_ID, mStudentId);
        startActivity(dialogActivity);

        /*getIntent().putExtra(StringConstants.KEY_STUDENT_ID, mStudentId);

        if(ProcessInfo.getInstance().getMorphoDatabase()!=null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mSensor.captureFinger();
                }
            }).start();

        }else{
            //Initialize sensor
            new Thread(new Runnable() {
                @Override
                public void run() {
                    ManvishCommonUtil.initializeSensor(CameraActivity.this,mSensor);
                }
            }).start();
        }*/

    }

    byte[] photoByte = null;

    private void displayAndDecodeImage(final byte[] data) {

        iSPhotoCaptured = true;

        mTvPlaceFinger.setVisibility(View.VISIBLE);


        photoButton.setBackgroundResource(R.drawable.menu_button_selected);
        switch (ManvishPrefConstants.SELECTED_REG_OR_VER.read()) {
            case StringConstants.VERIFICATION:

                photoButton.setText("VERIFY FINGER");
                mTvPlaceFinger.setText("VERIFY FINGER NOW");

                break;

            case StringConstants.OFFLINE_VERIFICATION:

                photoButton.setText("VERIFY FINGER");
                mTvPlaceFinger.setText("VERIFY FINGER NOW");

                break;

            case StringConstants.REGISTRATION:

                photoButton.setText("CAPTURE FINGER");

                // Show which finger he has to capture
                //mTvPlaceFinger.setText("CAPTURE FINGER NOW");

                if (TextUtils.isEmpty(mFingerConfigArray[0])) {
                    setFingerNametoRegister(0);
                } else {
                    setFingerNametoRegister(Integer.parseInt(mFingerConfigArray[0]));
                    noOfFingerToCaptureForFirst++;
                }


                break;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                photoButton.setEnabled(true);
            }
        }, 500);


        Thread thread = new Thread() {
            @Override
            public void run() {


                if (data != null && data.length != 0) {

                    try {
                        // insert photograph
                        if (ManvishPrefConstants.IS_TEST.read()
                                || ManvishPrefConstants.SELECTED_REG_OR_VER.read().
                                equalsIgnoreCase(StringConstants.OFFLINE_VERIFICATION)) {

                            // Do not save photograph for Testing or OFFLINE VERIFICATION
                        } else {
                            byte[] rotatedByte = ManvishCommonUtil.rotateByteArrayImage(data, 90);
                            if (rotatedByte == null) {
                                Log.d(StringConstants.TAG, null);
                            } else {
                                Log.d(StringConstants.TAG, rotatedByte.length + "");
                            }
                            dbAdapter.insertRegisteredPhotograph(ManvishPrefConstants.SELECTED_EVENT_CODE.read(),
                                    ManvishPrefConstants.SELECTED_VENUE.read(),
                                    mSelectedStudent.getStudentID(),
                                    mSelectedStudent.getName(),
                                    ManvishPrefConstants.SELECTED_DATE.read(),
                                    ManvishPrefConstants.SELECTED_BATCH.read(),
                                    ManvishPrefConstants.SELECTED_CLASS.read(),
                                    rotatedByte, currentDate, currentTime); // data comes around 18 KB .
                        }


                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                }

            }
        };
        thread.start();
        final Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0,
                data.length);
        mIVPhotograph.setImageBitmap(bitmap);
    }

    private void initializeViews() {

        btnConfigFinger = (Button) findViewById(R.id.btnSelectFinger);
        btnConfigFinger.setEnabled(false);//Initially selecting the finger will be false
        mTvPlaceFinger = (TextView) findViewById(R.id.tvPlaceFinger);
        mTvPlaceFinger.setVisibility(View.GONE);//Here View.Gone Doesnot take any space on the Layout

        mBtnCancel = (Button) findViewById(R.id.btnCancel);
        mBtnCancel.setOnClickListener(this);
        mIvFingerPrint = (ImageView) findViewById(R.id.ivFinger);
        mIvFingerPrint.setVisibility(View.INVISIBLE);//here the view take some space on the Layout

        mTvUserId = (TextView) findViewById(R.id.tv_user_id);
        mTvUserName = (TextView) findViewById(R.id.tv_user_name);


        photoButton = (Button) findViewById(R.id.camera_photo_button);
        photoButton.setEnabled(true);

        mIVPhotograph = (ImageView) findViewById(R.id.ivPhoto);

    }

    @Override
    public void onPause() {

        super.onPause();
        if (camera != null) {
            // camera.stopPreview();
            camera.release();
            camera = null;
        }


        isPausedCalled = true;
    }

    @Override
    protected void onDestroy() {

        unRegisterReciever();

        noOfFingerToCaptureForSecond = 1;
        noOfFingerToCaptureForFirst = 1;

        mSelectedStudent.setTemplatebyteList(null);
        super.onDestroy();
    }

    Bitmap bm = null;

    private class FingureCapturedReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, final Intent intent) {



            onreceive = onreceive + 1;
            // Toast.makeText(CameraActivity.this,"Session code=="+ManvishPrefConstants.SELECTED_SESSION_CODE.read(),Toast.LENGTH_LONG).show();
            // finish activity after few seconds

            if (intent
                    .getStringExtra(StringConstants.KEY_INTENT_SUCCESS).equalsIgnoreCase(StringConstants.VALUE_SUCC)) {


                if (intent
                        .getParcelableExtra(StringConstants.KEY_INTENT_BITMAP) != null) {


                    Bitmap fingerImageBitmap = (Bitmap) intent
                            .getParcelableExtra(StringConstants.KEY_INTENT_BITMAP);


                    byte[] fingerByteArray = ManvishFileUtils.getBytesFromBitmap(fingerImageBitmap);

                    byte[] Bits = new byte[fingerByteArray.length * 4];
                    int i;

                    for (i = 0; i < fingerByteArray.length; i++) {

                        Bits[i * 4] = Bits[i * 4 + 1] = Bits[i * 4 + 2] = ((byte) ~fingerByteArray[i]);

                        // Invert the source bits
                        Bits[i * 4 + 3] = -1;// 0xff, that's the alpha.
                    }
                    //fingerByteArray=Bits;
                    // Now put these nice RGBA pixels into a Bitmap object
                    bm = Bitmap.createBitmap(128, 200, Bitmap.Config.ARGB_8888);
                    bm.copyPixelsFromBuffer(ByteBuffer.wrap(Bits));

                    mIvFingerPrint.setVisibility(View.VISIBLE);
                    mIvFingerPrint.setImageBitmap(bm);
                    fingerByteArray = ManvishFileUtils.getBytes(bm);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            mIvFingerPrint.setVisibility(View.INVISIBLE);

                        }
                    }, 1000);


                    byte[] templateByteArray = (byte[]) intent
                            .getByteArrayExtra(StringConstants.KEY_INTENT_TEMPLATE);

                    //   String fingerImageString=Base64.encodeToString(fingerByteArray,Base64.DEFAULT);

                    if (!ManvishPrefConstants.IS_TEST.read()) {
                        // Store image in toDB


                        switch (ManvishPrefConstants.SELECTED_REG_OR_VER.read()) {


                            case StringConstants.VERIFICATION:

                                // If verification is successfull ,
                                // then remove if ,failed verifications from Verification table .
//                                 String date,String venue,
//                                     String batch,String classRoom,String studentID

                                dbAdapter.deleteVerFailedData(ManvishPrefConstants.SELECTED_DATE.read(),
                                        ManvishPrefConstants.SELECTED_VENUE.read(),
                                        ManvishPrefConstants.SELECTED_BATCH.read(),
                                        ManvishPrefConstants.SELECTED_CLASS.read(),
                                        mSelectedStudent.getStudentID(), ManvishPrefConstants.SELECTED_SESSION_CODE.read()
                                );

                                dbAdapter.insertVerificationDetails(ManvishPrefConstants.SELECTED_EVENT_CODE.read(),
                                        ManvishPrefConstants.SELECTED_VENUE.read(),
                                        mSelectedStudent.getStudentID(), mSelectedStudent.getName(),
                                        ManvishPrefConstants.SELECTED_DATE.read(), ManvishPrefConstants.SELECTED_BATCH.read(),
                                        ManvishPrefConstants.SELECTED_CLASS.read(),
                                        StringConstants.STUDENT_REGISTERED + "",
                                        getCurrentDate(), getCurrentTime(),
                                        ManvishPrefConstants.SELECTED_SESSION_CODE.read(),
                                        ManvishPrefConstants.SELECTED_DATE.read(), mFingerConfigArray[0],ManvishPrefConstants.LATITUDE.read(),ManvishPrefConstants.LONGITUDE.read());


                                String previousSessionCode = mSelectedStudent.getSessionCode();
                                String currentSessionCode = ManvishPrefConstants.SELECTED_SESSION_CODE.read();
                                LinkedHashSet<String> hs = new LinkedHashSet<String>();
                                if (TextUtils.isEmpty(previousSessionCode.trim())) {

                                    hs.add(currentSessionCode + ":" + StringConstants.STUDENT_REGISTERED + "");
                                } else {
                                    String[] previousSessionCodeArray = previousSessionCode.split(",");

                                    for (String previousSessionStatus : previousSessionCodeArray) {

                                        if (previousSessionStatus.split(":")[0].trim().equalsIgnoreCase(currentSessionCode.trim())) {
                                            Log.d(StringConstants.TAG, "DiSCARDED PREVIOUS SESSION");
                                        } else {
                                            hs.add(previousSessionStatus);
                                        }
                                    }
                                    hs.add(currentSessionCode + ":" + StringConstants.STUDENT_REGISTERED + "");
                                }
                                String allSessionStatusCodes = "";
                                for (String string : hs) {

                                    allSessionStatusCodes = allSessionStatusCodes.trim() + string + ",";
                                }

                                dbAdapter.updatestudentVerStatus(ManvishPrefConstants.SELECTED_DATE.read(), ManvishPrefConstants.SELECTED_VENUE.read(),
                                        ManvishPrefConstants.SELECTED_BATCH.read(), ManvishPrefConstants.SELECTED_CLASS.read(),
                                        mSelectedStudent.getStudentID(), StringConstants.STUDENT_REGISTERED + "",
                                        allSessionStatusCodes);// Add ver status to SessionCode ,Later we can think for it .

                                // mSelectedStudent.setSessionCode(mSelectedStudent.getSessionCode()+","+ManvishPrefConstants.SELECTED_SESSION_CODE.read());
                                mSelectedStudent.setSessionCode(allSessionStatusCodes);
                                mSelectedStudent.setVerStatus(StringConstants.STUDENT_REGISTERED + "");
                                MeritTrackApplication.getInstance().getStudentListMap().put(mapKey, mSelectedStudent);
                                break;

                            case StringConstants.OFFLINE_VERIFICATION:
                                //Update LinkedHashmap
                                mSelectedStudent.setVerStatus(StringConstants.STUDENT_REGISTERED + "");
                                MeritTrackApplication.getInstance().getStudentListMap().put(mapKey, mSelectedStudent);
                                break;

                            case StringConstants.REGISTRATION://In this table  I have added the latitude and Longitude

                                dbAdapter.insertRegistrationDetails(ManvishPrefConstants.SELECTED_EVENT_CODE.read(),
                                        ManvishPrefConstants.SELECTED_VENUE.read(),
                                        mSelectedStudent.getStudentID(),
                                        mSelectedStudent.getName(),
                                        ManvishPrefConstants.SELECTED_DATE.read(),
                                        ManvishPrefConstants.SELECTED_BATCH.read(),
                                        ManvishPrefConstants.SELECTED_CLASS.read(),
                                        null, fingerByteArray, StringConstants.STUDENT_REGISTERED + "", whichFingerType + "", templateByteArray,
                                        currentDate, currentTime,ManvishPrefConstants.LATITUDE.read(),ManvishPrefConstants.LONGITUDE.read()
                                       );


                                dbAdapter.updatestudentRegStatus(ManvishPrefConstants.SELECTED_DATE.read(), ManvishPrefConstants.SELECTED_VENUE.read(),
                                        ManvishPrefConstants.SELECTED_BATCH.read(), ManvishPrefConstants.SELECTED_CLASS.read(),
                                        mSelectedStudent.getStudentID(), StringConstants.STUDENT_REGISTERED + "");

                                //Update LinkedHashmap
                                mSelectedStudent.setRegStatus(StringConstants.STUDENT_REGISTERED + "");
                                MeritTrackApplication.getInstance().getStudentListMap().put(mapKey, mSelectedStudent);
                                break;
                        }

                        mIvFingerPrint.setImageBitmap(fingerImageBitmap);

                    } else {

                        switch (ManvishPrefConstants.SELECTED_REG_OR_VER.read()) {

                            case StringConstants.VERIFICATION:
                                mSelectedStudent.setVerStatus(StringConstants.STUDENT_REGISTERED + "");
                                MeritTrackApplication.getInstance().getStudentListMap().put(mapKey, mSelectedStudent);

                                break;
                            case StringConstants.OFFLINE_VERIFICATION:
                                mSelectedStudent.setVerStatus(StringConstants.STUDENT_REGISTERED + "");
                                MeritTrackApplication.getInstance().getStudentListMap().put(mapKey, mSelectedStudent);
                                break;
                            case StringConstants.REGISTRATION:
                                mSelectedStudent.setRegStatus(StringConstants.STUDENT_REGISTERED + "");
                                MeritTrackApplication.getInstance().getStudentListMap().put(mapKey, mSelectedStudent);
                                break;

                        }

                    }

                }

            } else {

                if (!ManvishPrefConstants.IS_TEST.read()) {


                    switch (ManvishPrefConstants.SELECTED_REG_OR_VER.read()) {

                        case StringConstants.VERIFICATION:

                            // delete failed data,if exists from  before ..
                            dbAdapter.deleteVerFailedData(ManvishPrefConstants.SELECTED_DATE.read(),
                                    ManvishPrefConstants.SELECTED_VENUE.read(),
                                    ManvishPrefConstants.SELECTED_BATCH.read(),
                                    ManvishPrefConstants.SELECTED_CLASS.read(),
                                    mSelectedStudent.getStudentID(), ManvishPrefConstants.SELECTED_SESSION_CODE.read()
                            );

                            dbAdapter.insertVerificationDetails(ManvishPrefConstants.SELECTED_EVENT_CODE.read(), ManvishPrefConstants.SELECTED_VENUE.read(),
                                    mSelectedStudent.getStudentID(), mSelectedStudent.getName(), ManvishPrefConstants.SELECTED_DATE.read(),
                                    ManvishPrefConstants.SELECTED_BATCH.read(),
                                    ManvishPrefConstants.SELECTED_CLASS.read(),
                                    StringConstants.STUDENT_REGISTRATION_FAILED + "", getCurrentDate(), getCurrentTime(),
                                    ManvishPrefConstants.SELECTED_SESSION_CODE.read(),
                                    ManvishPrefConstants.SELECTED_DATE.read(), mFingerConfigArray[0],ManvishPrefConstants.LATITUDE.read(),ManvishPrefConstants.LONGITUDE.read());


                            String previousSessionCode = mSelectedStudent.getSessionCode();
                            String currentSessionCode = ManvishPrefConstants.SELECTED_SESSION_CODE.read();
                            LinkedHashSet<String> hs = new LinkedHashSet<String>();
                            if (TextUtils.isEmpty(previousSessionCode.trim())) {

                                hs.add(currentSessionCode + ":" + StringConstants.STUDENT_REGISTRATION_FAILED + "");
                            } else {
                                String[] previousSessionCodeArray = previousSessionCode.split(",");

                                for (String previousSessionStatus : previousSessionCodeArray) {

                                    if (previousSessionStatus.split(":")[0].trim().equalsIgnoreCase(currentSessionCode.trim())) {
                                        Log.d(StringConstants.TAG, "DiSCARDED PREVIOUS SESSION");
                                    } else {
                                        hs.add(previousSessionStatus);
                                    }
                                }
                                hs.add(currentSessionCode + ":" + StringConstants.STUDENT_REGISTRATION_FAILED + "");
                            }
                            String allSessionStatusCodes = "";
                            for (String string : hs) {

                                allSessionStatusCodes = allSessionStatusCodes.trim() + string + ",";
                            }

                            dbAdapter.updatestudentVerStatus(ManvishPrefConstants.SELECTED_DATE.read(), ManvishPrefConstants.SELECTED_VENUE.read(),
                                    ManvishPrefConstants.SELECTED_BATCH.read(), ManvishPrefConstants.SELECTED_CLASS.read(),
                                    mSelectedStudent.getStudentID(), StringConstants.STUDENT_REGISTRATION_FAILED + "", allSessionStatusCodes);

                            //mSelectedStudent.setSessionCode(mSelectedStudent.getSessionCode()+","+ManvishPrefConstants.SELECTED_SESSION_CODE.read());
                            mSelectedStudent.setSessionCode(allSessionStatusCodes);
                            mSelectedStudent.setVerStatus(StringConstants.STUDENT_REGISTRATION_FAILED + "");
                            MeritTrackApplication.getInstance().getStudentListMap().put(mapKey, mSelectedStudent);
                            break;
                        case StringConstants.OFFLINE_VERIFICATION:

                            mSelectedStudent.setVerStatus(StringConstants.STUDENT_REGISTRATION_FAILED + "");
                            MeritTrackApplication.getInstance().getStudentListMap().put(mapKey, mSelectedStudent);
                            break;
                        case StringConstants.REGISTRATION:


                            dbAdapter.insertRegistrationDetails(ManvishPrefConstants.SELECTED_EVENT_CODE.read(),
                                    ManvishPrefConstants.SELECTED_VENUE.read(),
                                    mSelectedStudent.getStudentID(),
                                    mSelectedStudent.getName(),
                                    ManvishPrefConstants.SELECTED_DATE.read(),
                                    ManvishPrefConstants.SELECTED_BATCH.read(),
                                    ManvishPrefConstants.SELECTED_CLASS.read(),
                                    null, null, StringConstants.STUDENT_REGISTRATION_FAILED + "", whichFingerType + "", null,
                                    currentDate, currentTime,ManvishPrefConstants.LATITUDE.read(),ManvishPrefConstants.LONGITUDE.read());


                            String status = null;

                            // Check if ,Finger Satus is already registered
                            if (!mSelectedStudent.getRegStatus().equalsIgnoreCase(StringConstants.STUDENT_REGISTERED + "".trim())) {
                                mSelectedStudent.setRegStatus(StringConstants.STUDENT_REGISTRATION_FAILED + "");
                                status = StringConstants.STUDENT_REGISTRATION_FAILED + "";
                            } else {
                                mSelectedStudent.setRegStatus(mSelectedStudent.getRegStatus());
                                status = mSelectedStudent.getRegStatus() + "";
                            }

                            dbAdapter.updatestudentRegStatus(ManvishPrefConstants.SELECTED_DATE.read(), ManvishPrefConstants.SELECTED_VENUE.read(),
                                    ManvishPrefConstants.SELECTED_BATCH.read(), ManvishPrefConstants.SELECTED_CLASS.read(),
                                    mSelectedStudent.getStudentID(), status);

                            //Update LinkedHashmap


                            MeritTrackApplication.getInstance().getStudentListMap().put(mapKey, mSelectedStudent);

                            break;
                    }


                } else {


                    switch (ManvishPrefConstants.SELECTED_REG_OR_VER.read()) {

                        case StringConstants.VERIFICATION:
                            mSelectedStudent.setVerStatus(StringConstants.STUDENT_REGISTRATION_FAILED + "");
                            MeritTrackApplication.getInstance().getStudentListMap().put(mapKey, mSelectedStudent);
                            break;
                        case StringConstants.OFFLINE_VERIFICATION:
                            mSelectedStudent.setVerStatus(StringConstants.STUDENT_REGISTRATION_FAILED + "");
                            MeritTrackApplication.getInstance().getStudentListMap().put(mapKey, mSelectedStudent);
                            break;
                        case StringConstants.REGISTRATION:

                            if (!mSelectedStudent.getRegStatus().equalsIgnoreCase(StringConstants.STUDENT_REGISTERED + "".trim())) {
                                mSelectedStudent.setRegStatus(StringConstants.STUDENT_REGISTRATION_FAILED + "");
                            }

                            MeritTrackApplication.getInstance().getStudentListMap().put(mapKey, mSelectedStudent);
                            break;

                    }

                }
                // Handle Failure Here==
            }
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub


                    //REgistration can be done for multiple fingers ,but verification can only be done for single finger
                    if (ManvishPrefConstants.SELECTED_REG_OR_VER.read().equalsIgnoreCase(StringConstants.REGISTRATION)) {

                        customFinger = 0;

                        if (!ManvishPrefConstants.IS_TEST.read()) {

                            int hwManyTimesFirstFingerWillGo = 1 + Integer.parseInt(mExtraImages[0]);
                            int hwManyTimesSecondFingerWillGo = 1 + Integer.parseInt(mExtraImages[1]);

                            if (noOfFingerToCaptureForFirst <= hwManyTimesFirstFingerWillGo) {


                                if (customFinger != 0) {
                                    // Means user has selected custom finger
                                    setFingerNametoRegister(customFinger);
                                    customFinger = 0;
                                } else {
                                    setFingerNametoRegister(Integer.parseInt(mFingerConfigArray[0]));
                                }

                                noOfFingerToCaptureForFirst++;
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        photoButton.setEnabled(true);
                                    }
                                }, 500);

                            } else {

                                if (noOfFingerToCaptureForSecond <= hwManyTimesSecondFingerWillGo) {

                                    if (customFinger != 0) {
                                        // Means user has selected custom finger
                                        setFingerNametoRegister(customFinger);
                                        customFinger = 0;
                                    } else {
                                        setFingerNametoRegister(Integer.parseInt(mFingerConfigArray[1]));
                                    }

                                    noOfFingerToCaptureForSecond++;
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            photoButton.setEnabled(true);
                                        }
                                    }, 500);

                                } else {

                                    photoButton.setEnabled(false);
                                    //pabitra == disable capture button here

                                    // Take a SD card backup and then finish
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {

                                            if (!ManvishPrefConstants.IS_TEST.read())
                                                takeBackUpToSDCard();
                                        }
                                    }).start();

                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            finish();
                                        }
                                    }, 700);


                                }


                            }
                        } else {
                            // Do not do anything ,Let the user to press Capture finger button .
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            }, 700);
                        }

                    } else {

                        if (intent
                                .getStringExtra(StringConstants.KEY_INTENT_SUCCESS).equalsIgnoreCase(StringConstants.VALUE_SUCC)) {

                            photoButton.setEnabled(false);
                            //  Now this comes for verification ..
                            //  Check if it is not for Test ,then take a backup
                            if (ManvishPrefConstants.IS_TEST.read()) {
                                finish();
                            } else {

                                // Take a SD card backup and then finish
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {

                                        if (!ManvishPrefConstants.IS_TEST.read())
                                            takeBackUpToSDCard();
                                    }
                                }).start();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        finish();
                                    }
                                }, 700);

                            }

                        } else {

                            // Take a SD card backup and then finish
                            new Thread(new Runnable() {
                                @Override
                                public void run() {

                                    if (!ManvishPrefConstants.IS_TEST.read())
                                        takeBackUpToSDCard();
                                }
                            }).start();

                            // Do not finish  for verification ,If it fails .
                            // ,let the user verify again ,but finish it if verification success
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    photoButton.setEnabled(true);
                                }
                            }, 500);
                        }
                    }
                }
            }, 300);


        }


    }//end of Fingured captured reciever class

    String[] mFingerConfigArray;//It gives a array of finger positions configed for a project
    String[] mExtraImages;
    int mWhichFinger; //Gives the finger name
    static int mFingerConfigPosition = 0; // Gives the mFingerConfigArray positions ,increment it to get values ;

    private void setFingerNametoRegister(int mWhichFinger) {

        // Get the finger name to show to user ,for which finger he needs to register ,Chnge this
        // text for multiple finger registration .

        whichFingerType = mWhichFinger; // Assign this to the global variable ,for Saving purpsoe
        if (mWhichFinger == 0) {
            mWhichFinger = 2;
        }

        String lfingerName = "";

        switch (mWhichFinger) {
            case StringConstants.FINGER_RIGHT_THUMB:
                lfingerName = "RIGHT THUMB";
                break;
            case StringConstants.FINGER_RIGHT_INDEX:
                lfingerName = "RIGHT INDEX";
                break;
            case StringConstants.FINGER_RIGHT_MIDDLE:
                lfingerName = "RIGHT MIDDLE";
                break;
            case StringConstants.FINGER_RIGHT_RING:
                lfingerName = "RIGHT RING";
                break;
            case StringConstants.FINGER_RIGHT_LITTLE:
                lfingerName = "RIGHT LITTLE";
                break;
            case StringConstants.FINGER_LEFT_THUMB:
                lfingerName = "LEFT THUMB";
                break;
            case StringConstants.FINGER_LEFT_INDEX:
                lfingerName = "LEFT INDEX";
                break;
            case StringConstants.FINGER_LEFT_MIDDLE:
                lfingerName = "LEFT MIDDLE";
                break;
            case StringConstants.FINGER_LEFT_RING:
                lfingerName = "LEFT RING";
                break;
            case StringConstants.FINGER_LEFT_LITTLE:
                lfingerName = "LEFT LITTLE";
                break;

        }

        mTvPlaceFinger.setText("PLACE " + lfingerName + " FINGER");

        ColorStateList mList = mTvPlaceFinger.getTextColors();
        int color = mList.getDefaultColor();
        redColor = getResources().getColor(R.color.red);
        greencolor = getResources().getColor(R.color.green_manvish);
        if (color == redColor) {
            mTvPlaceFinger.setTextColor(greencolor);
        } else {
            mTvPlaceFinger.setTextColor(redColor);
        }

        mTvPlaceFinger.setVisibility(View.VISIBLE);

        mFingerConfigPosition++;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancel:

                if (iSPhotoCaptured) {
                    // Reset

                    if (!ManvishPrefConstants.IS_TEST.read()) {
                        showAbdonDiaolg(mStudentId);
                    } else {
                        finish();
                    }

                } else {
                    finish();
                }
                break;


            default:
                break;
        }
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {

        displayAndDecodeImage(data);
    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        // Do nothing
    }

    private void unRegisterReciever() {
        try {
            LocalBroadcastManager.getInstance(CameraActivity.this)
                    .unregisterReceiver(mFingureCapturedReciever);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


    public void configFinger(View v) {
        FragmentManager manager = getFragmentManager();

        /** Instantiating the DialogFragment class */
        AlertDialogRadioFragment alert = new AlertDialogRadioFragment();

        /** Creating a bundle object to store the selected item's index */
        Bundle b = new Bundle();

        /** Storing the selected item's index in the bundle object */
        b.putInt("position", position);

        /** Setting the bundle object to the dialog fragment object */
        alert.setArguments(b);

        /** Creating the dialog fragment object, which will in turn open the alert dialog window */
        alert.show(manager, "alert_dialog_radio");
    }

    /**
     * Stores the selected item's position
     */
    int position = 0;

    @Override
    public void onPositiveClick(int position) {
        this.position = position;

        /** Getting the reference of the textview from the main layout */
        //Toast.makeText(this, "" + AlertDialogRadioFragment.Android.code[this.position], Toast.LENGTH_SHORT).show();
        customFinger = position + 1; // As postion starts from 0 and finger value from 1
        setFingerNametoRegister(customFinger);
    }


    int customFinger = 0;

    private void  setStudentList() {


        new AsyncTask<Void, Void, Boolean>() {
            protected void onPreExecute() {
                showProgressBar("Fetching Student Data");

            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {

                    if (ManvishPrefConstants.IS_TEST.read()) {

                        Student student;
                        for (int i = 0; i < 50; i++) {
                            student = new Student();
                            student.setStudentID("test-" + i);
                            student.setName("test-name-" + i);
                            student.setRegStatus("0");
                            student.setVerStatus("0");
                            studentLinkedHashMap.put(
                                    ManvishPrefConstants.SELECTED_DATE.read() +
                                            ManvishPrefConstants.SELECTED_VENUE.read() +
                                            ManvishPrefConstants.SELECTED_BATCH.read() +
                                            ManvishPrefConstants.SELECTED_CLASS.read() +
                                            student.getStudentID(), student
                            );
                        }
                        MeritTrackApplication.getInstance().setStudentListMap(studentLinkedHashMap);
                    } else {
                        studentLinkedHashMap = dbAdapter.getStudentMapListFromDateVenueBatchClass
                                (ManvishPrefConstants.SELECTED_DATE.read(),
                                        ManvishPrefConstants.SELECTED_VENUE.read(),
                                        ManvishPrefConstants.SELECTED_BATCH.read(),
                                        ManvishPrefConstants.SELECTED_CLASS.read(),
                                        ManvishPrefConstants.SELECTED_REG_OR_VER.read());

                    }

                    if (MeritTrackApplication.getInstance().getStudentListMap().size() > 0) {
                        return true;
                    } else {
                        return false;
                    }

                } catch (Exception e) {
                    com.activeandroid.util.Log.e("MANVISH",
                            "Exception in parsing file"
                                    + e.getMessage());

                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean success) {


                if (success) {

                    mSelectedStudent = MeritTrackApplication.getInstance().getStudentListMap().get(mapKey);
                    mTvUserId.setText(mSelectedStudent.getStudentID());
                    mTvUserName.setText(mSelectedStudent.getName());

                    if (!ManvishPrefConstants.SELECTED_REG_OR_VER.read().equalsIgnoreCase(StringConstants.REGISTRATION)) {

                        mIVPhotograph.setVisibility(View.VISIBLE);

                        if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("1")) {
                            // For stage 1 ,means Registration or Pretest ,get photograph which are recently captured,
                            // while going for session Verification

                            byte[] photo = dbAdapter.getRegPhotoForPostTest(ManvishPrefConstants.SELECTED_DATE.read(), ManvishPrefConstants.SELECTED_VENUE.read(),
                                    ManvishPrefConstants.SELECTED_BATCH.read(), ManvishPrefConstants.SELECTED_CLASS.read()
                                    , mSelectedStudent.getStudentID());//In the session Verification try to get all details like DATE,BATCH ETC

                            if (photo != null && photo.length != 0) {//Here we ary trying to decode the Image and set the Image for
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(photo,
                                        0, photo.length);
                                mIVPhotograph.setImageBitmap(decodedByte);
                            }


                        } else if (ManvishPrefConstants.STAGE.read().equalsIgnoreCase("3")) {

                            String photograph= dbAdapter.getStudentPhotograph(ManvishPrefConstants.SELECTED_DATE.read(),
                                    ManvishPrefConstants.SELECTED_VENUE.read(),
                                    ManvishPrefConstants.SELECTED_BATCH.read(),
                                    ManvishPrefConstants.SELECTED_CLASS.read(),
                                    mStudentId);
                            if (!TextUtils.isEmpty(photograph)) {


                                try {
                                    byte[] decodedString = Base64.decode(
                                            photograph, Base64.DEFAULT);
                                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,
                                            0, decodedString.length);
                                    mIVPhotograph.setImageBitmap(decodedByte);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    }

                } else {

                }

                dismissProgressBar();
            }

        }.execute();
    }

    ProgressDialog mProgressDialog;

    private void showProgressBar(String msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
        }


        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    private void dismissProgressBar() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    public void Capturefinger(View v) {
        // Instead of automatically going for Capture Finger ,Click this button to capture Finger

    }

    private String getCurrentDate() {
        // String lCurrentDate = "";
        Date now = new Date();
        int hours = now.getHours();
        hours = (hours > 12) ? hours - 12 : hours;
        int minutes = now.getMinutes();
        int h1 = hours / 10;
        int h2 = hours - h1 * 10;
        int m1 = minutes / 10;
        int m2 = minutes - m1 * 10;

        Calendar calendar = Calendar.getInstance();
        Log.d("hi", String.format("%d:%d -> %d %d: %d %d", hours, minutes, h1,
                h2, m1, m2));
        Date date = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        return dateFormat.format(date);
    }

    private String getCurrentTime() {
        // String lCurrentDate = "";
        Date now = new Date();
        int hours = now.getHours();
        hours = (hours > 12) ? hours - 12 : hours;
        int minutes = now.getMinutes();
        int h1 = hours / 10;
        int h2 = hours - h1 * 10;
        int m1 = minutes / 10;
        int m2 = minutes - m1 * 10;

        Calendar calendar = Calendar.getInstance();
        Log.d("hi", String.format("%d:%d -> %d %d: %d %d", hours, minutes, h1,
                h2, m1, m2));
        Date date = calendar.getTime();
        DateFormat homeDateFormat = new SimpleDateFormat("HH:mm:ss");

        return homeDateFormat.format(date);
    }

    // convert from bitmap to byte array
    public static byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }

    private void showAbdonDiaolg(final String studentID) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                this);

        alertDialog.setTitle("Do you want to abandon the registration?");
        alertDialog.setMessage(" Note: registration details of this user no more exist ");

        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                // Delete registered data
                dbAdapter.deleteRegisteredData(ManvishPrefConstants.SELECTED_DATE.read(),
                        ManvishPrefConstants.SELECTED_VENUE.read(),
                        ManvishPrefConstants.SELECTED_BATCH.read(),
                        ManvishPrefConstants.SELECTED_CLASS.read(), studentID);

                //Update Student table
                dbAdapter.updatestudentRegStatus(ManvishPrefConstants.SELECTED_DATE.read(),
                        ManvishPrefConstants.SELECTED_VENUE.read(),
                        ManvishPrefConstants.SELECTED_BATCH.read(),
                        ManvishPrefConstants.SELECTED_CLASS.read(), studentID, "0");

                String mapKey = ManvishPrefConstants.SELECTED_DATE.read() +
                        ManvishPrefConstants.SELECTED_VENUE.read() +
                        ManvishPrefConstants.SELECTED_BATCH.read() +
                        ManvishPrefConstants.SELECTED_CLASS.read() +
                        studentID;
                LinkedHashMap<String, Student> studentLinkedHashMap = MeritTrackApplication.getInstance().getStudentListMap();
                Student student = studentLinkedHashMap.get(mapKey);
                student.setRegStatus("0");
                dialog.cancel();

                finish();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event

                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void takeBackUpToSDCard() {
        //If SD card is present take a xml backup of registration or Verification ..

        // The file path Should be /Sdcard/MERITRAC_SD_BACKUP/Projectcode/PretestorPreCouncil .


        if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.PRETEST)) {


            Long tsLong = System.currentTimeMillis() / 1000;
            String filename = tsLong.toString();


            File testfile = new File("/mnt/sdcard2/beeest/mtn.txt");
            testfile.mkdir();
            //File createfile = new File("/mnt/sdcard2/best/mtn.txt/file.xml");
            //FileUtils.writeStringToFile(createfile,"pabitra");


            if (ManvishPrefConstants.SELECTED_REG_OR_VER.read().equalsIgnoreCase(StringConstants.REGISTRATION)) {
                try {
                    filename = filename + "-" + mSelectedStudent.getStudentID() + "_reg" + ".xml";
                    File file = new File("/mnt/sdcard2/MeritTracSDBackUp/" + ManvishPrefConstants.SELECTED_EVENT_CODE.read()
                            + "/T");
                    file.mkdir();
                    File createfile = new File("/mnt/sdcard2/MeritTracSDBackUp/" + ManvishPrefConstants.SELECTED_EVENT_CODE.read()
                            + "/" + "T/" + filename);

                    ArrayList<TemplateWrite> templateListToUpload = dbAdapter.getTemplateToUpload(ManvishPrefConstants.SELECTED_DATE.read(),
                            ManvishPrefConstants.SELECTED_VENUE.read(), ManvishPrefConstants.SELECTED_BATCH.read(),
                            ManvishPrefConstants.SELECTED_CLASS.read(),
                            mSelectedStudent.getStudentID());

                    ArrayList<ImageWrite> fingerImageListToUpload = dbAdapter.getFingerImageToUpload(ManvishPrefConstants.SELECTED_DATE.read(),
                            ManvishPrefConstants.SELECTED_VENUE.read(), ManvishPrefConstants.SELECTED_BATCH.read(),
                            ManvishPrefConstants.SELECTED_CLASS.read(), mSelectedStudent.getStudentID());

                    byte[] photoGraph = dbAdapter.getRegPhotoForPostTest(ManvishPrefConstants.SELECTED_DATE.read(),
                            ManvishPrefConstants.SELECTED_VENUE.read(), ManvishPrefConstants.SELECTED_BATCH.read(),
                            ManvishPrefConstants.SELECTED_CLASS.read(),
                            mSelectedStudent.getStudentID());
                    String photoString = "";
                    if (photoGraph != null && photoGraph.length != 0) {
                        photoString = Base64.encodeToString(photoGraph, Base64.DEFAULT);
                    }

                    EnrStudentWrite enrStudentWrite = new EnrStudentWrite();
                    enrStudentWrite.setStudentId(mSelectedStudent.getStudentID());
                    enrStudentWrite.setUnitId(ManvishPrefConstants.SERIAL_NO.read());
                    enrStudentWrite.setProjCode(ManvishPrefConstants.SELECTED_EVENT_CODE.read());
                    enrStudentWrite.setRegDate(currentDate);
                    enrStudentWrite.setRegTime(currentTime);
                    enrStudentWrite.setPhotograph(photoString);
                    enrStudentWrite.setTemplate(templateListToUpload);
                    enrStudentWrite.setImage(fingerImageListToUpload);

                    ArrayList<EnrStudentWrite> enrStudentWriteArrayList = new ArrayList<>();
                    enrStudentWriteArrayList.add(enrStudentWrite);
                    EnrData enrDataWrite = new EnrData();
                    enrDataWrite.setStudent(enrStudentWriteArrayList);


                    RootModel rootModelWrite = new RootModel();

                    ProjectDetails projectDetails = new ProjectDetails();
                    projectDetails.setProjCode(ManvishPrefConstants.SELECTED_EVENT_CODE.read());
                    projectDetails.setStage("" + 1);
                    projectDetails.setVenueCode(ManvishPrefConstants.SELECTED_VENUE.read()); // Venue is not needed as we are inserting data DateWise

                    rootModelWrite.setProjectDetails(projectDetails);
                    rootModelWrite.setEnrData(enrDataWrite);

                    String xmlString = ManvishFileUtils.getUploadPreTestXml(rootModelWrite);

                    if (xmlString != null) {
                        FileUtils.writeStringToFile(createfile, xmlString);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (ManvishPrefConstants.SELECTED_REG_OR_VER.read().equalsIgnoreCase(StringConstants.VERIFICATION)) {
                filename = filename + "-" + mSelectedStudent.getStudentID() + "_ver" + ".xml";
                try {

                    File fileDir = new File("/mnt/sdcard2/MeritTracSDBackUp/" + ManvishPrefConstants.SELECTED_EVENT_CODE.read()
                            + "/T/");
                    fileDir.mkdir();
                    File file = new File("/mnt/sdcard2/MeritTracSDBackUp/" + ManvishPrefConstants.SELECTED_EVENT_CODE.read()
                            + "/T/" + filename);
                    RootModel rootModelWrite = new RootModel();

                    ProjectDetails projectDetails = new ProjectDetails();
                    projectDetails.setProjCode(ManvishPrefConstants.SELECTED_EVENT_CODE.read());
                    projectDetails.setStage("" + 1);
                    projectDetails.setVenueCode(ManvishPrefConstants.SELECTED_VENUE.read()); // Venue is not needed as we are inserting data DateWise

                    rootModelWrite.setProjectDetails(projectDetails);

                    AcsStudentWrite acsStudentWrite = new AcsStudentWrite();

                    acsStudentWrite.setStudentId(mSelectedStudent.getStudentID());
                    System.out.println("print  student id==" + mSelectedStudent.getStudentID());
                    acsStudentWrite.setUnitId(ManvishPrefConstants.SERIAL_NO.read());
                    System.out.println("print serial ID==" + ManvishPrefConstants.SERIAL_NO.read());
                    acsStudentWrite.setProjCode(ManvishPrefConstants.SELECTED_EVENT_CODE.read());
                    System.out.println("print selected event stage code==" + ManvishPrefConstants.SELECTED_EVENT_CODE.read());
                    acsStudentWrite.setVerDate(currentDate);
                    System.out.println("print ver date==" + currentDate);
                    acsStudentWrite.setVerTime(currentTime);
                    System.out.println("print ver time==" + currentTime);
                    acsStudentWrite.setInOut("INN");
                    acsStudentWrite.setStatus(mSelectedStudent.getVerStatus());
                    acsStudentWrite.setFingerType(mWhichFinger + "");
                    if (TextUtils.isEmpty(ManvishPrefConstants.SELECTED_SESSION_CODE.read())) {
                        acsStudentWrite.setSessionCode(" ");//hardcoded
                        System.out.println("print session code==" + ManvishPrefConstants.SELECTED_SESSION_CODE.read());
                    } else {
                        acsStudentWrite.setSessionCode(ManvishPrefConstants.SELECTED_SESSION_CODE.read());//hardcoded
                    }

                    ArrayList<AcsStudentWrite> acsStudentWriteArrayList = new ArrayList<>();
                    acsStudentWriteArrayList.add(acsStudentWrite);

                    Acslog acslog = new Acslog();
                    acslog.setStudent(acsStudentWriteArrayList);
                    rootModelWrite.setAcslog(acslog);


                    String xmlString = ManvishFileUtils.getUploadPreTestXml(rootModelWrite);

                    if (xmlString != null) {
                        FileUtils.writeStringToFile(file, xmlString);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (ManvishPrefConstants.SELECTED_EVENT_STAGE.read().equalsIgnoreCase(StringConstants.PRECOUNCIL)) {
            Long tsLong = System.currentTimeMillis() / 1000;
            String filename = tsLong.toString();
            filename = filename + "-" + mSelectedStudent.getStudentID() + ".xml";
            ;
//            File file = new File(Environment.getExternalStorageDirectory() + "/MeritTracSDBackUp/" + ManvishPrefConstants.SELECTED_EVENT_CODE.read()
//                    + "/" +"C/"+ filename);

            File fileDir = new File("/mnt/sdcard2/MeritTracSDBackUp/" + ManvishPrefConstants.SELECTED_EVENT_CODE.read()
                    + "/C/");
            fileDir.mkdir();
            File file = new File("/mnt/sdcard2/MeritTracSDBackUp/" + ManvishPrefConstants.SELECTED_EVENT_CODE.read()
                    + "/C/" + filename);

            if (ManvishPrefConstants.SELECTED_REG_OR_VER.read().equalsIgnoreCase(StringConstants.VERIFICATION)) {
                try {
                    RootModel rootModelWrite = new RootModel();

                    ProjectDetails projectDetails = new ProjectDetails();
                    projectDetails.setProjCode(ManvishPrefConstants.SELECTED_EVENT_CODE.read());
                    projectDetails.setStage("" + 1);
                    projectDetails.setVenueCode(ManvishPrefConstants.SELECTED_VENUE.read()); // Venue is not needed as we are inserting data DateWise

                    rootModelWrite.setProjectDetails(projectDetails);

                    AcsStudentWrite acsStudentWrite = new AcsStudentWrite();

                    acsStudentWrite.setStudentId(mSelectedStudent.getStudentID());
                    System.out.println("print  student id==" + mSelectedStudent.getStudentID());
                    acsStudentWrite.setUnitId(ManvishPrefConstants.SERIAL_NO.read());
                    System.out.println("print serial ID==" + ManvishPrefConstants.SERIAL_NO.read());
                    acsStudentWrite.setProjCode(ManvishPrefConstants.SELECTED_EVENT_CODE.read());
                    System.out.println("print selected event stage code==" + ManvishPrefConstants.SELECTED_EVENT_CODE.read());
                    acsStudentWrite.setVerDate(currentDate);
                    System.out.println("print ver date==" + currentDate);
                    acsStudentWrite.setVerTime(currentTime);
                    System.out.println("print ver time==" + currentTime);
                    acsStudentWrite.setInOut("INN");
                    acsStudentWrite.setStatus(mSelectedStudent.getVerStatus());
                    if (mFingerConfigArray != null && mFingerConfigArray.length != 0) {
                        acsStudentWrite.setFingerType(mFingerConfigArray[0]);
                    } else {
                        acsStudentWrite.setFingerType("2"); // HardCoded Value ..
                    }

                    if (TextUtils.isEmpty(ManvishPrefConstants.SELECTED_SESSION_CODE.read())) {
                        acsStudentWrite.setSessionCode(" ");//hardcoded
                        System.out.println("print session code==" + ManvishPrefConstants.SELECTED_SESSION_CODE.read());
                    } else {
                        acsStudentWrite.setSessionCode(ManvishPrefConstants.SELECTED_SESSION_CODE.read());//hardcoded
                    }

                    ArrayList<AcsStudentWrite> acsStudentWriteArrayList = new ArrayList<>();
                    acsStudentWriteArrayList.add(acsStudentWrite);

                    Acslog acslog = new Acslog();
                    acslog.setStudent(acsStudentWriteArrayList);
                    rootModelWrite.setAcslog(acslog);


                    String xmlString = ManvishFileUtils.getUploadPreTestXml(rootModelWrite);

                    if (xmlString != null) {
                        FileUtils.writeStringToFile(file, xmlString);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    protected void onStop() {

        super.onStop();
    }


}


