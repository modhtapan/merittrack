package com.manvish.merittrack.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.manvish.merittrack.Model.Project;
import com.manvish.merittrack.R;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.utils.ManvishCommonUtil;

import java.util.LinkedHashMap;

/**
 * Created by pabitra on 4/5/16.
 */
public class RegVerButtonActivity extends BaseActivity {

    Button btnRegistration;
    Button buttonhome;
    Button btnVerification, btnOfflineVerification;
    LinkedHashMap<String, String> mHmSECodeSEDate;

    int mNoOfSession;
    DBAdapter dbAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_ver);
        btnRegistration = (Button) findViewById(R.id.btn_registration);
        btnVerification = (Button) findViewById(R.id.btn_verification);
        btnOfflineVerification = (Button) findViewById(R.id.btn_offlineVerification);
        buttonhome = (Button) findViewById(R.id.btnHome);

        ManvishPrefConstants.SELECTED_SESSION_CODE.write("");

        if (!ManvishPrefConstants.IS_TEST.read()) {

            System.out.println("print db name==" + MeritTrackApplication.getInstance().getDbName());
            dbAdapter = DBAdapter.getInstance(this, MeritTrackApplication.getInstance().getDbName(), null, 1);
            Project project = dbAdapter.getProjectDetails();

            if (TextUtils.isEmpty(project.getNoOfVerSession())) {

                mNoOfSession = Integer.parseInt(project.getNoOfVerSession());

                if (mNoOfSession < 1) {
                    btnVerification.setVisibility(View.GONE);
                }
            }
        } else {
            btnVerification.setVisibility(View.VISIBLE);
            btnOfflineVerification.setVisibility(View.GONE);

        }


        // For Pre-council Stage ,No need to show ,These buttons to select ,
        // directly select Verification

        String selectedEventStage = ManvishPrefConstants.SELECTED_EVENT_STAGE.read();

        if (selectedEventStage.equalsIgnoreCase(StringConstants.PRECOUNCIL)) { // U can compare with stage also ,
            // if stage is 3
            goToVerificationActivity();
        }

        // finish menu activity
        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                //ManvishCommonUtil.goToMainactivity(RegVerButtonActivity.this);
            }
        });


        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManvishCommonUtil.goToMainactivity(RegVerButtonActivity.this);
            }
        });
    }

    public void registration(View v) {
        //go to select Date Activity with reg or var flag
        ManvishPrefConstants.SELECTED_SESSION_CODE.write("");
        ManvishPrefConstants.SELECTED_REG_OR_VER.write(StringConstants.REGISTRATION);
        Intent intent = new Intent(this, SelectDateActivity.class);
        startActivity(intent);
    }

    public void verification(View v) {
        goToSessionCodeActivity();
    }

    public void goToVerificationActivity() {
        //go to select Date Activity with reg or var flag
        ManvishPrefConstants.SELECTED_SESSION_CODE.write("");
        ManvishPrefConstants.SELECTED_REG_OR_VER.write(StringConstants.VERIFICATION);
        Intent intent = new Intent(this, SelectDateActivity.class);
        finish();
        startActivity(intent);
    }

    public void goToSessionCodeActivity() {
        //go to select Date Activity with reg or var flag
        ManvishPrefConstants.SELECTED_REG_OR_VER.write(StringConstants.VERIFICATION);
        Intent intent = new Intent(this, RegVerSessionCodesActivity.class);
        startActivity(intent);
    }

    public void offlineVerification(View view) {
        ManvishPrefConstants.SELECTED_SESSION_CODE.write("");
        ManvishPrefConstants.SELECTED_REG_OR_VER.write(StringConstants.OFFLINE_VERIFICATION);
        Intent intent = new Intent(this, SelectDateActivity.class);
        startActivity(intent);

    }


    @Override
    protected void onResume() {
        super.onResume();
        setHeading();
        /*Batterylevel b1=new Batterylevel();
        b1.Batterylevelindicator();*/
        //  Batterylevelindicator();
    }
}
