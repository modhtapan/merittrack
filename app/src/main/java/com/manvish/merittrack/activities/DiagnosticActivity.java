package com.manvish.merittrack.activities;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.manvish.merittrack.R;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.utils.BitmapUtility;
import com.manvish.merittrack.utils.InitializeSensor;
import com.manvish.merittrack.utils.ManvishCommonUtil;
import com.manvish.merittrack.utils.ManvishFileUtils;
import com.manvish.merittrack.view.ManvishAlertDialog;
import com.morpho.morphosample.info.ProcessInfo;

import java.io.IOException;
import java.util.List;

@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
public class DiagnosticActivity extends BaseActivity implements
        View.OnClickListener, PictureCallback {

    public static final String TAG = "CameraFragment";
    private Camera camera;

    private Button photoButton;

    private ImageView mIvFingerPrint;
    private Button mBtnCancel;
    private TextView mDateTimeTextView;

    private boolean isPausedCalled = false;


    private TextView mTvPlaceFinger;
    private FingureCapturedReciever mFingureCapturedReciever;


    private TextView mTvVenueCode;
    private boolean mTestMode;
    private byte[] mCameraImage;
    private byte[] mFingerImage;
    private byte[] mFingerTemplate;
    private String mCurrentDate, mCurrentTime, mRegStatus = "0", mVerStatus = "0", mLongitude, mLatitude;

    SurfaceView surfaceView;
    private static String mStartTime;
    int onreceive;
    Button btnConfigFinger;
    DBAdapter dbAdapter;
    private InitializeSensor mSensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        mSensor=new InitializeSensor(this);

        setContentView(R.layout.activity_camera);
        dbAdapter = DBAdapter.getInstance();
        dbAdapter.closeDatabase();
        dbAdapter = DBAdapter.getInstance(this, "common.db", null, 1); // Common is the common databse name
        onreceive = 0;
        mTestMode = getIntent().getBooleanExtra("testmode", false);

        mCurrentDate = ManvishCommonUtil.getCurrentDate();
        mCurrentTime = ManvishCommonUtil.getCurrentTime();
        mLongitude = "";
        mLatitude = "";

        mFingureCapturedReciever = new FingureCapturedReciever();
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mFingureCapturedReciever,
                new IntentFilter(StringConstants.ACTION_FINGER_CAPTURED));

        initializeViews();
        photoButton = (Button) findViewById(R.id.camera_photo_button);

        if (camera == null) {
            try {
                camera = Camera.open();
                // photoButton.setVisibility(View.GONE);
            } catch (Exception e) {
                Log.e(TAG, "No camera with exception: " + e.getMessage());
                photoButton.setEnabled(false);
                /*Toast.makeText(this, "Please Try Again", Toast.LENGTH_LONG)
                        .show();*/
                // new ManvishAlertDialog(getApplicationContext(), title,
                // message)
                ManvishCommonUtil.showCustomToast(DiagnosticActivity.this, "Please try again!!!");


            }
        }

        photoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (photoButton.getText().toString().contains("PHOTO")) {

                    // photoButton.setVisibility(View.GONE);
                    photoButton.setEnabled(false);
                    mTvPlaceFinger.setVisibility(View.VISIBLE);
                    mTvPlaceFinger.setText("PLACE FINGER FOR REGISTRATION");
                    if (camera == null)
                        return;
                    camera.takePicture(null, null, DiagnosticActivity.this);

                } else if (photoButton.getText().toString().contains("CAPTURE")) {
                    photoButton.setEnabled(false);
                    ManvishPrefConstants.SELECTED_REG_OR_VER.write(StringConstants.REGISTRATION);
                    startConnection();

                } else if (photoButton.getText().toString().contains("VERIFY")) {
                    ManvishPrefConstants.SELECTED_REG_OR_VER.write(StringConstants.VERIFICATION);
                    ManvishPrefConstants.IS_DIAGNOSTIC.write(true);
                    photoButton.setEnabled(false);
                    startConnection();
                }


            }
        });

        surfaceView = (SurfaceView) findViewById(R.id.camera_surface_view);
        SurfaceHolder holder = surfaceView.getHolder();
        holder.addCallback(new Callback() {

            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (camera != null) {


                        camera.setDisplayOrientation(90);
                        camera.setPreviewDisplay(holder);
                        camera.setParameters(getCameraParams(camera));
                        Camera.Parameters params = camera.getParameters();

                        // Check what resolutions are supported by your camera
                        List<Camera.Size> sizes = params
                                .getSupportedPictureSizes();

                        // Iterate through all available resolutions and choose
                        // one.
                        // The chosen resolution will be stored in mSize.
                        Log.e("Size:", "Width:" + sizes.get(0).width);
                        Log.e("Size:", "Height:" + sizes.get(0).height);
                        params.setPictureSize(sizes.get(0).width,
                                sizes.get(0).height);
                        camera.setParameters(params);
                        camera.startPreview();

                        // set preview size and make any resize, rotate or
                        // reformatting changes here


                        // start preview with new settings
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Error setting up preview", e);
                }
            }

            public void surfaceChanged(SurfaceHolder holder, int format,
                                       int width, int height) {
                // nothing to do here
            }

            public void surfaceDestroyed(SurfaceHolder holder) {
                // nothing here
            }

        });

       // mSensor=MeritTrackApplication.getInstance().getInitializeSensor();

    }

    private Camera.Parameters getCameraParams(Camera mCamera) {
        Camera.Parameters params = mCamera.getParameters();
        params.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
        params.setPictureSize(320, 240);

        List<String> focusModes = params.getSupportedFocusModes();

        if (focusModes.contains(Camera.Parameters.FLASH_MODE_AUTO)) {
            params.setFocusMode(Camera.Parameters.FLASH_MODE_AUTO);
        } else if (focusModes
                .contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }

        params.setExposureCompensation(0);
        params.setPictureFormat(ImageFormat.JPEG);
        params.setJpegQuality(50);


        return params;
    }

    @Override
    public void onResume() {
        super.onResume();
        setHeading();

        if (camera == null) {
            try {
                if (isPausedCalled) {
                    camera = Camera.open();
                    // photoButton.setEnabled(true);
                    isPausedCalled = false;
                }
            } catch (Exception e) {
                Log.i(TAG, "No camera: " + e.getMessage());
                photoButton.setEnabled(false);
                /*Toast.makeText(this, "No camera detected", Toast.LENGTH_LONG)
                        .show();*/
                ManvishCommonUtil.showCustomToast(DiagnosticActivity.this, "No camera detected");
            }
        }


    }

    private void startConnection() {
        startConnectionFragment();
        mBtnCancel.setEnabled(true);
    }

    private void startConnectionFragment() {

        Intent dialogActivity = new Intent(this, MorphoSampleActivity.class);
         startActivity(dialogActivity);

//        if(ProcessInfo.getInstance().getMorphoDatabase()!=null) {
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    mSensor.captureFinger();
//                }
//            }).start();
//
//        }else{
//            //Initialize sensor
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    ManvishCommonUtil.initializeSensor(DiagnosticActivity.this,mSensor);
//                }
//            }).start();
//        }

    }

    private void displayAndDecodeImage(final byte[] data) {

        Thread thread = new Thread() {
            @Override
            public void run() {
                mCameraImage = data;
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0,
                        data.length);

                if (bitmap != null) {
                    try {

                        //Save captured photo graph here
                        // capturedPhoto.save();
                        DiagnosticActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                photoButton.setEnabled(true);
                                photoButton.setText("CAPTURE FINGER");
                                photoButton.setBackgroundResource(R.drawable.menu_button_selected);
                            }
                        });

                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                }

            }
        };
        thread.start();

    }

    private void initializeViews() {
        btnConfigFinger = (Button) findViewById(R.id.btnSelectFinger);
        btnConfigFinger.setVisibility(View.INVISIBLE);
        mTvPlaceFinger = (TextView) findViewById(R.id.tvPlaceFinger);
        mTvPlaceFinger.setVisibility(View.GONE);

        mBtnCancel = (Button) findViewById(R.id.btnCancel);

        // mBtnEnter.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        mIvFingerPrint = (ImageView) findViewById(R.id.ivFinger);
        mIvFingerPrint.setVisibility(View.INVISIBLE);

        // set the profile pic for mIvProfilePic
        //mIvProfilePic = (ImageView) findViewById(R.id.ivPhoto);


    }

    @Override
    public void onPause() {

        super.onPause();

        if (camera != null) {
            camera.release();
            camera = null;
        }

        isPausedCalled = true;
    }

    @Override
    protected void onDestroy() {

        try {
            unRegisterReciever();
            ManvishPrefConstants.IS_DIAGNOSTIC.write(false);
        }catch (Exception e){
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private class FingureCapturedReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            onreceive = onreceive + 1;
            // finish activity after few seconds
            photoButton.setEnabled(true);
            mBtnCancel.setEnabled(true);
            if (intent
                    .getParcelableExtra(StringConstants.KEY_INTENT_BITMAP) != null) {
                // Store image in toDB
                mTvPlaceFinger.setText("PLACE FINGER FOR VERIFICATION");
                mRegStatus = "1";
                Bitmap bitmap = (Bitmap) intent
                        .getParcelableExtra(StringConstants.KEY_INTENT_BITMAP);

                byte[] fpTemplate = (byte[]) intent
                        .getByteArrayExtra(StringConstants.KEY_INTENT_TEMPLATE);

                if (photoButton.getText().toString().contains("VERIFY")) {
                    if (fpTemplate != null && fpTemplate.length != 0) {
                        mVerStatus = "1";
                    }
                }

                mFingerTemplate = fpTemplate;
                MeritTrackApplication.getInstance().setDiagonsticTemplate(fpTemplate);
                //


                Bitmap fingerImageBitmap = (Bitmap) intent
                        .getParcelableExtra(StringConstants.KEY_INTENT_BITMAP);

                byte[] fingerByteArray = ManvishFileUtils.getBytesFromBitmap(fingerImageBitmap);
                mIvFingerPrint.setVisibility(View.VISIBLE);
                mFingerImage = fingerByteArray;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mIvFingerPrint.setVisibility(View.GONE);
                    }
                }, 2050);
                mIvFingerPrint.setImageBitmap(fingerImageBitmap);
            }


            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub

                    if (photoButton.getText().toString().contains("VERIFY")) {

                        if (dbAdapter != null) {
                            try {
                                dbAdapter.insertDataToDiagTable(mCurrentDate, mCurrentTime, mLongitude, mLatitude, mCameraImage, mFingerImage, mFingerTemplate, mRegStatus,
                                        mVerStatus);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        finish();
                    }
                    photoButton.setText("VERIFY FINGER");
                }
            }, 300);
        }
    }//End of the Fingerimage bitmap class


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancel:

                finish();
                break;


            default:
                break;
        }
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {

        displayAndDecodeImage(data);
    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        // Do nothing
    }

    private void unRegisterReciever() {
        try {
            LocalBroadcastManager.getInstance(DiagnosticActivity.this)
                    .unregisterReceiver(mFingureCapturedReciever);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }



}
