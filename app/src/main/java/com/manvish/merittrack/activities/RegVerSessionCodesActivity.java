package com.manvish.merittrack.activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.activeandroid.util.Log;
import com.manvish.merittrack.R;
import com.manvish.merittrack.adapters.SessionCodeListAdapter;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.utils.ManvishCommonUtil;

import java.util.ArrayList;

/**
 * Created by pabitra on 4/18/16.
 */
public class RegVerSessionCodesActivity extends BaseActivity {

    Button btnCancel;
    Button buttonhome;
    ListView mSessionCodeListView;
    DBAdapter dbAdapter;
    ArrayList<String> mSessionCodeList;
    SessionCodeListAdapter mSessionCodeListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_ver_session);
        btnCancel = (Button) findViewById(R.id.cancelButton);
        buttonhome = (Button) findViewById(R.id.btnHome);


        dbAdapter = DBAdapter.getInstance(this, MeritTrackApplication.getInstance().getDbName(), null, 1);
        mSessionCodeListView = (ListView) findViewById(android.R.id.list);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setSessionCodeList();

        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManvishCommonUtil.goToMainactivity(RegVerSessionCodesActivity.this);
            }
        });
    }


    private void setSessionCodeList() {


        new AsyncTask<Void, Void, Boolean>() {
            protected void onPreExecute() {
                showProgressBar("Fetching Batches");

            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {

                    mSessionCodeList = dbAdapter.getVerSessionCodeList();

                    if(ManvishPrefConstants.IS_TEST.read()){
                        mSessionCodeList=new ArrayList<String>();
                        mSessionCodeList.add("TEST-SESSION");
                    }

                    if (mSessionCodeList.size() > 0) {
                        return true;
                    } else {
                        return false;
                    }

                } catch (Exception e) {
                    Log.e("MANVISH",
                            "Exception in parsing file"
                                    + e.getMessage());

                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean success) {

                if (success) {
                    mSessionCodeListAdapter = new SessionCodeListAdapter(mSessionCodeList);
                    mSessionCodeListView.setAdapter(mSessionCodeListAdapter);
                }

                dismissProgressBar();
            }

        }.execute();
    }


    ProgressDialog mProgressDialog;

    private void showProgressBar(String msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
        }


        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    private void dismissProgressBar() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }


//    public void ShowAlertDialogSessionDateList(ArrayList<String> sessionDateList)
//    {
//
//        //Create sequence of items
//        final CharSequence[] sessiondateArray = sessionDateList.toArray(new String[sessionDateList.size()]);
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
//        dialogBuilder.setTitle("SELECT SESSION DATE");
//        dialogBuilder.setItems(sessiondateArray, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int item) {
//
//                String selectedText = sessiondateArray[item].toString();  //Selected item in listview
//                System.out.println("selected session date ="+selectedText);
//
//            }
//        });
//        //Create alert dialog object via builder
//        AlertDialog alertDialogObject = dialogBuilder.create();
//        //Show the dialog
//        alertDialogObject.show();
//    }


    @Override
    protected void onResume() {
        super.onResume();
        setHeading();
        /*Batterylevel b1=new Batterylevel();
        b1.Batterylevelindicator();*/
        // Batterylevelindicator();
    }
}
