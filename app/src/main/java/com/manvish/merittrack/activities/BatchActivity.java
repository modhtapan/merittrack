package com.manvish.merittrack.activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.util.Log;
import com.manvish.merittrack.R;
import com.manvish.merittrack.adapters.BatchListAdapter;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.utils.ManvishCommonUtil;

import java.util.ArrayList;

// MenuActivity for showing all menu lists
public class BatchActivity extends BaseActivity {

    ListView menuListView;
    BatchListAdapter batchListAdapter;
    ArrayList<String> batchList;
    DBAdapter dbAdapter;
    Button buttonhome;
    TextView txteventname, txtviewselecteddate, txtselectedvenue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_batch);

        buttonhome = (Button) findViewById(R.id.btnHome);
        txteventname = (TextView) findViewById(R.id.tv_event_name);
        txtviewselecteddate = (TextView) findViewById(R.id.tv_selected_date);
        txtselectedvenue = (TextView) findViewById(R.id.tv_selected_venue);
        dbAdapter = DBAdapter.getInstance(this, MeritTrackApplication.getInstance().getDbName(), null, 1);

        menuListView = (ListView) findViewById(android.R.id.list);

        if (ManvishPrefConstants.IS_TEST.read()) {
            batchListAdapter = new BatchListAdapter(null);
            menuListView.setAdapter(batchListAdapter);
        } else {
            setBatchList();
        }

        txteventname.setText(ManvishPrefConstants.SELECTED_EVENT_CODE.read());
        txtviewselecteddate.setText(ManvishPrefConstants.SELECTED_DATE.read());
        txtselectedvenue.setText(ManvishPrefConstants.SELECTED_VENUE.read());

        // finish menu activity
        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                // ManvishCommonUtil.goToMainactivity(BatchActivity.this);
            }
        });

        //Button for Home activity
        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManvishCommonUtil.goToMainactivity(BatchActivity.this);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        setHeading();
        // mBatInfoReceiver.onReceive(Context c, Intent i);
        /*Batterylevel b1=new Batterylevel();
        b1.Batterylevelindicator();*/
    }

    private void setBatchList() {


        new AsyncTask<Void, Void, Boolean>() {
            protected void onPreExecute() {
                showProgressBar("Fetching Batches");

            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {

                    batchList = dbAdapter.getUniqueBatchStudentTable();

                    if (batchList.size() > 0) {
                        return true;
                    } else {
                        return false;
                    }

                } catch (Exception e) {
                    Log.e("MANVISH",
                            "Exception in parsing file"
                                    + e.getMessage());

                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean success) {

                if (success) {
                    batchListAdapter = new BatchListAdapter(batchList);
                    menuListView.setAdapter(batchListAdapter);
                }

                dismissProgressBar();
            }

        }.execute();
    }

    ProgressDialog mProgressDialog;

    private void showProgressBar(String msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
        }


        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    private void dismissProgressBar() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }
}
