package com.manvish.merittrack.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.util.Log;
import com.manvish.merittrack.R;
import com.manvish.merittrack.adapters.DateListAdapter;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.db.DBAdapter;

import java.util.ArrayList;

// MenuActivity for showing all menu lists
public class SelectDateActivity extends BaseActivity {


    ArrayList<String> mDateList;
    DBAdapter dbAdapter;
    ListView menuListView;
    DateListAdapter dateListAdapter;
    Button buttonhome;
    TextView tveventname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date);
        buttonhome = (Button) findViewById(R.id.btnHome);
        tveventname = (TextView) findViewById(R.id.tvEventName);
        tveventname.setText(ManvishPrefConstants.SELECTED_EVENT_CODE.read());

        dbAdapter = DBAdapter.getInstance(this, MeritTrackApplication.getInstance().getDbName(), null, 1);
        menuListView = (ListView) findViewById(android.R.id.list);

        String selectedEventStage = ManvishPrefConstants.SELECTED_EVENT_STAGE.read();
        if (selectedEventStage.equalsIgnoreCase(StringConstants.POSTTEST) || selectedEventStage.equalsIgnoreCase(StringConstants.POSTCOUNCIL)) {
            //cancelAlarmManager();
        }

        if (ManvishPrefConstants.IS_TEST.read()) {
            dateListAdapter = new DateListAdapter(null, dbAdapter);
            menuListView.setAdapter(dateListAdapter);
        } else {
            setDateList();
        }

        // finish menu activity
        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //setAlarmManager();
                finish();
                // ManvishCommonUtil.goToMainactivity(SelectDateActivity.this);
            }
        });

        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*finish();
                ManvishCommonUtil.goToMainactivity(SelectDateActivity.this);*/

                Intent intent = new Intent(SelectDateActivity.this, MainActivity.class);
                startActivity(intent);
                finish();


            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        //setHeading();
        setHeading();
        /*Batterylevel b1=new Batterylevel();
        b1.Batterylevelindicator();*/
        // Batterylevelindicator();
    }


    private void setDateList() {


        new AsyncTask<Void, Void, Boolean>() {
            protected void onPreExecute() {
                showProgressBar("Fetching Exam Dates");

            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {

                    mDateList = dbAdapter.getUniqueDateListFromStudentTable();

                    if (mDateList.size() > 0) {
                        return true;
                    } else {
                        return false;
                    }

                } catch (Exception e) {
                    Log.e("MANVISH",
                            "Exception in parsing file"
                                    + e.getMessage());

                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean success) {

                if (success) {
                    dateListAdapter = new DateListAdapter(mDateList, dbAdapter);
                    menuListView.setAdapter(dateListAdapter);
                }

                dismissProgressBar();
            }

        }.execute();
    }

    ProgressDialog mProgressDialog;

    private void showProgressBar(String msg) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
        }


        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    private void dismissProgressBar() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }
}
