// The present software is not subject to the US Export Administration Regulations (no exportation license required), May 2012
package com.manvish.merittrack.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Surface;
import android.widget.Button;
import android.widget.Toast;

import com.manvish.merittrack.R;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.fragment.ProcessFragment;
import com.manvish.merittrack.utils.ManvishCommonUtil;
import com.manvish.merittrack.view.ManvishAlertDialog;
import com.morpho.android.usb.USBManager;

import com.morpho.morphosample.info.CaptureInfo;
import com.morpho.morphosample.info.MorphoInfo;
import com.morpho.morphosample.info.ProcessInfo;
import com.morpho.morphosample.info.VerifyInfo;
import com.morpho.morphosample.info.subtype.AuthenticationMode;
import com.morpho.morphosample.info.subtype.CaptureType;
import com.morpho.morphosmart.sdk.ErrorCodes;
import com.morpho.morphosmart.sdk.MorphoDevice;
import com.morpho.morphosmart.sdk.StrategyAcquisitionMode;
import com.morpho.morphosmart.sdk.TemplateFVPType;
import com.morpho.morphosmart.sdk.TemplateType;

import java.util.Observable;
import java.util.Observer;


@SuppressWarnings("deprecation")
@SuppressLint("UseValueOf")
public class MorphoSampleActivity extends BaseActivity implements Observer {

    public static final String TAG = "MERITRACK";
    private Handler mHandler = new Handler();
    public static boolean isRebootSoft = false;
    static MorphoDevice morphoDevice = new MorphoDevice();
    boolean isActivityActive = false;

    boolean isDeviceDetected;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActivityActive = false;
        closeDeviceAndFinishActivity();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_morpho_sample);
        if (morphoDevice == null) {
            Log.e("MorphoSampleActivity", "morphoDevice:null");
        }


        if (USBManager.getInstance().isDevicesHasPermission() == true) {

            enumerate();
        } else {
            grantPermission();
        }

    }

    public void grantPermission() {
        USBManager.getInstance().initialize(this,
                "com.morpho.morphosample.USB_ACTION");
    }

    String sensorName;

    @SuppressLint("UseValueOf")
    public void enumerate() {

        Integer nbUsbDevice = new Integer(0);

        int ret = morphoDevice.initUsbDevicesNameEnum(nbUsbDevice);

        if (ret == ErrorCodes.MORPHO_OK) {

            if (nbUsbDevice > 0) {
                sensorName = morphoDevice.getUsbDeviceName(0);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        connection();
                    }
                }, 50);

            } else {


               /* Toast.makeText(this, "device not detected or permission not asked", Toast.LENGTH_LONG).show();*/
                Log.d(TAG, "Device not detected or permission not asked");

                ManvishCommonUtil.showCustomToast(this,"Device is Busy ,Try Again");
                finish();
            }
        } else {

            ManvishCommonUtil.showCustomToast(this,"Device is Busy ,Try Again");
            finish();


        }
    }

    @SuppressLint("UseValueOf")
    public void connection() {
        int ret = morphoDevice.openUsbDevice(sensorName, 0);

        if (ret != 0) {

            ManvishCommonUtil.showCustomToast(this,"Device is Busy ,Try Again");
            finish();

            /*Toast.makeText(this, ErrorCodes.getError(ret,
                    morphoDevice.getInternalError()), Toast.LENGTH_LONG).show();
*/

        } else {
            ProcessInfo.getInstance().setMSOSerialNumber(sensorName);
            String productDescriptor = morphoDevice.getProductDescriptor();
            java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(
                    productDescriptor, "\n");
            if (tokenizer.hasMoreTokens()) {
                String l_s_current = tokenizer.nextToken();
                if (l_s_current.contains("FINGER VP")
                        || l_s_current.contains("FVP")) {
                    MorphoInfo.m_b_fvp = true;
                }
            }


            if (ret != ErrorCodes.MORPHO_OK) {

                ManvishCommonUtil.showCustomToast(this,"Device is Busy ,Try Again");
                finish();

            } else {
                morphoDevice.closeDevice();


                if (ProcessInfo.getInstance().getMorphoDevice() == null) {
                    String sensorName = ProcessInfo.getInstance().getMSOSerialNumber();
                    ret = morphoDevice.openUsbDevice(sensorName, 0);

                    if (ret == ErrorCodes.MORPHO_OK) {

                        byte[] sensorWindowPosition = morphoDevice.getConfigParam(MorphoDevice.CONFIG_SENSOR_WIN_POSITION_TAG);
                        int position = 0;
                        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@Step 1 " + sensorWindowPosition[0]);
                        if (sensorWindowPosition != null) {
                            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@Step 1 " + sensorWindowPosition[0]);
                            position = sensorWindowPosition[0];
                            if (position != 3) {
                                byte[] fpOrient = new byte[1];
                                fpOrient[0] = 0x03;
                                int ret1 = morphoDevice.setConfigParam(MorphoDevice.CONFIG_SENSOR_WIN_POSITION_TAG, fpOrient);
                                if (ret1 != ErrorCodes.MORPHO_OK) {

                                    System.out.println("################################Step 2 fail");
                                } else {
                                    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$Step 3 success ");
                                    isRebootSoft = true;

                                    ret = morphoDevice.rebootSoft(30, this);

                                }
                            }
                        }


                    } else {
                        ManvishCommonUtil.showCustomToast(this,"Device is Busy ,Try Again");
                        finish();
                    }
                }

                ProcessInfo.getInstance().setMorphoDevice(morphoDevice);


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onStartStop();
                    }
                }, 50);

            }
        }
    }

    @Override
    protected void onResume() {

        setHeading();
        super.onResume();

        // morphoDevice.resumeConnection(30, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        try {
            if (morphoDevice != null && ProcessInfo.getInstance().isStarted()) {
                morphoDevice.cancelLiveAcquisition();
                morphoDevice.closeDevice();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        super.onStop();
    }

    protected void alert(int codeError, int internalError, String title,
                         String message) {

          ManvishCommonUtil.showCustomToast(this,"Device is Busy ,Try Again");
          finish();
    }


    public void stopProcess() {
        if (ProcessInfo.getInstance().isStarted()) {
            this.stop();
        }

    }

    private void stop() {

        //unlockScreenOrientation();

        ProcessInfo.getInstance().setStarted(false);
        if (ProcessInfo.getInstance().isCommandBioStart()) {
            ProcessInfo.getInstance().getMorphoDevice().cancelLiveAcquisition();
        }

    }


    private void unlockScreenOrientation() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

    private void lockScreenOrientation() {
        final int orientation = getResources().getConfiguration().orientation;
        final int rotation = getWindowManager().getDefaultDisplay()
                .getOrientation();

        if (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_90) {
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        } else if (rotation == Surface.ROTATION_180
                || rotation == Surface.ROTATION_270) {
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
            } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
            }
        }
    }


    public void closeDeviceAndFinishActivity() {
        try {
            morphoDevice.closeDevice();

        } catch (Exception e) {
            Log.e("ERROR", e.toString());
        } finally {
            ProcessInfo.getInstance().setMorphoDevice(null);
            ProcessInfo.getInstance().setMorphoDatabase(null);
            ProcessInfo.getInstance().setDatabaseSelectedIndex(-1);
        }
    }

    @Override
    public synchronized void update(Observable observable, Object data) {
        Boolean isOpenOK = (Boolean) data;

        MorphoSampleActivity.isRebootSoft = false;

        if (isOpenOK == true) {


        } else {
            mHandler.post(new Runnable() {
                @Override
                public synchronized void run() {
                    ManvishCommonUtil.showCustomToast(MorphoSampleActivity.this,"Device is Busy ,Try Again");
                    finish();
                }
            });
        }
    }


    public final void onStartStop() {
        if (ProcessInfo.getInstance().isStarted()) {
            this.stop();
        } else {

            //For Capture
            MorphoInfo info = null;
            CaptureInfo.getInstance().setIDNumber("12");
            CaptureInfo.getInstance().setLastName("sahoo");
            CaptureInfo.getInstance().setFirstName("pabi");
            CaptureInfo.getInstance().setLatentDetect(true);
            CaptureInfo.getInstance().setCaptureType(CaptureType.Verif);
            CaptureInfo.getInstance().setTemplateType(
                    TemplateType.MORPHO_PK_ISO_FMR);
            CaptureInfo.getInstance().setTemplateFVPType(
                    TemplateFVPType.MORPHO_NO_PK_FVP);

            info = CaptureInfo.getInstance();

            // For Verification
            VerifyInfo veriFyInstance = VerifyInfo.getInstance();
            veriFyInstance.setAuthenticationMode(AuthenticationMode.File);
            veriFyInstance.setFileName("Filename");
            MorphoInfo verifyInfo = veriFyInstance;


            // MorphoInfo info = current.retrieveSettings();
            if (info != null) {
                //enableDisableIHM(false);
                // Button startbutton = (Button) findViewById(R.id.startstop);

                switch (ManvishPrefConstants.SELECTED_REG_OR_VER.read()) {

                    case StringConstants.VERIFICATION:

                        ProcessInfo.getInstance().setStrategyAcquisitionMode(StrategyAcquisitionMode.MORPHO_ACQ_EXPERT_MODE);
                        ProcessInfo.getInstance().setMorphoInfo(verifyInfo);

                        break;

                    case StringConstants.OFFLINE_VERIFICATION:

                        ProcessInfo.getInstance().setStrategyAcquisitionMode(StrategyAcquisitionMode.MORPHO_ACQ_EXPERT_MODE);
                        ProcessInfo.getInstance().setMorphoInfo(verifyInfo);

                        break;
                    case StringConstants.REGISTRATION:

                        ProcessInfo.getInstance().setStrategyAcquisitionMode(StrategyAcquisitionMode.MORPHO_ACQ_EXPERT_MODE);
                        ProcessInfo.getInstance().setMorphoInfo(info);

                        break;
                }

                ProcessInfo.getInstance().setMorphoSample(this);

                startProcessFragment();
                try {
                    ProcessInfo.getInstance().setCommandBioStart(true);

                    ProcessInfo.getInstance().setStarted(true);
                   // lockScreenOrientation();
                } catch (Exception e) {

                    ManvishCommonUtil.showCustomToast(this,"Device is Busy ,Try Again");
                    finish();
                }


            }else{

                ManvishCommonUtil.showCustomToast(this,"Device is Busy ,Try Again");
                finish();
            }
        }
    }

    // finish();
    // }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        isActivityActive = true;
        super.onStart();

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }


    private void startProcessFragment() {

        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container_sensor, new ProcessFragment())
                .commit();
    }

}
