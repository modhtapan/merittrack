package com.manvish.merittrack.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.activeandroid.util.Log;
import com.manvish.merittrack.Model.Config;
import com.manvish.merittrack.R;
import com.manvish.merittrack.adapters.EventCodeListAdapter;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.db.DBAdapter;
import com.manvish.merittrack.view.ManvishAlertDialog;

import java.util.LinkedHashMap;

// MenuActivity for showing all menu lists
public class EventCodesActivity extends BaseActivity {
    Button buttonhome;
    DBAdapter dbAdapter;
    LinkedHashMap<String, String> eventMap;
    ListView menuListView;
    EventCodeListAdapter eventcodeListAdapter;
    String stage; //pretest means stage 1 and precouncil means stage 2

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_codes);

        buttonhome = (Button) findViewById(R.id.btnHomeevent);

        // finish menu activity
        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                // ManvishCommonUtil.goToMainactivity(EventCodesActivity.this);
            }
        });


        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(EventCodesActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        dbAdapter = DBAdapter.getInstance();
        // Close previus DB if exist , to connect to new DB
        if(dbAdapter!=null) {
            dbAdapter.closeDatabase();
        }

        dbAdapter = DBAdapter.getInstance(this, "common.db", null, 1); // Common is the common databse name
        menuListView = (ListView) findViewById(android.R.id.list);

        stage = getIntent().getStringExtra(StringConstants.KEY_STAGE);
        ManvishPrefConstants.STAGE.write(stage);


        if (ManvishPrefConstants.IS_TEST.read()) {
            eventcodeListAdapter = new EventCodeListAdapter(EventCodesActivity.this, null);
            menuListView.setAdapter(eventcodeListAdapter);
        } else {
            Config config = dbAdapter.getConfigData();
            if(config!=null) {
                ManvishPrefConstants.THREAD_SCHEDULING.write(config.getThreadScheduling());
                ManvishPrefConstants.COMPANY_LOGO.write(config.getLogo());
                ManvishPrefConstants.COMPANY_NAME.write(config.getCompanyDetails());
                stage = getIntent().getStringExtra(StringConstants.KEY_STAGE);
            }
           // ManvishPrefConstants.STAGE.write(stage);

            setEventCodeList();
        }
        dbAdapter.closeDatabase();

    }

    @Override
    protected void onResume() {
        setHeading();
        super.onResume();

    }

    private void setEventCodeList() {


        new AsyncTask<Void, Void, Boolean>() {
            protected void onPreExecute() {


            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {

                    eventMap = dbAdapter.getEventsFromCommonDatabaseCommonTable(stage);

                    if (eventMap.size() > 0) {
                        return true;
                    } else {
                        return false;
                    }

                } catch (Exception e) {
                    Log.e("MANVISH",
                            "Exception in parsing file"
                                    + e.getMessage());

                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean success) {

                if (success) {
                    eventcodeListAdapter = new EventCodeListAdapter(EventCodesActivity.this, eventMap);
                    menuListView.setAdapter(eventcodeListAdapter);
                    dbAdapter.closeDatabase();
                } else {

                    new ManvishAlertDialog(EventCodesActivity.this, "No Data", "No Project Found ,Please contact Admin").showAlertDialog();
                }

            }

        }.execute();
    }
}
