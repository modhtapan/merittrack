package com.manvish.merittrack.activities;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.manvish.merittrack.R;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.utils.ManvishCommonUtil;
import com.manvish.merittrack.utils.Utils;

// DeviceDetailsActivity for showing device settings
public class DeviceDetailsActivity extends BaseActivity {


    Button buttonhome;
    TextView serialId, macAddress, FirmwareVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_details);

        serialId = (TextView) findViewById(R.id.serialIdValue);
        macAddress = (TextView) findViewById(R.id.macAddressValue);
        FirmwareVersion = (TextView) findViewById(R.id.appVersionValue);

        buttonhome = (Button) findViewById(R.id.btnHome);

        //serialId.setText(Utils.getSerialId(this));
        serialId.setText(ManvishPrefConstants.SERIAL_NO.read());

        if (ManvishPrefConstants.MAC_ADDRESS.read() != null) {

            macAddress.setText(ManvishPrefConstants.MAC_ADDRESS.read());
        } else {
            macAddress.setText(Utils.availMacAddress(this));
        }
        // set app version from gradle
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            FirmwareVersion.setText("miFaun-Mi006-v1.0 - " + pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        // finish activity
        Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManvishCommonUtil.goToMainactivity(DeviceDetailsActivity.this);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        // setHeading();
        setHeading();
        /*Batterylevel b1=new Batterylevel();
        b1.Batterylevelindicator();
*/       // Batterylevelindicator();
    }
}
