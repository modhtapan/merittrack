package com.manvish.merittrack.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.manvish.merittrack.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Headeractivity extends BaseActivity {


    private TextView mHeaderDateView;
    TextView battery_level;
    ImageView iv_bat_icon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_header);


        battery_level = (TextView) findViewById(R.id.battery_level);
        iv_bat_icon = (ImageView) findViewById(R.id.iv_bat_icon);

        //Register the receiver which triggers event
        //when battery charge is changed
        registerReceiver(mBatInfoReceiver, new IntentFilter(
                Intent.ACTION_BATTERY_CHANGED));


    }


    //Create Broadcast Receiver Object along with class definition
    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
        @Override
        //When Event is published, onReceive method is called
        public void onReceive(Context c, Intent i) {
            //Get Battery %
            int level = i.getIntExtra("level", 0);
            //Find the progressbar creating in main.xml
            //ProgressBar pb = (ProgressBar) findViewById(R.id.progressbar);
            //Set progress level with battery % value
            // pb.setProgress(level);
            //Find textview control created in main.xml
            battery_level = (TextView) findViewById(R.id.battery_level);
            //Set TextView with text
            battery_level.setText("Battery Level: " + Integer.toString(level) + "%");
        }

    };


    protected void setHeading() {

        // check if textview is null then get from layout
        if (mHeaderDateView == null) {
            mHeaderDateView = (TextView) findViewById(R.id.headerDate);
        }

        Date now = new Date();
        int hours = now.getHours();
        hours = (hours > 12) ? hours - 12 : hours;
        int minutes = now.getMinutes();
        int h1 = hours / 10;
        int h2 = hours - h1 * 10;
        int m1 = minutes / 10;
        int m2 = minutes - m1 * 10;

        Calendar calendar = Calendar.getInstance();

        Date date = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        mHeaderDateView.setText(dateFormat.format(date));
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
