package com.manvish.merittrack.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.activeandroid.util.Log;
import com.manvish.merittrack.R;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.constants.ManvishPrefConstants;
import com.manvish.merittrack.constants.StringConstants;
import com.manvish.merittrack.http.DownloadData;
import com.manvish.merittrack.utils.InternetAvailability;
import com.manvish.merittrack.utils.ManvishCommonUtil;
import com.manvish.merittrack.utils.ParseAupdZipFile;
import com.manvish.merittrack.utils.UploadFile;
import com.manvish.merittrack.utils.Utils;
import com.manvish.merittrack.view.ManvishAlertDialog;
import com.morpho.android.usb.USBManager;

import net.gotev.uploadservice.UploadServiceBroadcastReceiver;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

// MainActivity for showing landing screen
public class MainActivity extends BaseActivity {

    AUPDDOwnloadReciever mAUPDDownloadReciever;
    String AUPD_URL;
    String AUPD_URL_TIME;
    String AUPD_URL_ACKNOWLEDGEMENT;
    String BASE_URL;


    //Declaring all the Textviews and the Imageviews for the company details and the Logos
    TextView tvcompanyname, tvcompanyvenue;
    ImageView companyimageView;
    LocalBroadcastManager lBmgr;

    ProgressDialog mUploadProgressDialog;
    String mOfflineUploadPath;
    private Button mBtnMenu,mBtnUnlock;
    Context context=this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAUPDDownloadReciever = new AUPDDOwnloadReciever();


        //Initilize the resources
        tvcompanyname = (TextView) findViewById(R.id.companyName);
        tvcompanyvenue = (TextView) findViewById(R.id.companyVenue);
        companyimageView = (ImageView) findViewById(R.id.companyImage);

        //
        //set the company image and textview
        tvcompanyname.setText("MeritTrac Services Pvt. Ltd.");
        //   tvcompanyvenue.setText(ManvishPrefConstants.SELECTED_VENUE.read());
        //companyimageView.setBackgroundResource(ManvishPrefConstants.COMPANY_LOGO.read());
        //companyimageView.setImageBitmap(ManvishPrefConstants.COMPANY_LOGO.read());

        ManvishPrefConstants.IS_TEST.write(false);

        if (ManvishPrefConstants.SERVER_IP.read() == null) {

            ManvishPrefConstants.SERVER_IP.write(StringConstants.AUPD_BASE_URL);
            ManvishPrefConstants.SERVER_PORT.write(StringConstants.AUPD_BASE_PORT);
            BASE_URL = StringConstants.AUPD_HTTP + StringConstants.AUPD_BASE_URL + ":" + StringConstants.AUPD_BASE_PORT + "/" + StringConstants.SERVICE_NAME + "/";

        } else {
            BASE_URL = ManvishPrefConstants.SERVER_IP.read();
            String port = ManvishPrefConstants.SERVER_PORT.read();
            BASE_URL = StringConstants.AUPD_HTTP + BASE_URL + ":" + port + "/" + StringConstants.SERVICE_NAME + "/";
        }

        Log.e("base", BASE_URL);

        //All urls are hard coded for now
        AUPD_URL = BASE_URL + "AUPDB";
        AUPD_URL_TIME = BASE_URL + "TIME";
        AUPD_URL_ACKNOWLEDGEMENT = BASE_URL + "SUCC";


        mUploadProgressDialog = new ProgressDialog(MainActivity.this);
        mUploadProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mUploadProgressDialog.setMessage("Uploading...");
        mUploadProgressDialog.setCancelable(false);
        mUploadProgressDialog.setProgress(0);
        mBtnMenu = (Button) findViewById(R.id.menuButton);
        mBtnUnlock=(Button)findViewById(R.id.btnUnlock);

        if(ManvishPrefConstants.IS_LOCKED.read()){
            // disable Menu Button
            mBtnMenu.setEnabled(false);
            mBtnUnlock.setVisibility(View.VISIBLE);//when it is lock Unlock should be visible

            // show Unlock button
        }else {
           // downloadStart();
        }

        mBtnUnlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEnterPasswordToUnLock(MainActivity.this,"manvish1234");
            }
        });
        mOfflineUploadPath = Environment.getExternalStorageDirectory() + "/MeritTrack/Upload_Offline/";

        // go to menu activity

        mBtnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!ManvishCommonUtil.isSystemApp(getPackageName(), MainActivity.this)) {

                    // Normal app
                    if (USBManager.getInstance().isDevicesHasPermission() != true) {
                        grantPermission();
                    } else {
                        handler.removeCallbacks(runnableHandler);
                        Intent intent = new Intent(MainActivity.this, MenuActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {

                    //System app ,do something here
                    handler.removeCallbacks(runnableHandler);
                    Intent intent = new Intent(MainActivity.this, MenuActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        try {
            if (USBManager.getInstance().isDevicesHasPermission() != true) {
                grantPermission();
            } else {
                // Toast.makeText(this,"permission granted for sensor",Toast.LENGTH_SHORT).show();
            }

            if( ManvishPrefConstants.MAC_ADDRESS.read()==null) {
                Utils.availMacAddress(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void grantPermission() {
        USBManager.getInstance().initialize(this,
                "com.morpho.morphosample.USB_ACTION");
    }

    @Override
    protected void onResume() {
        super.onResume();

        TextView dateView = (TextView) findViewById(R.id.date);
        TextView dayView = (TextView) findViewById(R.id.day);

        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateView.setText(dateFormat.format(date) + "");
        dayView.setText(new SimpleDateFormat("EEEE", Locale.ENGLISH)
                .format(date.getTime()) + "");

        setHeading();
        /*Batterylevel b1=new Batterylevel();
        b1.Batterylevelindicator();*/
        // Batterylevelindicator();

        uploadServiceBroadcastReceiver.register(this);
    }

    private class AUPDDOwnloadReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(
                    mAUPDDownloadReciever);

            Log.d("Admin Log In Activity", intent.getStringExtra(StringConstants.URL_TAG_KEY));

            if (intent.getStringExtra(StringConstants.URL_TAG_KEY)
                    .equalsIgnoreCase(StringConstants.URL_TAG_AUPD)) {


                if (intent.getStringExtra(StringConstants.KEY_INTENT_SUCCESS)
                        .equalsIgnoreCase("SUCCESS")) {

                    dismissProgressBar();
                    parseZipFileAndSaveIntoDatabase();

                    i++; //This value is just for testing ..

                } else {
                    //ManvishCommonUtil.showCustomToast(MainActivity.this, "Data download went wrong");
                    dismissProgressBar();

                    offlineUploadData();
                }
            }

            if (intent.getStringExtra(StringConstants.URL_TAG_KEY)
                    .equalsIgnoreCase(StringConstants.URL_TAG_AKN)) {

                dismissProgressBar();
                try {
                    unregisterReceiver(mAUPDDownloadReciever);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (intent.getStringExtra(StringConstants.KEY_INTENT_SUCCESS)
                        .equalsIgnoreCase("SUCCESS")) {
                    ManvishCommonUtil.showCustomToast(MainActivity.this, "AKN success");

                } else {
                    ManvishCommonUtil.showCustomToast(MainActivity.this, "AKN fails");
                }
                offlineUploadData();
            }

        }
    }

    Handler handler = new Handler();
    Runnable runnableHandler = new Runnable() {
        @Override
        public void run() {
            downloadStart(); // start hitting AUPD URl in certain amount of time interval .
        }
    };

    LinearLayout mProgressDialog;
    TextView mProgressMsgTextView;

    private void showProgressBar(String msg) {

        mProgressDialog = (LinearLayout) findViewById(R.id.progressBarLayout);
        mProgressDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mProgressMsgTextView = (TextView) findViewById(R.id.progressBarMessage);
        mProgressMsgTextView.setText(msg);
        mProgressDialog.setVisibility(View.VISIBLE);
    }

    private void dismissProgressBar() {
        if (mProgressDialog != null) {
            mProgressDialog.setVisibility(View.GONE);
        }
    }

    int i;

    private void downloadStart() {

        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(
                mAUPDDownloadReciever,
                new IntentFilter(StringConstants.ACTION_AUPD_DOWNLOAD));

        Log.d("URL", AUPD_URL);
        System.out.println("print url==" + AUPD_URL);
        if (InternetAvailability
                .isConnectingToInternet(this)) {

            showProgressBar("Data download in progress ...");
            i = 0;


            //Toast.makeText(this,AUPD_URL,Toast.LENGTH_LONG).show();

            new DownloadData(AUPD_URL, this, null)
                    .startDownLoadService();

        } else {

            ManvishCommonUtil.showCustomToastForNetwork(MainActivity.this,"No Network , Try Again");
            LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(
                    mAUPDDownloadReciever);

            dismissProgressBar();

            handler.postDelayed(runnableHandler, StringConstants.DOWNLOAD_TIME_INTERVAL);
        }


    }


    private void parseZipFileAndSaveIntoDatabase() {


        new AsyncTask<Void, Void, Boolean>() {
            protected void onPreExecute() {
                showProgressBar(" Updating Database ,please Wait ");

            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {

                    ParseAupdZipFile parseAUPDAndSave = new ParseAupdZipFile(
                            context, Environment.getExternalStorageDirectory() + "/MeritTrack/Download/"
                            + "AUPD.zip"); // This is not needed here,Can be removed after server fix .

                    String unZipedfolderPath = Environment.getExternalStorageDirectory() + "/Merittrack/Download/";
                    return parseAUPDAndSave.parseDataFromFolder(unZipedfolderPath);

                } catch (Exception e) {
                    Log.e("MANVISH",
                            "Exception in parsing file"
                                    + e.getMessage());
                    // dismissProgressBar();
                    return false;
                }

            }

            @Override
            protected void onPostExecute(Boolean success) {


                if (success) {
                    new DownloadData(AUPD_URL_ACKNOWLEDGEMENT,
                            MainActivity.this, null)
                            .startDownLoadService();

                    LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(
                            mAUPDDownloadReciever,
                            new IntentFilter(StringConstants.ACTION_AUPD_DOWNLOAD));
                    dismissProgressBar();

                } else {

                    dismissProgressBar();
                    ManvishCommonUtil.showCustomToast(MainActivity.this, "Saving data went wrong.");

                }
            }

        }.execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (MeritTrackApplication.getInstance().getStudentListMap() != null) {
            MeritTrackApplication.getInstance().getStudentListMap().clear();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        uploadServiceBroadcastReceiver.unregister(this);
    }

    public final UploadServiceBroadcastReceiver uploadServiceBroadcastReceiver = new UploadServiceBroadcastReceiver() {


        @Override
        public void onProgress(String uploadId, int progress) {
            super.onProgress(uploadId, progress);

            try {
                Log.d("upload", "progress:" + progress);
                Log.d(StringConstants.TAG, "progress");
                mUploadProgressDialog.show();
                mUploadProgressDialog.setProgress(progress);
                final int progressvalue = progress;
            }catch (Exception e){
                handler.postDelayed(runnableHandler, StringConstants.DOWNLOAD_TIME_INTERVAL);
            }
        }

        @Override
        public void onError(String uploadId, Exception exception) {
            super.onError(uploadId, exception);
            Log.d("upload", "error:" + exception.toString());
            ManvishCommonUtil.showCustomToast(MainActivity.this,"Server not found");
            mUploadProgressDialog.dismiss();
            mUploadProgressDialog.setProgress(0);
            Log.d(StringConstants.TAG, "Error");
           // handler.postDelayed(runnableHandler, StringConstants.DOWNLOAD_TIME_INTERVAL);
            showUploadStatus("Status","Server not found",MainActivity.this);
        }

        @Override
        public void onCompleted(String uploadId, int serverResponseCode, byte[] serverResponseBody) {
            super.onCompleted(uploadId, serverResponseCode, serverResponseBody);


            Log.d("upload", "complete");
            mUploadProgressDialog.setProgress(100);
            mUploadProgressDialog.dismiss();
            mUploadProgressDialog.setProgress(0);
            String responseString=null;

            if(serverResponseBody!=null && serverResponseBody.length!=0) {
                try {

                    byte[] responseByte = Base64.decode(serverResponseBody, Base64.DEFAULT);
                    responseString = new String(responseByte);
                } catch (Exception e) {
                    responseString = null;
                    e.printStackTrace();
                    //showUploadStatus("Status","Upload Completed",MainActivity.this);
                }
            }


            String command=null;



            if (serverResponseCode == 200) {


                if(responseString!=null) {

                    if (responseString.contains("SUCC") && responseString.contains(ManvishPrefConstants.SERIAL_NO.read())
                    ) {
                       // ManvishCommonUtil.showCustomToast(MainActivity.this, "Upload Completed");
                        showUploadStatus("Status","Upload Completed",MainActivity.this);

                        if (mOfflineUploadPath != null && new File(mOfflineUploadPath).exists()) {
                            // new File(mOfflineUploadPath).delete();
                            try {
                               // FileUtils.deleteDirectory(new File(mOfflineUploadPath));
                                // FileUtils.forceDeleteOnExit(new File(mOfflineUploadPath));
                                FileUtils.forceDelete(new File(mOfflineUploadPath));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        //ManvishCommonUtil.showCustomToast(MainActivity.this, "Upload Failed");
                        showUploadStatus("Status","Upload Failed",MainActivity.this);
                    }
                }else{
                    //ManvishCommonUtil.showCustomToast(MainActivity.this, "Contact admin for upload status");
                    showUploadStatus("Status","Contact admin for upload status",MainActivity.this);
                }

            }else{
                //ManvishCommonUtil.showCustomToast(MainActivity.this,"Server not reachable");
                showUploadStatus("Status","Server not reachable",MainActivity.this);
            }

            // Log.d(StringConstants.TAG,"Upload Completed");

        }
    };

    private void offlineUploadData() {

        File f = new File(mOfflineUploadPath);
        File file[] = f.listFiles();
        if (file != null && file.length != 0) {
            handler.removeCallbacks(runnableHandler);
            Log.d("Files", "Size: " + file.length);
            for (int i = 0; i < file.length; i++) {

                Log.d("Files", "FileName:" + file[i].getName());

                UploadFile.uploadMultipart(MainActivity.this, file[i].getName(), file[i].getAbsolutePath());

            }
        } else {
            handler.postDelayed(runnableHandler, StringConstants.DOWNLOAD_TIME_INTERVAL);
        }
    }

    private void unlockApp(){
        ManvishPrefConstants.IS_LOCKED.write(false);
        mBtnMenu.setEnabled(true);
        mBtnMenu.setVisibility(View.VISIBLE);
        mBtnUnlock.setVisibility(View.INVISIBLE);
        Intent int_menu=new Intent(MainActivity.this,MenuActivity.class);
        startActivity(int_menu);
        handler.removeCallbacks(runnableHandler);
    }

    public void showEnterPasswordToUnLock(final Activity context, final String password) {
        final Dialog dialog = new Dialog(context);
        android.util.Log.d("Password","pwd:"+password);
        ManvishPrefConstants.PROJECT_PASSWORD.write(password);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.refresh_dialog);
        dialog.setCancelable(false);
        final EditText etAccessCode = (EditText) dialog
                .findViewById(R.id.et_access_code);
        Button btnAccess = (Button) dialog.findViewById(R.id.btnAccess);
        Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });

        btnAccess.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                android.util.Log.d("Password ::",password);//manvish1234

                if (etAccessCode.getText().toString()
                        .equalsIgnoreCase(password)) {
                    unlockApp();

                } else {
                    new ManvishAlertDialog(context,
                            "Wrong access code",
                            "Please enter correct access code")
                            .showAlertDialog();
                }

            }
        });

        dialog.show();

    }




    public void showUploadStatus(final String title,final String message,final Activity avt) {

        AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setCancelable(false);
        // Setting Dialog Message
        alertDialogBuilder.setMessage(message);

        // Setting Icon to Dialog
        // alertDialog.setIcon(R.drawable.delete);

        // Setting Positive "Yes" Button
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();

                        handler.postDelayed(runnableHandler, StringConstants.DOWNLOAD_TIME_INTERVAL);

                    }
                });

      AlertDialog  alertDialog = alertDialogBuilder.create();

        alertDialog.show();

    }



}
