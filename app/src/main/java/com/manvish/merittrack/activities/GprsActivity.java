package com.manvish.merittrack.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.manvish.merittrack.R;
import com.manvish.merittrack.utils.ManvishCommonUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class GprsActivity extends Activity {


    // constants
    static final String STATUS_ON = "Mobile Data: Enable";
    static final String STATUS_OFF = "Mobile Data: Disable";

    static final String TURN_ON = "Enable";
    static final String TURN_OFF = "Disable";

    // controls
    TextView TVMobileData;
    ToggleButton tBMobileData;
    Button btback;

    Button buttonhome;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gprs);


        // load controls
        TVMobileData = (TextView) findViewById(R.id.TVMobileData);
        tBMobileData = (ToggleButton) findViewById(R.id.tBMobileData);
        btback = (Button) findViewById(R.id.buttonbackgprs);
        buttonhome = (Button) findViewById(R.id.btnHome);

        //onclick event for button
        btback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i90 = new Intent(GprsActivity.this, MiFaunActivity.class
                );
                startActivity(i90);
            }
        });

        // set click event for button
        tBMobileData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check current state first
                boolean state = isMobileDataEnable();
                // toggle the state
                if (state) toggleMobileDataConnection(false);
                else toggleMobileDataConnection(true);
                // update UI to new state
                updateUI(!state);
            }
        });

        buttonhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManvishCommonUtil.goToMainactivity(GprsActivity.this);
            }
        });
    }

    public void updateUI(boolean state) {
        //set text according to state
        if (state) {
            TVMobileData.setText(STATUS_ON);
            tBMobileData.setText(TURN_OFF);
        } else {
            TVMobileData.setText(STATUS_OFF);
            tBMobileData.setText(TURN_ON);
        }
    }

    public boolean isMobileDataEnable() {
        boolean mobileDataEnabled = false; // Assume disabled
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Class cmClass = Class.forName(cm.getClass().getName());
            Method method = cmClass.getDeclaredMethod("getMobileDataEnabled");
            method.setAccessible(true); // Make the method callable
            // get the setting for "mobile data"
            mobileDataEnabled = (Boolean) method.invoke(cm);
        } catch (Exception e) {
            // Some problem accessible private API and do whatever error handling you want here
        }
        return mobileDataEnabled;
    }

    public boolean toggleMobileDataConnection(boolean ON) {
        try {
            //create instance of connectivity manager and get system connectivity service
            final ConnectivityManager conman = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            //create instance of class and get name of connectivity manager system service class
            final Class conmanClass = Class.forName(conman.getClass().getName());
            //create instance of field and get mService Declared field
            final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
            //Attempt to set the value of the accessible flag to true
            iConnectivityManagerField.setAccessible(true);
            //create instance of object and get the value of field conman
            final Object iConnectivityManager = iConnectivityManagerField.get(conman);
            //create instance of class and get the name of iConnectivityManager field
            final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
            //create instance of method and get declared method and type
            final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
            //Attempt to set the value of the accessible flag to true
            setMobileDataEnabledMethod.setAccessible(true);
            //dynamically invoke the iConnectivityManager object according to your need (true/false)
            setMobileDataEnabledMethod.invoke(iConnectivityManager, ON);
        } catch (Exception e) {
        }
        return true;
    }
}

