package com.manvish.merittrack.activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.manvish.merittrack.R;
import com.manvish.merittrack.application.MeritTrackApplication;
import com.manvish.merittrack.utils.ManvishCommonUtil;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

// BaseActivity for show time-date in header part of other activities that extend BaseActivity
public class BaseActivity extends Activity {


    private TextView mHeaderDateView;
    private TextView batterylevel,tvssid;
   // BroadcastReceiver mBatInfoReceiver;
    private AlarmManager mAlarmManager;
    private PendingIntent mPendingIntent;
    private ImageView batt_icon,ivsdcard,ivwifi;
    private SharedPreferences rememberPref;
    private SharedPreferences.Editor preferenceEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




        // mTvWifiStatus=(TextView)findViewById(R.id.tv_wifi_status);
        ivwifi=(ImageView)findViewById(R.id.iv_wifi);
        tvssid=(TextView)findViewById(R.id.tv_ssid);
        ivsdcard=(ImageView)findViewById(R.id.iv_sd_card);



        IntentFilter batIntent = new IntentFilter();
        batIntent.addAction(Intent.ACTION_BATTERY_CHANGED);

        registerReceiver(mBatInfoReceiver, batIntent);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        registerReceiver(mBatInfoReceiver, intentFilter);

        //code for ssid detection





    }

    // set date-time header
    protected void setHeading() {

        // check if textview is null then get from layout
        if (mHeaderDateView == null) {
            mHeaderDateView = (TextView) findViewById(R.id.headerDate);
            //batterylevel=(TextView)findViewById(R.id.battery_level);
        }
        if(batterylevel==null){
            batterylevel=(TextView)findViewById(R.id.battery_level);

        }
        if(ivwifi==null){
            ivwifi=(ImageView)findViewById(R.id.iv_wifi);
        }

        if(batt_icon==null){
            batt_icon=(ImageView)findViewById(R.id.iv_bat_icon);
        }
        if(tvssid==null){
            tvssid=(TextView)findViewById(R.id.tv_ssid);
        }
        if(ivsdcard==null){
            ivsdcard=(ImageView)findViewById(R.id.iv_sd_card);
        }

        //code for displaying only the wifi ssid
         try {
            if (ManvishCommonUtil.isWifiOn(this)) {
                ivwifi.setImageResource(R.drawable.wifi_network);

                WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                String ssid = null;
                ssid = wifiInfo.getSSID();
                tvssid.setText(ssid);

            } else {

                ivwifi.setImageResource(R.drawable.wifi_off);
                tvssid.setText("SSID Notavailable ");
            }
        }catch (Exception e){
        e.printStackTrace();
        }

        Date now = new Date();
        int hours = now.getHours();
        hours = (hours > 12) ? hours - 12 : hours;
        int minutes = now.getMinutes();
        int h1 = hours / 10;
        int h2 = hours - h1 * 10;
        int m1 = minutes / 10;
        int m2 = minutes - m1 * 10;

        Calendar calendar = Calendar.getInstance();

        Date date = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        mHeaderDateView.setText(dateFormat.format(date));

     }
    BroadcastReceiver   mBatInfoReceiver = new BroadcastReceiver() {
        @Override
        //When Event is published, onReceive method is called
        public void onReceive(Context c, Intent intent) {


            final String action = intent.getAction();
            if (action.equals(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION)) {
                if (intent.getBooleanExtra(WifiManager.EXTRA_SUPPLICANT_CONNECTED, false)) {
                    if(ivwifi!=null) {   //  mTvWifiStatus.setText("WIFI : ON");
                        ivwifi.setImageResource(R.drawable.wifi_network);
                    }
                } else {
                    if(ivwifi!=null) {
                            ivwifi.setImageResource(R.drawable.wifi_off);
                        }
                }
            } else {
                  int level = intent.getIntExtra("level", 0);
                  int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 100);
                int per=(level*100)/scale;
                if(batterylevel!=null) {
                    batterylevel.setText("" + Integer.toString(level) + "%");
                }

                int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

                boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                        status == BatteryManager.BATTERY_STATUS_FULL;


                int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
                if(level == 100) {
                    String icon = "stat_sys_battery_full";
                    if (batt_icon != null) {
                        batt_icon.setImageResource(getResources().getIdentifier(icon, "drawable", "com.morpho.morphosample"));
                    }
                }
//code for getting Low battery message
               /* if(intent.getAction().equals(Intent.ACTION_BATTERY_LOW)&&(per<=10&&per>5))*/{


                }
                if(isCharging){
                    String icon="stat_sys_battery_charge_anim"+level;
                    if(batt_icon!=null) {
                        batt_icon.setImageResource(getResources().getIdentifier(icon, "drawable", "com.morpho.morphosample"));
                    }
                }
                else {
                    String icon = "stat_sys_battery_"+level;
                    if(batt_icon!=null) {
                        batt_icon.setImageResource(getResources().getIdentifier(icon, "drawable", "com.morpho.morphosample"));
                    }

                    if(level<10 && MeritTrackApplication.mLowLevel!=level){

                        MeritTrackApplication.mLowLevel = level;
                        LayoutInflater inflater = getLayoutInflater();
                        View layout = inflater.inflate(R.layout.battery_custom_toast,
                                (ViewGroup) findViewById(R.id.toast_batterylow_layout));
                        ImageView iv_bat_low=(ImageView)layout.findViewById(R.id.iv_battery_low);
                        iv_bat_low.setImageResource(R.drawable.low_battery_icon2);
                        TextView tv_bat_low=(TextView)layout.findViewById(R.id.tv_battery_alert);
                        tv_bat_low.setText("LOW BATTERY!!!!,CHARGE BATTERY");
                        Toast toast = new Toast(getApplicationContext());
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toast.setDuration(Toast.LENGTH_LONG);
                        toast.setView(layout);
                        toast.show();
                    }



                }
              }
            //code for sd card detection
            try {

                    File testfile = new File("/mnt/sdcard2");
                    Log.e("length",testfile.listFiles().length+"");

                    if(ivsdcard!=null) {
                        ivsdcard.setImageResource(R.drawable.sd_card_mounted_01);
                    }

            }catch (Exception e){
                e.printStackTrace();

                if(ivsdcard!=null) {
                    ivsdcard.setImageResource(R.drawable.unmounted);
                }
            }



         }
    };

    private String getSDcardDirectoryPath() {
        return System.getenv("SECONDARY_STORAGE");
    }



    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(mBatInfoReceiver);
        } catch (Exception e){
            e.printStackTrace();
        }
        super.onDestroy();

    }
}













