package com.manvish.merittrack.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by pabitra on 5/1/16.
 */
public class CustomGridViewItemButton extends Button {

    public CustomGridViewItemButton(Context context,AttributeSet attrs) {
        super(context,attrs);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
