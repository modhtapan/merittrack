package com.manvish.merittrack.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;

public class ManvishAlertDialog {

    //private Context mContext;
    private String title;
    private String message;
    private Activity avt;
    AlertDialog.Builder alertDialogBuilder=null ;
    AlertDialog alertDialog=null;

    public ManvishAlertDialog(Activity avt, String title, String message) {
        //this.mContext = mContext;
        this.title = title;
        this.message = message;
        this.avt = avt;
    }

    public void showAlertDialog() {

        alertDialogBuilder= new AlertDialog.Builder(avt);

        if(alertDialogBuilder!=null && alertDialog!=null){

            if(alertDialog.isShowing()){
                alertDialog.dismiss();
            }
            alertDialogBuilder=null;
            alertDialog=null;
        }

        // Setting Dialog Title
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setCancelable(false);
        // Setting Dialog Message
        alertDialogBuilder.setMessage(message);

        // Setting Icon to Dialog
        // alertDialog.setIcon(R.drawable.delete);

        // Setting Positive "Yes" Button
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();

                        // goToRegistrationPage(studentDetailsModel);
                        if (message.contains("Wi-Fi")) {
                            avt.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                            //avt.finish();
                        }
                        if (title.contains("NO TEMPLATE FOUND")) {

                            avt.finish();
                        }

                        if (title.contains("STATUS") && message.contains("completed")) {

                            avt.finish();
                        }


                    }
                });

        alertDialog = alertDialogBuilder.create();

        if(alertDialog.isShowing()){
            alertDialog.dismiss();
        }
        alertDialog.show();

    }

}
